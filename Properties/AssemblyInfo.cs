﻿using System.Reflection;
using System.Runtime.InteropServices;

// 有关程序集的一般信息由以下控制
// 更改这些特性值可修改与程序集关联的信息
[assembly: AssemblyTitle("APP.Core")]
[assembly: AssemblyDescription("APP.Core")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Fanzhou Technology")]
[assembly: AssemblyProduct("APP.Core")]
[assembly: AssemblyCopyright("Copyright © 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// 将 ComVisible 设置为 false 会使此程序集中的类型对 COM 组件不可见
// 如果需要从 COM 访问此程序集中的某个类型，请针对该类型将 ComVisible 特性设置为 true
[assembly: ComVisible(false)]

// 如果此项目向 COM 公开，则下列 GUID 用于类型库的 ID
[assembly: Guid("007e7fb8-b1fc-4717-bf6f-dcc333037752")]

// 程序集的版本信息由下列四个值组成:
//  主版本
//  次版本
//  生成号
//  修订号
// 可以指定所有这些值，也可以使用“生成号”和“修订号”的默认值
// 方法是按如下所示使用“*”
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion(CoreConfig.CoreVersion_1_10_95)]
[assembly: AssemblyFileVersion(CoreConfig.CoreVersion_1_10_95)]
