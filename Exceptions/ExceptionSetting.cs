﻿using System.Collections.Generic;

namespace APP.Core.Exceptions
{
    /// <summary>
    /// T4 专用，其他地方请勿调用
    /// 使用方法：
    ///  1、配置 Exception 信息，按分类归属；
    ///  2、Name：用于生成 Exception Name；Message：Exception 对应的错误信息，支持参数；
    ///  3、配置完后，Build 项目，再使用项目目录下的 T4（GenerateException.tt） 进行重新生成；
    ///  4、生成结果：Exception 名称为配置的名称 + "Exception"，例如 HaveNoPrivilegeToVisitAPIException，code 为当前所在区间值 + 所在顺位，例如 0 区间第一位对应 0001；
    /// </summary>
    public static class ExceptionSetting
    {
        public class SettingModel
        {
            public string Name { get; set; }
            public string Message { get; set; }
        }

        public static IDictionary<int, IList<SettingModel>> Settings = new Dictionary<int, IList<SettingModel>>
        {
            #region 系统异常, code: 0001-0999
            {
                0,  // code 区间
                new List<SettingModel>
                {
                    new SettingModel { Name = "HaveNoPrivilegeToAccessAPI", Message = "无权限访问该接口" },
                    new SettingModel { Name = "HaveNoPrivilegeToAccessPage", Message = "无权限访问该页面" },
                    new SettingModel { Name = "HaveNoPrivilegeToOperate", Message = "无权限进行操作" },
                    new SettingModel { Name = "InvalidRequest", Message = "无效的请求" },
                    new SettingModel { Name = "InvalidParameter", Message = "无效的请求参数" },
                    new SettingModel { Name = "InvalidCaptchaCode", Message = "验证码不正确" },
                }
            },
            #endregion
            
            #region 用户异常, code: 1001-1999
            {
                1,  // code 区间
                new List<SettingModel>
                {
                    new SettingModel { Name = "AccountIsExist", Message = "账户已存在" },
                    new SettingModel { Name = "AccountIsNotExist", Message = "账户不存在" },
                    new SettingModel { Name = "WrongLoginPassword", Message = "密码不正确" },
                }
            },
            #endregion
        };
    }
}
