﻿using System;
using System.Runtime.Serialization;

namespace APP.Core.Exceptions
{
    public class APPBaseException : Exception
    {
        public string Code { get; protected set; }

        public object[] Args { get; protected set; }


        public APPBaseException() { }

        public APPBaseException(string message) : base(message) { }

        public APPBaseException(string message, Exception innerException) : base(message, innerException) { }

        protected APPBaseException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
