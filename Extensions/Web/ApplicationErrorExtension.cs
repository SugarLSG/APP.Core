﻿using APP.Core.Exceptions;
using APP.Core.Utilities;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace APP.Core.Extensions.Web
{
    public static class ApplicationErrorExtension
    {
        /// <summary>
        /// 处理程序错误（用于 Application_Error）
        /// </summary>
        /// <param name="redirectArea"></param>
        /// <param name="redirectController"></param>
        /// <param name="redirectAction"></param>
        /// <param name="alwaysJson">异常始终响应为 Json（默认 Post 且 Ajax 请求响应为 Json）</param>
        public static void Handle(string redirectArea = null, string redirectController = "Error", string redirectAction = "Index", bool alwaysJson = false)
        {
            // 获取异常
            var exception = HttpContext.Current.Server.GetLastError();
            if (exception == null)
                return;

            // 是否系统自定义异常
            if (exception is APPBaseException || (exception.InnerException != null && exception.InnerException is APPBaseException))
            {
                if (!(exception is APPBaseException))
                    exception = exception.InnerException;

                // 自定义异常，记录到主日志文件（WARN）
                LogUtility.LogWarn(exception);
            }
            else
            {
                var request = HttpContext.Current.Request;
                // 非自定义异常，记录到主日志文件（ERROR）
                LogUtility.LogError(string.Format(
                    "请求URL: ({0}){1}\r\n请求方法: (Area){2} (Controller){3} (Action){4}\r\n请求Referer: {5}\r\n用户信息: (UserId: {6}) {7} {8}\r\n请求参数: {9}",
                    request.RequestType,
                    request.Url,
                    request.RequestContext.RouteData.DataTokens["area"],
                    request.RequestContext.RouteData.Values["controller"],
                    request.RequestContext.RouteData.Values["action"],
                    request.UrlReferrer,
                    AuthenticationUtility.AuthorizedUserId,
                    request.UserHostAddress,
                    request.UserAgent,
                    request.Params
                ), exception);
            }

            // 是否 Post & Ajax 请求
            if (alwaysJson || HttpContext.Current.Response.ContentType.ToLower().Contains("json") || (ClientRequestUtility.IsPostRequest && ClientRequestUtility.IsAjaxRequest))
            {
                // 清除 Error
                HttpContext.Current.Server.ClearError();

                // Post & Ajax 请求，封装 JSON 结果
                var response = HttpContext.Current.Response;
                var appException = exception is APPBaseException ? exception as APPBaseException : null;
                response.ContentEncoding = Encoding.UTF8;
                response.ContentType = CoreConfig.CONTENTTYPE_APPLICATION_JSON;
                response.Write(appException == null ?
                    JsonUtility.SerializeAjaxFailureJSON(CoreConfig.HTTPREQUEST_500, ((int)HttpStatusCode.InternalServerError).ToString()) :
                    JsonUtility.SerializeAjaxFailureJSON(appException.ToMessage(), appException.Code)
                );
            }
            else
            {
                // 非 Post & Ajax 请求，返回错误界面

                // 是否重定向自定义错误页面
                // 设置 Web.config -> configuration -> system.web -> customErrors -> mode = Off / On
                if (!HttpContext.Current.IsCustomErrorEnabled)
                    return;

                // 清除 Error
                HttpContext.Current.Server.ClearError();

                // 重定向自定义界面
                HttpContext.Current.Response.TrySkipIisCustomErrors = true;
                var routeData = new RouteData();
                if (redirectArea.HasValue())
                    routeData.DataTokens.Add("area", redirectArea);
                routeData.Values.Add("controller", redirectController);
                routeData.Values.Add("action", redirectAction);
                routeData.Values.Add("httpException", new HttpException(null, exception));
                ControllerBuilder.Current
                    .GetControllerFactory()
                    .CreateController(new RequestContext(new HttpContextWrapper(HttpContext.Current), routeData), "Error")
                    .Execute(new RequestContext(new HttpContextWrapper(HttpContext.Current), routeData));
            }
        }
    }
}
