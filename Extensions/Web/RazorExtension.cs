﻿using APP.Core.Caches;
using APP.Core.Utilities;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;

namespace System.Web.Mvc.Html
{
    public static class RazorExtension
    {
        private enum HashVersionLevel
        {
            /// <summary>
            /// 日期
            /// </summary>
            [Description("日期")]
            Day,

            /// <summary>
            /// 小时
            /// </summary>
            [Description("小时")]
            Hour,

            /// <summary>
            /// 分钟
            /// </summary>
            [Description("分钟")]
            Minute
        }

        public class InputSelectItem
        {
            /// <summary>
            /// 显示文字
            /// </summary>
            public string Text { get; set; }

            /// <summary>
            /// Item 值
            /// </summary>
            public object Value { get; set; }

            /// <summary>
            /// 是否已选中
            /// </summary>
            public bool IsChecked { get; set; }

            /// <summary>
            /// Html 属性，input 使用
            /// </summary>
            public IDictionary<string, object> HtmlAttributes { get; set; }
        }


        /// <summary>
        /// 缓存管理
        /// </summary>
        private static ICacheProvider CacheProvider { get { return CacheManager.MemoryCacheProvider; } }


        #region Private Method

        private static string GetHashVersion(HashVersionLevel level)
        {
            return DateTime.Now.ToString(level == HashVersionLevel.Day ? "yyyyMMdd" : (level == HashVersionLevel.Hour ? "yyyyMMddHH" : "yyyyMMddHHmm"));
        }

        /// <summary>
        /// 将 Object 类型 HtmlAttributes 转换为 Dictionary 类型
        /// </summary>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        private static IDictionary<string, object> ConvertToHtmlDictionaryAttributes(object htmlAttributes)
        {
            // 封装 Attributes
            IDictionary<string, object> htmlDictionaryAttributes = null;
            if (htmlAttributes != null)
            {
                htmlDictionaryAttributes = new Dictionary<string, object>();
                foreach (var property in htmlAttributes.GetType().GetProperties())
                    htmlDictionaryAttributes.Add(property.Name, property.GetValue(htmlAttributes));
            }

            return htmlDictionaryAttributes;
        }

        /// <summary>
        /// 生成字典对应 Has Code
        /// </summary>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        private static string GenerateDictionaryHashCode(IDictionary<string, object> htmlAttributes)
        {
            return htmlAttributes == null || htmlAttributes.Count == 0 ?
                string.Empty :
                string.Join("|", htmlAttributes.Keys.Select(i => string.Format("{0}:{1}", i, htmlAttributes[i])));
        }

        /// <summary>
        /// 转为具体字段名
        /// </summary>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        private static string ConvertFieldName(string fieldName)
        {
            return fieldName.Substring(fieldName.LastIndexOf('.') + 1);
        }

        /// <summary>
        /// 获取标签对应 Id
        /// </summary>
        /// <param name="htmlString"></param>
        /// <returns></returns>
        private static string GetTagId(string htmlString)
        {
            var idMatch = Regex.Match(htmlString, "^<.* id=\"([^\"]+)\" .*>$", RegexOptions.IgnoreCase);
            return idMatch != null && idMatch.Success ? idMatch.Groups[1].Value : string.Empty;
        }

        /// <summary>
        /// 获取标签对应 Name
        /// </summary>
        /// <param name="htmlString"></param>
        /// <returns></returns>
        private static string GetTagName(string htmlString)
        {
            var idMatch = Regex.Match(htmlString, "^<.* name=\"([^\"]+)\" .*>$", RegexOptions.IgnoreCase);
            return idMatch != null && idMatch.Success ? idMatch.Groups[1].Value : string.Empty;
        }

        #endregion


        #region Hash Version Path

        /// <summary>
        /// 生成文件 Hash 版本路径
        /// 仅对 CSS、JS 文件生效
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="contentPath"></param>
        /// <returns></returns>
        public static MvcHtmlString HashVersionPathFor(this HtmlHelper htmlHelper, string contentPath)
        {
            return htmlHelper.HashVersionPathFor(contentPath, null);
        }

        /// <summary>
        /// 生成文件 Hash 版本路径
        /// 仅对 CSS、JS 文件生效
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="contentPath"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString HashVersionPathFor(this HtmlHelper htmlHelper, string contentPath, object htmlAttributes)
        {
            return htmlHelper.HashVersionPathFor(contentPath, ConvertToHtmlDictionaryAttributes(htmlAttributes));
        }

        /// <summary>
        /// 生成文件 Hash 版本路径
        /// 仅对 CSS、JS 文件生效
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="contentPath"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString HashVersionPathFor(this HtmlHelper htmlHelper, string contentPath, IDictionary<string, object> htmlAttributes)
        {
            // 判断是否 css 或 js 文件路径
            var fileExtension = Path.GetExtension(contentPath);
            if (fileExtension != ".css" && fileExtension != ".js")
                return null;

            // 获取版本号
            var version = CacheProvider.Get<string>(CoreConfig.CACHEKEY_HASHFILE_VERSION_PREFIX);
            if (!version.HasValue())
            {
                version = GetHashVersion(HashVersionLevel.Hour);
                CacheProvider.Set(CoreConfig.CACHEKEY_HASHFILE_VERSION_PREFIX, version, CacheExpirationType.Absolute, 60);
            }

            // 生成 Hash Code
            return MvcHtmlString.Create(string.Format(
                fileExtension == ".css" ?
                    "<link rel=\"stylesheet\" type=\"text/css\" href=\"/{0}?v={1}\"{2} />" :
                    "<script type=\"text/javascript\" src=\"/{0}?v={1}\"{2}></script>",
                contentPath.TrimStart('~', '/', '\\').Replace('\\', '/'),
                version,
                htmlAttributes == null || htmlAttributes.Count == 0 ?
                    string.Empty :
                    string.Join(" ", htmlAttributes.Keys.Select(i => string.Format("{0}=\"{1}\"", i, htmlAttributes[i])))
            ));
        }

        #endregion

        #region Hash Version Url

        /// <summary>
        /// 生成文件 Hash 版本 Url
        /// 仅对 CSS、JS 文件生效
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        public static MvcHtmlString HashVersionUrlFor(this HtmlHelper htmlHelper, string url)
        {
            return htmlHelper.HashVersionUrlFor(url, null);
        }

        /// <summary>
        /// 生成文件 Hash 版本 Url
        /// 仅对 CSS、JS 文件生效
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="url"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString HashVersionUrlFor(this HtmlHelper htmlHelper, string url, object htmlAttributes)
        {
            return htmlHelper.HashVersionUrlFor(url, ConvertToHtmlDictionaryAttributes(htmlAttributes));
        }

        /// <summary>
        /// 生成文件 Hash 版本 Url
        /// 仅对 CSS、JS 文件生效
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="url"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString HashVersionUrlFor(this HtmlHelper htmlHelper, string url, IDictionary<string, object> htmlAttributes)
        {
            // 判断是否 css 或 js 文件路径
            var fileExtension = Path.GetExtension(url);
            if (fileExtension != ".css" && fileExtension != ".js")
                return null;

            // 获取版本号
            var version = CacheProvider.Get<string>(CoreConfig.CACHEKEY_HASHFILE_VERSION_PREFIX);
            if (!version.HasValue())
            {
                version = GetHashVersion(HashVersionLevel.Day);
                CacheProvider.Set(CoreConfig.CACHEKEY_HASHFILE_VERSION_PREFIX, version, CacheExpirationType.Absolute, 60);
            }

            // 生成 Hash Code
            return MvcHtmlString.Create(string.Format(
                fileExtension == ".css" ?
                    "<link rel=\"stylesheet\" type=\"text/css\" href=\"{0}?v={1}\"{2} />" :
                    "<script type=\"text/javascript\" src=\"{0}?v={1}\"{2}></script>",
                url,
                version,
                htmlAttributes == null || htmlAttributes.Count == 0 ?
                    string.Empty :
                    string.Join(" ", htmlAttributes.Keys.Select(i => string.Format("{0}=\"{1}\"", i, htmlAttributes[i])))
            ));
        }

        #endregion

        #region To Json

        /// <summary>
        /// 对象生成 JavaScript Json
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="obj"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static MvcHtmlString ObjectToJson(this HtmlHelper htmlHelper, object obj, string fieldName)
        {
            return MvcHtmlString.Create(string.Format(
                "var {0} = JSON.parse('{1}');",
                fieldName,
                obj == null ? "{}" : JsonUtility.Serialize(obj)
            ));
        }

        /// <summary>
        /// 列表生成 JavaScript Json
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="list"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static MvcHtmlString ListToJson(this HtmlHelper htmlHelper, IEnumerable list, string fieldName)
        {
            return MvcHtmlString.Create(string.Format(
                "var {0} = JSON.parse('{1}');",
                fieldName,
                list == null ? "[]" : JsonUtility.Serialize(list)
            ));
        }

        #endregion

        #region Collection Length Validation Message

        private const string DEFAULT_COLLECTIONLENGTHHIDDEN_PREFIX = "COLLECTIONLENGTH_";

        /// <summary>
        /// 生成集合长度消息验证模块
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static MvcHtmlString CollectionLengthValidationMessageFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            return htmlHelper.CollectionLengthValidationMessageFor(expression, null, null);
        }

        /// <summary>
        /// 生成集合长度消息验证模块
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="validationMessage"></param>
        /// <returns></returns>
        public static MvcHtmlString CollectionLengthValidationMessageFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string validationMessage)
        {
            return htmlHelper.CollectionLengthValidationMessageFor(expression, validationMessage, null);
        }

        /// <summary>
        /// 生成集合长度消息验证模块
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString CollectionLengthValidationMessageFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes)
        {
            return htmlHelper.CollectionLengthValidationMessageFor(expression, null, ConvertToHtmlDictionaryAttributes(htmlAttributes));
        }

        /// <summary>
        /// 生成集合长度消息验证模块
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString CollectionLengthValidationMessageFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.CollectionLengthValidationMessageFor(expression, null, htmlAttributes);
        }

        /// <summary>
        /// 生成集合长度消息验证模块
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="validationMessage"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString CollectionLengthValidationMessageFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string validationMessage, object htmlAttributes)
        {
            return htmlHelper.CollectionLengthValidationMessageFor(expression, validationMessage, ConvertToHtmlDictionaryAttributes(htmlAttributes));
        }

        /// <summary>
        /// 生成集合长度消息验证模块
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="validationMessage"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString CollectionLengthValidationMessageFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string validationMessage, IDictionary<string, object> htmlAttributes)
        {
            var mvcHiddenString = htmlHelper.HiddenFor(expression).ToHtmlString();
            var mvcMessageString = htmlHelper.ValidationMessageFor(expression, validationMessage, htmlAttributes).ToHtmlString();

            var tagOriginalName = GetTagName(mvcHiddenString);
            var tagNewName = DEFAULT_COLLECTIONLENGTHHIDDEN_PREFIX + tagOriginalName;

            mvcHiddenString = mvcHiddenString.Replace(string.Format("name=\"{0}\"", tagOriginalName), string.Format("data-originalname=\"{0}\" name=\"{1}\"", tagOriginalName, tagNewName));
            mvcMessageString = mvcMessageString.Replace(string.Format("data-valmsg-for=\"{0}\"", tagOriginalName), string.Format("data-valmsg-for=\"{0}\"", tagNewName));
            return MvcHtmlString.Create(mvcHiddenString + mvcMessageString);
        }

        #endregion

        #region Text Length Validation Message

        /// <summary>
        /// 生成文字长度验证模块（长度显示）
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static MvcHtmlString TextLengthSpanFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            return htmlHelper.TextLengthSpanFor(expression, null);
        }

        /// <summary>
        /// 生成文字长度验证模块（长度显示）
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString TextLengthSpanFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes)
        {
            return htmlHelper.TextLengthSpanFor(expression, ConvertToHtmlDictionaryAttributes(htmlAttributes));
        }

        /// <summary>
        /// 生成文字长度验证模块（长度显示）
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString TextLengthSpanFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IDictionary<string, object> htmlAttributes)
        {
            var modelMetadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var htmlFieldName = ConvertFieldName(ExpressionHelper.GetExpressionText(expression));
            var property = modelMetadata.ContainerType.GetProperty(htmlFieldName);

            if (property != null)
            {
                var textLengthAttribute = property.GetCustomAttribute<TextLengthAttribute>();
                if (textLengthAttribute != null)
                {
                    var value = property.GetValue(modelMetadata.Container) as string;
                    htmlAttributes = htmlAttributes.Merge("data-textlengthspan-for", htmlFieldName);

                    // span
                    var spanTag = new TagBuilder("span");
                    spanTag.MergeAttributes(htmlAttributes);
                    spanTag.InnerHtml = string.Format("{0}/{1}", value.HasValue() ? value.Length : 0, textLengthAttribute.MaximumLength);

                    return new MvcHtmlString(spanTag.ToString(TagRenderMode.Normal));
                }
            }

            return MvcHtmlString.Empty;
        }

        #endregion

        #region Radio Button List

        /// <summary>
        /// 生成 RadioButton 列表
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="inputSelectList"></param>
        /// <returns></returns>
        public static MvcHtmlString RadioButtonListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<InputSelectItem> inputSelectList)
        {
            return htmlHelper.RadioButtonListFor(expression, inputSelectList, null);
        }

        /// <summary>
        /// 生成 RadioButton 列表
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="inputSelectList"></param>
        /// <param name="htmlAttributes">Label 使用</param>
        /// <returns></returns>
        public static MvcHtmlString RadioButtonListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<InputSelectItem> inputSelectList, object htmlAttributes)
        {
            return htmlHelper.RadioButtonListFor(expression, inputSelectList, ConvertToHtmlDictionaryAttributes(htmlAttributes));
        }

        /// <summary>
        /// 生成 RadioButton 列表
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="inputSelectList"></param>
        /// <param name="htmlAttributes">Label 使用</param>
        /// <returns></returns>
        public static MvcHtmlString RadioButtonListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<InputSelectItem> inputSelectList, IDictionary<string, object> htmlAttributes)
        {
            var htmlFieldName = ConvertFieldName(ExpressionHelper.GetExpressionText(expression));

            var htmlString = "";
            if (inputSelectList != null && inputSelectList.Any())
            {
                htmlAttributes = htmlAttributes.Merge("class", htmlAttributes != null && htmlAttributes.ContainsKey("class") ? "radio " + htmlAttributes["class"] : "radio");

                htmlString = string.Join(" ", inputSelectList.Select((item, index) =>
                {
                    // radio
                    item.HtmlAttributes = item.HtmlAttributes.Merge("id", string.Format("Radio_{0}_{1}_", htmlFieldName, index));
                    var radioTag = htmlHelper.RadioButtonFor(expression, item.Value, item.HtmlAttributes);

                    // span
                    var spanTag = new TagBuilder("span");
                    spanTag.InnerHtml = item.Text;

                    // label
                    var labelTag = new TagBuilder("label");
                    labelTag.MergeAttributes(htmlAttributes);
                    labelTag.InnerHtml = string.Format("{0}{1}", radioTag.ToString(), spanTag.ToString(TagRenderMode.Normal));

                    return labelTag.ToString(TagRenderMode.Normal);
                }));
            }

            return MvcHtmlString.Create(htmlString);
        }

        #endregion

        #region Enum Radio Button List

        /// <summary>
        /// 生成枚举 RadioButton 列表
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static MvcHtmlString EnumRadioButtonListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression) where TEnum : struct
        {
            return htmlHelper.EnumRadioButtonListFor(expression, null);
        }

        /// <summary>
        /// 生成枚举 RadioButton 列表
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static MvcHtmlString EnumRadioButtonListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum?>> expression) where TEnum : struct
        {
            return htmlHelper.EnumRadioButtonListFor(expression, null);
        }

        /// <summary>
        /// 生成枚举 RadioButton 列表
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString EnumRadioButtonListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, object htmlAttributes) where TEnum : struct
        {
            return htmlHelper.EnumRadioButtonListFor(expression, ConvertToHtmlDictionaryAttributes(htmlAttributes));
        }

        /// <summary>
        /// 生成枚举 RadioButton 列表
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString EnumRadioButtonListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum?>> expression, object htmlAttributes) where TEnum : struct
        {
            return htmlHelper.EnumRadioButtonListFor(expression, ConvertToHtmlDictionaryAttributes(htmlAttributes));
        }

        /// <summary>
        /// 生成枚举 RadioButton 列表
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString EnumRadioButtonListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, IDictionary<string, object> htmlAttributes) where TEnum : struct
        {
            var modelMetadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var htmlFieldName = ConvertFieldName(ExpressionHelper.GetExpressionText(expression));
            var property = modelMetadata.ContainerType.GetProperty(htmlFieldName);
            var propertyValue = property != null ? (TEnum?)property.GetValue(modelMetadata.Container) : null;

            var items = typeof(TEnum).GetEnumNames()
                .Select(i =>
                {
                    var e = i.ToEnum<TEnum>();
                    return new InputSelectItem
                    {
                        Text = (e as Enum).ToMessage(),
                        Value = e,
                        IsChecked = e.Equals(propertyValue)
                    };
                });

            return htmlHelper.RadioButtonListFor(expression, items, htmlAttributes);
        }

        /// <summary>
        /// 生成枚举 RadioButton 列表
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString EnumRadioButtonListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum?>> expression, IDictionary<string, object> htmlAttributes) where TEnum : struct
        {
            var modelMetadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var htmlFieldName = ConvertFieldName(ExpressionHelper.GetExpressionText(expression));
            var property = modelMetadata.ContainerType.GetProperty(htmlFieldName);
            var propertyValue = property != null ? (TEnum?)property.GetValue(modelMetadata.Container) : null;

            var items = typeof(TEnum).GetEnumNames()
                .Select(i =>
                {
                    var e = i.ToEnum<TEnum>();
                    return new InputSelectItem
                    {
                        Text = (e as Enum).ToMessage(),
                        Value = e,
                        IsChecked = e.Equals(propertyValue)
                    };
                });

            return htmlHelper.RadioButtonListFor(expression, items, htmlAttributes);
        }

        #endregion

        #region Check Box List

        /// <summary>
        /// 生成 CheckBox 列表
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="inputSelectList"></param>
        /// <returns></returns>
        public static MvcHtmlString CheckBoxListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, IEnumerable<TProperty>>> expression, IEnumerable<InputSelectItem> inputSelectList)
        {
            return htmlHelper.CheckBoxListFor(expression, inputSelectList, null);
        }

        /// <summary>
        /// 生成 CheckBox 列表
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="inputSelectList"></param>
        /// <param name="htmlAttributes">Label 使用</param>
        /// <returns></returns>
        public static MvcHtmlString CheckBoxListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, IEnumerable<TProperty>>> expression, IEnumerable<InputSelectItem> inputSelectList, object htmlAttributes)
        {
            return htmlHelper.CheckBoxListFor(expression, inputSelectList, ConvertToHtmlDictionaryAttributes(htmlAttributes));
        }

        /// <summary>
        /// 生成 CheckBox 列表
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="inputSelectList"></param>
        /// <param name="htmlAttributes">Label 使用</param>
        /// <returns></returns>
        public static MvcHtmlString CheckBoxListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, IEnumerable<TProperty>>> expression, IEnumerable<InputSelectItem> inputSelectList, IDictionary<string, object> htmlAttributes)
        {
            var htmlFieldName = ConvertFieldName(ExpressionHelper.GetExpressionText(expression));

            var htmlString = "";
            if (inputSelectList != null && inputSelectList.Any())
            {
                htmlAttributes = htmlAttributes.Merge("class", htmlAttributes != null && htmlAttributes.ContainsKey("class") ? "checkbox " + htmlAttributes["class"] : "checkbox");

                htmlString = string.Join(" ", inputSelectList.Select((item, index) =>
                {
                    // checkbox
                    var checkboxTag = new TagBuilder("input");
                    IDictionary<string, object> checkboxHtmlAttributes = new Dictionary<string, object>
                    {
                        { "type", "checkbox" },
                        { "id", string.Format("CheckBox_{0}_{1}_", htmlFieldName, index) },
                        { "name", string.Format("{0}[{1}]", htmlFieldName, index) },
                        { "value", item.Value }
                    };
                    if (item.IsChecked)
                        checkboxHtmlAttributes.Add("checked", "checked");
                    checkboxHtmlAttributes = checkboxHtmlAttributes.Merge(item.HtmlAttributes);
                    checkboxTag.MergeAttributes(checkboxHtmlAttributes);

                    // span
                    var spanTag = new TagBuilder("span");
                    spanTag.InnerHtml = item.Text;

                    // label
                    var labelTag = new TagBuilder("label");
                    labelTag.MergeAttributes(htmlAttributes);
                    labelTag.InnerHtml = string.Format("{0}{1}", checkboxTag.ToString(TagRenderMode.Normal), spanTag.ToString(TagRenderMode.Normal));

                    return labelTag.ToString(TagRenderMode.Normal);
                }));
            }

            return MvcHtmlString.Create(htmlString);
        }

        #endregion

        #region Enum Check Box List

        /// <summary>
        /// 生成枚举 CheckBox 列表
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static MvcHtmlString EnumCheckBoxListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, IEnumerable<TEnum>>> expression) where TEnum : struct
        {
            return htmlHelper.EnumCheckBoxListFor(expression, null);
        }

        /// <summary>
        /// 生成枚举 CheckBox 列表
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString EnumCheckBoxListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, IEnumerable<TEnum>>> expression, object htmlAttributes) where TEnum : struct
        {
            return htmlHelper.EnumCheckBoxListFor(expression, ConvertToHtmlDictionaryAttributes(htmlAttributes));
        }

        /// <summary>
        /// 生成枚举 CheckBox 列表
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString EnumCheckBoxListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, IEnumerable<TEnum>>> expression, IDictionary<string, object> htmlAttributes) where TEnum : struct
        {
            var modelMetadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var htmlFieldName = ConvertFieldName(ExpressionHelper.GetExpressionText(expression));
            var property = modelMetadata.ContainerType.GetProperty(htmlFieldName);
            var propertyValues = property != null ? property.GetValue(modelMetadata.Container) as IEnumerable<TEnum> : null;

            var items = typeof(TEnum).GetEnumNames()
                .Select(i =>
                {
                    var e = i.ToEnum<TEnum>();
                    return new InputSelectItem
                    {
                        Text = (e as Enum).ToMessage(),
                        Value = e,
                        IsChecked = propertyValues != null && propertyValues.Contains(e)
                    };
                });

            return htmlHelper.CheckBoxListFor(expression, items, htmlAttributes);
        }

        #endregion

        #region Enum Select

        /// <summary>
        /// 生成枚举 DropDownList 列表
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="EmptyOptionText"></param>
        /// <returns></returns>
        public static MvcHtmlString EnumSelectFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, string EmptyOptionText = null) where TEnum : struct
        {
            return htmlHelper.EnumSelectFor(expression, null, EmptyOptionText);
        }

        /// <summary>
        /// 生成枚举 DropDownList 列表
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="EmptyOptionText"></param>
        /// <returns></returns>
        public static MvcHtmlString EnumSelectFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum?>> expression, string EmptyOptionText = null) where TEnum : struct
        {
            return htmlHelper.EnumSelectFor(expression, null, EmptyOptionText);
        }

        /// <summary>
        /// 生成枚举 DropDownList 列表
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <param name="EmptyOptionText"></param>
        /// <returns></returns>
        public static MvcHtmlString EnumSelectFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, object htmlAttributes, string EmptyOptionText = null) where TEnum : struct
        {
            return htmlHelper.EnumSelectFor(expression, ConvertToHtmlDictionaryAttributes(htmlAttributes), EmptyOptionText);
        }

        /// <summary>
        /// 生成枚举 DropDownList 列表
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <param name="EmptyOptionText"></param>
        /// <returns></returns>
        public static MvcHtmlString EnumSelectFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum?>> expression, object htmlAttributes, string EmptyOptionText = null) where TEnum : struct
        {
            return htmlHelper.EnumSelectFor(expression, ConvertToHtmlDictionaryAttributes(htmlAttributes), EmptyOptionText);
        }

        /// <summary>
        /// 生成枚举 DropDownList 列表
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <param name="EmptyOptionText"></param>
        /// <returns></returns>
        public static MvcHtmlString EnumSelectFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, IDictionary<string, object> htmlAttributes, string EmptyOptionText = null) where TEnum : struct
        {
            var modelMetadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var htmlFieldName = ConvertFieldName(ExpressionHelper.GetExpressionText(expression));
            var property = modelMetadata.ContainerType.GetProperty(htmlFieldName);
            var propertyValue = property != null ? (TEnum?)property.GetValue(modelMetadata.Container) : null;

            var items = typeof(TEnum).GetEnumNames()
                .Select(i =>
                {
                    var e = i.ToEnum<TEnum>();
                    return new SelectListItem
                    {
                        Text = (e as Enum).ToMessage(),
                        Value = e.ToString(),
                        Selected = e.Equals(propertyValue)
                    };
                })
                .ToList();
            if (EmptyOptionText.HasValue())
                items.Insert(0, new SelectListItem
                {
                    Text = EmptyOptionText,
                    Value = "",
                    Selected = !propertyValue.HasValue
                });

            return htmlHelper.DropDownListFor(expression, items, htmlAttributes);
        }

        /// <summary>
        /// 生成枚举 DropDownList 列表
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <param name="EmptyOptionText"></param>
        /// <returns></returns>
        public static MvcHtmlString EnumSelectFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum?>> expression, IDictionary<string, object> htmlAttributes, string EmptyOptionText = null) where TEnum : struct
        {
            var modelMetadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var htmlFieldName = ConvertFieldName(ExpressionHelper.GetExpressionText(expression));
            var property = modelMetadata.ContainerType.GetProperty(htmlFieldName);
            var propertyValue = property != null ? (TEnum?)property.GetValue(modelMetadata.Container) : null;

            var items = typeof(TEnum).GetEnumNames()
                .Select(i =>
                {
                    var e = i.ToEnum<TEnum>();
                    return new SelectListItem
                    {
                        Text = (e as Enum).ToMessage(),
                        Value = e.ToString(),
                        Selected = e.Equals(propertyValue)
                    };
                })
                .ToList();
            if (EmptyOptionText.HasValue())
                items.Insert(0, new SelectListItem
                {
                    Text = EmptyOptionText,
                    Value = "",
                    Selected = !propertyValue.HasValue
                });

            return htmlHelper.DropDownListFor(expression, items, htmlAttributes);
        }

        #endregion

        #region Date Time Range Picker

        /// <summary>
        /// 生成日期 & 时间 & 范围选择器
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="startExpression"></param>
        /// <param name="endExpression"></param>
        /// <param name="isTime"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="format"></param>
        /// <param name="placeholderFormat"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        private static MvcHtmlString GeneratePicker<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> startExpression, Expression<Func<TModel, TProperty>> endExpression, bool isTime, DateTime? min, DateTime? max, string format, string placeholderFormat, IDictionary<string, object> htmlAttributes)
        {
            var startModelMetadata = ModelMetadata.FromLambdaExpression(startExpression, htmlHelper.ViewData);
            var startHtmlFieldName = ConvertFieldName(ExpressionHelper.GetExpressionText(startExpression));
            var startProperty = startModelMetadata.ContainerType.GetProperty(startHtmlFieldName);
            var startPropertyValue = (startProperty != null ? startProperty.GetValue(startModelMetadata.Container) : null) as DateTime?;

            var isRange = endExpression != null;
            string endHtmlFieldName = null;
            PropertyInfo endProperty = null;
            DateTime? endPropertyValue = null;
            if (isRange)
            {
                var endModelMetadata = ModelMetadata.FromLambdaExpression(endExpression, htmlHelper.ViewData);
                endHtmlFieldName = ConvertFieldName(ExpressionHelper.GetExpressionText(endExpression));
                endProperty = endModelMetadata.ContainerType.GetProperty(endHtmlFieldName);
                endPropertyValue = (endProperty != null ? endProperty.GetValue(endModelMetadata.Container) : null) as DateTime?;
            }

            if (startProperty != null && (!isRange || (isRange && endProperty != null)))
            {
                // input
                var id = Guid.NewGuid().ToString("N");
                var placeholder = placeholderFormat;
                if (!isRange)
                {
                    var displayNameAttribute = startProperty.GetCustomAttribute<DisplayNameAttribute>();
                    placeholder = string.Format(placeholder.HasValue() ? placeholder : "{0}", displayNameAttribute == null ? startHtmlFieldName : displayNameAttribute.DisplayName);
                }
                htmlAttributes = htmlAttributes.Merge(new Dictionary<string, object>
                {
                    { "type", "text" },
                    { "id", id },
                    { "placeholder", placeholder },
                    { "data-toggle", (isTime ? "datetime" : "date") + (isRange ? "range" : "") },
                    { "data-start", startPropertyValue.HasValue ? startPropertyValue.Value.ToString(isTime ? "yyyy-MM-dd HH:mm:ss" : "yyyy-MM-dd") : null },
                    { "data-startname", startHtmlFieldName },
                    { "data-end", endPropertyValue.HasValue ? endPropertyValue.Value.ToString(isTime ? "yyyy-MM-dd HH:mm:ss" : "yyyy-MM-dd") : null },
                    { "data-endname", endHtmlFieldName },
                    { "data-min", min.HasValue ? min.Value.ToString("yyyy-MM-dd") : null },
                    { "data-max", max.HasValue ? max.Value.ToString("yyyy-MM-dd") : null },
                    { "data-format", format },
                    { "data-close", startProperty.PropertyType.Name.Contains("Nullable") && startProperty.GetCustomAttribute<RequiredAttribute>() == null }
                });
                var inputTag = new TagBuilder("input");
                inputTag.MergeAttributes(htmlAttributes);

                // start hidden
                var startHiddenTag = htmlHelper.HiddenFor(startExpression, new Dictionary<string, object> { { "data-for", id } });

                if (isRange)
                {
                    // end hidden
                    var endHiddenTag = htmlHelper.HiddenFor(endExpression, new Dictionary<string, object> { { "data-for", id } });

                    return new MvcHtmlString(startHiddenTag.ToString() + endHiddenTag.ToString() + inputTag.ToString(TagRenderMode.Normal));
                }
                else return new MvcHtmlString(startHiddenTag.ToString() + inputTag.ToString(TagRenderMode.Normal));
            }

            return MvcHtmlString.Empty;
        }


        /// <summary>
        /// 生成日期选择器
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="format"></param>
        /// <param name="placeholderFormat"></param>
        /// <returns></returns>
        public static MvcHtmlString DatePickerFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, DateTime? min = null, DateTime? max = null, string format = null, string placeholderFormat = "请选择{0}")
        {
            return htmlHelper.DatePickerFor(expression, null, min, max, format, placeholderFormat);
        }

        /// <summary>
        /// 生成日期选择器
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="format"></param>
        /// <param name="placeholderFormat"></param>
        /// <returns></returns>
        public static MvcHtmlString DatePickerFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes, DateTime? min = null, DateTime? max = null, string format = null, string placeholderFormat = "请选择{0}")
        {
            return htmlHelper.DatePickerFor(expression, ConvertToHtmlDictionaryAttributes(htmlAttributes), min, max, format, placeholderFormat);
        }

        /// <summary>
        /// 生成日期选择器
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="format"></param>
        /// <param name="placeholderFormat"></param>
        /// <returns></returns>
        public static MvcHtmlString DatePickerFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IDictionary<string, object> htmlAttributes, DateTime? min = null, DateTime? max = null, string format = null, string placeholderFormat = "请选择{0}")
        {
            return htmlHelper.GeneratePicker(expression, null, false, min, max, format, placeholderFormat, htmlAttributes);
        }


        /// <summary>
        /// 生成日期时间选择器
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="format"></param>
        /// <param name="placeholderFormat"></param>
        /// <returns></returns>
        public static MvcHtmlString DateTimePickerFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, DateTime? min = null, DateTime? max = null, string format = null, string placeholderFormat = "请选择{0}")
        {
            return htmlHelper.DateTimePickerFor(expression, null, min, max, format, placeholderFormat);
        }

        /// <summary>
        /// 生成日期时间选择器
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="format"></param>
        /// <param name="placeholderFormat"></param>
        /// <returns></returns>
        public static MvcHtmlString DateTimePickerFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes, DateTime? min = null, DateTime? max = null, string format = null, string placeholderFormat = "请选择{0}")
        {
            return htmlHelper.DateTimePickerFor(expression, ConvertToHtmlDictionaryAttributes(htmlAttributes), min, max, format, placeholderFormat);
        }

        /// <summary>
        /// 生成日期时间选择器
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="format"></param>
        /// <param name="placeholderFormat"></param>
        /// <returns></returns>
        public static MvcHtmlString DateTimePickerFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IDictionary<string, object> htmlAttributes, DateTime? min = null, DateTime? max = null, string format = null, string placeholderFormat = "请选择{0}")
        {
            return htmlHelper.GeneratePicker(expression, null, true, min, max, format, placeholderFormat, htmlAttributes);
        }


        /// <summary>
        /// 生成日期范围选择器
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="startExpression"></param>
        /// <param name="endExpression"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="format"></param>
        /// <param name="placeholder"></param>
        /// <returns></returns>
        public static MvcHtmlString DateRangePickerFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> startExpression, Expression<Func<TModel, TProperty>> endExpression, DateTime? min = null, DateTime? max = null, string format = null, string placeholder = "请选择日期范围")
        {
            return htmlHelper.DateRangePickerFor(startExpression, endExpression, null, min, max, format, placeholder);
        }

        /// <summary>
        /// 生成日期范围选择器
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="startExpression"></param>
        /// <param name="endExpression"></param>
        /// <param name="htmlAttributes"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="format"></param>
        /// <param name="placeholder"></param>
        /// <returns></returns>
        public static MvcHtmlString DateRangePickerFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> startExpression, Expression<Func<TModel, TProperty>> endExpression, object htmlAttributes, DateTime? min = null, DateTime? max = null, string format = null, string placeholder = "请选择日期范围")
        {
            return htmlHelper.DateRangePickerFor(startExpression, endExpression, ConvertToHtmlDictionaryAttributes(htmlAttributes), min, max, format, placeholder);
        }

        /// <summary>
        /// 生成日期范围选择器
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="startExpression"></param>
        /// <param name="endExpression"></param>
        /// <param name="htmlAttributes"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="format"></param>
        /// <param name="placeholder"></param>
        /// <returns></returns>
        public static MvcHtmlString DateRangePickerFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> startExpression, Expression<Func<TModel, TProperty>> endExpression, IDictionary<string, object> htmlAttributes, DateTime? min = null, DateTime? max = null, string format = null, string placeholder = "请选择日期范围")
        {
            return htmlHelper.GeneratePicker(startExpression, endExpression, false, min, max, format, placeholder, htmlAttributes);
        }


        /// <summary>
        /// 生成日期时间范围选择器
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="startExpression"></param>
        /// <param name="endExpression"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="format"></param>
        /// <param name="placeholder"></param>
        /// <returns></returns>
        public static MvcHtmlString DateTimeRangePickerFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> startExpression, Expression<Func<TModel, TProperty>> endExpression, DateTime? min = null, DateTime? max = null, string format = null, string placeholder = "请选择日期时间范围")
        {
            return htmlHelper.DateTimeRangePickerFor(startExpression, endExpression, null, min, max, format, placeholder);
        }

        /// <summary>
        /// 生成日期时间范围选择器
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="startExpression"></param>
        /// <param name="endExpression"></param>
        /// <param name="htmlAttributes"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="format"></param>
        /// <param name="placeholder"></param>
        /// <returns></returns>
        public static MvcHtmlString DateTimeRangePickerFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> startExpression, Expression<Func<TModel, TProperty>> endExpression, object htmlAttributes, DateTime? min = null, DateTime? max = null, string format = null, string placeholder = "请选择日期时间范围")
        {
            return htmlHelper.DateTimeRangePickerFor(startExpression, endExpression, ConvertToHtmlDictionaryAttributes(htmlAttributes), min, max, format, placeholder);
        }

        /// <summary>
        /// 生成日期时间范围选择器
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="startExpression"></param>
        /// <param name="endExpression"></param>
        /// <param name="htmlAttributes"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="format"></param>
        /// <param name="placeholder"></param>
        /// <returns></returns>
        public static MvcHtmlString DateTimeRangePickerFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> startExpression, Expression<Func<TModel, TProperty>> endExpression, IDictionary<string, object> htmlAttributes, DateTime? min = null, DateTime? max = null, string format = null, string placeholder = "请选择日期时间范围")
        {
            return htmlHelper.GeneratePicker(startExpression, endExpression, true, min, max, format, placeholder, htmlAttributes);
        }

        #endregion

        #region Require Label

        /// <summary>
        /// 生成 Label（自动判断字段是否必填，并添加必填样式）
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="requiredClassName"></param>
        /// <returns></returns>
        public static MvcHtmlString RequireLabelFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string requiredClassName = "require")
        {
            return htmlHelper.RequireLabelFor(expression, null, requiredClassName);
        }

        /// <summary>
        /// 生成 Label（自动判断字段是否必填，并添加必填样式）
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <param name="requiredClassName"></param>
        /// <returns></returns>
        public static MvcHtmlString RequireLabelFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes, string requiredClassName = "require")
        {
            return htmlHelper.RequireLabelFor(expression, ConvertToHtmlDictionaryAttributes(htmlAttributes), requiredClassName);
        }

        /// <summary>
        /// 生成 Label（自动判断字段是否必填，并添加必填样式）
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <param name="requiredClassName"></param>
        /// <returns></returns>
        public static MvcHtmlString RequireLabelFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IDictionary<string, object> htmlAttributes, string requiredClassName = "require")
        {
            var modelMetadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var htmlFieldName = ConvertFieldName(ExpressionHelper.GetExpressionText(expression));
            var property = modelMetadata.ContainerType.GetProperty(htmlFieldName);

            if (property != null)
            {
                var requiredAttribute = property.GetCustomAttribute<RequiredAttribute>();
                if (requiredAttribute != null)
                    htmlAttributes = htmlAttributes.Merge("class", htmlAttributes != null && htmlAttributes.ContainsKey("class") ? htmlAttributes["class"] + " " + requiredClassName : requiredClassName);
            }

            return htmlHelper.LabelFor(expression, htmlAttributes);
        }

        #endregion

        #region Placeholder Text Box

        /// <summary>
        /// 生成 TextBox（自动添加占位文字）
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static MvcHtmlString PlaceholderTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string format = "请输入{0}")
        {
            return htmlHelper.PlaceholderTextBoxFor(expression, null, format);
        }

        /// <summary>
        /// 生成 TextBox（自动添加占位文字）
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static MvcHtmlString PlaceholderTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes, string format = "请输入{0}")
        {
            return htmlHelper.PlaceholderTextBoxFor(expression, ConvertToHtmlDictionaryAttributes(htmlAttributes), format);
        }

        /// <summary>
        /// 生成 TextBox（自动添加占位文字）
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <param name="addPlaceholder"></param>
        /// <param name="placeholderFormat"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static MvcHtmlString PlaceholderTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IDictionary<string, object> htmlAttributes, string format = "请输入{0}")
        {
            var modelMetadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var htmlFieldName = ConvertFieldName(ExpressionHelper.GetExpressionText(expression));
            var property = modelMetadata.ContainerType.GetProperty(htmlFieldName);

            if (property != null)
            {
                var displayNameAttribute = property.GetCustomAttribute<DisplayNameAttribute>();
                var placeholder = string.Format(format.HasValue() ? format : "{0}", displayNameAttribute == null ? htmlFieldName : displayNameAttribute.DisplayName);

                htmlAttributes = htmlAttributes.Merge("placeholder", placeholder);
            }

            return htmlHelper.TextBoxFor(expression, htmlAttributes);
        }

        #endregion

        #region Placeholder Text Area

        /// <summary>
        /// 生成 TextArea（自动添加占位文字）
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="format"></param>
        /// <param name="rows"></param>
        /// <returns></returns>
        public static MvcHtmlString PlaceholderTextAreaFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string format = "请输入{0}", int rows = 3)
        {
            return htmlHelper.PlaceholderTextAreaFor(expression, null, format, rows);
        }

        /// <summary>
        /// 生成 TextArea（自动添加占位文字）
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <param name="format"></param>
        /// <param name="rows"></param>
        /// <returns></returns>
        public static MvcHtmlString PlaceholderTextAreaFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes, string format = "请输入{0}", int rows = 3)
        {
            return htmlHelper.PlaceholderTextAreaFor(expression, ConvertToHtmlDictionaryAttributes(htmlAttributes), format, rows);
        }

        /// <summary>
        /// 生成 TextArea（自动添加占位文字）
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <param name="addPlaceholder"></param>
        /// <param name="placeholderFormat"></param>
        /// <param name="format"></param>
        /// <param name="rows"></param>
        /// <returns></returns>
        public static MvcHtmlString PlaceholderTextAreaFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IDictionary<string, object> htmlAttributes, string format = "请输入{0}", int rows = 3)
        {
            var modelMetadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var htmlFieldName = ConvertFieldName(ExpressionHelper.GetExpressionText(expression));
            var property = modelMetadata.ContainerType.GetProperty(htmlFieldName);

            if (property != null)
            {
                var displayNameAttribute = property.GetCustomAttribute<DisplayNameAttribute>();
                var placeholder = string.Format(format.HasValue() ? format : "{0}", displayNameAttribute == null ? htmlFieldName : displayNameAttribute.DisplayName);

                htmlAttributes = htmlAttributes.Merge("placeholder", placeholder);
            }

            htmlAttributes = htmlAttributes.Merge("rows", rows);
            return htmlHelper.TextAreaFor(expression, htmlAttributes);
        }

        #endregion
    }
}
