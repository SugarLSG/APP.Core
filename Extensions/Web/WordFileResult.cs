﻿using System.IO;
using System.Text;

namespace System.Web.Mvc
{
    public class WordFileResult : ActionResult
    {
        public byte[] Bytes { get; private set; }
        public string FileName { get; private set; }


        public WordFileResult(MemoryStream stream, string fileName)
        {
            if (stream == null)
                throw new ArgumentNullException("stream");
            if (!fileName.HasValue())
                throw new ArgumentNullException("fileName");

            Bytes = stream.ToArray();
            FileName = fileName;

            Handle();
        }

        public WordFileResult(byte[] bytes, string fileName)
        {
            if (bytes == null)
                throw new ArgumentNullException("bytes");
            if (!fileName.HasValue())
                throw new ArgumentNullException("fileName");

            Bytes = bytes;
            FileName = fileName;

            Handle();
        }

        private void Handle()
        {
            var response = HttpContext.Current.Response;

            response.Clear();
            response.Charset = Encoding.UTF8.HeaderName;
            response.ContentEncoding = Encoding.UTF8;
            response.ContentType = CoreConfig.CONTENTTYPE_APPLICATION_WORD;

            response.AddHeader("Content-Disposition", "attachment; filename=" + HttpContext.Current.Server.UrlEncode(FileName));
            response.BinaryWrite(Bytes);
            response.Flush();
            response.End();
        }


        public override void ExecuteResult(ControllerContext context)
        {
        }
    }
}
