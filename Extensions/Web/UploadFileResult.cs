﻿using APP.Core.Utilities;
using System.Net;
using System.Text;

namespace System.Web.Mvc
{
    public class UploadFileResult : ActionResult
    {
        public UploadFileResult(object data = null)
        {
            var response = HttpContext.Current.Response;

            response.Clear();
            response.Charset = Encoding.UTF8.HeaderName;
            response.ContentEncoding = Encoding.UTF8;
            response.ContentType = CoreConfig.CONTENTTYPE_APPLICATION_JSON;

            response.Write(JsonUtility.Serialize(new
            {
                result = true,
                code = ((int)HttpStatusCode.OK).ToString(),
                message = CoreConfig.MESSAGE_OPERATE_SUCCESS,
                id = HttpContext.Current.Request.Form["id"],
                data = data,
                timestamp = DateTime.Now.ToTimestamp()
            }));
            response.Flush();
            response.End();
        }


        public override void ExecuteResult(ControllerContext context)
        {
        }
    }
}
