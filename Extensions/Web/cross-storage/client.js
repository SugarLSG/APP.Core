﻿var crossStorage = function (opts) {
    opts = {
        debug: opts.debug || false,
        origin: opts.origin,
        path: opts.path,
        iframe: undefined,
        codes: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.split(''),
        isReady: false,
        requestQueue: [],
        callBackList: {}
    }

    var engine = {
        _init: function () {
            // 判断是否支持
            if (window.postMessage && window.JSON && window.localStorage) {
                // 初始化 iframe
                if (!opts.iframe) {
                    opts.iframe = document.createElement("iframe");
                    opts.iframe.style.cssText = "width:0;height:0;display:none;";
                    opts.iframe.src = opts.origin + opts.path;

                    document.body.appendChild(opts.iframe);
                }

                // 监听消息
                if (window.addEventListener) {
                    window.addEventListener("message", engine._handle, false);
                } else if (window.attachEvent) {
                    window.attachEvent("onmessage", engine._handle);
                }

                // 标识已准备好
                if (opts.iframe.addEventListener) {
                    opts.iframe.addEventListener("load", engine._iframeLoaded, false);
                } else if (opts.iframe.attachEvent) {
                    opts.iframe.attachEvent("onload", engine._iframeLoaded, false);
                }
            } else {
                throw new Error('cross-storage: this browser unsupport cross storage.');
            }
        },

        _handle: function (event) {
            // 处理消息
            var eventData = JSON.parse(event.data);

            // 判断是否 cross storage 消息
            if (eventData.method.indexOf('cross-storage:') === 0) {
                if (opts.debug) {
                    var debugMessage = 'cross-storage-client.response: ' + JSON.stringify(eventData);
                    console.log(debugMessage);
                    alert(debugMessage);
                }

                // 回调
                var callBack = opts.callBackList[eventData.id];
                callBack && callBack(eventData.result, eventData.data);
                opts.callBackList[eventData.id] = undefined;
            }
        },
        _iframeLoaded: function (event) {
            opts.isReady = true;

            for (var i = 0; i < opts.requestQueue.length ; ++i) {
                var request = opts.requestQueue[i];
                engine._request(request.method, request.params, request.callBack);
            }
            opts.requestQueue = [];
        },
        _requestId: function (method) {
            var now = new Date();
            return 'cs_'
                + (((((now.getDate() - 1) * 24 + now.getHours()) * 60 + now.getMinutes()) * 60 + now.getSeconds()) * 1000 + now.getMilliseconds())
                + opts.codes[parseInt((Math.random() * opts.codes.length))]
                + opts.codes[parseInt((Math.random() * opts.codes.length))]
                + opts.codes[parseInt((Math.random() * opts.codes.length))]
                + opts.codes[parseInt((Math.random() * opts.codes.length))]
                + '_' + method;
        },
        _request: function (method, params, callBack) {
            if (opts.isReady) {
                var id = engine._requestId(method);
                callBack && (opts.callBackList[id] = callBack);

                opts.iframe.contentWindow.postMessage(JSON.stringify({
                    id: id,
                    debug: opts.debug,
                    method: 'cross-storage:' + method,
                    params: params
                }), opts.origin);
            } else {
                opts.requestQueue.push({
                    method: method,
                    params: params,
                    callBack: callBack
                });
            }
        },


        _set: function (name, value, callBack) {
            engine._request('set', { name: name, value: value }, callBack);
        },
        _get: function (name, callBack) {
            engine._request('get', { name: name }, callBack);
        },
        _remove: function (name, callBack) {
            engine._request('remove', { name: name }, callBack);
        },
        _clear: function (callBack) {
            engine._request('clear', {}, callBack);
        }
    };

    // 初始化
    engine._init();

    return {
        set: function (name, value, callBack) {
            engine._set(name, value, callBack);
        },
        get: function (name, callBack) {
            engine._get(name, callBack);
        },
        remove: function (name, callBack) {
            engine._remove(name, callBack);
        },
        clear: function (callBack) {
            engine._clear(callBack);
        }
    }
};
