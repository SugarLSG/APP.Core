﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System.IO;
using System.Text;
using System.Web.Mvc.Html;

namespace System.Web.Mvc
{
    public class PdfViewResult : ActionResult
    {
        private class PdfView : IView
        {
            public void Render(ViewContext viewContext, TextWriter writer)
            {
                throw new NotImplementedException();
            }
        }

        private class MSYHFontFactory : FontFactoryImp
        {
            private static readonly string fontPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "msyh.ttc,0");

            public override Font GetFont(string fontname, string encoding, bool embedded, float size, int style, BaseColor color, bool cached)
            {
                BaseFont baseFont = BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                return new Font(baseFont, size, style, color);
            }
        }


        public Controller Controller { get; private set; }
        public string ViewName { get; private set; }
        public object ViewData { get; private set; }
        public string FileName { get; private set; }


        public PdfViewResult(Controller controller, string viewName, string fileName)
        {
            if (controller == null)
                throw new ArgumentNullException("controller");
            if (!viewName.HasValue())
                throw new ArgumentNullException("viewName");
            if (!fileName.HasValue())
                throw new ArgumentNullException("viewName");

            Controller = controller;
            ViewName = viewName;
            ViewData = null;
            FileName = fileName;

            Handle();
        }

        public PdfViewResult(Controller controller, string viewName, object viewData, string fileName)
        {
            if (controller == null)
                throw new ArgumentNullException("controller");
            if (!viewName.HasValue())
                throw new ArgumentNullException("viewName");
            if (!fileName.HasValue())
                throw new ArgumentNullException("viewName");

            Controller = controller;
            ViewName = viewName;
            ViewData = viewData;
            FileName = fileName;

            Handle();
        }

        private void Handle()
        {
            // 获取页面
            var builder = new StringBuilder();
            using (var stringWriter = new StringWriter(builder))
            {
                var httpResponse = new HttpResponse(stringWriter);
                var httpContext = new HttpContext(HttpContext.Current.Request, httpResponse);
                var controllerContext = new ControllerContext(new HttpContextWrapper(httpContext), Controller.ControllerContext.RouteData, Controller.ControllerContext.Controller);

                var oldHttpContext = HttpContext.Current;
                HttpContext.Current = httpContext;

                using (var viewPage = new ViewPage())
                {
                    var html = new HtmlHelper(new ViewContext(controllerContext, new PdfView(), new ViewDataDictionary(), new TempDataDictionary(), stringWriter), viewPage);
                    html.RenderPartial(ViewName, ViewData);
                    HttpContext.Current = oldHttpContext;
                }
            }

            // 转为字符串
            var htmlText = builder.ToString();
            if (string.IsNullOrEmpty(htmlText))
                return;
            // 避免当 htmlText 为纯文字时，转 PDF 会抛异常
            htmlText = "<div>" + htmlText + "</div>";

            // 转为 PDF
            var bytes = Encoding.UTF8.GetBytes(htmlText);
            using (var inputStream = new MemoryStream(bytes))
            using (var document = new Document(PageSize.A4, 0, 0, 0, 0))
            using (var outputStream = new MemoryStream())
            using (var pdfWriter = PdfWriter.GetInstance(document, outputStream))
            {
                document.Open();
                XMLWorkerHelper.GetInstance().ParseXHtml(pdfWriter, document, inputStream, null, Encoding.UTF8, new MSYHFontFactory());
                var action = PdfAction.GotoLocalPage(1, new PdfDestination(PdfDestination.XYZ, 0, 0, 1f), pdfWriter);
                pdfWriter.SetOpenAction(action);
                document.Close();

                var pdfData = outputStream.ToArray();

                var response = HttpContext.Current.Response;
                response.Clear();
                response.Charset = Encoding.UTF8.HeaderName;
                response.ContentEncoding = Encoding.UTF8;
                response.ContentType = CoreConfig.CONTENTTYPE_APPLICATION_PDF;
                response.AddHeader("Content-Disposition", "attachment; filename=" + HttpContext.Current.Server.UrlEncode(FileName));
                response.BinaryWrite(pdfData);
                response.Flush();
                response.End();
            }
        }


        public override void ExecuteResult(ControllerContext context)
        {
        }
    }
}
