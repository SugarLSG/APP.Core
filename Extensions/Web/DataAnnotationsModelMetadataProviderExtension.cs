﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace APP.Core.Extensions.Web
{
    public class DataAnnotationsModelMetadataProviderExtension : DataAnnotationsModelMetadataProvider
    {
        private Type DefaultResourceType { get; set; }


        public DataAnnotationsModelMetadataProviderExtension(Type defaultResourceType)
        {
            DefaultResourceType = defaultResourceType;
        }

        protected override ModelMetadata CreateMetadata(IEnumerable<Attribute> attributes, Type containerType, Func<object> modelAccessor, Type modelType, string propertyName)
        {
            // 设置错误信息
            // 检查顺序 具体配置（ErrorMessage） => 公用配置（DefaultResourceType）
            foreach (ValidationAttribute validationAttribute in attributes.Where(x => x is ValidationAttribute))
            {
                // 判断是否已设置 ErrorMessageXXX 属性
                // 若已设置，则按已设置的 ErrorMessageXXX 属性显示
                if (!validationAttribute.ErrorMessage.HasValue()    // 是否已设置 ErrorMessage
                    && (validationAttribute.ErrorMessageResourceType == null || !validationAttribute.ErrorMessageResourceName.HasValue()))  // 是否已设置 ErrorMessageResourceType & ErrorMessageResourceName
                {
                    // 对应 ErrorMessageResourceType
                    var resourceType = validationAttribute.ErrorMessageResourceType ?? DefaultResourceType;

                    var attributeShortName = validationAttribute.GetType().Name.Replace("Attribute", "");
                    var commonAttributeKey = string.Format(CoreConfig.RESOURCEKEY_ATTRIBUTEMESSAGE_FORMAT, attributeShortName); // 公用配置对应 Attribute Key 格式
                    // 对应 ErrorMessageResourceName
                    var resourceKey = validationAttribute.ErrorMessageResourceName ?? commonAttributeKey;
                    // 检查该 Attribute 是否已配置公用配置
                    if (resourceType.GetProperty(resourceKey) != null)
                    {
                        // 设置为对应 ErrorMessageResourceType & ErrorMessageResourceName
                        validationAttribute.ErrorMessageResourceType = resourceType;
                        validationAttribute.ErrorMessageResourceName = resourceKey;
                    }
                }
            }

            return base.CreateMetadata(attributes, containerType, modelAccessor, modelType, propertyName);
        }
    }
}
