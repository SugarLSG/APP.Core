﻿/*
 扩展错误信息生成方法
 */
(function ($) {
    var RegexDoubleByte = /[^\x00-\xff]/g,                  // 双字节正则表达式
        RegexIdentityCode = /^(\d{15}|\d{17}(\d|x|X))$/,    // 身份证正则表达式
        IdentityCodeProvinceCodes = {
            '11': '北京', '12': '天津', '13': '河北', '14': '山西', '15': '内蒙古',
            '21': '辽宁', '22': '吉林', '23': '黑龙江',
            '31': '上海', '32': '江苏', '33': '浙江', '34': '安徽', '35': '福建', '36': '江西', '37': '山东',
            '41': '河南', '42': '湖北', '43': '湖南', '44': '广东', '45': '广西', '46': '海南',
            '50': '重庆', '51': '四川', '52': '贵州', '53': '云南', '54': '西藏',
            '61': '陕西', '62': '甘肃', '63': '青海', '64': '宁夏', '65': '新疆',
            '71': '台湾',
            '81': '香港', '82': '澳门', '91': '国外'
        },                                                                                  // 身份证省份代码
        IdentityCodeWeightings = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2],     // 身份证加权因子
        IdentityCodeChecksums = ['1', '0', 'x', '9', '8', '7', '6', '5', '4', '3', '2'];    // 身份证验证码

    var getElementValue = function (propertyName) {
        var $control = $('[name="' + propertyName + '"]');
        var type = $control && $control.length ? $control.attr('type') : undefined;
        if (!type) {
            type = '';
        }
        if (type.toUpperCase() === 'RADIO') {
            $control = $(':radio[name="' + propertyName + '"]:checked');
        } else if (type.toUpperCase() === 'CHECKBOX') {
            if (!$control.is(':checked'))
                $control = undefined;
        }

        return $control && $control.length ? $control.val() : '';
    };

    /* 默认提示信息 */
    if ($ && $.validator) {
        $.extend($.validator, {
            messages: {
                required: '请先输入值',
                remote: '数据验证不通过',
                email: '格式错误',
                url: '格式错误',
                date: '格式错误',
                dateISO: '格式错误',
                number: '格式错误',
                digits: '格式错误',
                creditcard: '格式错误',
                equalTo: '输入的值不一致',
                maxlength: $.validator.format('至多输入{0}个字符'),
                minlength: $.validator.format('至少输入{0}个字符'),
                rangelength: $.validator.format('请输入{0}~{1}个字符'),
                range: $.validator.format('请输入{0}~{1}'),
                max: $.validator.format('最大不能超过{0}'),
                min: $.validator.format('最小不能小于{0}')
            },
        });
    }

    /* Password Start */
    // 验证逻辑
    $.validator.addMethod('password',
        function (value, element, parameters) {
            // 允许为空
            if (value === undefined || value === '') return true;

            var pattern = parameters['pattern'];

            if (value && pattern) {
                return (new RegExp(pattern, 'g')).test(value);
            } else {
                return true;
            }
        }
    );

    // 验证入口
    $.validator.unobtrusive.adapters.add(
        'password',
        ['pattern'],
        function (options) {
            options.rules['password'] = {
                pattern: options.params['pattern']
            };
            options.messages['password'] = options.message;
        });
    /* Password End */

    /* Equals Start */
    // 验证逻辑
    $.validator.addMethod('equals',
        function (value, element, parameters) {
            // 允许为空
            if (value === undefined || value === '') return true;

            var equalsType = parameters['equalstype'],
                equalsTarget = parameters['equalstarget'],
                targetValue;

            if (equalsType === 'value') {
                targetValue = equalsTarget;
            } else if (equalsType === 'property') {
                targetValue = getElementValue(equalsTarget);
            }

            return value === targetValue;
        }
    );

    // 验证入口
    $.validator.unobtrusive.adapters.add(
        'equals',
        ['equalstype', 'equalstarget'],
        function (options) {
            options.rules['equals'] = {
                equalstype: options.params['equalstype'],
                equalstarget: options.params['equalstarget']
            };
            options.messages['equals'] = options.message;
        });
    /* Equals End */

    /* NotEquals Start */
    // 验证逻辑
    $.validator.addMethod('notequals',
        function (value, element, parameters) {
            // 允许为空
            if (value === undefined || value === '') return true;

            var equalsType = parameters['equalstype'],
                equalsTarget = parameters['equalstarget'],
                targetValue;

            if (equalsType === 'value') {
                targetValue = equalsTarget;
            } else if (equalsType === 'property') {
                targetValue = getElementValue(equalsTarget);
            }

            return value !== targetValue;
        }
    );

    // 验证入口
    $.validator.unobtrusive.adapters.add(
        'notequals',
        ['equalstype', 'equalstarget'],
        function (options) {
            options.rules['notequals'] = {
                equalstype: options.params['equalstype'],
                equalstarget: options.params['equalstarget']
            };
            options.messages['notequals'] = options.message;
        });
    /* NotEquals End */

    /* RequiredIfEquals Start */
    // 验证逻辑
    $.validator.addMethod('requiredifequals',
        function (value, element, parameters) {
            var propertyname = parameters['propertyname'],
                propertyvalues = parameters['propertyvalues'],
                propertyvalue = getElementValue(propertyname),
                propertyValueList = propertyvalues.split(',');

            if (propertyValueList.contains(propertyvalue)) {
                return $.validator.methods.required.call(this, value, element, parameters);
            } else {
                return true;
            }
        }
    );

    // 验证入口
    $.validator.unobtrusive.adapters.add(
        'requiredifequals',
        ['propertyname', 'propertyvalues'],
        function (options) {
            options.rules['requiredifequals'] = {
                propertyname: options.params['propertyname'],
                propertyvalues: options.params['propertyvalues']
            };
            options.messages['requiredifequals'] = options.message;
        });
    /* RequiredIfEquals End */

    /* RequiredIfNotEquals Start */
    // 验证逻辑
    $.validator.addMethod('requiredifnotequals',
        function (value, element, parameters) {
            var propertyname = parameters['propertyname'],
                propertyvalues = parameters['propertyvalues'],
                propertyvalue = getElementValue(propertyname),
                propertyValueList = propertyvalues.split(',');

            if (!propertyValueList.contains(propertyvalue)) {
                return $.validator.methods.required.call(this, value, element, parameters);
            } else {
                return true;
            }
        }
    );

    // 验证入口
    $.validator.unobtrusive.adapters.add(
        'requiredifnotequals',
        ['propertyname', 'propertyvalues'],
        function (options) {
            options.rules['requiredifnotequals'] = {
                propertyname: options.params['propertyname'],
                propertyvalues: options.params['propertyvalues']
            };
            options.messages['requiredifnotequals'] = options.message;
        });
    /* RequiredIfNotEquals End */

    /* RequiredIfEmpty Start */
    // 验证逻辑
    $.validator.addMethod('requiredifempty',
        function (value, element, parameters) {
            var propertyname = parameters['propertyname'],
                propertyvalue = getElementValue(propertyname);

            if (propertyvalue === undefined || propertyvalue === null || propertyvalue === '') {
                return $.validator.methods.required.call(this, value, element, parameters);
            } else {
                return true;
            }
        }
    );

    // 验证入口
    $.validator.unobtrusive.adapters.add(
        'requiredifempty',
        ['propertyname'],
        function (options) {
            options.rules['requiredifempty'] = {
                propertyname: options.params['propertyname']
            };
            options.messages['requiredifempty'] = options.message;
        });
    /* RequiredIfEmpty End */

    /* RequiredIfNotEmpty Start */
    // 验证逻辑
    $.validator.addMethod('requiredifnotempty',
        function (value, element, parameters) {
            var propertyname = parameters['propertyname'],
                propertyvalue = getElementValue(propertyname);

            if (propertyvalue !== undefined && propertyvalue !== null && propertyvalue !== '') {
                return $.validator.methods.required.call(this, value, element, parameters);
            } else {
                return true;
            }
        }
    );

    // 验证入口
    $.validator.unobtrusive.adapters.add(
        'requiredifnotempty',
        ['propertyname'],
        function (options) {
            options.rules['requiredifnotempty'] = {
                propertyname: options.params['propertyname']
            };
            options.messages['requiredifnotempty'] = options.message;
        });
    /* RequiredIfNotEmpty End */

    /* TextLength Start */
    // 验证逻辑
    $.validator.addMethod('textlength',
        function (value, element, parameters) {
            var maximumlength = parseInt(parameters['maximumlength']),
                minimumlength = parseInt(parameters['minimumlength']),
                doublebytelength = parseInt(parameters['doublebytelength']),
                elementName = $(element).attr('name'),
                $spanFor = $('span[data-textlengthspan-for="' + elementName + '"]'),
                stringLength = 0;

            if (value !== undefined && value !== '') {
                var cnMatchs = value.match(RegexDoubleByte);
                var cnCount = cnMatchs ? cnMatchs.length : 0;
                stringLength = cnCount * doublebytelength;
                stringLength += (value.length - cnCount);
            }

            $spanFor.html(stringLength + '/' + maximumlength);
            if (minimumlength <= stringLength && stringLength <= maximumlength) {
                $spanFor.removeClass('field-validation-error');
                return true;
            } else {
                $spanFor.addClass('field-validation-error');
                return false;
            }
        }
    );

    // 验证入口
    $.validator.unobtrusive.adapters.add(
        'textlength',
        ['maximumlength', 'minimumlength', 'doublebytelength'],
        function (options) {
            options.rules['textlength'] = {
                maximumlength: options.params['maximumlength'],
                minimumlength: options.params['minimumlength'],
                doublebytelength: options.params['doublebytelength']
            };
            options.messages['textlength'] = options.message;
        });
    /* TextLength End */

    /* CollectionLength Start */
    // 验证逻辑
    $.validator.addMethod('collectionlength',
        function (value, element, parameters) {
            var maximumlength = parseInt(parameters['maximumlength']),
                minimumlength = parseInt(parameters['minimumlength']),
                originalname = $(element).data('originalname'),
                index = 0,
                totalCount = 0;

            while (true) {
                var $control = $('[name^="' + originalname + '[' + (index++) + ']"]');
                if (!$control || !$control.length)
                    break;

                var type = $control ? $control.attr('type') : undefined;
                if (!type) {
                    type = '';
                }
                if (type.toUpperCase() === 'RADIO' || type.toUpperCase() === 'CHECKBOX') {
                    $control = $('[name^="' + originalname + '[' + (index - 1) + ']"]:checked');
                    if (!$control || !$control.length)
                        continue;
                }

                ++totalCount;
            };

            return minimumlength <= totalCount && totalCount <= maximumlength;
        }
    );

    // 验证入口
    $.validator.unobtrusive.adapters.add(
        'collectionlength',
        ['maximumlength', 'minimumlength'],
        function (options) {
            options.rules['collectionlength'] = {
                maximumlength: options.params['maximumlength'],
                minimumlength: options.params['minimumlength']
            };
            options.messages['collectionlength'] = options.message;
        });
    /* CollectionLength End */

    /* IdentityCode Start */
    // 验证逻辑
    $.validator.addMethod('identitycode',
        function (value, element, parameters) {
            // 允许为空
            if (value === undefined || value === '') return true;

            // 位数验证
            if (RegexIdentityCode.test(value)) {
                // 省份验证
                var provinceCode = value.substr(0, 2);
                if (IdentityCodeProvinceCodes[provinceCode]) {
                    // 出生年月日验证
                    var year, month, day;
                    if (value.length === 15) {
                        // 15位
                        year = parseInt('19' + value.substr(6, 2));
                        month = parseInt(value.substr(8, 2));
                        day = parseInt(value.substr(10, 2));
                    } else {
                        // 18位
                        year = parseInt(value.substr(6, 4));
                        month = parseInt(value.substr(10, 2));
                        day = parseInt(value.substr(12, 2));
                    }
                    var date = new Date(year, month - 1, day);
                    if (date.getFullYear() === year && date.getMonth() + 1 === month && date.getDate() === day) {
                        // 校验位验证
                        if (value.length === 15) {
                            // 15位
                            return true;
                        } else {
                            // 18位
                            var vals = value.split(''),
                                sum = 0;
                            for (var i = 0; i < 17; ++i) {
                                sum += parseInt(vals[i]) * IdentityCodeWeightings[i];
                            }
                            var checksums = IdentityCodeChecksums[sum % 11];
                            return checksums === vals[17].toLowerCase();
                        }
                    }
                }
            }

            return false;
        }
    );

    // 验证入口
    $.validator.unobtrusive.adapters.add(
        'identitycode',
        function (options) {
            options.rules['identitycode'] = {};
            options.messages['identitycode'] = options.message;
        });
    /* IdentityCode End */

    /* DigitalRange Start */
    // 验证逻辑
    $.validator.addMethod('digitalrange',
        function (value, element, parameters) {
            var digitalValue = parseFloat(value),
                minimum = parseFloat(parameters['minimum']),
                maximum = parseFloat(parameters['maximum']),
                minimumProperty = parameters['minimumproperty'],
                maximumProperty = parameters['maximumproperty'];

            if (isNaN(digitalValue))
                return true;

            if (minimumProperty) {
                var min = parseFloat(getElementValue(minimumProperty));
                !isNaN(min) && (minimum = min);
            }
            if (maximumProperty) {
                var max = parseFloat(getElementValue(maximumProperty));
                !isNaN(max) && (maximum = max);
            }

            return minimum <= digitalValue && digitalValue <= maximum;
        }
    );

    // 验证入口
    $.validator.unobtrusive.adapters.add(
        'digitalrange',
        ['minimum', 'maximum', 'minimumproperty', 'maximumproperty'],
        function (options) {
            options.rules['digitalrange'] = {
                minimum: options.params['minimum'],
                maximum: options.params['maximum'],
                minimumproperty: options.params['minimumproperty'],
                maximumproperty: options.params['maximumproperty']
            };
            options.messages['digitalrange'] = options.message;
        });
    /* DigitalRange End */

    /* PostRemote Start */
    // 验证逻辑
    $.validator.addMethod('postremote',
        function (value, element, parameters) {
            // 允许为空
            if (value === undefined || value === '') return true;

            var url = parameters['url'],
                additionalfields = parameters['additionalfields'],
                $element = $(element),
                elementName = $element.attr('name'),
                postData = {};

            // 非提交状态，输入结束后再验证（当前控件是否焦点）
            if (!IsSubmiting && $element.is(':focus')) {
                return true;
            }

            postData[elementName.split('.')[0]] = value;
            if (additionalfields !== undefined && additionalfields !== null && additionalfields !== '') {
                $.each(additionalfields.split(','), function (i, v) {
                    postData[v.split('.')[0]] = getElementValue(v);
                });
            }

            // 是否已验证
            var uniqueData = JSON.stringify({
                url: url,
                postData: postData
            });
            var lastrequestdata = $element.data('lastrequestdata');
            var isvalid = uniqueData === lastrequestdata ? $element.data('isvalid') : false;
            if (uniqueData !== lastrequestdata || isvalid === undefined) {
                $element.data('lastrequestdata', uniqueData);
                $element.data('isvalid', false);
                $element.attr({ readonly: true });

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: postData,
                    dataType: 'json',
                    contentType: 'application/x-www-form-urlencoded',
                    async: true,
                    cache: false,
                    success: function (responseData, successString, response) {
                        if (responseData === true || responseData === false) {
                            isvalid = responseData;
                        }
                    },
                    complete: function (response, completeString) {
                        $element.data('isvalid', isvalid).valid();
                        $element.attr({ readonly: false });
                    }
                });
            }

            return isvalid;
        }
    );

    // 验证入口
    $.validator.unobtrusive.adapters.add(
        'postremote',
        ['url', 'additionalfields'],
        function (options) {
            options.rules['postremote'] = {
                url: options.params['url'],
                additionalfields: options.params['additionalfields']
            };
            options.messages['postremote'] = options.message;
        });
    /* PostRemote End */
}(jQuery));
