﻿using System.Collections.Generic;

namespace System.Linq
{
    public static class LinqExtension
    {
        /// <summary>
        /// 按指定键对序列的元素进行排重
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source">源数据</param>
        /// <param name="keysSelector">用于从元素中提取键的函数（允许多个，自由组合）</param>
        /// <returns></returns>
        public static IEnumerable<TSource> DistinctBy<TSource>(this IEnumerable<TSource> source, Func<TSource, object> keysSelector)
        {
            if (source == null)
                yield break;

            var seenKeys = new HashSet<object>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keysSelector(element)))
                {
                    yield return element;
                }
            }
        }

        /// <summary>
        /// 返回泛型序列中的指定字段最大的项
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source">源数据</param>
        /// <param name="keySelector">判断最大的字段（单个）</param>
        /// <returns></returns>
        public static IEnumerable<TSource> MaxBy<TSource>(this IEnumerable<TSource> source, Func<TSource, double?> keySelector)
        {
            var result = new List<TSource>();

            if (source != null)
            {
                double? max = null;
                foreach (TSource element in source)
                {
                    var value = keySelector(element);

                    if (value.HasValue)
                    {
                        if (!max.HasValue || value.Value == max.Value)
                        {
                            result.Add(element);
                            max = value;
                        }
                        else if (max.HasValue && value.Value > max.Value)
                        {
                            result = new List<TSource> { element };
                            max = value;
                        }
                    }
                }
            }

            return result;
        }
        /// <summary>
        /// 返回泛型序列中的指定字段最大的项
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source">源数据</param>
        /// <param name="keySelector">判断最大的字段（单个）</param>
        /// <returns></returns>
        public static IEnumerable<TSource> MaxBy<TSource>(this IEnumerable<TSource> source, Func<TSource, double> keySelector)
        {
            return source.MaxBy(e => (double?)keySelector(e));
        }
        /// <summary>
        /// 返回泛型序列中的指定字段最大的项
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source">源数据</param>
        /// <param name="keySelector">判断最大的字段（单个）</param>
        /// <returns></returns>
        public static IEnumerable<TSource> MaxBy<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal?> keySelector)
        {
            return source.MaxBy(e => (double?)keySelector(e));
        }
        /// <summary>
        /// 返回泛型序列中的指定字段最大的项
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source">源数据</param>
        /// <param name="keySelector">判断最大的字段（单个）</param>
        /// <returns></returns>
        public static IEnumerable<TSource> MaxBy<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal> keySelector)
        {
            return source.MaxBy(e => (double?)keySelector(e));
        }
        /// <summary>
        /// 返回泛型序列中的指定字段最大的项
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source">源数据</param>
        /// <param name="keySelector">判断最大的字段（单个）</param>
        /// <returns></returns>
        public static IEnumerable<TSource> MaxBy<TSource>(this IEnumerable<TSource> source, Func<TSource, float?> keySelector)
        {
            return source.MaxBy(e => (double?)keySelector(e));
        }
        /// <summary>
        /// 返回泛型序列中的指定字段最大的项
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source">源数据</param>
        /// <param name="keySelector">判断最大的字段（单个）</param>
        /// <returns></returns>
        public static IEnumerable<TSource> MaxBy<TSource>(this IEnumerable<TSource> source, Func<TSource, float> keySelector)
        {
            return source.MaxBy(e => (double?)keySelector(e));
        }
        /// <summary>
        /// 返回泛型序列中的指定字段最大的项
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source">源数据</param>
        /// <param name="keySelector">判断最大的字段（单个）</param>
        /// <returns></returns>
        public static IEnumerable<TSource> MaxBy<TSource>(this IEnumerable<TSource> source, Func<TSource, long?> keySelector)
        {
            return source.MaxBy(e => (double?)keySelector(e));
        }
        /// <summary>
        /// 返回泛型序列中的指定字段最大的项
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source">源数据</param>
        /// <param name="keySelector">判断最大的字段（单个）</param>
        /// <returns></returns>
        public static IEnumerable<TSource> MaxBy<TSource>(this IEnumerable<TSource> source, Func<TSource, long> keySelector)
        {
            return source.MaxBy(e => (double?)keySelector(e));
        }
        /// <summary>
        /// 返回泛型序列中的指定字段最大的项
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source">源数据</param>
        /// <param name="keySelector">判断最大的字段（单个）</param>
        /// <returns></returns>
        public static IEnumerable<TSource> MaxBy<TSource>(this IEnumerable<TSource> source, Func<TSource, int?> keySelector)
        {
            return source.MaxBy(e => (double?)keySelector(e));
        }
        /// <summary>
        /// 返回泛型序列中的指定字段最大的项
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source">源数据</param>
        /// <param name="keySelector">判断最大的字段（单个）</param>
        /// <returns></returns>
        public static IEnumerable<TSource> MaxBy<TSource>(this IEnumerable<TSource> source, Func<TSource, int> keySelector)
        {
            return source.MaxBy(e => (double?)keySelector(e));
        }

        /// <summary>
        /// 返回泛型序列中的指定字段最小的项
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source">源数据</param>
        /// <param name="keySelector">判断最小的字段（单个）</param>
        /// <returns></returns>
        public static IEnumerable<TSource> MinBy<TSource>(this IEnumerable<TSource> source, Func<TSource, double?> keySelector)
        {
            var result = new List<TSource>();

            if (source != null)
            {
                double? min = null;
                foreach (TSource element in source)
                {
                    var value = keySelector(element);

                    if (value.HasValue)
                    {
                        if (!min.HasValue || value.Value == min.Value)
                        {
                            result.Add(element);
                            min = value;
                        }
                        else if (min.HasValue && value.Value < min.Value)
                        {
                            result = new List<TSource> { element };
                            min = value;
                        }
                    }
                }
            }

            return result;
        }
        /// <summary>
        /// 返回泛型序列中的指定字段最小的项
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source">源数据</param>
        /// <param name="keySelector">判断最小的字段（单个）</param>
        /// <returns></returns>
        public static IEnumerable<TSource> MinBy<TSource>(this IEnumerable<TSource> source, Func<TSource, double> keySelector)
        {
            return source.MinBy(e => (double?)keySelector(e));
        }
        /// <summary>
        /// 返回泛型序列中的指定字段最小的项
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source">源数据</param>
        /// <param name="keySelector">判断最小的字段（单个）</param>
        /// <returns></returns>
        public static IEnumerable<TSource> MinBy<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal?> keySelector)
        {
            return source.MinBy(e => (double?)keySelector(e));
        }
        /// <summary>
        /// 返回泛型序列中的指定字段最小的项
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source">源数据</param>
        /// <param name="keySelector">判断最小的字段（单个）</param>
        /// <returns></returns>
        public static IEnumerable<TSource> MinBy<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal> keySelector)
        {
            return source.MinBy(e => (double?)keySelector(e));
        }
        /// <summary>
        /// 返回泛型序列中的指定字段最小的项
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source">源数据</param>
        /// <param name="keySelector">判断最小的字段（单个）</param>
        /// <returns></returns>
        public static IEnumerable<TSource> MinBy<TSource>(this IEnumerable<TSource> source, Func<TSource, float?> keySelector)
        {
            return source.MinBy(e => (double?)keySelector(e));
        }
        /// <summary>
        /// 返回泛型序列中的指定字段最小的项
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source">源数据</param>
        /// <param name="keySelector">判断最小的字段（单个）</param>
        /// <returns></returns>
        public static IEnumerable<TSource> MinBy<TSource>(this IEnumerable<TSource> source, Func<TSource, float> keySelector)
        {
            return source.MinBy(e => (double?)keySelector(e));
        }
        /// <summary>
        /// 返回泛型序列中的指定字段最小的项
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source">源数据</param>
        /// <param name="keySelector">判断最小的字段（单个）</param>
        /// <returns></returns>
        public static IEnumerable<TSource> MinBy<TSource>(this IEnumerable<TSource> source, Func<TSource, long?> keySelector)
        {
            return source.MinBy(e => (double?)keySelector(e));
        }
        /// <summary>
        /// 返回泛型序列中的指定字段最小的项
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source">源数据</param>
        /// <param name="keySelector">判断最小的字段（单个）</param>
        /// <returns></returns>
        public static IEnumerable<TSource> MinBy<TSource>(this IEnumerable<TSource> source, Func<TSource, long> keySelector)
        {
            return source.MinBy(e => (double?)keySelector(e));
        }
        /// <summary>
        /// 返回泛型序列中的指定字段最小的项
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source">源数据</param>
        /// <param name="keySelector">判断最小的字段（单个）</param>
        /// <returns></returns>
        public static IEnumerable<TSource> MinBy<TSource>(this IEnumerable<TSource> source, Func<TSource, int?> keySelector)
        {
            return source.MinBy(e => (double?)keySelector(e));
        }
        /// <summary>
        /// 返回泛型序列中的指定字段最小的项
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source">源数据</param>
        /// <param name="keySelector">判断最小的字段（单个）</param>
        /// <returns></returns>
        public static IEnumerable<TSource> MinBy<TSource>(this IEnumerable<TSource> source, Func<TSource, int> keySelector)
        {
            return source.MinBy(e => (double?)keySelector(e));
        }

        /// <summary>
        /// 将字典与指定键值合并
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dic">字典</param>
        /// <param name="key">指定键</param>
        /// <param name="value">指定值</param>
        /// <returns></returns>
        public static IDictionary<TKey, TValue> Merge<TKey, TValue>(this IDictionary<TKey, TValue> dic, TKey key, TValue value)
        {
            if (dic == null)
                dic = new Dictionary<TKey, TValue>();

            if (dic.ContainsKey(key)) dic[key] = value;
            else dic.Add(key, value);

            return dic;
        }
        /// <summary>
        /// 将字典与指定键值合并
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dic">字典</param>
        /// <param name="item">指定键值</param>
        /// <returns></returns>
        public static IDictionary<TKey, TValue> Merge<TKey, TValue>(this IDictionary<TKey, TValue> dic, KeyValuePair<TKey, TValue> item)
        {
            return dic.Merge(item.Key, item.Value);
        }
        /// <summary>
        /// 将字典与指定键值列表合并
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dic">字典</param>
        /// <param name="items">指定键值列表</param>
        /// <returns></returns>
        public static IDictionary<TKey, TValue> Merge<TKey, TValue>(this IDictionary<TKey, TValue> dic, IEnumerable<KeyValuePair<TKey, TValue>> items)
        {
            if (dic == null)
                dic = new Dictionary<TKey, TValue>();

            if (items != null)
            {
                foreach (var item in items)
                    dic.Merge(item);
            }

            return dic;
        }
        /// <summary>
        /// 将两个字典合并
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dic1">字典1</param>
        /// <param name="dic2">字典2</param>
        /// <returns></returns>
        public static IDictionary<TKey, TValue> Merge<TKey, TValue>(this IDictionary<TKey, TValue> dic1, IDictionary<TKey, TValue> dic2)
        {
            if (dic1 == null)
                dic1 = new Dictionary<TKey, TValue>();

            if (dic2 != null)
            {
                foreach (var item in dic2)
                    dic1.Merge(item);
            }

            return dic1;
        }

        /// <summary>
        /// 若指定键不存在，则添加对应键值
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dic1"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static IDictionary<TKey, TValue> AddIfNotExists<TKey, TValue>(this IDictionary<TKey, TValue> dic, TKey key, TValue value)
        {
            if (dic == null)
                dic = new Dictionary<TKey, TValue>();

            if (!dic.ContainsKey(key))
                dic.Add(key, value);

            return dic;
        }
        /// <summary>
        /// 若指定键不存在，则添加对应键值
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dic1"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public static IDictionary<TKey, TValue> AddIfNotExists<TKey, TValue>(this IDictionary<TKey, TValue> dic, KeyValuePair<TKey, TValue> item)
        {
            return dic.AddIfNotExists(item.Key, item.Value);
        }

        private class ASCIIComparer : IComparer<string>
        {
            public int Compare(string x, string y)
            {
                return string.CompareOrdinal(x, y);
            }
        }
        /// <summary>
        /// 按指定键按 ASCII 升序对序列的元素进行排序
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source">源数据</param>
        /// <param name="keySelector">用于从元素中提取键的函数</param>
        /// <returns></returns>
        public static IEnumerable<TSource> OrderByASCII<TSource>(this IEnumerable<TSource> source, Func<TSource, string> keySelector)
        {
            var comparer = new ASCIIComparer();
            return source.OrderBy(i => keySelector(i), comparer);
        }
        /// <summary>
        /// 按指定键按 ASCII 降序对序列的元素进行排序
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source">源数据</param>
        /// <param name="keySelector">用于从元素中提取键的函数</param>
        /// <returns></returns>
        public static IEnumerable<TSource> OrderByASCIIDescending<TSource>(this IEnumerable<TSource> source, Func<TSource, string> keySelector)
        {
            return source.OrderByASCII(i => keySelector(i)).Reverse();
        }
    }
}
