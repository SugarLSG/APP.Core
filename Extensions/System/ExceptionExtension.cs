﻿using APP.Core.Exceptions;
using APP.Core.Utilities;
using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

public static class ExceptionExtension
{
    /// <summary>
    /// 转为配置消息
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static string ToMessage(this APPBaseException e)
    {
        var type = e.GetType();

        // 是否有信息参数
        if (e.Args != null && e.Args.Any())
        {
            // 有参数，不使用缓存
            var descriptionAttribute = type.GetCustomAttribute<DescriptionAttribute>();
            return descriptionAttribute == null ? e.Message : string.Format(descriptionAttribute.Description, e.Args);
        }

        return CacheUtility.GetOrSetFromCache(
            string.Format("{0}{1}", CoreConfig.CACHEKEY_EXCEPTIONMESSAGE_PREFIX, type.Name),
            () =>
            {
                var descriptionAttribute = type.GetCustomAttribute<DescriptionAttribute>();
                return descriptionAttribute == null ? e.Message : descriptionAttribute.Description;
            }
        );
    }

    /// <summary>
    /// 转为配置Code
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static string ToCode(this Exception e)
    {
        return e is APPBaseException ? (e as APPBaseException).Code : "500";
    }
}
