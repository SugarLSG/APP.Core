﻿using System;
using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

public static class ObjectExtension
{
    public enum DeepCopyLevel
    {
        /// <summary>
        /// 仅拷贝属性
        /// </summary>
        Property
    }


    /// <summary>
    /// 深拷贝
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj"></param>
    /// <param name="copyLevel"></param>
    /// <returns></returns>
    public static T DeepCopy<T>(this T obj, DeepCopyLevel copyLevel = DeepCopyLevel.Property) where T : class
    {
        if (obj == null)
            return null;

        var type = obj.GetType();
        var instance = Activator.CreateInstance(type);

        if (copyLevel == DeepCopyLevel.Property)
        {
            foreach (var property in type.GetProperties(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic))
            {
                if (property.CanWrite)
                {
                    var value = property.GetValue(obj);
                    if (value != null)
                    {
                        var propertyType = property.PropertyType;
                        if (propertyType.IsClass && propertyType != CoreConfig.STRING_TYPE)
                            property.SetValue(instance, property.GetValue(obj).DeepCopy());
                        else
                            property.SetValue(instance, property.GetValue(obj));
                    }
                }
            }
        }

        return (T)instance;
    }


    /// <summary>
    /// 判断所有设置了 ValidationAttribute 的属性的有效性
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static bool IsValid(this object obj)
    {
        if (obj == null)
            return true;

        var objType = obj.GetType();
        // 值类型
        if (objType.IsValueType || objType == CoreConfig.STRING_TYPE)
            return true;
        // 集合类型
        else if (obj is IEnumerable)
        {
            foreach (var item in obj as IEnumerable)
                if (!item.IsValid())
                    return false;
        }
        // 其他类型
        else
        {
            foreach (var property in objType.GetProperties())
            {
                var propertyValue = property.GetValue(obj);

                // 判断属性有效性
                foreach (var attr in property.GetCustomAttributes<ValidationAttribute>(true))
                {
                    if (attr.GetValidationResult(propertyValue, new ValidationContext(obj) { MemberName = property.Name }) != null)
                        return false;
                }

                // 判断属性值有效性
                if (!propertyValue.IsValid())
                    return false;
            }
        }

        return true;
    }
}
