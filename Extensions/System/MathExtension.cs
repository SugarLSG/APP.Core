﻿using System;
using System.Collections.Generic;
using System.Linq;

public static class MathExtension
{
    private static readonly string Hex32Code = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static readonly char[] Hex32Codes = Hex32Code.ToArray();
    private const int DIGITAL_PRECISION = 2;


    private static string GetAmountStringFormat(int accuracy)
    {
        return "0.".PadRight(accuracy + 2, '0');
    }

    /// <summary>
    /// 10进制转为32进制（带正负符号）
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    public static string To32Hex(this int number)
    {
        return ((long)number).To32Hex();
    }

    /// <summary>
    /// 10进制转为32进制（带正负符号）
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    public static string To32Hex(this long number)
    {
        var sign = number < 0 ? "-" : string.Empty;

        long temp = Math.Abs(number), remainder = 0;
        var result = new List<char>();
        do
        {
            remainder = temp % 32;
            temp /= 32;
            result.Add(Hex32Codes[remainder]);
        } while (temp != 0);

        result.Reverse();
        return sign + string.Join("", result);
    }

    /// <summary>
    /// 32进制转为10进制（带正负符号）
    /// </summary>
    /// <param name="code"></param>
    /// <returns></returns>
    public static long Parse32Hex(this string code)
    {
        int i = 0;
        long value = 0;
        foreach (var c in code.TrimStart('-').ToArray().Reverse())
            value += (long)Math.Pow(32, i++) * Hex32Code.IndexOf(c);

        return code.StartsWith("-") ? -value : value;
    }

    /// <summary>
    /// 转为包含精度的数值
    /// </summary>
    /// <param name="digital"></param>
    /// <param name="precision">精度</param>
    /// <returns></returns>
    public static double ToPrecision(this float digital, int precision = DIGITAL_PRECISION)
    {
        return Convert.ToDouble(digital.ToString(GetAmountStringFormat(precision)));
    }

    /// <summary>
    /// 转为包含精度的数值
    /// </summary>
    /// <param name="digital"></param>
    /// <param name="precision">精度</param>
    /// <returns></returns>
    public static double ToPrecision(this double digital, int precision = DIGITAL_PRECISION)
    {
        return Convert.ToDouble(digital.ToString(GetAmountStringFormat(precision)));
    }
}
