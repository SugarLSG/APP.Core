﻿using APP.Core.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

public static class EnumExtension
{
    /// <summary>
    /// 转为配置消息
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static string ToMessage(this Enum e)
    {
        var type = e.GetType();
        return CacheUtility.GetOrSetFromCache(
            string.Format("{0}{1}.{2}", CoreConfig.CACHEKEY_ENUMMESSAGE_PREFIX, type.Name, e.ToString()),
            () =>
            {
                var field = type.GetField(e.ToString());
                var descriptionAttribute = field.GetCustomAttribute<DescriptionAttribute>();
                // 是否已配置 Attribute
                return descriptionAttribute == null ? e.ToString() : descriptionAttribute.Description;
            }
        );
    }

    /// <summary>
    /// 转为指定枚举
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value"></param>
    /// <param name="ignoreCase"></param>
    /// <returns></returns>
    public static T ToEnum<T>(this int value, bool ignoreCase = false) where T : struct
    {
        return (T)Enum.Parse(typeof(T), value.ToString());
    }

    /// <summary>
    /// 转为指定枚举（可空）
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value"></param>
    /// <param name="ignoreCase"></param>
    /// <returns></returns>
    public static T? ToEnumNullable<T>(this int? value, bool ignoreCase = false) where T : struct
    {
        return value.HasValue && Enum.IsDefined(typeof(T), value.Value) ? value.Value.ToEnum<T>(ignoreCase) : (T?)null;
    }

    /// <summary>
    /// 转为指定枚举
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value"></param>
    /// <param name="ignoreCase"></param>
    /// <returns></returns>
    public static T ToEnum<T>(this string value, bool ignoreCase = false) where T : struct
    {
        return (T)Enum.Parse(typeof(T), value);
    }

    /// <summary>
    /// 转为指定枚举（可空）
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value"></param>
    /// <param name="ignoreCase"></param>
    /// <returns></returns>
    public static T? ToEnumNullable<T>(this string value, bool ignoreCase = false) where T : struct
    {
        return value.HasValue() && Enum.IsDefined(typeof(T), value) ? value.ToEnum<T>(ignoreCase) : (T?)null;
    }

    /// <summary>
    /// 获取指定枚举列表
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static IEnumerable<T> GetEnumList<T>() where T : struct
    {
        return typeof(T).GetEnumNames().Select(n => n.ToEnum<T>());
    }
}
