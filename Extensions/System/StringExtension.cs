﻿using System;

public static class StringExtension
{
    /// <summary>
    /// 是否有值
    /// </summary>
    /// <param name="str"></param>
    /// <param name="trimSpace"></param>
    /// <returns></returns>
    public static bool HasValue(this string str, bool trimSpace = true)
    {
        if (trimSpace && !string.IsNullOrEmpty(str))
            str = str.Trim();

        return !string.IsNullOrEmpty(str);
    }

    /// <summary>
    /// 截断字符串
    /// 例："abcdefg".Substring(1, "d") => bcd
    /// </summary>
    /// <param name="str"></param>
    /// <param name="startIndex"></param>
    /// <param name="endString"></param>
    /// <returns></returns>
    public static string Substring(this string str, int startIndex, string endString)
    {
        if (!str.HasValue())
            throw new ArgumentNullException("string");

        var endStartIndex = str.IndexOf(endString);
        if (endStartIndex == -1)
            throw new ArgumentOutOfRangeException("endString");

        return str.Substring(startIndex, endStartIndex + endString.Length - startIndex);
    }

    /// <summary>
    /// 获取包含指定字符串数量
    /// </summary>
    /// <param name="str"></param>
    /// <param name="value"></param>
    /// <param name="ignoreCase"></param>
    /// <returns></returns>
    public static int Count(this string str, string value, bool ignoreCase = false)
    {
        if (!str.HasValue())
            throw new ArgumentNullException("string");
        if (!value.HasValue())
            throw new ArgumentNullException("value");

        var tempString = str;
        int valueLength = value.Length,
            count = 0;
        while (true)
        {
            var index = ignoreCase ?
                tempString.IndexOf(value, StringComparison.InvariantCultureIgnoreCase) :
                tempString.IndexOf(value);
            if (index < 0)
                break;

            tempString = tempString.Substring(0, index) + tempString.Substring(index + valueLength);
            ++count;
        }

        return count;
    }
}
