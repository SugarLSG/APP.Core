﻿using System;
using System.Collections.Generic;

namespace APP.Core.Extensions.System
{
    public class EqualityComparerExtension<T> : IEqualityComparer<T>
    {
        private Func<T, T, bool> Comparer { get; set; }

        public EqualityComparerExtension(Func<T, T, bool> comparer)
        {
            Comparer = comparer;
        }

        public bool Equals(T x, T y)
        {
            return Comparer(x, y);
        }

        public int GetHashCode(T obj)
        {
            return obj.GetHashCode();
        }
    }
}
