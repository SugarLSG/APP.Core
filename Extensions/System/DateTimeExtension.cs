﻿using System;

public static class DateTimeExtension
{
    public enum TimestampLevel
    {
        /// <summary>
        /// 毫秒级别
        /// </summary>
        Millisecond,
        /// <summary>
        /// 秒级别
        /// </summary>
        Second,
        /// <summary>
        /// 分钟级别
        /// </summary>
        Minute,
        /// <summary>
        /// 小时级别
        /// </summary>
        Hour
    }


    public const string DATETIME_DEFAULTFORMAT = "yyyy-MM-dd HH:mm:ss";


    /// <summary>
    /// 具体时间转为时间戳
    /// </summary>
    /// <param name="dt"></param>
    /// <param name="level"></param>
    /// <returns></returns>
    public static long ToTimestamp(this DateTime dt, TimestampLevel level = TimestampLevel.Second)
    {
        switch (level)
        {
            case TimestampLevel.Millisecond:
                return (long)(dt - CoreConfig.APP_STARTTIME).TotalMilliseconds;
            case TimestampLevel.Minute:
                return (long)(dt - CoreConfig.APP_STARTTIME).TotalMinutes;
            case TimestampLevel.Hour:
                return (long)(dt - CoreConfig.APP_STARTTIME).TotalHours;
            default:
                return (long)(dt - CoreConfig.APP_STARTTIME).TotalSeconds;
        }
    }

    /// <summary>
    /// 时间戳转为具体时间
    /// </summary>
    /// <param name="ts"></param>
    /// <param name="level"></param>
    /// <returns></returns>
    public static DateTime ParseTimestamp(this long ts, TimestampLevel level = TimestampLevel.Second)
    {
        switch (level)
        {
            case TimestampLevel.Millisecond:
                return CoreConfig.APP_STARTTIME.Add(TimeSpan.FromMilliseconds(ts));
            case TimestampLevel.Minute:
                return CoreConfig.APP_STARTTIME.Add(TimeSpan.FromMinutes(ts));
            case TimestampLevel.Hour:
                return CoreConfig.APP_STARTTIME.Add(TimeSpan.FromHours(ts));
            default:
                return CoreConfig.APP_STARTTIME.Add(TimeSpan.FromSeconds(ts));
        }
    }

    /// <summary>
    /// 时间戳转为具体时间
    /// </summary>
    /// <param name="ts"></param>
    /// <param name="level"></param>
    /// <returns></returns>
    public static DateTime ParseTimestamp(this int ts, TimestampLevel level = TimestampLevel.Second)
    {
        return ((long)ts).ParseTimestamp(level);
    }


    /// <summary>
    /// 转为默认格式
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static string ToDefaultFormat(this DateTime dt)
    {
        return dt.ToString(DATETIME_DEFAULTFORMAT);
    }

    /// <summary>
    /// 转为星期
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static string ToWeekDay(this DateTime dt)
    {
        switch (dt.DayOfWeek)
        {
            case DayOfWeek.Sunday: return "周日";
            case DayOfWeek.Monday: return "周一";
            case DayOfWeek.Tuesday: return "周二";
            case DayOfWeek.Wednesday: return "周三";
            case DayOfWeek.Thursday: return "周四";
            case DayOfWeek.Friday: return "周五";
            case DayOfWeek.Saturday: return "周六";
        }

        return string.Empty;
    }
}
