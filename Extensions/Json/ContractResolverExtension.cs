﻿using Newtonsoft.Json.Serialization;

namespace APP.Core.Extensions.Json
{
    public class ContractResolverExtension : DefaultContractResolver
    {
        public enum PropertyNameType
        {
            /// <summary>
            /// 不处理
            /// </summary>
            None,
            /// <summary>
            /// 全小写
            /// </summary>
            Lowercase,
            /// <summary>
            /// 全大写
            /// </summary>
            Uppercase,
            /// <summary>
            /// 驼峰
            /// </summary>
            CamelCase
        }

        private PropertyNameType PPropertyNameType { get; set; }


        /// <summary>
        /// 解析扩展
        /// </summary>
        /// <param name="propertyNameType">属性名解析类型</param>
        public ContractResolverExtension(PropertyNameType propertyNameType)
        {
            PPropertyNameType = propertyNameType;
        }

        protected override string ResolvePropertyName(string propertyName)
        {
            switch (PPropertyNameType)
            {
                case PropertyNameType.Lowercase: return propertyName.ToLower();
                case PropertyNameType.Uppercase: return propertyName.ToUpper();
                case PropertyNameType.CamelCase:
                    if (propertyName.Length == 1) return propertyName.ToLower();
                    else return propertyName.Substring(0, 1).ToLower() + propertyName.Substring(1);
            }

            return propertyName;
        }
    }
}
