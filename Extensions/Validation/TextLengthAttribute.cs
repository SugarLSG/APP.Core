﻿using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace System.ComponentModel.DataAnnotations
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class TextLengthAttribute : ValidationAttribute, IClientValidatable
    {
        /// <summary>
        /// 双字节字符所占长度
        /// </summary>
        private const int SQL_DOUBLEBYTE_LENGTH = 2;


        public int MaximumLength { get; }
        public int MinimumLength { get; }
        public int DoubleByteLength { get; }


        /// <summary>
        /// 验证文字长度（匹配双字节字符长度）
        /// </summary>
        /// <param name="maximumLength">最长长度，数据库设置的长度，例如 varchar(64)，匹配 64</param>
        /// <param name="minimumLength">最短长度，数据库设置的长度，例如 varchar(64)，匹配 64</param>
        /// <param name="doubleByteLength">双字节字符长度</param>
        public TextLengthAttribute(int maximumLength, int minimumLength = 0, int doubleByteLength = SQL_DOUBLEBYTE_LENGTH)
        {
            if (maximumLength < 0)
                throw new ArgumentException("maximumLength");
            if (minimumLength < 0)
                throw new ArgumentException("minimumLength");
            if (maximumLength < minimumLength)
                throw new ArgumentOutOfRangeException("minimumLength & maximumLength");

            MaximumLength = maximumLength;
            MinimumLength = minimumLength;
            DoubleByteLength = doubleByteLength;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var validationRule = new ModelClientValidationRule
            {
                ErrorMessage = string.Format(CultureInfo.CurrentCulture, ErrorMessageString, metadata.DisplayName, MaximumLength, MinimumLength),
                ValidationType = "textlength",
            };
            validationRule.ValidationParameters.Add("maximumlength", MaximumLength);
            validationRule.ValidationParameters.Add("minimumlength", MinimumLength);
            validationRule.ValidationParameters.Add("doublebytelength", DoubleByteLength);

            yield return validationRule;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var stringLength = 0;
            if (value != null)
            {
                var valueString = value.ToString();
                var cnCount = valueString.HasValue() ? Regex.Matches(valueString, CoreConfig.RegexDoubleByte).Count : 0;

                stringLength = cnCount * DoubleByteLength;
                stringLength += (valueString.Length - cnCount);
            }

            return MinimumLength <= stringLength && stringLength <= MaximumLength ? null : new ValidationResult(null);
        }
    }
}
