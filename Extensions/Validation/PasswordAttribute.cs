﻿using APP.Core.Utilities;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace System.ComponentModel.DataAnnotations
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class PasswordAttribute : ValidationAttribute, IClientValidatable
    {
        public enum EncryptType
        {
            /// <summary>
            /// 不使用加密
            /// </summary>
            None,
            /// <summary>
            /// RSA 16进制 加密
            /// </summary>
            RSA_Hex,
            /// <summary>
            /// RSA Base64 加密
            /// </summary>
            RSA_Base64,
            /// <summary>
            /// BASE64 加密
            /// </summary>
            BASE64,
            /// <summary>
            /// 密码 加密
            /// </summary>
            Password
        }


        public string Pattern { get; }
        public Regex Regex { get; }
        public EncryptType PEncryptType { get; }


        /// <summary>
        /// 验证密码
        /// </summary>
        /// <param name="pattern">正则表达式</param>
        /// <param name="encryptType">加密类型</param>
        public PasswordAttribute(string pattern = CoreConfig.RegexStrongPassword6_32, EncryptType encryptType = EncryptType.Password)
        {
            if (!pattern.HasValue())
                throw new ArgumentNullException("pattern");

            Pattern = pattern;
            Regex = new Regex(Pattern);
            PEncryptType = encryptType;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var validationRule = new ModelClientValidationRule
            {
                ErrorMessage = string.Format(CultureInfo.CurrentCulture, ErrorMessageString, metadata.DisplayName),
                ValidationType = "password",
            };
            validationRule.ValidationParameters.Add("pattern", Pattern);

            yield return validationRule;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null && (value as string).HasValue())
            {
                var valueString = value.ToString();

                var decryptString = string.Empty;
                try
                {
                    if (PEncryptType == EncryptType.RSA_Hex)
                        decryptString = SecurityUtility.RSADecrypt_Hex(valueString);
                    else if (PEncryptType == EncryptType.RSA_Base64)
                        decryptString = SecurityUtility.RSADecrypt_Base64(valueString);
                    else if (PEncryptType == EncryptType.BASE64)
                        decryptString = SecurityUtility.BASE64Decrypt(valueString);
                    else if (PEncryptType == EncryptType.Password)
                        decryptString = SecurityUtility.PasswordDecrypt(valueString);
                }
                catch { decryptString = string.Empty; }

                if (Regex.IsMatch(decryptString))
                {
                    // 赋值解密后的密码
                    validationContext.ObjectInstance.GetType().GetProperty(validationContext.MemberName).SetValue(validationContext.ObjectInstance, decryptString);
                    return null;
                }
            }
            // 允许为空
            else return null;

            return new ValidationResult(null);
        }
    }
}
