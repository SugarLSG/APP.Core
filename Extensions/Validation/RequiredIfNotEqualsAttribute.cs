﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace System.ComponentModel.DataAnnotations
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class RequiredIfNotEqualsAttribute : ValidationAttribute, IClientValidatable
    {
        public string PropertyName { get; }
        public object[] PropertyValues { get; }


        /// <summary>
        /// 指定属性不等于某个值，验证非空
        /// </summary>
        /// <param name="propertyName">要验证相等的属性名称（支持层级，以英文.分隔）</param>
        /// <param name="propertyValues">要验证相等的属性值（仅支持基础数据类型）</param>
        public RequiredIfNotEqualsAttribute(string propertyName, params object[] propertyValues)
        {
            if (!propertyName.HasValue())
                throw new ArgumentNullException("propertyName");
            if (propertyValues == null || propertyValues.Length == 0)
                throw new ArgumentNullException("propertyValues");

            PropertyName = propertyName;
            PropertyValues = propertyValues;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var validationRule = new ModelClientValidationRule
            {
                ErrorMessage = string.Format(CultureInfo.CurrentCulture, ErrorMessageString, metadata.DisplayName),
                ValidationType = "requiredifnotequals",
            };
            validationRule.ValidationParameters.Add("propertyname", PropertyName);
            validationRule.ValidationParameters.Add("propertyvalues", string.Join(",", PropertyValues.Select(i => i.ToString())));

            yield return validationRule;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var objectType = validationContext.ObjectType;
            var objectInstance = validationContext.ObjectInstance;
            var propertyValue = objectType.GetProperty(PropertyName.Split('.').Last()).GetValue(objectInstance);

            return !PropertyValues.Contains(propertyValue) && value == null ? new ValidationResult(null) : null;
        }
    }
}
