﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;

namespace System.ComponentModel.DataAnnotations
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class CollectionLengthAttribute : ValidationAttribute, IClientValidatable
    {
        public int MaximumLength { get; }
        public int MinimumLength { get; }


        /// <summary>
        /// 验证集合长度
        /// </summary>
        /// <param name="maximumLength">最长长度</param>
        /// <param name="minimumLength">最短长度</param>
        public CollectionLengthAttribute(int maximumLength, int minimumLength = 0)
        {
            if (maximumLength < 0)
                throw new ArgumentException("maximumLength");
            if (minimumLength < 0)
                throw new ArgumentException("minimumLength");
            if (maximumLength < minimumLength)
                throw new ArgumentOutOfRangeException("minimumLength & maximumLength");

            MaximumLength = maximumLength;
            MinimumLength = minimumLength;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var validationRule = new ModelClientValidationRule
            {
                ErrorMessage = string.Format(CultureInfo.CurrentCulture, ErrorMessageString, metadata.DisplayName, MaximumLength, MinimumLength),
                ValidationType = "collectionlength",
            };
            validationRule.ValidationParameters.Add("maximumlength", MaximumLength);
            validationRule.ValidationParameters.Add("minimumlength", MinimumLength);

            yield return validationRule;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                var collectionValue = value as ICollection;
                var count = collectionValue == null ? 0 : collectionValue.Count;

                return MinimumLength <= count && count <= MaximumLength ? null : new ValidationResult(null);
            }
            // 允许为空
            else return null;
        }
    }
}
