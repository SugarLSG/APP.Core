﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace System.ComponentModel.DataAnnotations
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class IdentityCodeAttribute : ValidationAttribute, IClientValidatable
    {
        // 身份证正则表达式
        private const string RegexIdentityCode = @"^(\d{15}|\d{17}(\d|x|X))$";
        // 身份证省份代码
        private static readonly string[] IdentityCodeProvinceCodes = new string[]
        {
            "11", // 北京
            "12", // 天津
            "13", // 河北
            "14", // 山西
            "15", // 内蒙古
            "21", // 辽宁
            "22", // 吉林
            "23", // 黑龙江
            "31", // 上海
            "32", // 江苏
            "33", // 浙江
            "34", // 安徽
            "35", // 福建
            "36", // 江西
            "37", // 山东
            "41", // 河南
            "42", // 湖北
            "43", // 湖南
            "44", // 广东
            "45", // 广西
            "46", // 海南
            "50", // 重庆
            "51", // 四川
            "52", // 贵州
            "53", // 云南
            "54", // 西藏
            "61", // 陕西
            "62", // 甘肃
            "63", // 青海
            "64", // 宁夏
            "65", // 新疆
            "71", // 台湾
            "81", // 香港
            "82", // 澳门
            "91", // 国外
        };
        // 身份证加权因子
        private static readonly int[] IdentityCodeWeightings = new int[] { 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 };
        // 身份证验证码
        private static readonly string[] IdentityCodeChecksums = new string[] { "1", "0", "x", "9", "8", "7", "6", "5", "4", "3", "2" };


        /// <summary>
        /// 验证身份证号码
        /// </summary>
        public IdentityCodeAttribute() { }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var validationRule = new ModelClientValidationRule
            {
                ErrorMessage = string.Format(CultureInfo.CurrentCulture, ErrorMessageString, metadata.DisplayName),
                ValidationType = "identitycode",
            };

            yield return validationRule;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null && (value as string).HasValue())
            {
                var valueString = value.ToString();

                // 位数验证
                if (Regex.IsMatch(valueString, RegexIdentityCode))
                {
                    // 省份验证
                    var provinceCode = valueString.Substring(0, 2);
                    if (IdentityCodeProvinceCodes.Contains(provinceCode))
                    {
                        // 出生年月日验证
                        int year, month, day;
                        if (valueString.Length == 15)
                        {
                            // 15位
                            year = Convert.ToInt32("19" + valueString.Substring(6, 2));
                            month = Convert.ToInt32(valueString.Substring(8, 2));
                            day = Convert.ToInt32(valueString.Substring(10, 2));
                        }
                        else
                        {
                            // 18位
                            year = Convert.ToInt32(valueString.Substring(6, 4));
                            month = Convert.ToInt32(valueString.Substring(10, 2));
                            day = Convert.ToInt32(valueString.Substring(12, 2));
                        }
                        var date = new DateTime(year, month, day);
                        if (date.Year == year && date.Month == month && date.Day == day)
                        {
                            // 校验位验证
                            if (valueString.Length == 15)
                            {
                                // 15位
                                return null;
                            }
                            else
                            {
                                // 18位
                                int sum = 0;
                                for (var i = 0; i < 17; ++i)
                                    sum += Convert.ToInt32(valueString.Substring(i, 1)) * IdentityCodeWeightings[i];

                                var checksums = IdentityCodeChecksums[sum % 11];
                                if (checksums == valueString.Substring(17, 1).ToLower())
                                    return null;
                            }
                        }
                    }
                }
            }
            // 允许为空
            else return null;

            return new ValidationResult(null);
        }
    }
}
