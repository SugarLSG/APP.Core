﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace System.ComponentModel.DataAnnotations
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class RequiredIfEmptyAttribute : ValidationAttribute, IClientValidatable
    {
        public string PropertyName { get; }


        /// <summary>
        /// 指定属性等于空，验证非空
        /// </summary>
        /// <param name="propertyName">要验证相等的属性名称（支持层级，以英文.分隔）</param>
        public RequiredIfEmptyAttribute(string propertyName)
        {
            if (!propertyName.HasValue())
                throw new ArgumentNullException("propertyName");

            PropertyName = propertyName;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var validationRule = new ModelClientValidationRule
            {
                ErrorMessage = string.Format(CultureInfo.CurrentCulture, ErrorMessageString, metadata.DisplayName),
                ValidationType = "requiredifempty",
            };
            validationRule.ValidationParameters.Add("propertyname", PropertyName);

            yield return validationRule;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var objectType = validationContext.ObjectType;
            var objectInstance = validationContext.ObjectInstance;
            var propertyValue = objectType.GetProperty(PropertyName.Split('.').Last()).GetValue(objectInstance);
            if (propertyValue is string)
                propertyValue = (propertyValue as string).HasValue() ? propertyValue : null;

            return propertyValue == null && value == null ? new ValidationResult(null) : null;
        }
    }
}
