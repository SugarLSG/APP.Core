﻿using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;

namespace System.ComponentModel.DataAnnotations
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class EqualsAttribute : ValidationAttribute, IClientValidatable
    {
        public enum EqualsType
        {
            /// <summary>
            /// 与指定的值相比较
            /// </summary>
            Value,
            /// <summary>
            /// 与指定Property的值相比较
            /// </summary>
            Property
        }


        public EqualsType PEqualsType { get; }
        public object EqualsTarget { get; }


        /// <summary>
        /// 验证相等
        /// </summary>
        /// <param name="equalsType">验证类型：指定值 OR 指定属性</param>
        /// <param name="equalsTarget">equalsType 对应的值 OR 属性</param>
        public EqualsAttribute(EqualsType equalsType, object equalsTarget)
        {
            if (equalsTarget == null)
                throw new ArgumentNullException("equalsTarget");

            PEqualsType = equalsType;
            EqualsTarget = equalsTarget;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var validationRule = new ModelClientValidationRule
            {
                ErrorMessage = string.Format(CultureInfo.CurrentCulture, ErrorMessageString, metadata.DisplayName),
                ValidationType = "equals",
            };
            validationRule.ValidationParameters.Add("equalstype", PEqualsType.ToString().ToLower());
            validationRule.ValidationParameters.Add("equalstarget", EqualsTarget);

            yield return validationRule;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var targetValue = EqualsTarget;
            if (PEqualsType == EqualsType.Property)
            {
                var objectType = validationContext.ObjectType;
                var objectInstance = validationContext.ObjectInstance;
                targetValue = objectType.GetProperty(EqualsTarget.ToString()).GetValue(objectInstance);
            }

            return targetValue != null && targetValue.Equals(value) ? null : new ValidationResult(null);
        }
    }
}
