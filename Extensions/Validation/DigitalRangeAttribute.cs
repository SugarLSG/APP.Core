﻿using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;

namespace System.ComponentModel.DataAnnotations
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class DigitalRangeAttribute : ValidationAttribute, IClientValidatable
    {
        public double Minimum { get; }
        public double Maximum { get; }
        public string MinimumProperty { get; }
        public string MaximumProperty { get; }


        /// <summary>
        /// 验证大小
        /// </summary>
        /// <param name="minimum">固定最小值（可相等）</param>
        /// <param name="maximum">固定最大值（可相等）</param>
        public DigitalRangeAttribute(int minimum, int maximum)
        {
            if (minimum > maximum)
                throw new ArgumentOutOfRangeException("minimum & maximum");

            Minimum = minimum;
            Maximum = maximum;
        }

        /// <summary>
        /// 验证大小
        /// </summary>
        /// <param name="minimumProperty">指定Property值为最小值（可相等）</param>
        /// <param name="defaultMinimum">默认固定最小值（可相等）</param>
        /// <param name="maximum">固定最大值（可相等）</param>
        public DigitalRangeAttribute(string minimumProperty, int defaultMinimum, int maximum)
        {
            if (defaultMinimum > maximum)
                throw new ArgumentOutOfRangeException("defaultMinimum & maximum");
            if (!minimumProperty.HasValue())
                throw new ArgumentNullException("minimumProperty");

            Minimum = defaultMinimum;
            Maximum = maximum;
            MinimumProperty = minimumProperty;
        }

        /// <summary>
        /// 验证大小
        /// </summary>
        /// <param name="minimum">固定最小值（可相等）</param>
        /// <param name="maximumProperty">指定Property值为最大值（可相等）</param>
        /// <param name="defaultMaximum">默认固定最大值（可相等）</param>
        public DigitalRangeAttribute(int minimum, string maximumProperty, int defaultMaximum)
        {
            if (minimum > defaultMaximum)
                throw new ArgumentOutOfRangeException("minimum & defaultMaximum");
            if (!maximumProperty.HasValue())
                throw new ArgumentNullException("maximumProperty");

            Minimum = minimum;
            Maximum = defaultMaximum;
            MaximumProperty = maximumProperty;
        }

        /// <summary>
        /// 验证大小
        /// </summary>
        /// <param name="minimumProperty">指定Property值为最小值（可相等）</param>
        /// <param name="defaultMinimum">默认固定最小值（可相等）</param>
        /// <param name="maximumProperty">指定Property值为最大值（可相等）</param>
        /// <param name="defaultMaximum">默认固定最大值（可相等）</param>
        public DigitalRangeAttribute(string minimumProperty, int defaultMinimum, string maximumProperty, int defaultMaximum)
        {
            if (defaultMinimum > defaultMaximum)
                throw new ArgumentOutOfRangeException("defaultMinimum & defaultMaximum");
            if (!minimumProperty.HasValue())
                throw new ArgumentNullException("minimumProperty");
            if (!maximumProperty.HasValue())
                throw new ArgumentNullException("maximumProperty");

            Minimum = defaultMinimum;
            Maximum = defaultMaximum;
            MinimumProperty = minimumProperty;
            MaximumProperty = maximumProperty;
        }

        /// <summary>
        /// 验证大小
        /// </summary>
        /// <param name="minimum">固定最小值（可相等）</param>
        /// <param name="maximum">固定最大值（可相等）</param>
        public DigitalRangeAttribute(double minimum, double maximum)
        {
            if (minimum > maximum)
                throw new ArgumentOutOfRangeException("minimum & maximum");

            Minimum = minimum;
            Maximum = maximum;
        }

        /// <summary>
        /// 验证大小
        /// </summary>
        /// <param name="minimumProperty">指定Property值为最小值（可相等）</param>
        /// <param name="defaultMinimum">默认固定最小值（可相等）</param>
        /// <param name="maximum">固定最大值（可相等）</param>
        public DigitalRangeAttribute(string minimumProperty, double defaultMinimum, double maximum)
        {
            if (defaultMinimum > maximum)
                throw new ArgumentOutOfRangeException("defaultMinimum & maximum");
            if (!minimumProperty.HasValue())
                throw new ArgumentNullException("minimumProperty");

            Minimum = defaultMinimum;
            Maximum = maximum;
            MinimumProperty = minimumProperty;
        }

        /// <summary>
        /// 验证大小
        /// </summary>
        /// <param name="minimum">固定最小值（可相等）</param>
        /// <param name="maximumProperty">指定Property值为最大值（可相等）</param>
        /// <param name="defaultMaximum">默认固定最大值（可相等）</param>
        public DigitalRangeAttribute(double minimum, string maximumProperty, double defaultMaximum)
        {
            if (minimum > defaultMaximum)
                throw new ArgumentOutOfRangeException("minimum & defaultMaximum");
            if (!maximumProperty.HasValue())
                throw new ArgumentNullException("maximumProperty");

            Minimum = minimum;
            Maximum = defaultMaximum;
            MaximumProperty = maximumProperty;
        }

        /// <summary>
        /// 验证大小
        /// </summary>
        /// <param name="minimumProperty">指定Property值为最小值（可相等）</param>
        /// <param name="defaultMinimum">默认固定最小值（可相等）</param>
        /// <param name="maximumProperty">指定Property值为最大值（可相等）</param>
        /// <param name="defaultMaximum">默认固定最大值（可相等）</param>
        public DigitalRangeAttribute(string minimumProperty, double defaultMinimum, string maximumProperty, double defaultMaximum)
        {
            if (defaultMinimum > defaultMaximum)
                throw new ArgumentOutOfRangeException("defaultMinimum & defaultMaximum");
            if (!minimumProperty.HasValue())
                throw new ArgumentNullException("minimumProperty");
            if (!maximumProperty.HasValue())
                throw new ArgumentNullException("maximumProperty");

            Minimum = defaultMinimum;
            Maximum = defaultMaximum;
            MinimumProperty = minimumProperty;
            MaximumProperty = maximumProperty;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var validationRule = new ModelClientValidationRule
            {
                ErrorMessage = string.Format(CultureInfo.CurrentCulture, ErrorMessageString, metadata.DisplayName, Minimum, Maximum),
                ValidationType = "digitalrange",
            };
            validationRule.ValidationParameters.Add("minimum", Minimum);
            validationRule.ValidationParameters.Add("maximum", Maximum);
            validationRule.ValidationParameters.Add("minimumproperty", MinimumProperty);
            validationRule.ValidationParameters.Add("maximumproperty", MaximumProperty);

            yield return validationRule;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
                return null;

            var digitalValue = value is double ? (double)value : (value is float ? (float)value : (int)value);
            var minimum = Minimum;
            var maximum = Maximum;
            var objectType = validationContext.ObjectType;
            var objectInstance = validationContext.ObjectInstance;
            if (MinimumProperty.HasValue())
            {
                var min = objectType.GetProperty(MinimumProperty).GetValue(objectInstance);
                if (min != null && !double.TryParse(min.ToString(), out minimum)) minimum = Minimum;
            }
            if (MaximumProperty.HasValue())
            {
                var max = objectType.GetProperty(MaximumProperty).GetValue(objectInstance);
                if (max != null && !double.TryParse(max.ToString(), out maximum)) maximum = Maximum;
            }

            return minimum <= digitalValue && digitalValue <= maximum ? null : new ValidationResult(null);
        }
    }
}
