﻿using System.Collections.Generic;
using System.Text;

namespace APP.Core.Builders
{
    internal class SpringConfigTemplate : BaseTemplate
    {
        private IEnumerable<string> fileNames;

        public SpringConfigTemplate(IEnumerable<string> fileNames)
        {
            this.fileNames = fileNames;
            Encoding = Encoding.UTF8;

            TransformText();
        }

        public void TransformText()
        {
            var content = new StringBuilder();

            content.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            content.AppendLine();
            content.AppendLine("<!-- 本文件由模版生成，请勿修改！ -->");
            content.AppendLine();
            content.AppendLine("<objects xmlns=\"http://www.springframework.net\">");
            content.AppendLine();

            foreach (var fileName in fileNames)
            {
                content.AppendLine(string.Format(
                    "  <import resource=\"~/Configs/{0}\"/>",
                    fileName
                ));
            }

            content.AppendLine();
            content.AppendLine("</objects>");

            Content = content.ToString();
        }
    }
}
