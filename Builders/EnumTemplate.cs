﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace APP.Core.Builders
{
    internal class EnumTemplate : BaseTemplate
    {
        private IDictionary<string, IEnumerable<Tuple<int, string, string>>> enums;

        public EnumTemplate(IDictionary<string, IEnumerable<Tuple<int, string, string>>> enums)
        {
            this.enums = enums;
            Encoding = Encoding.UTF8;

            TransformText();
        }

        public void TransformText()
        {
            var content = new StringBuilder();

            content.AppendLine("// 本文件由模版生成，请勿修改！");
            content.AppendLine();

            foreach (var e in enums)
            {
                var valueStr = string.Join(", ", e.Value.Select(i => string.Format("{0}: {1}", i.Item2, i.Item1)));
                var nameStr = string.Join(", ", e.Value.Select(i => string.Format("{0}: '{1}'", i.Item2, i.Item2)));
                var messageStr = string.Join(", ", e.Value.Select(i => string.Format("{0}: '{1}'", i.Item2, i.Item3)));

                content.AppendLine(string.Format(
                    "var {0} = {{ {1}, name: {{ {2} }}, message: {{ {3} }} }};",
                    e.Key,
                    valueStr,
                    nameStr,
                    messageStr
                ));
            }

            Content = content.ToString();
        }
    }
}
