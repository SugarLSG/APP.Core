﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace APP.Core.Builders
{
    internal class TaskSpringTemplate : BaseTemplate
    {
        private IEnumerable<string> addedClassNames;
        private IEnumerable<KeyValuePair<string, Type>> types;

        public TaskSpringTemplate(IEnumerable<string> addedClassNames, IEnumerable<KeyValuePair<string, Type>> types)
        {
            this.addedClassNames = addedClassNames;
            this.types = types;
            Encoding = Encoding.UTF8;

            TransformText();
        }

        public void TransformText()
        {
            var content = new StringBuilder();

            content.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            content.AppendLine();
            content.AppendLine("<!-- 本文件由模版生成，请勿修改！ -->");
            content.AppendLine();
            content.AppendLine("<objects xmlns=\"http://www.springframework.net\">");
            content.AppendLine();

            foreach (var type in types)
            {
                content.AppendLine(string.Format(
                    "  <object name=\"{0}\" type=\"Spring.Scheduling.Quartz.JobDetailObject, Spring.Scheduling.Quartz2\">",
                    type.Key
                ));
                content.AppendLine(string.Format(
                    "    <property name=\"JobType\" value=\"{0}, {1}\"/>",
                    type.Value.FullName,
                    type.Value.Assembly.GetName().Name
                ));
                content.AppendLine("    <property name=\"JobDataAsMap\">");
                content.AppendLine("      <dictionary>");

                foreach (var property in type.Value.GetProperties(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic))
                {
                    var propertyName = property.Name.ToLower();
                    if (addedClassNames.Contains(propertyName))
                    {
                        content.AppendLine(string.Format(
                            "        <entry key=\"{0}\" value-ref=\"{1}\"/>",
                            property.Name,
                            propertyName
                        ));
                    }
                }

                content.AppendLine("      </dictionary>");
                content.AppendLine("    </property>");
                content.AppendLine("  </object>");
                content.AppendLine();
            }

            content.AppendLine("</objects>");

            Content = content.ToString();
        }
    }
}
