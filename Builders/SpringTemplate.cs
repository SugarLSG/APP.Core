﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace APP.Core.Builders
{
    internal class SpringTemplate : BaseTemplate
    {
        private IEnumerable<string> addedClassNames;
        private IEnumerable<KeyValuePair<string, Type>> types;

        public SpringTemplate(IEnumerable<string> addedClassNames, IEnumerable<KeyValuePair<string, Type>> types)
        {
            this.addedClassNames = addedClassNames;
            this.types = types;
            Encoding = Encoding.UTF8;

            TransformText();
        }

        public void TransformText()
        {
            var content = new StringBuilder();

            content.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            content.AppendLine();
            content.AppendLine("<!-- 本文件由模版生成，请勿修改！ -->");
            content.AppendLine();
            content.AppendLine("<objects xmlns=\"http://www.springframework.net\">");
            content.AppendLine();

            foreach (var type in types)
            {
                content.AppendLine(string.Format(
                    "  <object id=\"{0}\" type=\"{1}, {2}\"{3}>",
                    type.Key,
                    type.Value.FullName,
                    type.Value.Assembly.GetName().Name,
                    type.Key.EndsWith("controller") ? " singleton=\"false\"" : string.Empty
                ));

                foreach (var property in type.Value.GetProperties(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic))
                {
                    var propertyName = property.Name.ToLower();
                    if (addedClassNames.Contains(propertyName))
                    {
                        content.AppendLine(string.Format(
                            "    <property name=\"{0}\" ref=\"{1}\" />",
                            property.Name,
                            propertyName
                        ));
                    }
                }

                content.AppendLine("  </object>");
                content.AppendLine();
            }

            content.AppendLine("</objects>");

            Content = content.ToString();
        }
    }
}
