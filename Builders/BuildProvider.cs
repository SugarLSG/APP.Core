﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace APP.Core.Builders
{
    public static class BuildProvider
    {
        /// <summary>
        /// 生成 Spring 配置
        /// </summary>
        /// <param name="dllDirectory"></param>
        /// <param name="saveDirectory"></param>
        /// <param name="configs"></param>
        /// <returns></returns>
        public static void GenerateSpringConfig(string dllDirectory, string saveDirectory, IDictionary<string, IEnumerable<string>> configs)
        {
            // 已生成类名
            IEnumerable<string> addedClassNames = new List<string> { "baserepository" };
            var fileNames = new List<string> { "nhibernate.xml" };

            foreach (var config in configs)
            {
                var assemblyTypes = Assembly.LoadFrom(string.Format("{0}\\{1}", dllDirectory, config.Key))
                    .ExportedTypes
                    .Where(t =>
                    {
                        var isMatch = t.IsClass;
                        if (!isMatch) return false;

                        isMatch = false;
                        foreach (var v in config.Value)
                            if (t.Name.ToLower().EndsWith(v))
                            {
                                isMatch = true;
                                break;
                            }
                        return isMatch;
                    })
                    .Select(t =>
                    {
                        var typeFullName = t.FullName;
                        string typeId = null;
                        if (typeFullName.Contains(".Areas."))
                            typeId = typeFullName.Substring(typeFullName.IndexOf(".Areas.") + ".Areas.".Length).Replace(".Controllers.", string.Empty).Replace(".", string.Empty);
                        else if (typeFullName.Contains(".Controllers."))
                            typeId = typeFullName.Substring(typeFullName.IndexOf(".Controllers.") + ".Controllers.".Length).Replace(".", string.Empty);
                        else
                            typeId = t.Name;
                        return new KeyValuePair<string, Type>(typeId.ToLower(), t);
                    })
                    .OrderBy(t => t.Key);
                addedClassNames = addedClassNames.Concat(assemblyTypes.Select(t => t.Key));

                var dllName = config.Key.ToLower().Replace(".dll", string.Empty).Replace(".exe", string.Empty);
                var fileName = dllName + ".xml";
                if (dllName.Contains("task"))
                {
                    var template = new TaskSpringTemplate(addedClassNames, assemblyTypes);
                    template.RenderToFile(saveDirectory + fileName);
                }
                else
                {
                    var template = new SpringTemplate(addedClassNames, assemblyTypes);
                    template.RenderToFile(saveDirectory + fileName);
                }
                fileNames.Add(fileName);
            }

            var scTemplate = new SpringConfigTemplate(fileNames);
            scTemplate.RenderToFile(saveDirectory + "spring.xml");
        }

        /// <summary>
        /// 生成 Enum 配置
        /// </summary>
        /// <param name="dllDirectory"></param>
        /// <param name="savePath"></param>
        /// <param name="configs"></param>
        /// <param name="excludes"></param>
        public static void GenerateEnumConfig(string dllDirectory, string savePath, IEnumerable<string> configs, IEnumerable<string> excludes)
        {
            var enums = new Dictionary<string, IEnumerable<Tuple<int, string, string>>>();
            foreach (var config in configs)
            {
                Assembly.LoadFrom(string.Format("{0}\\{1}.dll", dllDirectory, config))
                    .ExportedTypes
                    .Where(t => t.IsEnum && t.IsPublic && (excludes == null || !excludes.Contains(t.Name)))
                    .OrderBy(e => e.Name)
                    .ToList()
                    .ForEach(e =>
                    {
                        var items = new List<Tuple<int, string, string>>();
                        foreach (var value in e.GetEnumValues())
                            items.Add(new Tuple<int, string, string>(Convert.ToInt32(value), e.GetEnumName(value), ((Enum)Enum.Parse(e, value.ToString())).ToMessage()));

                        enums.Add(e.Name, items.OrderBy(i => i.Item1));
                    });
            }

            var template = new EnumTemplate(enums);
            template.RenderToFile(savePath);
        }
    }
}
