﻿using APP.Core.Utilities;
using System;
using System.IO;
using System.Text;

namespace APP.Core.Builders
{
    internal abstract class BaseTemplate
    {
        public Encoding Encoding { get; set; }
        public string Content { get; protected set; }

        public void RenderToFile(string path)
        {
            try
            {
                if (File.Exists(path))
                    File.Delete(path);

                using (var sw = new StreamWriter(path, false, Encoding))
                {
                    sw.Write(Content);
                }
            }
            catch (Exception ex)
            {
                LogUtility.LogError(ex);
            }
        }
    }
}
