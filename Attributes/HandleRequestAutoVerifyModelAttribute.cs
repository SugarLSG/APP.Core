﻿using APP.Core.Utilities;
using System;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace APP.Core.Attributes
{
    public class HandleRequestAutoVerifyModelAttribute : ActionFilterAttribute
    {
        public enum ResponseContentMode
        {
            /// <summary>
            /// 封装成数组 Json
            /// </summary>
            Array,
            /// <summary>
            /// 封装成 div 标签
            /// </summary>
            Div,
            /// <summary>
            /// 封装成 p 标签
            /// </summary>
            P,
            /// <summary>
            /// 封装成 ol+li 标签
            /// </summary>
            Ol,
            /// <summary>
            /// 封装成 ul+li 标签
            /// </summary>
            Ul
        }

        public bool IsAutoVerify { get; }
        public string ErrorCode { get; }
        public ResponseContentMode PResponseContentMode { get; }


        /// <summary>
        /// （请求）自动验证 Model（Order: 21）
        /// </summary>
        /// <param name="isAutoVerify">是否自动验证</param>
        /// <param name="errorCode">错误 Code</param>
        /// <param name="responseContentMode">错误信息封装模式</param>
        public HandleRequestAutoVerifyModelAttribute(bool isAutoVerify = true, string errorCode = "0005", ResponseContentMode responseContentMode = ResponseContentMode.P)
        {
            if (!errorCode.HasValue())
                throw new ArgumentNullException("errorCode");

            Order = 21;
            IsAutoVerify = isAutoVerify;
            ErrorCode = errorCode;
            PResponseContentMode = responseContentMode;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // 是否 Post 提交
            // 是否开启自动验证
            if (!ClientRequestUtility.IsPostRequest || !IsAutoVerify)
                return;

            // 是否验证通过
            var modelState = filterContext.Controller.ViewData.ModelState;
            if (modelState.IsValid)
                return;

            // 获取验证失败信息
            var invalidMessages = modelState.Values
                .Where(ms => ms.Errors.Any())
                .SelectMany(ms => ms.Errors.Select(e => e.ErrorMessage));
            string invalidMessage = null;
            switch (PResponseContentMode)
            {
                case ResponseContentMode.Array: invalidMessage = JsonUtility.Serialize(invalidMessages); break;
                case ResponseContentMode.Div: invalidMessage = string.Format("<div>{0}</div>", string.Join("</div><div>", invalidMessages)); break;
                case ResponseContentMode.P: invalidMessage = string.Format("<p>{0}</p>", string.Join("</p><p>", invalidMessages)); break;
                case ResponseContentMode.Ol: invalidMessage = string.Format("<ol><li>{0}</li></ol>", string.Join("</li><li>", invalidMessages)); break;
                case ResponseContentMode.Ul: invalidMessage = string.Format("<ul><li>{0}</li></ul>", string.Join("</li><li>", invalidMessages)); break;
            }

            // 封装 Response
            filterContext.Result = new ContentResult
            {
                Content = JsonUtility.SerializeAjaxFailureJSON(invalidMessage, ErrorCode, invalidMessages),
                ContentEncoding = Encoding.UTF8,
                ContentType = CoreConfig.CONTENTTYPE_APPLICATION_JSON
            };
        }
    }
}
