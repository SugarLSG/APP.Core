﻿using System;

namespace APP.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class IgnoreConvertTypeScalarAttribute : Attribute
    {
        /// <summary>
        /// For BaseRepository
        /// </summary>
        public IgnoreConvertTypeScalarAttribute() { }
    }
}
