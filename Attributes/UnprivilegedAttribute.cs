﻿using System;

namespace APP.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class UnprivilegedAttribute : Attribute
    {
        /// <summary>
        /// 标识不生成权限
        /// （AllowAnonymousAttribute 不需要登录，UnprivilegedAttribute 需要登录但不需要权限）
        /// </summary>
        public UnprivilegedAttribute() { }
    }
}
