﻿using APP.Core.Utilities;
using System.Web.Mvc;

namespace APP.Core.Attributes
{
    public class HandleResponseContentTypeAttribute : ActionFilterAttribute
    {
        public enum ResponseContentType
        {
            /// <summary>
            /// Json - application/json
            /// </summary>
            Json,
            /// <summary>
            /// Xml - application/xml
            /// </summary>
            Xml,
            /// <summary>
            /// FormUrlEncoded - application/x-www-form-urlencoded
            /// </summary>
            FormUrlEncoded
        }


        public bool IsOnlyPost { get; }
        public bool IsOnlyAjax { get; }
        public ResponseContentType ContentType { get; }


        /// <summary>
        /// （响应）修改内容类型（Order: 21）
        /// </summary>
        /// <param name="isOnlyPost">是否只处理 Post 请求</param>
        /// <param name="isOnlyAjax">是否只处理 Ajax 请求</param>
        /// <param name="contentType">指定内容类型</param>
        public HandleResponseContentTypeAttribute(bool isOnlyPost = true, bool isOnlyAjax = true, ResponseContentType contentType = ResponseContentType.Json)
        {
            Order = 21;
            IsOnlyPost = isOnlyPost;
            IsOnlyAjax = isOnlyAjax;
            ContentType = contentType;
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            // 是否只处理 Post 请求
            if (IsOnlyPost && !ClientRequestUtility.IsPostRequest)
                return;
            // 是否只处理 Ajax 请求
            if (IsOnlyAjax && !ClientRequestUtility.IsAjaxRequest)
                return;

            // 修改 Response ContentType 为对应类型
            var response = filterContext.HttpContext.Response;
            if (response != null)
            {
                switch (ContentType)
                {
                    case ResponseContentType.Json: response.ContentType = CoreConfig.CONTENTTYPE_APPLICATION_JSON; break;
                    case ResponseContentType.Xml: response.ContentType = CoreConfig.CONTENTTYPE_APPLICATION_XML; break;
                    case ResponseContentType.FormUrlEncoded: response.ContentType = CoreConfig.CONTENTTYPE_APPLICATION_FORMURLENCODED; break;
                }
            }
        }
    }
}
