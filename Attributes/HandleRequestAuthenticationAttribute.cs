﻿using APP.Core.Utilities;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace APP.Core.Attributes
{
    public abstract class HandleRequestAuthenticationAttribute : AuthorizeAttribute
    {
        public enum FilterMode
        {
            /// <summary>
            /// 未设置（全部允许）
            /// </summary>
            Unset,
            /// <summary>
            /// 允许（仅允许设置的筛选规则）
            /// </summary>
            Accepted,
            /// <summary>
            /// 拦截（仅拦截设置的筛选规则）
            /// </summary>
            Unaccepted
        }


        public FilterMode Mode { get; }
        public IEnumerable<string> Patterns { get; }
        public HttpRequest Request { get { return HttpContext.Current.Request; } }


        /// <summary>
        /// （请求）功能权限验证（Order: 10）
        /// </summary>
        /// <param name="filterMode">筛选模式</param>
        /// <param name="patterns">筛选规则</param>
        public HandleRequestAuthenticationAttribute(FilterMode filterMode = FilterMode.Unset, IEnumerable<string> patterns = null)
        {
            Order = 10;
            Mode = filterMode;
            Patterns = patterns == null ? new string[0] : patterns.Select(p => ((p.StartsWith("/") ? "" : "/") + p).ToLower());
        }

        /// <summary>
        /// 判断是否有功能权限
        /// </summary>
        /// <returns></returns>
        protected abstract bool IsAuthenticated(HttpContextBase httpContext);

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            // 获取当前请求路径
            var requestPath = ClientRequestUtility.RequestRealPath;

            // 路径筛选
            if (Mode == FilterMode.Unset ||                                                         // 不设置，则全部通过
                (Mode == FilterMode.Accepted && Patterns.Any(p => requestPath.StartsWith(p))) ||    // 仅接受配置的规则
                (Mode == FilterMode.Unaccepted && !Patterns.Any(p => requestPath.StartsWith(p))))   // 仅拒绝配置的规则
            {
                return IsAuthenticated(httpContext);
            }

            return true;
        }
    }
}
