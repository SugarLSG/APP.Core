﻿using APP.Core.Utilities;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace APP.Core.Attributes
{
    public class HandleResponseAuthExpiredAttribute : ActionFilterAttribute
    {
        private const string DEFAULT_KEY = "AUTH.EXPIRED";


        public string CookieKey { get; }


        /// <summary>
        /// （响应）记录用户凭证过期时间（Order: 10）
        /// </summary>
        /// <param name="cookieKey"></param>
        public HandleResponseAuthExpiredAttribute(string cookieKey = DEFAULT_KEY)
        {
            if (!cookieKey.HasValue())
                throw new ArgumentNullException("key");

            Order = 10;
            CookieKey = cookieKey;
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            var httpContext = filterContext.RequestContext.HttpContext;
            var request = httpContext.Request;
            var response = httpContext.Response;

            try
            {
                var cookie = request.Cookies[AuthenticationUtility.CookieKey];
                var ticket = FormsAuthentication.Decrypt(cookie.Value);

                // 记录过期时间
                request.Cookies.Remove(CookieKey);
                var expiredCookie = new HttpCookie(CookieKey, SecurityUtility.BASE64Encrypt(ticket.Expiration.ToString("yyyyMMddHHmm")));
                response.Cookies.Set(expiredCookie);

                return;
            }
            catch { }
        }
    }
}
