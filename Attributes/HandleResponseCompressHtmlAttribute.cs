﻿using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace APP.Core.Attributes
{
    public class HandleResponseCompressHtmlAttribute : ActionFilterAttribute
    {
        private static readonly Regex _regexHtml = new Regex(@"(?<=\s)\s+(?![^<>]*</pre>)");
        private static readonly Encoding _defaultEncoding = Encoding.UTF8;


        /// <summary>
        /// （响应）压缩Html（Order: 20）
        /// </summary>
        public HandleResponseCompressHtmlAttribute()
        {
            Order = 20;
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            var response = filterContext.HttpContext.Response;
            if (response != null && response.ContentType == CoreConfig.CONTENTTYPE_TEXT_HTML && response.Filter != null)
                response.Filter = new CompressionStream(response.Filter);
        }

        /// <summary>
        /// 压缩数据
        /// </summary>
        private class CompressionStream : Stream
        {
            private readonly Stream _base = null;

            public override bool CanRead { get { return _base.CanRead; } }
            public override bool CanSeek { get { return _base.CanSeek; } }
            public override bool CanWrite { get { return _base.CanWrite; } }
            public override long Length { get { return _base.Length; } }
            public override long Position
            {
                get { return _base.Position; }
                set { _base.Position = value; }
            }

            public CompressionStream(Stream stream)
            {
                _base = stream;
            }

            public override void Write(byte[] buffer, int offset, int count)
            {
                var html = _regexHtml.Replace(_defaultEncoding.GetString(buffer, offset, count), string.Empty);
                var temp = _defaultEncoding.GetBytes(html);
                _base.Write(temp, 0, temp.Length);
            }
            public override void Flush()
            {
                _base.Flush();
            }
            public override long Seek(long offset, SeekOrigin origin)
            {
                return _base.Seek(offset, origin);
            }
            public override void SetLength(long value)
            {
                _base.SetLength(value);
            }
            public override int Read(byte[] buffer, int offset, int count)
            {
                return _base.Read(buffer, offset, count);
            }
        }
    }
}
