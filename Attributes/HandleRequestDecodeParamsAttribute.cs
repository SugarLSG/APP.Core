﻿using APP.Core.Utilities;
using System;
using System.Linq;
using System.Web.Mvc;

namespace APP.Core.Attributes
{
    public class HandleRequestDecodeParamsAttribute : ActionFilterAttribute
    {
        private static Type ByteType = typeof(byte);
        private static Type ByteNullableType = typeof(byte?);
        private static Type BoolType = typeof(bool);
        private static Type BoolNullableType = typeof(bool?);
        private static Type ShortType = typeof(short);
        private static Type ShortNullableType = typeof(short?);
        private static Type UshortType = typeof(ushort);
        private static Type UshortNullableType = typeof(ushort?);
        private static Type IntType = typeof(int);
        private static Type IntNullableType = typeof(int?);
        private static Type UintType = typeof(uint);
        private static Type UintNullableType = typeof(uint?);
        private static Type LongType = typeof(long);
        private static Type LongNullableType = typeof(long?);
        private static Type UlongType = typeof(ulong);
        private static Type UlongNullableType = typeof(ulong?);
        private static Type FloatType = typeof(float);
        private static Type FloatNullableType = typeof(float?);
        private static Type DoubleType = typeof(double);
        private static Type DoubleNullableType = typeof(double?);
        private static Type DecimalType = typeof(decimal);
        private static Type DecimalNullableType = typeof(decimal?);


        public string ParameterName { get; }
        public Exception Exception { get; }
        public bool IsOnlyGet { get; }


        /// <summary>
        /// （请求）解码加密参数（Order: 20）
        /// </summary>
        /// <param name="exception">参数错误异常</param>
        /// <param name="isOnlyGet">是否只处理 Get 请求</param>
        public HandleRequestDecodeParamsAttribute(Exception exception, bool isOnlyGet = true)
        {
            if (exception == null)
                throw new ArgumentNullException("exception");

            Order = 20;
            ParameterName = "param";
            Exception = exception;
            IsOnlyGet = isOnlyGet;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // 是否只处理 Get 请求
            if (IsOnlyGet && !ClientRequestUtility.IsGetRequest)
                return;

            var param = filterContext.RequestContext.RouteData.Values[ParameterName];
            if (param != null && param.ToString().HasValue())
            {
                try
                {
                    var ps = SecurityUtility.ParameterDecrypt(param.ToString());
                    var parameters = filterContext.ActionDescriptor.GetParameters();

                    for (int i = 0, length = Math.Min(ps.Length, parameters.Length); i < length; ++i)
                    {
                        var p = ps[i];
                        if (p != null)
                        {
                            var parameter = parameters[i];
                            var parameterType = parameter.ParameterType;

                            if (parameterType == ByteType || parameterType == ByteNullableType)
                                filterContext.ActionParameters[parameter.ParameterName] = byte.Parse(p.ToString());
                            else if (parameterType == BoolType || parameterType == BoolNullableType)
                                filterContext.ActionParameters[parameter.ParameterName] = bool.Parse(p.ToString());
                            else if (parameterType == ShortType || parameterType == ShortNullableType)
                                filterContext.ActionParameters[parameter.ParameterName] = short.Parse(p.ToString());
                            else if (parameterType == UshortType || parameterType == UshortNullableType)
                                filterContext.ActionParameters[parameter.ParameterName] = ushort.Parse(p.ToString());
                            else if (parameterType == IntType || parameterType == IntNullableType)
                                filterContext.ActionParameters[parameter.ParameterName] = int.Parse(p.ToString());
                            else if (parameterType == UintType || parameterType == UintNullableType)
                                filterContext.ActionParameters[parameter.ParameterName] = uint.Parse(p.ToString());
                            else if (parameterType == LongType || parameterType == LongNullableType)
                                filterContext.ActionParameters[parameter.ParameterName] = long.Parse(p.ToString());
                            else if (parameterType == UlongType || parameterType == UlongNullableType)
                                filterContext.ActionParameters[parameter.ParameterName] = ulong.Parse(p.ToString());
                            else if (parameterType == FloatType || parameterType == FloatNullableType)
                                filterContext.ActionParameters[parameter.ParameterName] = float.Parse(p.ToString());
                            else if (parameterType == DoubleType || parameterType == DoubleNullableType)
                                filterContext.ActionParameters[parameter.ParameterName] = double.Parse(p.ToString());
                            else if (parameterType == DecimalType || parameterType == DecimalNullableType)
                                filterContext.ActionParameters[parameter.ParameterName] = decimal.Parse(p.ToString());
                            else if (parameterType.IsEnum || (parameterType.GenericTypeArguments != null && parameterType.GenericTypeArguments.Any() && parameterType.GenericTypeArguments[0].IsEnum))
                                filterContext.ActionParameters[parameter.ParameterName] = Enum.Parse(parameterType.IsEnum ? parameterType : parameterType.GenericTypeArguments[0], p.ToString());
                            else
                                filterContext.ActionParameters[parameter.ParameterName] = p;
                        }
                    }
                }
                catch
                {
                    LogUtility.LogError("error parameter: " + param);
                    throw Exception;
                }
            }
        }
    }
}
