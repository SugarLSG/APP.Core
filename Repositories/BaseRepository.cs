﻿using APP.Core.AOP.Attributes;
using APP.Core.Attributes;
using APP.Core.Caches;
using APP.Core.Enums;
using APP.Core.Models;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using NHibernate.Type;
using Spring.Data.NHibernate.Generic;
using Spring.Data.NHibernate.Generic.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace APP.Core.Repositories
{
    public class BaseRepository : HibernateDaoSupport, IBaseRepository
    {
        /// <summary>
        /// 缓存管理
        /// </summary>
        private ICacheProvider CacheProvider { get { return CacheManager.MemoryCacheProvider; } }

        /// <summary>
        /// 转换类型标量
        /// </summary>
        /// <param name="type">待转换类型</param>
        /// <returns>类型标量</returns>
        private IDictionary<string, IType> ConvertTypeScalar(Type type)
        {
            // 类型标量
            IDictionary<string, IType> scalars = null;
            // 是否已缓存结果
            var cacheKey = string.Format("{0}{1}", CoreConfig.CACHEKEY_TYPESCALARS_PREFIX, type.FullName);
            if (CacheProvider.Contains(cacheKey))
                scalars = CacheProvider.Get<IDictionary<string, IType>>(cacheKey);
            else
            {
                scalars = new Dictionary<string, IType>();
                if (!type.IsValueType && type != CoreConfig.STRING_TYPE)
                {
                    foreach (var property in type.GetProperties())
                    {
                        if (property.GetCustomAttribute<IgnoreConvertTypeScalarAttribute>() == null)
                            scalars.Add(property.Name, NHibernateUtil.GuessType(property.PropertyType));
                    }
                }

                // 缓存结果
                CacheProvider.Set(cacheKey, scalars, CacheExpirationType.None);
            }

            return scalars;
        }

        public void CommitTransaction()
        {
            HibernateTemplate.Flush();
        }

        [Notify(typeof(AddOrUpdateEntityPrepNotify))]
        public object AddEntity(object entity)
        {
            return HibernateTemplate.Save(entity);
        }
        [Notify(typeof(AddOrUpdateEntityPrepNotify))]
        public void AddOrUpdateEntity(object entity)
        {
            HibernateTemplate.SaveOrUpdate(entity);
        }

        public int ExecuteUpdate(string sql)
        {
            return ExecuteUpdate(sql, null);
        }
        public int ExecuteUpdate(string sql, IDictionary<string, object> parameters)
        {
            return HibernateTemplate.Execute(session =>
            {
                // 创建 SQL Query
                var query = session.CreateSQLQuery(sql);

                // 设置参数
                if (parameters != null && parameters.Count != 0)
                    foreach (var parameter in parameters)
                    {
                        if (sql.Contains(":" + parameter.Key))
                            query.SetParameter(parameter.Key, parameter.Value);
                    }

                HibernateTemplate.PrepareQuery(query);

                return query.ExecuteUpdate();
            });
        }
        [Notify(typeof(AddOrUpdateEntityPrepNotify))]
        public void UpdateEntity(object entity)
        {
            HibernateTemplate.Update(entity);
        }

        public long ExecuteCount(string sql)
        {
            return ExecuteCount(sql, null);
        }
        public long ExecuteCount(string sql, IDictionary<string, object> parameters)
        {
            HibernateTemplate.CacheQueries = false;
            return HibernateTemplate.Execute(session =>
            {
                // 创建 SQL Query
                var query = session.CreateSQLQuery(sql);

                // 设置参数
                if (parameters != null && parameters.Count != 0)
                    foreach (var parameter in parameters)
                    {
                        if (sql.Contains(":" + parameter.Key))
                            query.SetParameter(parameter.Key, parameter.Value);
                    }

                HibernateTemplate.PrepareQuery(query);

                return query.UniqueResult<long>();
            });
        }
        public int GetEntityCount<T>(IEnumerable<ICriterion> criterions)
        {
            HibernateTemplate.CacheQueries = false;
            return HibernateTemplate.Execute(session =>
            {
                // 创建 Entity Criteria
                var criteria = session.CreateCriteria(typeof(T));

                // 设置条件
                if (criterions != null && criterions.Count() != 0)
                    foreach (var criterion in criterions)
                        criteria.Add(criterion);

                // 设置查询结果（Count）
                criteria.SetProjection(Projections.RowCount());

                HibernateTemplate.PrepareCriteria(criteria);

                return criteria.UniqueResult<int>();
            });
        }

        public void DeleteEntity(object entity)
        {
            HibernateTemplate.Delete(entity);
        }
        public void DeleteEntityById<T>(object id)
        {
            var entity = GetEntityById<T>(id);

            if (entity != null)
                DeleteEntity(entity);
        }

        public T Execute<T>(string sql)
        {
            return ExecuteList<T>(sql, 0, 1).FirstOrDefault();
        }
        public T Execute<T>(string sql, IDictionary<string, IType> scalars)
        {
            return ExecuteList<T>(sql, scalars, 0, 1).FirstOrDefault();
        }
        public T Execute<T>(string sql, IDictionary<string, object> parameters)
        {
            return ExecuteList<T>(sql, parameters, 0, 1).FirstOrDefault();
        }
        public T Execute<T>(string sql, IDictionary<string, IType> scalars, IDictionary<string, object> parameters)
        {
            return ExecuteList<T>(sql, scalars, parameters, 0, 1).FirstOrDefault();
        }
        public object Execute(string sql)
        {
            return ExecuteList(sql, 0, 1).FirstOrDefault();
        }
        public object Execute(string sql, IDictionary<string, object> parameters)
        {
            return ExecuteList(sql, parameters, 0, 1).FirstOrDefault();
        }
        public T GetEntityById<T>(object id, bool getCache = true)
        {
            HibernateTemplate.CacheQueries = getCache;
            return HibernateTemplate.Get<T>(id);
        }
        public T GetOrCreateEntityById<T>(object id, bool getCache = true)
        {
            T entity = default(T);

            if (id != null)
                entity = GetEntityById<T>(id, getCache);
            if (entity == null)
                entity = Activator.CreateInstance<T>();

            return entity;
        }
        public T GetEntity<T>()
        {
            return GetEntityList<T>(0, 1).FirstOrDefault();
        }
        public T GetEntity<T>(IEnumerable<ICriterion> criterions)
        {
            return GetEntityList<T>(criterions, 0, 1).FirstOrDefault();
        }
        public T GetEntity<T>(IEnumerable<Order> orders)
        {
            return GetEntityList<T>(orders, 0, 1).FirstOrDefault();
        }
        public T GetEntity<T>(IEnumerable<ICriterion> criterions, IEnumerable<Order> orders)
        {
            return GetEntityList<T>(criterions, orders, 0, 1).FirstOrDefault();
        }

        public IList<T> ExecuteList<T>(string sql, int firstIndex = 0, int maxCount = int.MaxValue)
        {
            // 查询结果标量
            var scalars = ConvertTypeScalar(typeof(T));
            return ExecuteList<T>(sql, scalars, null, firstIndex, maxCount);
        }
        public IList<T> ExecuteList<T>(string sql, IDictionary<string, IType> scalars, int firstIndex = 0, int maxCount = int.MaxValue)
        {
            return ExecuteList<T>(sql, scalars, null, firstIndex, maxCount);
        }
        public IList<T> ExecuteList<T>(string sql, IDictionary<string, object> parameters, int firstIndex = 0, int maxCount = int.MaxValue)
        {
            // 查询结果标量
            var scalars = ConvertTypeScalar(typeof(T));
            return ExecuteList<T>(sql, scalars, parameters, firstIndex, maxCount);
        }
        public IList<T> ExecuteList<T>(string sql, IDictionary<string, IType> scalars, IDictionary<string, object> parameters, int firstIndex = 0, int maxCount = int.MaxValue)
        {
            HibernateTemplate.CacheQueries = false;
            return HibernateTemplate.ExecuteFind(session =>
            {
                // 创建 SQL Query
                var query = session.CreateSQLQuery(sql);

                // 设置结果标量
                if (scalars != null && scalars.Count != 0)
                    foreach (var scalar in scalars)
                        query.AddScalar(scalar.Key, scalar.Value);

                // 设置参数
                if (parameters != null && parameters.Count != 0)
                    foreach (var parameter in parameters)
                    {
                        if (sql.Contains(":" + parameter.Key))
                            query.SetParameter(parameter.Key, parameter.Value);
                    }

                // 设置查询结果数
                query.SetFirstResult(firstIndex);
                query.SetMaxResults(maxCount);

                if (scalars != null && scalars.Count != 0)
                    query.SetResultTransformer(new AliasToBeanResultTransformer(typeof(T)));
                HibernateTemplate.PrepareQuery(query);

                return query.List<T>();
            });
        }
        public IList<object> ExecuteList(string sql, int firstIndex = 0, int maxCount = int.MaxValue)
        {
            return ExecuteList(sql, null, firstIndex, maxCount);
        }
        public IList<object> ExecuteList(string sql, IDictionary<string, object> parameters, int firstIndex = 0, int maxCount = int.MaxValue)
        {
            HibernateTemplate.CacheQueries = false;
            return HibernateTemplate.ExecuteFind(session =>
            {
                // 创建 SQL Query
                var query = session.CreateSQLQuery(sql);

                // 设置参数
                if (parameters != null && parameters.Count != 0)
                    foreach (var parameter in parameters)
                    {
                        if (sql.Contains(":" + parameter.Key))
                            query.SetParameter(parameter.Key, parameter.Value);
                    }

                // 设置查询结果数
                query.SetFirstResult(firstIndex);
                query.SetMaxResults(maxCount);

                HibernateTemplate.PrepareQuery(query);

                return query.List<object>();
            });
        }
        public IList<T> GetEntityList<T>(int firstIndex = 0, int maxCount = int.MaxValue)
        {
            return GetEntityList<T>(null, null, firstIndex, maxCount);
        }
        public IList<T> GetEntityList<T>(IEnumerable<ICriterion> criterions, int firstIndex = 0, int maxCount = int.MaxValue)
        {
            return GetEntityList<T>(criterions, null, firstIndex, maxCount);
        }
        public IList<T> GetEntityList<T>(IEnumerable<Order> orders, int firstIndex = 0, int maxCount = int.MaxValue)
        {
            return GetEntityList<T>(null, orders, firstIndex, maxCount);
        }
        public IList<T> GetEntityList<T>(IEnumerable<ICriterion> criterions, IEnumerable<Order> orders, int firstIndex = 0, int maxCount = int.MaxValue)
        {
            HibernateTemplate.CacheQueries = false;
            return HibernateTemplate.ExecuteFind(session =>
            {
                // 创建 Entity Criteria
                var criteria = session.CreateCriteria(typeof(T));

                // 设置条件
                if (criterions != null && criterions.Count() != 0)
                    foreach (var criterion in criterions)
                        criteria.Add(criterion);

                // 设置排序
                if (orders != null && orders.Count() != 0)
                    foreach (var order in orders)
                        criteria.AddOrder(order);

                // 设置查询结果数
                criteria.SetFirstResult(firstIndex);
                criteria.SetMaxResults(maxCount);

                HibernateTemplate.PrepareCriteria(criteria);

                return criteria.List<T>();
            });
        }

        public PagingResult<T> ExecutePagingList<T>(string sql, int firstIndex = 0, int maxCount = int.MaxValue)
        {
            // 查询结果标量
            var scalars = ConvertTypeScalar(typeof(T));
            return ExecutePagingList<T>(sql, null, scalars, null, firstIndex, maxCount);
        }
        public PagingResult<T> ExecutePagingList<T>(string sql, IDictionary<string, Sorted> orders, int firstIndex = 0, int maxCount = int.MaxValue)
        {
            // 查询结果标量
            var scalars = ConvertTypeScalar(typeof(T));
            return ExecutePagingList<T>(sql, orders, scalars, null, firstIndex, maxCount);
        }
        public PagingResult<T> ExecutePagingList<T>(string sql, IDictionary<string, IType> scalars, int firstIndex = 0, int maxCount = int.MaxValue)
        {
            return ExecutePagingList<T>(sql, null, scalars, null, firstIndex, maxCount);
        }
        public PagingResult<T> ExecutePagingList<T>(string sql, IDictionary<string, object> parameters, int firstIndex = 0, int maxCount = int.MaxValue)
        {
            // 查询结果标量
            var scalars = ConvertTypeScalar(typeof(T));
            return ExecutePagingList<T>(sql, null, scalars, parameters, firstIndex, maxCount);
        }
        public PagingResult<T> ExecutePagingList<T>(string sql, IDictionary<string, Sorted> orders, IDictionary<string, IType> scalars, int firstIndex = 0, int maxCount = int.MaxValue)
        {
            return ExecutePagingList<T>(sql, orders, scalars, null, firstIndex, maxCount);
        }
        public PagingResult<T> ExecutePagingList<T>(string sql, IDictionary<string, Sorted> orders, IDictionary<string, object> parameters, int firstIndex = 0, int maxCount = int.MaxValue)
        {
            // 查询结果标量
            var scalars = ConvertTypeScalar(typeof(T));
            return ExecutePagingList<T>(sql, orders, scalars, parameters, firstIndex, maxCount);
        }
        public PagingResult<T> ExecutePagingList<T>(string sql, IDictionary<string, IType> scalars, IDictionary<string, object> parameters, int firstIndex = 0, int maxCount = int.MaxValue)
        {
            return ExecutePagingList<T>(sql, null, scalars, parameters, firstIndex, maxCount);
        }
        public PagingResult<T> ExecutePagingList<T>(string sql, IDictionary<string, Sorted> orders, IDictionary<string, IType> scalars, IDictionary<string, object> parameters, int firstIndex = 0, int maxCount = int.MaxValue)
        {
            HibernateTemplate.CacheQueries = false;
            return HibernateTemplate.Execute(session =>
            {
                // 创建数量 SQL Query
                var countSql = string.Format("SELECT COUNT(1) AS c FROM ({0}) pagingcount", sql);
                int lastIndex = -1, subqueryCount = 0;
                while (true)
                {
                    lastIndex = sql.IndexOf("from", lastIndex + 1, StringComparison.InvariantCultureIgnoreCase);
                    if (lastIndex < 0) break;

                    var temp = sql.Substring(0, lastIndex);
                    if (temp.Count("select", true) - 1 == subqueryCount)
                    {
                        // 判断 SELECT，识别子查询
                        countSql = "SELECT COUNT(1) AS c " + sql.Substring(lastIndex);
                        break;
                    }
                    ++subqueryCount;
                }
                var countQuery = session.CreateSQLQuery(countSql);

                // 补充排序 SQL Query
                if (orders != null && orders.Count != 0)
                {
                    sql = string.Format(
                        "{0} ORDER BY {1}",
                        sql,
                        string.Join(", ", orders.Select(o => string.Format("{0} {1}", o.Key, o.Value.ToMessage())))
                    );
                }
                var contentQuery = session.CreateSQLQuery(sql);

                // 设置结果标量
                if (scalars != null && scalars.Count != 0)
                    foreach (var scalar in scalars)
                        contentQuery.AddScalar(scalar.Key, scalar.Value);

                // 设置参数
                if (parameters != null && parameters.Count != 0)
                    foreach (var parameter in parameters)
                    {
                        var param = ":" + parameter.Key;
                        if (countSql.Contains(param))
                            countQuery.SetParameter(parameter.Key, parameter.Value);
                        if (sql.Contains(param))
                            contentQuery.SetParameter(parameter.Key, parameter.Value);
                    }

                // 设置查询结果数
                contentQuery.SetFirstResult(firstIndex);
                contentQuery.SetMaxResults(maxCount);

                HibernateTemplate.PrepareQuery(countQuery);
                if (scalars != null && scalars.Count != 0)
                    contentQuery.SetResultTransformer(new AliasToBeanResultTransformer(typeof(T)));
                HibernateTemplate.PrepareQuery(contentQuery);

                var count = countQuery.UniqueResult<long>();
                var data = contentQuery.List<T>();
                return new PagingResult<T>
                {
                    Data = data,
                    Size = maxCount,
                    TotalCount = (int)count
                };
            });
        }
        public PagingResult<T> GetEntityPagingList<T>(int firstIndex = 0, int maxCount = int.MaxValue)
        {
            return GetEntityPagingList<T>(null, null, firstIndex, maxCount);
        }
        public PagingResult<T> GetEntityPagingList<T>(IEnumerable<ICriterion> criterions, int firstIndex = 0, int maxCount = int.MaxValue)
        {
            return GetEntityPagingList<T>(criterions, null, firstIndex, maxCount);
        }
        public PagingResult<T> GetEntityPagingList<T>(IEnumerable<Order> orders, int firstIndex = 0, int maxCount = int.MaxValue)
        {
            return GetEntityPagingList<T>(null, orders, firstIndex, maxCount);
        }
        public PagingResult<T> GetEntityPagingList<T>(IEnumerable<ICriterion> criterions, IEnumerable<Order> orders, int firstIndex = 0, int maxCount = int.MaxValue)
        {
            HibernateTemplate.CacheQueries = false;
            return HibernateTemplate.Execute(session =>
            {
                // 创建 Entity Criteria
                var countCriteria = session.CreateCriteria(typeof(T));
                var contentCriteria = session.CreateCriteria(typeof(T));

                // 设置条件
                if (criterions != null && criterions.Count() != 0)
                    foreach (var criterion in criterions)
                    {
                        countCriteria.Add(criterion);
                        contentCriteria.Add(criterion);
                    }

                // 设置排序
                if (orders != null && orders.Count() != 0)
                    foreach (var order in orders)
                        contentCriteria.AddOrder(order);

                // 设置查询结果（Count）
                countCriteria.SetProjection(Projections.RowCount());

                // 设置查询结果数
                contentCriteria.SetFirstResult(firstIndex);
                contentCriteria.SetMaxResults(maxCount);

                HibernateTemplate.PrepareCriteria(countCriteria);
                HibernateTemplate.PrepareCriteria(contentCriteria);

                var count = countCriteria.UniqueResult<int>();
                var data = contentCriteria.List<T>();
                return new PagingResult<T>
                {
                    Data = data,
                    Size = maxCount,
                    TotalCount = count
                };
            });
        }
    }
}
