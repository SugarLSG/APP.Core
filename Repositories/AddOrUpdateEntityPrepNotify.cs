﻿using APP.Core.AOP.Notify;
using APP.Core.Utilities;
using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace APP.Core.Repositories
{
    public class AddOrUpdateEntityPrepNotify : IAOPPrepNotify
    {
        private static readonly bool AutoHandleEntityData = ConfigurationUtility.GetBool(CoreConfig.APPSETTINGS_AUTOHANDLEENTITYDATA);

        private static Regex CREATEDBY_REGEX = new Regex("^created?by$", RegexOptions.IgnoreCase);
        private static Regex CREATEDTIME_REGEX = new Regex("^created?time$", RegexOptions.IgnoreCase);
        private static Regex MODIFIEDBY_REGEX = new Regex("^modifi(ed|y)by$", RegexOptions.IgnoreCase);
        private static Regex MODIFIEDTIME_REGEX = new Regex("^modifi(ed|y)time$", RegexOptions.IgnoreCase);

        public void Prep(MethodInfo methodInfo, object[] args)
        {
            // 是否开启
            if (!AutoHandleEntityData)
                return;

            var methodName = methodInfo.Name;
            if (methodName == "AddEntity" || methodName == "UpdateEntity" || methodName == "AddOrUpdateEntity")
            {
                var entity = args[0];
                var properties = entity.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);

                object primaryValue = null;
                var primaryKey = properties.FirstOrDefault(p => p.GetCustomAttribute<KeyAttribute>() != null);
                if (primaryKey != null)
                    primaryValue = primaryKey.GetValue(entity);

                // 处理 string 类型字段
                foreach (var property in properties.Where(p => p.PropertyType == CoreConfig.STRING_TYPE))
                {
                    var value = property.GetValue(entity);
                    if (value != null)
                        property.SetValue(entity, value.ToString().Trim());
                }

                if (methodName == "AddEntity" || (methodName == "AddOrUpdateEntity" && primaryValue == null))
                {
                    // 自动赋值 create by & create time
                    var createdBy = properties.FirstOrDefault(p => CREATEDBY_REGEX.IsMatch(p.Name));
                    if (createdBy != null)
                        createdBy.SetValue(entity, AuthenticationUtility.AuthorizedUserId ?? 0);

                    var createdTime = properties.FirstOrDefault(p => CREATEDTIME_REGEX.IsMatch(p.Name));
                    if (createdTime != null)
                        createdTime.SetValue(entity, DateTime.Now);
                }

                // 自动赋值 modify by & modify time
                var modifiedBy = properties.FirstOrDefault(p => MODIFIEDBY_REGEX.IsMatch(p.Name));
                if (modifiedBy != null)
                    modifiedBy.SetValue(entity, AuthenticationUtility.AuthorizedUserId ?? 0);

                var modifiedTime = properties.FirstOrDefault(p => MODIFIEDTIME_REGEX.IsMatch(p.Name));
                if (modifiedTime != null)
                    modifiedTime.SetValue(entity, DateTime.Now);
            }
        }
    }
}
