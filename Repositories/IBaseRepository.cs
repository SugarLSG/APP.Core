﻿using APP.Core.Enums;
using APP.Core.Models;
using NHibernate.Criterion;
using NHibernate.Type;
using System.Collections.Generic;

namespace APP.Core.Repositories
{
    public interface IBaseRepository
    {
        /// <summary>
        /// 提交所有修改（包括 SAVE、UPDATE、DELETE）
        /// </summary>
        void CommitTransaction();

        /// <summary>
        /// 添加 Entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        object AddEntity(object entity);
        /// <summary>
        /// 添加或更新 Entity
        /// </summary>
        /// <param name="entity"></param>
        void AddOrUpdateEntity(object entity);

        /// <summary>
        /// 执行 SQL 更新数据
        /// （支持 UPDATE & DELETE）
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        int ExecuteUpdate(string sql);
        /// <summary>
        /// 执行 SQL 更新数据
        /// （支持 UPDATE & DELETE）
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters">允许为 null</param>
        /// <returns></returns>
        int ExecuteUpdate(string sql, IDictionary<string, object> parameters);
        /// <summary>
        /// 更新 Entity
        /// </summary>
        /// <param name="entity"></param>
        void UpdateEntity(object entity);

        /// <summary>
        /// 删除 Entity
        /// </summary>
        /// <param name="entity"></param>
        void DeleteEntity(object entity);
        /// <summary>
        /// 根据主键删除 Entity
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        void DeleteEntityById<T>(object id);

        /// <summary>
        /// 执行 SQL 获取数据数量
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        long ExecuteCount(string sql);
        /// <summary>
        /// 执行 SQL 获取数据数量
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters">允许为 null</param>
        /// <returns></returns>
        long ExecuteCount(string sql, IDictionary<string, object> parameters);
        /// <summary>
        /// 执行条件获取表数据数量
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="criterions">允许为 null</param>
        /// <returns></returns>
        int GetEntityCount<T>(IEnumerable<ICriterion> criterions);

        /// <summary>
        /// 执行 SQL 获取数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        T Execute<T>(string sql);
        /// <summary>
        /// 执行 SQL 获取数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="scalars"></param>
        /// <returns></returns>
        T Execute<T>(string sql, IDictionary<string, IType> scalars);
        /// <summary>
        /// 执行 SQL 获取数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters">允许为 null</param>
        /// <returns></returns>
        T Execute<T>(string sql, IDictionary<string, object> parameters);
        /// <summary>
        /// 执行 SQL 获取数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="scalars">允许为 null</param>
        /// <param name="parameters">允许为 null</param>
        /// <returns></returns>
        T Execute<T>(string sql, IDictionary<string, IType> scalars, IDictionary<string, object> parameters);
        /// <summary>
        /// 执行 SQL 获取数据
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        object Execute(string sql);
        /// <summary>
        /// 执行 SQL 获取数据
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters">允许为 null</param>
        /// <returns></returns>
        object Execute(string sql, IDictionary<string, object> parameters);
        /// <summary>
        /// 根据主键获取 Entity
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <param name="getCache">是否使用 NHibernate 二级缓存</param>
        /// <returns></returns>
        T GetEntityById<T>(object id, bool getCache = true);
        /// <summary>
        /// 根据主键获取 Entity
        /// 若为空，新建对应实例
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <param name="getCache">是否使用 NHibernate 二级缓存</param>
        /// <returns></returns>
        T GetOrCreateEntityById<T>(object id, bool getCache = true);
        /// <summary>
        /// 执行条件获取表数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T GetEntity<T>();
        /// <summary>
        /// 执行条件获取表数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="criterions">允许为 null</param>
        /// <returns></returns>
        T GetEntity<T>(IEnumerable<ICriterion> criterions);
        /// <summary>
        /// 执行条件获取表数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="orders">允许为 null</param>
        /// <returns></returns>
        T GetEntity<T>(IEnumerable<Order> orders);
        /// <summary>
        /// 执行条件获取表数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="criterions">允许为 null</param>
        /// <param name="orders">允许为 null</param>
        /// <returns></returns>
        T GetEntity<T>(IEnumerable<ICriterion> criterions, IEnumerable<Order> orders);

        /// <summary>
        /// 执行 SQL 获取数据列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="firstIndex"></param>
        /// <param name="maxCount"></param>
        /// <returns></returns>
        IList<T> ExecuteList<T>(string sql, int firstIndex = 0, int maxCount = int.MaxValue);
        /// <summary>
        /// 执行 SQL 获取数据列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="scalars">允许为 null</param>
        /// <param name="firstIndex"></param>
        /// <param name="maxCount"></param>
        /// <returns></returns>
        IList<T> ExecuteList<T>(string sql, IDictionary<string, IType> scalars, int firstIndex = 0, int maxCount = int.MaxValue);
        /// <summary>
        /// 执行 SQL 获取数据列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters">允许为 null</param>
        /// <param name="firstIndex"></param>
        /// <param name="maxCount"></param>
        /// <returns></returns>
        IList<T> ExecuteList<T>(string sql, IDictionary<string, object> parameters, int firstIndex = 0, int maxCount = int.MaxValue);
        /// <summary>
        /// 执行 SQL 获取数据列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="scalars">允许为 null</param>
        /// <param name="parameters">允许为 null</param>
        /// <param name="firstIndex"></param>
        /// <param name="maxCount"></param>
        /// <returns></returns>
        IList<T> ExecuteList<T>(string sql, IDictionary<string, IType> scalars, IDictionary<string, object> parameters, int firstIndex = 0, int maxCount = int.MaxValue);
        /// <summary>
        /// 执行 SQL 获取数据列表
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="firstIndex"></param>
        /// <param name="maxCount"></param>
        /// <returns></returns>
        IList<object> ExecuteList(string sql, int firstIndex = 0, int maxCount = int.MaxValue);
        /// <summary>
        /// 执行 SQL 获取数据列表
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters">允许为 null</param>
        /// <param name="firstIndex"></param>
        /// <param name="maxCount"></param>
        /// <returns></returns>
        IList<object> ExecuteList(string sql, IDictionary<string, object> parameters, int firstIndex = 0, int maxCount = int.MaxValue);
        /// <summary>
        /// 执行条件获取表数据列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="firstIndex"></param>
        /// <param name="maxCount"></param>
        /// <returns></returns>
        IList<T> GetEntityList<T>(int firstIndex = 0, int maxCount = int.MaxValue);
        /// <summary>
        /// 执行条件获取表数据列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="criterions">允许为 null</param>
        /// <param name="firstIndex"></param>
        /// <param name="maxCount"></param>
        /// <returns></returns>
        IList<T> GetEntityList<T>(IEnumerable<ICriterion> criterions, int firstIndex = 0, int maxCount = int.MaxValue);
        /// <summary>
        /// 执行条件获取表数据列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="orders">允许为 null</param>
        /// <param name="firstIndex"></param>
        /// <param name="maxCount"></param>
        /// <returns></returns>
        IList<T> GetEntityList<T>(IEnumerable<Order> orders, int firstIndex = 0, int maxCount = int.MaxValue);
        /// <summary>
        /// 执行条件获取表数据列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="criterions">允许为 null</param>
        /// <param name="orders">允许为 null</param>
        /// <param name="firstIndex"></param>
        /// <param name="maxCount"></param>
        /// <returns></returns>
        IList<T> GetEntityList<T>(IEnumerable<ICriterion> criterions, IEnumerable<Order> orders, int firstIndex = 0, int maxCount = int.MaxValue);

        /// <summary>
        /// 执行 SQL 获取数据分页列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="firstIndex"></param>
        /// <param name="maxCount"></param>
        /// <returns></returns>
        PagingResult<T> ExecutePagingList<T>(string sql, int firstIndex = 0, int maxCount = int.MaxValue);
        /// <summary>
        /// 执行 SQL 获取数据分页列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="orders">允许为 null</param>
        /// <param name="firstIndex"></param>
        /// <param name="maxCount"></param>
        /// <returns></returns>
        PagingResult<T> ExecutePagingList<T>(string sql, IDictionary<string, Sorted> orders, int firstIndex = 0, int maxCount = int.MaxValue);
        /// <summary>
        /// 执行 SQL 获取数据分页列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="scalars">允许为 null</param>
        /// <param name="firstIndex"></param>
        /// <param name="maxCount"></param>
        /// <returns></returns>
        PagingResult<T> ExecutePagingList<T>(string sql, IDictionary<string, IType> scalars, int firstIndex = 0, int maxCount = int.MaxValue);
        /// <summary>
        /// 执行 SQL 获取数据分页列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters">允许为 null</param>
        /// <param name="firstIndex"></param>
        /// <param name="maxCount"></param>
        /// <returns></returns>
        PagingResult<T> ExecutePagingList<T>(string sql, IDictionary<string, object> parameters, int firstIndex = 0, int maxCount = int.MaxValue);
        /// <summary>
        /// 执行 SQL 获取数据分页列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="orders">允许为 null</param>
        /// <param name="scalars">允许为 null</param>
        /// <param name="firstIndex"></param>
        /// <param name="maxCount"></param>
        /// <returns></returns>
        PagingResult<T> ExecutePagingList<T>(string sql, IDictionary<string, Sorted> orders, IDictionary<string, IType> scalars, int firstIndex = 0, int maxCount = int.MaxValue);
        /// <summary>
        /// 执行 SQL 获取数据分页列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="orders">允许为 null</param>
        /// <param name="parameters">允许为 null</param>
        /// <param name="firstIndex"></param>
        /// <param name="maxCount"></param>
        /// <returns></returns>
        PagingResult<T> ExecutePagingList<T>(string sql, IDictionary<string, Sorted> orders, IDictionary<string, object> parameters, int firstIndex = 0, int maxCount = int.MaxValue);
        /// <summary>
        /// 执行 SQL 获取数据分页列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="scalars">允许为 null</param>
        /// <param name="parameters">允许为 null</param>
        /// <param name="firstIndex"></param>
        /// <param name="maxCount"></param>
        /// <returns></returns>
        PagingResult<T> ExecutePagingList<T>(string sql, IDictionary<string, IType> scalars, IDictionary<string, object> parameters, int firstIndex = 0, int maxCount = int.MaxValue);
        /// <summary>
        /// 执行 SQL 获取数据分页列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="orders">允许为 null</param>
        /// <param name="scalars">允许为 null</param>
        /// <param name="parameters">允许为 null</param>
        /// <param name="firstIndex"></param>
        /// <param name="maxCount"></param>
        /// <returns></returns>
        PagingResult<T> ExecutePagingList<T>(string sql, IDictionary<string, Sorted> orders, IDictionary<string, IType> scalars, IDictionary<string, object> parameters, int firstIndex = 0, int maxCount = int.MaxValue);
        /// <summary>
        /// 执行条件获取表数据分页列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="firstIndex"></param>
        /// <param name="maxCount"></param>
        /// <returns></returns>
        PagingResult<T> GetEntityPagingList<T>(int firstIndex = 0, int maxCount = int.MaxValue);
        /// <summary>
        /// 执行条件获取表数据分页列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="criterions">允许为 null</param>
        /// <param name="firstIndex"></param>
        /// <param name="maxCount"></param>
        /// <returns></returns>
        PagingResult<T> GetEntityPagingList<T>(IEnumerable<ICriterion> criterions, int firstIndex = 0, int maxCount = int.MaxValue);
        /// <summary>
        /// 执行条件获取表数据分页列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="orders">允许为 null</param>
        /// <param name="firstIndex"></param>
        /// <param name="maxCount"></param>
        /// <returns></returns>
        PagingResult<T> GetEntityPagingList<T>(IEnumerable<Order> orders, int firstIndex = 0, int maxCount = int.MaxValue);
        /// <summary>
        /// 执行条件获取表数据分页列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="criterions">允许为 null</param>
        /// <param name="orders">允许为 null</param>
        /// <param name="firstIndex"></param>
        /// <param name="maxCount"></param>
        /// <returns></returns>
        PagingResult<T> GetEntityPagingList<T>(IEnumerable<ICriterion> criterions, IEnumerable<Order> orders, int firstIndex = 0, int maxCount = int.MaxValue);
    }
}
