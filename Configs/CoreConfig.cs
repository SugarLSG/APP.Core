﻿using APP.Core.Enums;
using APP.Core.Utilities;
using System;
using System.Reflection;

public static class CoreConfig
{
    #region 配置

    /// <summary>
    /// APP.Core 版本
    /// </summary>
    public const string CoreVersion_1_10_95 = "1.10.95";

    /// <summary>
    /// 检查版本号
    /// </summary>
    public static Action<string> CheckCoreVersion = new Action<string>(ver => { if (ver != Assembly.GetExecutingAssembly().GetCustomAttribute<AssemblyFileVersionAttribute>().Version) throw new Exception("core version is invalid."); });

    /// <summary>
    /// 程序开始时间
    /// </summary>
    public static readonly DateTime APP_STARTTIME = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1, 0, 0, 0));

    /// <summary>
    /// 系统版本
    /// </summary>
    public static readonly string SystemVersion = ConfigurationUtility.GetString(APPSETTINGS_SYSTEMVERSION) ?? "1.0.0.0";

    /// <summary>
    /// 系统环境
    /// </summary>
    public static readonly SystemEnvironment SystemEnvironment = ConfigurationUtility.GetString(APPSETTINGS_SYSTEMENVIRONMENT).ToEnumNullable<SystemEnvironment>() ?? SystemEnvironment.DEV;

    /// <summary>
    /// 本地 IP
    /// </summary>
    public const string IP_LOCAL = "127.0.0.1";

    /// <summary>
    /// 本地 Host
    /// </summary>
    public const string HOST_LOCAL = "localhost";

    /// <summary>
    /// Content Type - HTML
    /// </summary>
    public const string CONTENTTYPE_TEXT_HTML = "text/html";

    /// <summary>
    /// Content Type - JPEG
    /// </summary>
    public const string CONTENTTYPE_IMAGE_JPEG = "image/jpeg";

    /// <summary>
    /// Content Type - PNG
    /// </summary>
    public const string CONTENTTYPE_IMAGE_PNG = "image/png";

    /// <summary>
    /// Content Type - GIF
    /// </summary>
    public const string CONTENTTYPE_IMAGE_GIF = "image/gif";

    /// <summary>
    /// Content Type - JSON
    /// </summary>
    public const string CONTENTTYPE_APPLICATION_JSON = "application/json";

    /// <summary>
    /// Content Type - PDF
    /// </summary>
    public const string CONTENTTYPE_APPLICATION_PDF = "application/pdf";

    /// <summary>
    /// Content Type - WORD
    /// </summary>
    public const string CONTENTTYPE_APPLICATION_WORD = "application/msword";

    /// <summary>
    /// Content Type - EXCEL
    /// </summary>
    public const string CONTENTTYPE_APPLICATION_EXCEL = "application/vnd.ms-excel";

    /// <summary>
    /// Content Type - XML
    /// </summary>
    public const string CONTENTTYPE_APPLICATION_XML = "application/xml";

    /// <summary>
    /// Content Type - ZIP
    /// </summary>
    public const string CONTENTTYPE_APPLICATION_ZIP = "application/zip";

    /// <summary>
    /// Content Type - FORM URL ENCODED
    /// </summary>
    public const string CONTENTTYPE_APPLICATION_FORMURLENCODED = "application/x-www-form-urlencoded";

    /// <summary>
    /// 默认 UserAgent
    /// </summary>
    public const string USERAGENT_DEFAULT = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36";

    #endregion

    #region Key

    public const string APPSETTINGS_DATABASESETTINGS_SECTION = "databaseSettings";

    public const string APPSETTINGS_DATABASESETTINGS_REDISDEFAULTDB = "redisDefaultDb";

    public const string APPSETTINGS_DATABASESETTINGS_REDISHOST = "redisHost";

    public const string APPSETTINGS_DATABASESETTINGS_REDISPORT = "redisPort";

    public const string APPSETTINGS_DATABASESETTINGS_REDISPASSWORD = "redisPassword";


    public const string APPSETTINGS_SYSTEMVERSION = "systemVersion";

    public const string APPSETTINGS_SYSTEMENVIRONMENT = "systemEnvironment";

    public const string APPSETTINGS_AUTOHANDLEENTITYDATA = "autoHandleEntityData";

    #endregion

    #region Value

    public static readonly Type STRING_TYPE = typeof(string);

    #endregion

    #region Cache Key 前缀

    /// <summary>
    /// Attribute 资源 Key
    /// </summary>
    public const string RESOURCEKEY_ATTRIBUTEMESSAGE_FORMAT = "Attribute_{0}";

    /// <summary>
    /// 系统缓存前缀
    /// </summary>
    public const string SYSTEM_CACHEKEYPREFIX = "APPSYSTEM.";

    /// <summary>
    /// Hash 文件版本
    /// </summary>
    public const string CACHEKEY_HASHFILE_VERSION_PREFIX = SYSTEM_CACHEKEYPREFIX + "HASHFILEVERSION";

    /// <summary>
    /// 方法返回值缓存
    /// </summary>
    public const string CACHEKEY_METHODRETURN_PREFIX = SYSTEM_CACHEKEYPREFIX + "RETURNCACHE-";

    /// <summary>
    /// Enum信息缓存
    /// </summary>
    public const string CACHEKEY_ENUMMESSAGE_PREFIX = SYSTEM_CACHEKEYPREFIX + "ENUMMESSAGE-";

    /// <summary>
    /// Exception信息缓存
    /// </summary>
    public const string CACHEKEY_EXCEPTIONMESSAGE_PREFIX = SYSTEM_CACHEKEYPREFIX + "EXCEPTIONMESSAGE-";

    /// <summary>
    /// 类型标量缓存
    /// </summary>
    public const string CACHEKEY_TYPESCALARS_PREFIX = SYSTEM_CACHEKEYPREFIX + "TYPESCALARS-";

    /// <summary>
    /// 线程安全加锁分类缓存
    /// </summary>
    public const string CACHEKEY_THREADSAFETYDEFAULTBUCKET = SYSTEM_CACHEKEYPREFIX + "THREADSAFETYDEFAULTBUCKET";

    #endregion

    #region Message

    public const string MESSAGE_OPERATE_SUCCESS = "操作成功";

    public const string MESSAGE_OPERATE_FAILURE = "操作失败";

    public const string HTTPREQUEST_403 = "您无权限进行访问 ...";

    public const string HTTPREQUEST_404 = "找不到您要访问的资源 ...";

    public const string HTTPREQUEST_500 = "系统抛出异常，已通知管理员 ...";

    #endregion

    #region 正则表达式

    /// <summary>
    /// 双字节
    /// </summary>
    public const string RegexDoubleByte = @"[^\x00-\xff]";

    /// <summary>
    /// Boolean（可空）
    /// </summary>
    public const string RegexBoolean = @"^(TRUE|True|true|FALSE|False|false)?$";

    /// <summary>
    /// 小数（可空）
    /// 8位整数位，2位小数位
    /// </summary>
    public const string RegexDecimal8_2 = @"^((-?0(\.\d{1,2})?)|(-?[1-9]\d{0,7}(\.\d{1,2})?))?$";

    /// <summary>
    /// 整数（可空）
    /// 10位整数位
    /// </summary>
    public const string RegexInteger10 = @"^(0|(-?[1-9]\d{0,9}))?$";

    /// <summary>
    /// 日期（可空）
    /// </summary>
    public const string RegexDate = @"^(\d{4}(-\d{1,2}){0,2})?$";

    /// <summary>
    /// Email（可空）
    /// </summary>
    public const string RegexEmail = @"^([\da-zA-Z]+([\.\-_][\da-zA-Z]+)*@[\da-zA-Z]+([\.\-_][\da-zA-Z]+)*\.[a-zA-Z]{2,5})?$";

    /// <summary>
    /// 电话（可空）
    /// 3-4位区号 + 7-8位直拨号 + 1-4位分机号
    /// 10位号码
    /// 3 + 4 + 3位号码
    /// 4 + 2 + 4位号码
    /// </summary>
    public const string RegexTel = @"^(((\d{3,4}\-)?\d{7,8}(\-\d{1,4})?)|(\d{10})|(\d{3}-\d{4}-\d{3})|(\d{4}-\d{2}-\d{4}))?$";

    /// <summary>
    /// 手机（可空）
    /// (+)2位区号 + 11位直拨号 + 1-4位分机号
    /// </summary>
    public const string RegexMobile = @"^((\+?\d{2} )?[1-9]\d{10}(-\d{1,4})?)?$";

    /// <summary>
    /// 电话或手机（可空）
    /// </summary>
    public const string RegexTelMobile = @"^(((\d{3,4}\-)?\d{7,8}(\-\d{1,4})?)|(\d{10})|(\d{3}-\d{4}-\d{3})|(\d{4}-\d{2}-\d{4})|((\+?\d{2} )?[1-9]\d{10}(-\d{1,4})?))?$";

    /// <summary>
    /// 传真（可空）
    /// </summary>
    public const string RegexFax = @"^(\d{3,5}\-\d{7,8})?$";

    /// <summary>
    /// QQ号（可空）
    /// 4~16位数字
    /// </summary>
    public const string RegexQQ4_16 = @"^([1-9][0-9]{3,15})?$";

    /// <summary>
    /// 微信号（可空）
    /// 4~32位字符
    /// </summary>
    public const string RegexWechat4_32 = @"^([a-zA-Z][a-zA-Z0-9_\-]{3,31})?$";

    /// <summary>
    /// IP（可空）
    /// </summary>
    public const string RegexIP = @"^(((25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d)))\.){3}(25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d))))?$";

    /// <summary>
    /// 域名（可空）
    /// </summary>
    public const string RegexDomain = @"^([\da-zA-Z]+(\-?[\da-zA-Z])*(\.[\da-zA-Z]+(\-?[\da-zA-Z])*)+)?$";

    /// <summary>
    /// 账号（可空）
    /// 数字 或 大小写字母 或 特殊字符（@-_.）
    /// 3~32个字符
    /// </summary>
    public const string RegexAccount = @"^([\da-zA-Z@\-_\.]{3,32})?$";

    /// <summary>
    /// 强度密码（不可空）
    /// 必须是两种以上组合（与位置无关）
    ///  数字+大小写字母
    ///  数字+特殊字符（`~!@#$%^&*()-_=+\|[{\]};:'",<.>/?）
    ///  特殊字符+大小写字母
    ///  特殊字符+数字+大小写字母
    /// 6~32个字符
    /// </summary>
    public const string RegexStrongPassword6_32 = @"^(?![\d]+$)(?![a-zA-Z]+$)(?![`~!@#$%^&*()\-_=+\|[{\]};:'"",<.>/?]+$)[\da-zA-Z`~!@#$%^&*()\-_=+\|[{\]};:'"",<.>/?]{6,32}$";

    #endregion
}
