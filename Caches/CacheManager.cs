﻿using APP.Core.Utilities;

namespace APP.Core.Caches
{
    /// <summary>
    /// 缓存管理类
    /// </summary>
    public static class CacheManager
    {
        /// <summary>
        /// Http 缓存
        /// </summary>
        public static ICacheProvider HTTPCacheProvider { get { return Singleton<HTTPCacheProvider>.Instance; } }

        /// <summary>
        /// 内存缓存
        /// </summary>
        public static ICacheProvider MemoryCacheProvider { get { return Singleton<MemoryCacheProvider>.Instance; } }

        /// <summary>
        /// Redis 缓存
        /// </summary>
        public static IRedisCacheProvider RedisCacheProvider { get { return Singleton<RedisCacheProvider>.Instance; } }
    }
}
