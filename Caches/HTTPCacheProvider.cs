﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;

namespace APP.Core.Caches
{
    internal class HTTPCacheProvider : ICacheProvider
    {
        private static Cache cache
        {
            get { return HttpRuntime.Cache; }
        }


        public bool Set(string key, object data, CacheExpirationType expirationType, double? cacheMinutes = null)
        {
            if (!key.HasValue())
                return false;
            if (data == null)
                return false;
            if (expirationType != CacheExpirationType.None)
            {
                if (cacheMinutes == null)
                    return false;
                if (cacheMinutes <= 0)
                    return false;
            }

            cache.Insert(
                key,
                data,
                null,
                expirationType == CacheExpirationType.Absolute ? DateTime.UtcNow.AddMinutes(cacheMinutes.Value) : Cache.NoAbsoluteExpiration,
                expirationType == CacheExpirationType.Sliding ? TimeSpan.FromMinutes(cacheMinutes.Value) : Cache.NoSlidingExpiration
            );
            return true;
        }

        public bool Set(string key, object data, DateTime expirationTime)
        {
            var now = DateTime.Now;
            if (expirationTime <= now)
                return false;

            return Set(key, data, CacheExpirationType.Absolute, (expirationTime - now).TotalMinutes);
        }


        public bool Contains(string key)
        {
            if (!key.HasValue())
                return false;

            return Get(key) != null;
        }


        public object Get(string key)
        {
            if (!key.HasValue())
                return null;

            return cache.Get(key);
        }

        public T Get<T>(string key)
        {
            if (!key.HasValue())
                return default(T);

            var result = cache.Get(key);
            if (result == null)
                return default(T);

            return (T)result;
        }

        public IEnumerable<string> GetAllKeys()
        {
            var result = new List<string>();
            var enumerator = cache.GetEnumerator();
            while (enumerator.MoveNext())
                result.Add(enumerator.Key.ToString());

            return result;
        }


        public bool Remove(string key)
        {
            return cache.Remove(key) != null;
        }

        public void RemoveAll()
        {
            foreach (var key in GetAllKeys())
            {
                Remove(key);
            }
        }
    }
}
