﻿using System;
using System.Collections.Generic;

namespace APP.Core.Caches
{
    public interface IRedisCacheProvider
    {
        /// <summary>
        /// 添加1个缓存项（若已存在该 Key，更新数据）
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        bool SetValue<T>(string key, T data, int? db = null);

        /// <summary>
        /// 添加1个缓存项，并在指定时间过期（若已存在该 Key，更新数据）
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <param name="cacheMinutes"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        bool SetValue<T>(string key, T data, double cacheMinutes, int? db = null);

        /// <summary>
        /// 添加1个缓存项，并在指定时间过期（若已存在该 Key，更新数据）
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Key"></param>
        /// <param name="data"></param>
        /// <param name="expirationTime"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        bool SetValue<T>(string key, T data, DateTime expirationTime, int? db = null);

        /// <summary>
        /// 判断是否存在指定 Key 的缓存项
        /// </summary>
        /// <param name="key"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        bool ContainsValue(string key, int? db = null);

        /// <summary>
        /// 获取指定 Key 的缓存项
        /// </summary>
        /// <param name="key"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        T GetValue<T>(string key, int? db = null);

        /// <summary>
        /// 删除指定 Key 的缓存项
        /// </summary>
        /// <param name="key"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        bool RemoveValue(string key, int? db = null);


        /// <summary>
        /// 添加1个缓存列表到指定 Key 的 List 列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="list"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        bool AddList<T>(string key, IEnumerable<T> list, int? db = null);

        /// <summary>
        /// 添加1个缓存项到指定 Key 的 List 列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        bool AddListItem<T>(string key, T data, int? db = null);

        /// <summary>
        /// 判断是否存在指定 Key 的 List 列表
        /// </summary>
        /// <param name="key"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        bool ContainsList(string key, int? db = null);

        /// <summary>
        /// 获取指定 Key 的 List 列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        IEnumerable<T> GetList<T>(string key, int? db = null);

        /// <summary>
        /// 删除指定 Key 的 List 列表
        /// </summary>
        /// <param name="key"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        bool RemoveList(string key, int? db = null);

        /// <summary>
        /// 从指定 Key 的 List 列表删除指定 Value 的缓存项
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        T RemoveListItem<T>(string key, T data, int? db = null);


        /// <summary>
        /// 添加1个缓存列表到指定 Key 的 Set 列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="list"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        bool AddSet<T>(string key, IEnumerable<T> list, int? db = null);

        /// <summary>
        /// 添加1个缓存项到指定 Key 的 Set 列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        bool AddSetItem<T>(string key, T data, int? db = null);

        /// <summary>
        /// 判断是否存在指定 Key 的 Set 列表
        /// </summary>
        /// <param name="key"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        bool ContainsSet(string key, int? db = null);

        /// <summary>
        /// 判断是否存在指定 Key 的 Set 列表缓存项
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        bool ContainsSetItem<T>(string key, T data, int? db = null);

        /// <summary>
        /// 获取指定 Key 的 Set 列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        IEnumerable<T> GetSet<T>(string key, int? db = null);

        /// <summary>
        /// 删除指定 Key 的 Set 列表
        /// </summary>
        /// <param name="key"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        bool RemoveSet(string key, int? db = null);

        /// <summary>
        /// 从指定 Key 的 Set 列表删除指定 Value 的缓存项
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        T RemoveSetItem<T>(string key, T data, int? db = null);


        /// <summary>
        /// 添加1个缓存项到指定 Key 的 Sorted Set 列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        bool AddSortedSetItem<T>(string key, T data, double score, int? db = null);

        /// <summary>
        /// 判断是否存在指定 Key 的 Sorted Set 列表
        /// </summary>
        /// <param name="key"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        bool ContainsSortedSet(string key, int? db = null);

        /// <summary>
        /// 获取指定 Key 的 Sorted Set 列表缓存项的分值（不存在则为 null）
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        double? GetSortedSetItemScore<T>(string key, T data, int? db = null);

        /// <summary>
        /// 获取指定 Key 的 Sorted Set 列表（根据 score 正序）
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        IEnumerable<T> GetSortedSet<T>(string key, int? db = null);

        /// <summary>
        /// 获取指定 Key 的 Sorted Set 列表（根据 score 倒序）
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        IEnumerable<T> GetSortedSetDesc<T>(string key, int? db = null);

        /// <summary>
        /// 删除指定 Key 的 Sorted Set 列表
        /// </summary>
        /// <param name="key"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        bool RemoveSortedSet(string key, int? db = null);

        /// <summary>
        /// 从指定 Key 的 Sorted Set 列表删除指定 Value 的缓存项
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        T RemoveSortedSetItem<T>(string key, T data, int? db = null);


        /// <summary>
        /// 清空所有数据
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        bool FlushAll(int? db = null);
    }
}
