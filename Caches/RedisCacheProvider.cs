﻿using APP.Core.Utilities;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using static APP.Core.Extensions.Json.ContractResolverExtension;

namespace APP.Core.Caches
{
    /// <summary>
    /// db index:
    /// 0: key - value
    /// 1: key - list
    /// 2: key - set
    /// 3: key - sorted set
    /// </summary>
    internal class RedisCacheProvider : IRedisCacheProvider
    {
        private static readonly int DefaultDb = ConfigurationUtility.GetSectionInt(CoreConfig.APPSETTINGS_DATABASESETTINGS_SECTION, CoreConfig.APPSETTINGS_DATABASESETTINGS_REDISDEFAULTDB, 0);
        private static readonly string Host = ConfigurationUtility.GetSectionString(CoreConfig.APPSETTINGS_DATABASESETTINGS_SECTION, CoreConfig.APPSETTINGS_DATABASESETTINGS_REDISHOST);
        private static readonly int Port = ConfigurationUtility.GetSectionInt(CoreConfig.APPSETTINGS_DATABASESETTINGS_SECTION, CoreConfig.APPSETTINGS_DATABASESETTINGS_REDISPORT, 6379);
        private static readonly string Password = ConfigurationUtility.GetSectionString(CoreConfig.APPSETTINGS_DATABASESETTINGS_SECTION, CoreConfig.APPSETTINGS_DATABASESETTINGS_REDISPASSWORD);

        private static ConnectionMultiplexer Cache { get; set; }


        public RedisCacheProvider()
        {
            if (!Host.HasValue())
                throw new ArgumentNullException("redis host");

            Cache = ConnectionMultiplexer.Connect(new ConfigurationOptions
            {
                EndPoints = { { Host, Port } },
                Password = Password,
                AllowAdmin = true
            });
        }


        private RedisValue SerializeData<T>(T data)
        {
            return JsonUtility.Serialize(data, propertynametype: PropertyNameType.None);
        }

        private T DeserializeValue<T>(RedisValue value)
        {
            try { return JsonUtility.Deserialize<T>(value); }
            catch (Exception ex)
            {
                LogUtility.LogError(ex);
                return default(T);
            }
        }


        #region Key - Value

        public bool SetValue<T>(string key, T data, int? db = null)
        {
            if (!key.HasValue())
                return false;
            if (data == null)
                return false;

            return Cache.GetDatabase(db ?? DefaultDb).StringSet(key, SerializeData(data));
        }

        public bool SetValue<T>(string key, T data, double cacheMinutes, int? db = null)
        {
            if (!key.HasValue())
                return false;
            if (data == null)
                return false;
            if (cacheMinutes <= 0)
                return false;

            return Cache.GetDatabase(db ?? DefaultDb).StringSet(key, SerializeData(data), TimeSpan.FromMinutes(cacheMinutes));
        }

        public bool SetValue<T>(string key, T data, DateTime expirationTime, int? db = null)
        {
            if (!key.HasValue())
                return false;
            if (data == null)
                return false;
            if (expirationTime <= DateTime.Now)
                return false;

            return Cache.GetDatabase(db ?? DefaultDb).StringSet(key, SerializeData(data), expirationTime - DateTime.Now);
        }

        public bool ContainsValue(string key, int? db = null)
        {
            if (!key.HasValue())
                return false;

            return Cache.GetDatabase(db ?? DefaultDb).KeyExists(key);
        }

        public T GetValue<T>(string key, int? db = null)
        {
            if (!key.HasValue())
                return default(T);

            return DeserializeValue<T>(Cache.GetDatabase(db ?? DefaultDb).StringGet(key));
        }

        public bool RemoveValue(string key, int? db = null)
        {
            if (!key.HasValue())
                return false;

            return Cache.GetDatabase(db ?? DefaultDb).KeyDelete(key);
        }

        #endregion

        #region Key - List

        public bool AddList<T>(string key, IEnumerable<T> list, int? db = null)
        {
            if (!key.HasValue())
                return false;
            if (list == null || !list.Any())
                return false;

            return Cache.GetDatabase(db ?? DefaultDb).ListRightPush(key, list.Select(i => SerializeData(i)).ToArray()) == list.Count();
        }

        public bool AddListItem<T>(string key, T data, int? db = null)
        {
            if (!key.HasValue())
                return false;
            if (data == null)
                return false;

            return Cache.GetDatabase(db ?? DefaultDb).ListRightPush(key, SerializeData(data)) == 1;
        }

        public bool ContainsList(string key, int? db = null)
        {
            if (!key.HasValue())
                return false;

            return Cache.GetDatabase(db ?? DefaultDb).KeyExists(key);
        }

        public IEnumerable<T> GetList<T>(string key, int? db = null)
        {
            if (!key.HasValue())
                return null;

            return Cache.GetDatabase(db ?? DefaultDb).ListRange(key).Select(i => DeserializeValue<T>(i)).Where(i => i != null);
        }

        public bool RemoveList(string key, int? db = null)
        {
            if (!key.HasValue())
                return false;

            return Cache.GetDatabase(db ?? DefaultDb).KeyDelete(key);
        }

        public T RemoveListItem<T>(string key, T data, int? db = null)
        {
            if (!key.HasValue())
                return default(T);
            if (data == null)
                return default(T);

            return Cache.GetDatabase(db ?? DefaultDb).ListRemove(key, SerializeData(data)) == 1 ? data : default(T);
        }

        #endregion

        #region Key - Set

        public bool AddSet<T>(string key, IEnumerable<T> list, int? db = null)
        {
            if (!key.HasValue())
                return false;
            if (list == null || !list.Any())
                return false;

            return Cache.GetDatabase(db ?? DefaultDb).SetAdd(key, list.Select(i => SerializeData(i)).ToArray()) == list.Count();
        }

        public bool AddSetItem<T>(string key, T data, int? db = null)
        {
            if (!key.HasValue())
                return false;
            if (data == null)
                return false;

            return Cache.GetDatabase(db ?? DefaultDb).SetAdd(key, SerializeData(data));
        }

        public bool ContainsSet(string key, int? db = null)
        {
            if (!key.HasValue())
                return false;

            return Cache.GetDatabase(db ?? DefaultDb).KeyExists(key);
        }

        public bool ContainsSetItem<T>(string key, T data, int? db = null)
        {
            if (!key.HasValue())
                return false;
            if (data == null)
                return false;

            return Cache.GetDatabase(db ?? DefaultDb).SetContains(key, SerializeData(data));
        }

        public IEnumerable<T> GetSet<T>(string key, int? db = null)
        {
            if (!key.HasValue())
                return null;

            return Cache.GetDatabase(db ?? DefaultDb).SetMembers(key).Select(i => DeserializeValue<T>(i)).Where(i => i != null);
        }

        public bool RemoveSet(string key, int? db = null)
        {
            if (!key.HasValue())
                return false;

            return Cache.GetDatabase(db ?? DefaultDb).KeyDelete(key);
        }

        public T RemoveSetItem<T>(string key, T data, int? db = null)
        {
            if (!key.HasValue())
                return default(T);
            if (data == null)
                return default(T);

            return Cache.GetDatabase(db ?? DefaultDb).SetRemove(key, SerializeData(data)) ? data : default(T);
        }

        #endregion

        #region Key - Sorted Set

        public bool AddSortedSetItem<T>(string key, T data, double score, int? db = null)
        {
            if (!key.HasValue())
                return false;
            if (data == null)
                return false;

            return Cache.GetDatabase(db ?? DefaultDb).SortedSetAdd(key, SerializeData(data), score);
        }

        public bool ContainsSortedSet(string key, int? db = null)
        {
            if (!key.HasValue())
                return false;

            return Cache.GetDatabase(db ?? DefaultDb).KeyExists(key);
        }

        public double? GetSortedSetItemScore<T>(string key, T data, int? db = null)
        {
            if (!key.HasValue())
                return null;
            if (data == null)
                return null;

            return Cache.GetDatabase(db ?? DefaultDb).SortedSetScore(key, SerializeData(data));
        }

        public IEnumerable<T> GetSortedSet<T>(string key, int? db = null)
        {
            if (!key.HasValue())
                return null;

            return Cache.GetDatabase(db ?? DefaultDb).SortedSetRangeByScore(key).Select(i => DeserializeValue<T>(i)).Where(i => i != null);
        }

        public IEnumerable<T> GetSortedSetDesc<T>(string key, int? db = null)
        {
            if (!key.HasValue())
                return null;

            return Cache.GetDatabase(db ?? DefaultDb).SortedSetRangeByScore(key, order: Order.Descending).Select(i => DeserializeValue<T>(i)).Where(i => i != null);
        }

        public bool RemoveSortedSet(string key, int? db = null)
        {
            if (!key.HasValue())
                return false;

            return Cache.GetDatabase(db ?? DefaultDb).KeyDelete(key);
        }

        public T RemoveSortedSetItem<T>(string key, T data, int? db = null)
        {
            if (!key.HasValue())
                return default(T);
            if (data == null)
                return default(T);

            return Cache.GetDatabase(db ?? DefaultDb).SortedSetRemove(key, SerializeData(data)) ? data : default(T);
        }

        #endregion

        #region All

        public bool FlushAll(int? db = null)
        {
            foreach (var endPoints in Cache.GetEndPoints(true))
                Cache.GetServer(endPoints).FlushDatabase(db ?? DefaultDb);

            return true;
        }

        #endregion
    }
}
