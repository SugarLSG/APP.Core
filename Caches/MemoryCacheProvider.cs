﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;

namespace APP.Core.Caches
{
    internal class MemoryCacheProvider : ICacheProvider
    {
        private static MemoryCache cache
        {
            get { return MemoryCache.Default; }
        }


        public bool Set(string key, object data, CacheExpirationType expirationType, double? cacheMinutes = null)
        {
            if (!key.HasValue())
                return false;
            if (data == null)
                return false;
            if (expirationType != CacheExpirationType.None)
            {
                if (cacheMinutes == null)
                    return false;
                if (cacheMinutes <= 0)
                    return false;
            }

            var policy = new CacheItemPolicy();
            if (expirationType == CacheExpirationType.Absolute)
                policy.AbsoluteExpiration = DateTimeOffset.Now.AddMinutes(cacheMinutes.Value);
            else if (expirationType == CacheExpirationType.Sliding)
                policy.SlidingExpiration = TimeSpan.FromMinutes(cacheMinutes.Value);
            else
                policy.AbsoluteExpiration = DateTimeOffset.MaxValue;

            cache.Set(key, data, policy);
            return true;
        }

        public bool Set(string key, object data, DateTime expirationTime)
        {
            var now = DateTime.Now;
            if (expirationTime <= now)
                return false;

            return Set(key, data, CacheExpirationType.Absolute, (expirationTime - now).TotalMinutes);
        }


        public bool Contains(string key)
        {
            if (!key.HasValue())
                return false;

            return cache.Contains(key);
        }


        public object Get(string key)
        {
            if (!key.HasValue())
                return null;

            return cache.Get(key);
        }

        public T Get<T>(string key)
        {
            if (!key.HasValue())
                return default(T);

            var result = cache.Get(key);
            if (result == null)
                return default(T);

            return (T)result;
        }

        public IEnumerable<string> GetAllKeys()
        {
            return cache.ToArray().Select(i => i.Key);
        }


        public bool Remove(string key)
        {
            return cache.Remove(key) != null;
        }

        public void RemoveAll()
        {
            foreach (var key in GetAllKeys())
            {
                Remove(key);
            }
        }
    }
}
