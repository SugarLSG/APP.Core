﻿namespace APP.Core.Caches
{
    /// <summary>
    /// 缓存引擎
    /// </summary>
    public enum CacheEngine
    {
        /// <summary>
        /// HTTP 缓存
        /// </summary>
        HTTPCache,
        /// <summary>
        /// Memory 缓存
        /// </summary>
        MemoryCache,
        /// <summary>
        /// Redis 缓存
        /// </summary>
        RedisCache
    }
}
