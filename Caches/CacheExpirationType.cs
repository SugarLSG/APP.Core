﻿namespace APP.Core.Caches
{
    /// <summary>
    /// 缓存过期类型
    /// </summary>
    public enum CacheExpirationType
    {
        /// <summary>
        /// 不过期
        /// </summary>
        None,
        /// <summary>
        /// 指定绝对时间过期
        /// </summary>
        Absolute,
        /// <summary>
        /// 指定在最后一次访问后多久过期
        /// </summary>
        Sliding
    }
}
