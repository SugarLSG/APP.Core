﻿namespace APP.Core.Caches
{
    /// <summary>
    /// Redis 缓存方法
    /// </summary>
    public enum RedisCacheMethod
    {
        /// <summary>
        /// Key & Value
        /// </summary>
        Value = 1 << 0,
        /// <summary>
        /// Key & List
        /// </summary>
        List = 1 << 1,
        /// <summary>
        /// Key & Set
        /// </summary>
        Set = 1 << 2,
        /// <summary>
        /// Key & SortedSet
        /// </summary>
        SortedSet = 1 << 3
    }
}
