﻿using System;
using System.Collections.Generic;

namespace APP.Core.Caches
{
    public interface ICacheProvider
    {
        /// <summary>
        /// 添加1个缓存项（若已存在该 Key，更新数据）
        /// </summary>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <param name="expirationType"></param>
        /// <param name="cacheMinutes">expirationType != None，必须设置 cacheMinutes</param>
        /// <returns></returns>
        bool Set(string key, object data, CacheExpirationType expirationType, double? cacheMinutes = null);

        /// <summary>
        /// 添加1个缓存项，并在指定时间过期（若已存在该 Key，更新数据）
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="data"></param>
        /// <param name="expirationTime"></param>
        /// <returns></returns>
        bool Set(string key, object data, DateTime expirationTime);


        /// <summary>
        /// 判断是否有指定 Key 的缓存项
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        bool Contains(string key);


        /// <summary>
        /// 获取指定 Key 的缓存项
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        object Get(string key);

        /// <summary>
        /// 获取指定类型 Key 的缓存项
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        T Get<T>(string key);

        /// <summary>
        /// 获取所有缓存项的 Key
        /// </summary>
        /// <returns></returns>
        IEnumerable<string> GetAllKeys();


        /// <summary>
        /// 删除指定 Key 的缓存项
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        bool Remove(string key);

        /// <summary>
        /// 删除所有缓存项
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        void RemoveAll();
    }
}
