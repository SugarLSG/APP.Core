﻿using System.Collections.Generic;

namespace APP.Core.Models
{
    public class ExportExcelInfo
    {
        /// <summary>
        /// Sheet 名称
        /// </summary>
        public string SheetName { get; set; }

        /// <summary>
        /// 顶部标题行
        /// </summary>
        public string[] HeaderCells { get; set; }

        /// <summary>
        /// 数据行
        /// </summary>
        public IEnumerable<object[]> Rows { get; set; }
    }
}
