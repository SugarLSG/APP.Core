﻿using APP.Core.Enums;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace APP.Core.Models
{
    public class ChartSeriesInfo
    {
        /// <summary>
        /// 类型
        /// </summary>
        [JsonIgnore]
        public ChartType Type { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        [JsonProperty("type")]
        public string TypeName { get { return Type.ToString().ToLower(); } }

        /// <summary>
        /// 显示名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 数据
        /// </summary>
        public IEnumerable<ChartSeriesItemInfo> Data { get; set; }

        /// <summary>
        /// 颜色（不适用 Pie 类型）
        /// </summary>
        public string Color { get; set; }
    }

    public class ChartSeriesItemInfo
    {
        /// <summary>
        /// X轴描述（不适用 Pie 类型）
        /// </summary>
        public string XAxis { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        [JsonProperty("y")]
        public double Value { get; set; }

        /// <summary>
        /// 颜色（仅适用 Pie 类型）
        /// </summary>
        public string Color { get; set; }
    }
}
