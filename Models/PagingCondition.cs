﻿using APP.Core.Enums;
using System.Collections.Generic;

namespace APP.Core.Models
{
    public class PagingCondition
    {
        /// <summary>
        /// 页码（从 1 开始）
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// 查询数量
        /// </summary>
        public int Size { get; set; }

        /// <summary>
        /// 排序（key: 字段名，value: 排序方式）
        /// </summary>
        public IDictionary<string, Sorted> Sorts { get; set; }

        /// <summary>
        /// 查询首项
        /// </summary>
        public int FirstIndex { get { return (Page - 1) * Size; } }
    }
}
