﻿using APP.Core.Enums;

namespace APP.Core.Models
{
    public class ExportWordInfo
    {
        /// <summary>
        /// 数据行
        /// </summary>
        public ExportWordInfoRow[] Rows { get; set; }
    }

    public class ExportWordInfoRow
    {
        /// <summary>
        /// 文本对齐（默认左对齐）
        /// </summary>
        public Alignment? TextAlign { get; set; }


        /// <summary>
        /// 段前间距（20=1榜，默认40）
        /// </summary>
        public int? SpacingBefore { get; set; }

        /// <summary>
        /// 段后间距（20=1榜，默认40）
        /// </summary>
        public int? SpacingAfter { get; set; }


        /// <summary>
        /// 数据列（允许空）
        /// </summary>
        public ExportWordInfoRowItem[] Items { get; set; }
    }

    public class ExportWordInfoRowItem
    {
        /// <summary>
        /// 字体名称
        /// </summary>
        public string FontName { get; set; }

        /// <summary>
        /// 字体大小（默认12）
        /// </summary>
        public int? FontSize { get; set; }

        /// <summary>
        /// 是否粗体（默认false）
        /// </summary>
        public bool? IsBold { get; set; }

        /// <summary>
        /// 是否斜体（默认false）
        /// </summary>
        public bool? IsItalic { get; set; }

        /// <summary>
        /// 是否下划线（默认false）
        /// </summary>
        public bool? IsUnderline { get; set; }

        /// <summary>
        /// 颜色RGB值（默认000000）
        /// </summary>
        public string ColorRGBValue { get; set; }


        /// <summary>
        /// 文字
        /// </summary>
        public string Text { get; set; }
    }
}
