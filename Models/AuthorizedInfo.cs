﻿namespace APP.Core.Models
{
    public class AuthorizedInfo
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 用户账号
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 用户名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 头像路径
        /// </summary>
        public string HeadImageUrl { get; set; }
    }
}
