﻿using System;
using System.Collections.Generic;

namespace APP.Core.Models
{
    public class PagingResult<TResult>
    {
        /// <summary>
        /// 数据列表
        /// （当前查询结果）
        /// </summary>
        public IList<TResult> Data { get; set; }

        /// <summary>
        /// 附加数据
        /// </summary>
        public object Additional { get; set; }


        /// <summary>
        /// 查询数量
        /// </summary>
        public int Size { get; set; }

        /// <summary>
        /// 数据总数
        /// </summary>
        public int TotalCount { get; set; }

        /// <summary>
        /// 页面总数
        /// </summary>
        public int PageCount { get { return (int)Math.Ceiling((double)TotalCount / Size); } }
    }
}
