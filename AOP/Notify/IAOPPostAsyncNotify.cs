﻿using System.Reflection;

namespace APP.Core.AOP.Notify
{
    /// <summary>
    /// 后置异步通知
    /// </summary>
    public interface IAOPPostAsyncNotify
    {
        void Post(MethodInfo methodInfo, object[] args, object returnValue);
    }
}
