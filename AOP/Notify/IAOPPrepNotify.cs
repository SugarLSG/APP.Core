﻿using System.Reflection;

namespace APP.Core.AOP.Notify
{
    /// <summary>
    /// 前置通知
    /// </summary>
    public interface IAOPPrepNotify
    {
        void Prep(MethodInfo methodInfo, object[] args);
    }
}
