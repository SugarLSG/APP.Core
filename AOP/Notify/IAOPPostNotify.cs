﻿using System.Reflection;

namespace APP.Core.AOP.Notify
{
    /// <summary>
    /// 后置通知
    /// </summary>
    public interface IAOPPostNotify
    {
        void Post(MethodInfo methodInfo, object[] args, object returnValue);
    }
}
