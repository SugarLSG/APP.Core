﻿using APP.Core.Caches;
using System;

namespace APP.Core.AOP.Attributes
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class CacheAttribute : Attribute
    {
        public CacheExpirationType ExpirationType { get; }
        public double? CacheMinutes { get; }
        public string[] CacheParams { get; }

        /// <summary>
        /// 永久缓存（不过期）
        /// </summary>
        /// <param name="cacheParams">要加入缓存的参数，不传默认所有；null 为忽略所有；需指定参数请分别传入参数名</param>
        public CacheAttribute(params string[] cacheParams)
        {
            ExpirationType = CacheExpirationType.None;
            CacheParams = cacheParams;
        }

        /// <summary>
        /// 缓存
        /// </summary>
        /// <param name="expirationType">过期类型</param>
        /// <param name="cacheMinutes">缓存时间，仅当 expirationType != None 生效</param>
        /// <param name="cacheParams">要加入缓存的参数，不传默认所有；null 为忽略所有；需指定参数请分别传入参数名</param>
        public CacheAttribute(CacheExpirationType expirationType, double cacheMinutes, params string[] cacheParams)
        {
            if (expirationType != CacheExpirationType.None && cacheMinutes <= 0)
                throw new ArgumentException("缓存时间必须大于 0");

            ExpirationType = expirationType;
            CacheMinutes = cacheMinutes;
            CacheParams = cacheParams;
        }
    }
}
