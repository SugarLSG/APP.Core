﻿using System;

namespace APP.Core.AOP.Attributes
{
    /// <summary>
    /// 将方法改为异步执行
    /// 要求：仅支持方法返回类型为 void；建议在方法体内部自行处理异常；调用方式必须符合 AOP 配置；
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class AsyncAttribute : Attribute
    {
    }
}
