﻿using APP.Core.AOP.Notify;
using System;
using System.Linq;

namespace APP.Core.AOP.Attributes
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class NotifyAttribute : Attribute
    {
        public Type[] Notifies { get; }

        /// <summary>
        /// 仅支持继承自 IAOPPrepNotify 或 IAOPPostAsyncNotify 或 IAOPPostNotify 的类型
        /// </summary>
        /// <param name="notifies"></param>
        public NotifyAttribute(params Type[] notifies)
        {
            if (notifies == null || notifies.Length == 0)
                throw new ArgumentNullException("notifies");
            else if (notifies.Any(t => !(typeof(IAOPPrepNotify).IsAssignableFrom(t) || typeof(IAOPPostAsyncNotify).IsAssignableFrom(t) || typeof(IAOPPostNotify).IsAssignableFrom(t))))
                throw new ArgumentException("仅支持继承自 IAOPPrepNotify 或 IAOPPostNotify 的类型");

            Notifies = notifies.Distinct().ToArray();
        }
    }
}
