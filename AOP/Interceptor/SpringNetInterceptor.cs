﻿using AopAlliance.Intercept;
using APP.Core.AOP.Attributes;
using APP.Core.AOP.Notify;
using APP.Core.Utilities;
using Spring.Context.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace APP.Core.AOP.Interceptor
{
    public class SpringNetInterceptor : IMethodInterceptor
    {
        private static readonly Type AOPPREPNOTIFY_TYPE = typeof(IAOPPrepNotify);
        private static readonly Type AOPPOSTNOTIFY_TYPE = typeof(IAOPPostNotify);
        private static readonly Type AOPPOSTASYNCNOTIFY_TYPE = typeof(IAOPPostAsyncNotify);


        public object Invoke(IMethodInvocation invocation)
        {
            object returnValue = null;

            // 前置通知
            var aopNotifyAttribute = invocation.Method.GetCustomAttribute<NotifyAttribute>();
            if (aopNotifyAttribute != null)
                // 前置，returnMessage 为 null
                ExecuteAOPPrepNotifyLogic(invocation, aopNotifyAttribute);

            // 是否是无返回结果的方法体
            var isVoidReturnType = invocation.Method.ReturnType == typeof(void);

            // 缓存
            var cacheAttribute = invocation.Method.GetCustomAttribute<CacheAttribute>();
            // 已配置缓存
            // 且必须有返回结果
            if (cacheAttribute != null && !isVoidReturnType)
                returnValue = ExecuteCacheLogic(invocation, cacheAttribute);
            else
            {
                // 异步
                var asyncAttribute = invocation.Method.GetCustomAttribute<AsyncAttribute>();
                // 已配置异步
                // 且必须无返回结果
                if (asyncAttribute != null && isVoidReturnType)
                    ExecuteAsyncLogic(invocation);
                else
                    returnValue = invocation.Proceed();
            }

            // 后置通知
            if (aopNotifyAttribute != null)
                // 后置，returnMessage 不为 null
                ExecuteAOPPostNotifyLogic(invocation, returnValue, aopNotifyAttribute);

            return returnValue;
        }


        /// <summary>
        /// 执行缓存逻辑
        /// </summary>
        /// <param name="invocation"></param>
        /// <param name="cacheAttribute"></param>
        /// <returns></returns>
        private object ExecuteCacheLogic(IMethodInvocation invocation, CacheAttribute cacheAttribute)
        {
            // 缓存 Key
            var cacheKeyBuilder = new StringBuilder();
            cacheKeyBuilder.AppendFormat("{0}{1}.{2}@", CoreConfig.CACHEKEY_METHODRETURN_PREFIX, invocation.TargetType.FullName, invocation.Method.Name);
            var cacheParams = string.Join(
                "|",
                invocation.Method
                    .GetParameters()
                    .Select((p, i) => new KeyValuePair<string, object>(p.Name, invocation.Arguments[i]))
                    .Where(p => cacheAttribute.CacheParams == null || !cacheAttribute.CacheParams.Any() || cacheAttribute.CacheParams.Contains(p.Key))
                    .Select(p => string.Format("#{0}:{1}#", p.Key, JsonUtility.Serialize(p.Value)))
            );
            cacheKeyBuilder.Append(SecurityUtility.MD5Encrypt_16(cacheParams));
            var cacheKey = cacheKeyBuilder.ToString();

            // 过期类型
            var expirationType = cacheAttribute.ExpirationType;
            // 缓存时间
            var cacheMinutes = cacheAttribute.CacheMinutes;

            return CacheUtility.GetOrSetFromCache(
                cacheKey,
                () => { return invocation.Proceed(); },
                expirationType: expirationType,
                cacheMinutes: cacheMinutes
            );
        }

        /// <summary>
        /// 执行异步逻辑
        /// </summary>
        /// <param name="invocation"></param>
        private void ExecuteAsyncLogic(IMethodInvocation invocation)
        {
            Task.Run(() => { invocation.Proceed(); });
        }

        /// <summary>
        /// 执行 AOP 前置通知逻辑
        /// </summary>
        /// <param name="invocation"></param>
        /// <param name="aopNotifyAttribute"></param>
        private void ExecuteAOPPrepNotifyLogic(IMethodInvocation invocation, NotifyAttribute aopNotifyAttribute)
        {
            foreach (var type in aopNotifyAttribute.Notifies.Where(t => AOPPREPNOTIFY_TYPE.IsAssignableFrom(t)))
            {
                var context = ContextRegistry.GetContext();
                var setName = type.Name.ToLower();
                var notify = (context.ContainsObject(setName) ? context.GetObject(setName) : Singleton.Instance(type)) as IAOPPrepNotify;
                notify.Prep(invocation.Method, invocation.Arguments);
            }
        }

        /// <summary>
        /// 执行 AOP 后置通知逻辑
        /// </summary>
        /// <param name="invocation"></param>
        /// <param name="returnValue"></param>
        /// <param name="aopNotifyAttribute"></param>
        private void ExecuteAOPPostNotifyLogic(IMethodInvocation invocation, object returnValue, NotifyAttribute aopNotifyAttribute)
        {
            // 执行对应设置类型
            foreach (var type in aopNotifyAttribute.Notifies.Where(t => AOPPOSTNOTIFY_TYPE.IsAssignableFrom(t) || AOPPOSTASYNCNOTIFY_TYPE.IsAssignableFrom(t)))
            {
                var context = ContextRegistry.GetContext();
                var setName = type.Name.ToLower();

                // 同步
                if (AOPPOSTNOTIFY_TYPE.IsAssignableFrom(type))
                {
                    var notify = context.ContainsObject(setName) ? context.GetObject(setName) as IAOPPostNotify : null;
                    notify?.Post(invocation.Method, invocation.Arguments, returnValue);
                }
                // 异步
                else if (AOPPOSTASYNCNOTIFY_TYPE.IsAssignableFrom(type))
                {
                    var notify = context.ContainsObject(setName) ? context.GetObject(setName) as IAOPPostAsyncNotify : null;
                    Task.Run(() => { notify?.Post(invocation.Method, invocation.Arguments, returnValue); });
                }
            }
        }
    }
}
