﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace APP.Core.DB
{
    public class MySqlDBSchema
    {
        public IEnumerable<TableInfo> Tables { get; }
        public IDictionary<string, IEnumerable<ColumnInfo>> ColumnDic { get; }
        public IDictionary<string, IEnumerable<ForeignKeyInfo>> ForeignDic { get; }


        public MySqlDBSchema(string connString)
        {
            using (var conn = new MySqlConnection(connString))
            {
                conn.Open();
                Tables = GetTableList(conn);
                var columns = GetColumnList(conn);
                ColumnDic = Tables.ToDictionary(t => t.TableName, t => columns.Where(c => c.TableName == t.TableName));
                var foreigns = GetForeignList(conn);
                ForeignDic = Tables.ToDictionary(t => t.TableName, t => foreigns.Where(f => f.TableName == t.TableName));
            }
        }


        private IList<TableInfo> GetTableList(IDbConnection conn)
        {
            var cmd = conn.CreateCommand();
            cmd.CommandText = string.Format(@"
SELECT TABLE_NAME AS TABLENAME, TABLE_COMMENT AS DESCRIPTIONS
  FROM INFORMATION_SCHEMA.TABLES
 WHERE TABLE_SCHEMA = '{0}'",
            conn.Database);

            var tableList = new List<TableInfo>();
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    tableList.Add(new TableInfo
                    {
                        TableName = Convert.ToString(reader[0]),
                        Description = Convert.ToString(reader[1])
                    });
                }
            }
            return tableList;
        }

        private IList<ColumnInfo> GetColumnList(IDbConnection conn)
        {
            var cmd = conn.CreateCommand();
            cmd.CommandText = string.Format(@"
SELECT TABLE_NAME AS TABLENAME,
       COLUMN_NAME AS COLUMNNAME,
       DATA_TYPE AS COLUMNTYPE,
       CASE WHEN COLUMN_KEY = 'PRI' THEN 1 ELSE 0 END AS ISPRIMARYKEY,
       CASE WHEN EXTRA = 'auto_increment' THEN 1 ELSE 0 END AS ISIDENTITY,
       CASE WHEN IS_NULLABLE = 'YES' THEN 1 ELSE 0 END AS ISALLOWNULL,
       IFNULL(CHARACTER_MAXIMUM_LENGTH, -1) AS LENGTH,
       COLUMN_COMMENT AS DESCRIPTIONS
  FROM INFORMATION_SCHEMA.COLUMNS
 WHERE TABLE_SCHEMA = '{0}'
ORDER BY ORDINAL_POSITION",
            conn.Database);

            var columnList = new List<ColumnInfo>();
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    columnList.Add(new ColumnInfo
                    {
                        TableName = Convert.ToString(reader[0]),
                        ColumnName = Convert.ToString(reader[1]),
                        ColumnType = Convert.ToString(reader[2]),
                        IsPrimaryKey = Convert.ToBoolean(reader[3]),
                        IsIdentity = Convert.ToBoolean(reader[4]),
                        IsAllowNull = Convert.ToBoolean(reader[5]),
                        Length = Convert.ToInt32(reader[6]),
                        Description = Convert.ToString(reader[7])
                    });
                }
            }
            return columnList;
        }

        private IList<ForeignKeyInfo> GetForeignList(IDbConnection conn)
        {
            var cmd = conn.CreateCommand();
            cmd.CommandText = string.Format(@"
SELECT TABLE_NAME AS TABLENAME,
       COLUMN_NAME AS COLUMNNAME,
       REFERENCED_TABLE_NAME AS FK_TABLENAME,
       REFERENCED_COLUMN_NAME AS FK_COLUMNNAME,
       CONSTRAINT_NAME AS CONSTRAINTNAME
  FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
 WHERE CONSTRAINT_SCHEMA = '{0}' AND REFERENCED_TABLE_NAME IS NOT NULL",
            conn.Database);

            var infoList = new List<ForeignKeyInfo>();
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    infoList.Add(new ForeignKeyInfo
                    {
                        TableName = Convert.ToString(reader[0]),
                        ColumnName = Convert.ToString(reader[1]),
                        FK_TableName = Convert.ToString(reader[2]),
                        FK_ColumnName = Convert.ToString(reader[3]),
                        ConstraintName = Convert.ToString(reader[4])
                    });
                }
            }
            return infoList;
        }
    }
}
