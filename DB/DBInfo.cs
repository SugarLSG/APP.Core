﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace APP.Core.DB
{
    public enum DBType
    {
        /// <summary>
        /// SQL Server
        /// </summary>
        MSSQL,

        /// <summary>
        /// My SQL
        /// </summary>
        MYSQL
    }

    public class TableInfo
    {
        /// <summary>
        /// 表名
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// 表描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 表描述列表
        /// </summary>
        public IEnumerable<string> Descriptions
        {
            get { return DBInfoHelper.ConvertDescriptions(Description); }
        }
    }

    public class ColumnInfo
    {
        /// <summary>
        /// 表名
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// 列名
        /// </summary>
        public string ColumnName { get; set; }

        private string _columnType;
        /// <summary>
        /// 列类型
        /// </summary>
        public string ColumnType
        {
            get { return DBInfoHelper.ConvertColumnType(_columnType, EnumTypeInfo); }
            set { _columnType = value; }
        }

        /// <summary>
        /// 是否主键
        /// </summary>
        public bool IsPrimaryKey { get; set; }

        /// <summary>
        /// 是否标识列
        /// </summary>
        public bool IsIdentity { get; set; }

        /// <summary>
        /// 是否可空
        /// </summary>
        public bool IsAllowNull { get; set; }

        /// <summary>
        /// 列值是否可空
        /// </summary>
        public bool IsColumnValueNullable
        {
            get { return IsPrimaryKey || IsAllowNull; }
        }

        /// <summary>
        /// 列类型是否可空
        /// </summary>
        public bool IsColumnTypeNullable
        {
            get
            // 字符串，均默认类型可空（类型无需加"？"）
            { return ColumnType != "string" && IsColumnValueNullable; }
        }

        private int _length { get; set; }
        /// <summary>
        /// 列长度
        /// </summary>
        public int Length
        {
            get { return DBInfoHelper.ConvertColumnTypeLength(_columnType, _length); }
            set { _length = value; }
        }

        /// <summary>
        /// 列描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 列描述列表
        /// </summary>
        public IEnumerable<string> Descriptions
        {
            get { return DBInfoHelper.ConvertDescriptions(Description); }
        }

        /// <summary>
        /// 枚举类型信息
        /// </summary>
        public EnumTypeInfo EnumTypeInfo
        {
            get
            {
                return DBInfoHelper.GetEnumMark(Descriptions.ToArray(), TableName, ColumnName);
            }
        }
    }

    public class ForeignKeyInfo
    {
        /// <summary>
        /// 外键表名
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// 外键列名
        /// </summary>
        public string ColumnName { get; set; }

        /// <summary>
        /// 外键列属性名
        /// </summary>
        public string ColumnPropertyName
        {
            get { return DBInfoHelper.ConvertFKPropertyName(ColumnName); }
        }

        /// <summary>
        /// 外键对应的主键所在表名
        /// </summary>
        public string FK_TableName { get; set; }

        /// <summary>
        /// 外键对应的主键名
        /// </summary>
        public string FK_ColumnName { get; set; }

        /// <summary>
        /// 外键约束的名称
        /// </summary>
        public string ConstraintName { get; set; }
    }

    public class EnumTypeInfo
    {
        /// <summary>
        /// 类型名称
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// 值类型是否字符串
        /// </summary>
        public bool IsStringValue { get; set; }

        /// <summary>
        /// 值列表
        /// Item1: Value
        /// Item2: ENName
        /// Item3: CNName
        /// </summary>
        public IList<Tuple<string, string, string>> Values { get; set; }

        /// <summary>
        /// 枚举描述
        /// </summary>
        public IEnumerable<string> Descriptions { get; set; }
    }


    public static class DBInfoHelper
    {
        private static string[] fkNameSigns = new string[] { "id", "Id", "ID", "code", "Code", "CODE" };
        private static char[] endSigns = new char[] { '.', ';', ':', ',', '。', '；', '：', '，' };
        private static string[] doubleByteSigns = new string[] { "nchar", "nvarchar", "ntext" };

        private static string TrimEndSigns(string str)
        {
            str = str.Trim();
            foreach (var endSign in endSigns)
                str = str.TrimEnd(endSign);

            return str;
        }

        internal static IEnumerable<string> ConvertDescriptions(string description)
        {
            if (!description.HasValue())
                return new string[] { string.Empty };

            return description.Replace("\n", "").Split('\r').Select(d => TrimEndSigns(d));
        }

        /// <summary>
        /// [ENUM,ValueType〔,EnumType〕]
        /// Value-ENName-CNName
        /// ...
        /// </summary>
        /// <param name="descriptions"></param>
        /// <param name="tableName"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        internal static EnumTypeInfo GetEnumMark(string[] descriptions, string tableName, string columnName)
        {
            EnumTypeInfo info = null;

            for (int i = 0, length = descriptions.Length; i < length; ++i)
            {
                var d = descriptions[i];
                if (Regex.IsMatch(d, "ENUM\\[([^,]+)[,]?(\\S+)?\\]"))
                {
                    var match = Regex.Match(d, "ENUM\\[([^,]+)[,]?(\\S+)?\\]");

                    var typeName = match.Groups.Count > 2 ? match.Groups[2].Value : null;
                    info = new EnumTypeInfo
                    {
                        TypeName = typeName.HasValue() ? typeName : ConvertTableName(tableName) + columnName,
                        IsStringValue = match.Groups.Count > 1 ? match.Groups[1].Value.ToUpper() == "STRING" : false,
                        Descriptions = descriptions
                    };

                    if (!typeName.HasValue())
                    {
                        info.Values = new List<Tuple<string, string, string>>();
                        for (var j = i + 1; j < length; ++j)
                        {
                            var m = Regex.Match(descriptions[j], "(-?\\w+)-(\\w+)-(.+)");
                            var value = info.IsStringValue ? null : m.Groups[1].Value;
                            var enName = m.Groups[2].Value;
                            var cnName = m.Groups[3].Value;
                            info.Values.Add(new Tuple<string, string, string>(value, enName, cnName));
                        }
                    }

                    break;
                }
            }

            return info;
        }

        internal static int ConvertColumnTypeLength(string columnType, int length)
        {
            return length == -1 ?
                int.MaxValue :
                (doubleByteSigns.Contains(columnType) ? length / 2 : length);
        }

        internal static string ConvertFKPropertyName(string columnName)
        {
            foreach (var fkNameSign in fkNameSigns)
            {
                if (columnName.EndsWith(fkNameSign))
                    columnName = columnName.Replace(fkNameSign, string.Empty);
            }

            return columnName;
        }

        public static string ConvertTableName(string tableName)
        {
            return string.Join(string.Empty, tableName.Replace("$", "_")
                .Split('_')
                .Select(i =>
                {
                    if (!i.HasValue())
                        return i;

                    var first = i.Substring(0, 1).ToUpper();
                    return i.Length > 1 ?
                        first + i.Substring(1) :
                        first;
                }));
        }

        public static IEnumerable<string> ConvertFKPropertyDescriptions(string columnName, string fk_tableName)
        {
            return new string[] { string.Format("外键 {0} 对应 {1} 类实例;", TrimEndSigns(columnName), fk_tableName) };
        }

        internal static string ConvertColumnType(string type, EnumTypeInfo info)
        {
            // 判断是否枚举类型
            if (info != null)
                return info.TypeName;
            else
            {
                switch (type)
                {
                    case "bfile":
                    case "binary":
                    case "image":
                    case "varbinary":
                    case "mediumtext":
                    case "long":
                        return "byte[]";

                    case "char":
                    case "nchar":
                    case "varchar":
                    case "nvarchar":
                    case "text":
                    case "ntext":
                    case "longtext":
                    case "sysname":
                    case "string":
                        return "string";

                    case "time":
                        return "TimeSpan";

                    case "date":
                    case "datetime":
                    case "smalldatetime":
                    case "timestamp":
                        return "DateTime";

                    case "uniqueide":
                    case "uniqueidentifier":
                        return "Guid";

                    case "bit":
                    case "boolean":
                        return "bool";

                    case "integer":
                    case "int":
                    case "smallint":
                    case "tinyint":
                        return "int";

                    case "bigint":
                        return "long";

                    case "numeric":
                    case "money":
                    case "real":
                    case "smallmoney":
                    case "float":
                    case "decimal":
                    case "number":
                        return "double";

                    default:
                        return type;
                }
            }
        }
    }
}
