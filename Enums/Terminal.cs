﻿using System.ComponentModel;

namespace APP.Core.Enums
{
    /// <summary>
    /// 终端类型
    /// </summary>
    public enum Terminal
    {
        /// <summary>
        /// iOS
        /// </summary>
        [Description("iOS")]
        iOS,

        /// <summary>
        /// Android
        /// </summary>
        [Description("Android")]
        Android,

        /// <summary>
        /// H5
        /// </summary>
        [Description("H5")]
        H5
    }
}
