﻿using System.ComponentModel;

namespace APP.Core.Enums
{
    /// <summary>
    /// 对齐方式
    /// </summary>
    public enum Alignment
    {
        /// <summary>
        /// 左对齐
        /// </summary>
        [Description("左对齐")]
        Left,

        /// <summary>
        /// 中对齐
        /// </summary>
        [Description("中对齐")]
        Center,

        /// <summary>
        /// 右对齐
        /// </summary>
        [Description("右对齐")]
        Right
    }
}
