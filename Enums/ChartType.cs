﻿using System.ComponentModel;

namespace APP.Core.Enums
{
    /// <summary>
    /// 图表类型
    /// </summary>
    public enum ChartType
    {
        /// <summary>
        /// 曲线图
        /// </summary>
        [Description("曲线图")]
        Spline,

        /// <summary>
        /// 柱形图
        /// </summary>
        [Description("柱形图")]
        Column,

        /// <summary>
        /// 饼图
        /// </summary>
        [Description("饼图")]
        Pie,

        /// <summary>
        /// 面积图
        /// </summary>
        [Description("面积图")]
        Area,
    }
}
