﻿using System.ComponentModel;

namespace APP.Core.Enums
{
    /// <summary>
    /// 系统环境
    /// </summary>
    public enum SystemEnvironment
    {
        /// <summary>
        /// DEV
        /// </summary>
        [Description("DEV")]
        DEV,

        /// <summary>
        /// SIT
        /// </summary>
        [Description("SIT")]
        SIT,

        /// <summary>
        /// UAT
        /// </summary>
        [Description("UAT")]
        UAT,

        /// <summary>
        /// PRO
        /// </summary>
        [Description("PRO")]
        PRO
    }
}
