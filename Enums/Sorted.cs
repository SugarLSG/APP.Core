﻿using System.ComponentModel;

namespace APP.Core.Enums
{
    /// <summary>
    /// 排序
    /// </summary>
    public enum Sorted
    {
        /// <summary>
        /// 升序
        /// </summary>
        [Description("ASC")]
        Asc,

        /// <summary>
        /// 降序
        /// </summary>
        [Description("DESC")]
        Desc
    }
}
