﻿using System;
using System.Collections.Specialized;
using System.Configuration;

namespace APP.Core.Utilities
{
    public static class ConfigurationUtility
    {
        #region AppSettings

        public static string GetString(string key, string defaultValue = null)
        {
            var value = ConfigurationManager.AppSettings[key];
            return value ?? defaultValue;
        }
        public static bool GetBool(string key, bool defaultValue = false)
        {
            var valueStr = GetString(key, null);
            return valueStr.HasValue() ? Convert.ToBoolean(valueStr) : defaultValue;
        }
        public static int GetInt(string key, int defaultValue = 0)
        {
            var valueStr = GetString(key, null);
            return valueStr.HasValue() ? Convert.ToInt32(valueStr) : defaultValue;
        }
        public static long GetLong(string key, long defaultValue = 0)
        {
            var valueStr = GetString(key, null);
            return valueStr.HasValue() ? Convert.ToInt64(valueStr) : defaultValue;
        }
        public static double GetDouble(string key, double defaultValue = 0)
        {
            var valueStr = GetString(key, null);
            return valueStr.HasValue() ? Convert.ToDouble(valueStr) : defaultValue;
        }

        #endregion

        #region Sections

        public static string GetSectionString(string section, string key, string defaultValue = null)
        {
            var sectionSettings = ConfigurationManager.GetSection(section) as NameValueCollection;
            return sectionSettings == null ? defaultValue : sectionSettings[key];
        }
        public static bool GetSectionBool(string section, string key, bool defaultValue = false)
        {
            var valueStr = GetSectionString(section, key, null);
            return valueStr.HasValue() ? Convert.ToBoolean(valueStr) : defaultValue;
        }
        public static int GetSectionInt(string section, string key, int defaultValue = 0)
        {
            var valueStr = GetSectionString(section, key, null);
            return valueStr.HasValue() ? Convert.ToInt32(valueStr) : defaultValue;
        }
        public static long GetSectionLong(string section, string key, long defaultValue = 0)
        {
            var valueStr = GetSectionString(section, key, null);
            return valueStr.HasValue() ? Convert.ToInt64(valueStr) : defaultValue;
        }
        public static double GetSectionDouble(string section, string key, double defaultValue = 0)
        {
            var valueStr = GetSectionString(section, key, null);
            return valueStr.HasValue() ? Convert.ToDouble(valueStr) : defaultValue;
        }

        #endregion
    }
}
