﻿using System;
using System.Collections.Generic;

namespace APP.Core.Utilities
{
    public static class ThreadSafetyUtility
    {
        private static object _lock = new object();
        private static IDictionary<string, object> _lockList = new Dictionary<string, object>();


        /// <summary>
        /// 线程安全处理
        /// </summary>
        /// <param name="condition">执行 handle 的条件</param>
        /// <param name="handle">实际执行逻辑</param>
        public static void Handle(Func<bool> condition, Action handle)
        {
            Handle(CoreConfig.CACHEKEY_THREADSAFETYDEFAULTBUCKET, condition, handle);
        }

        /// <summary>
        /// 线程安全处理
        /// </summary>
        /// <param name="bucket">加锁分类</param>
        /// <param name="condition">执行 handle 的条件</param>
        /// <param name="handle">实际执行逻辑</param>
        public static void Handle(string bucket, Func<bool> condition, Action handle)
        {
            if (condition())
            {
                if (!_lockList.ContainsKey(bucket))
                {
                    lock (_lock)
                    {
                        if (!_lockList.ContainsKey(bucket))
                        {
                            _lockList.Add(bucket, new object());
                        }
                    }
                }

                var lockObj = _lockList[bucket];
                lock (lockObj)
                {
                    if (condition())
                    {
                        handle();
                    }
                }
            }
        }
    }
}
