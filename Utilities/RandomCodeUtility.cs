﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace APP.Core.Utilities
{
    public static class RandomCodeUtility
    {
        private static readonly Random _random = new Random();


        /// <summary>
        /// 生成随机码
        /// </summary>
        /// <param name="length"></param>
        /// <param name="randomCase">随机大小写</param>
        /// <returns></returns>
        public static string RandomCode(int length, bool randomCase = false)
        {
            var bytes = new byte[length / 2];
            new RNGCryptoServiceProvider().GetBytes(bytes);

            var codes = string.Join("", bytes.Select(i => string.Format("{0:X2}", i)));
            if (randomCase)
            {
                var sb = new StringBuilder();
                foreach (var c in codes)
                {
                    if (_random.Next() % 2 == 0)
                        sb.Append(c.ToString().ToUpper());
                    else
                        sb.Append(c.ToString().ToLower());
                }
                return sb.ToString();
            }

            return codes;
        }

        /// <summary>
        /// 生成随机数字码
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string RandomNumberCode(int length)
        {
            // 例：length = 4
            //   format = 0000
            //   maxNumber = 9999
            var format = string.Join(string.Empty, new int[length]);
            var maxNumber = Convert.ToInt64("1" + format) - 1;

            return (_random.NextDouble() * maxNumber).ToString(format);
        }
    }
}
