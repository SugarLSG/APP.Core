﻿using APP.Core.Models;
using System;
using System.Web;
using System.Web.Security;

namespace APP.Core.Utilities
{
    public class AuthenticationUtility
    {
        private const string DEFAULT_KEY = "AUTH";


        /// <summary>
        /// Cookie Key
        /// </summary>
        public static string CookieKey { get; private set; }
        /// <summary>
        /// Cookie 版本
        /// </summary>
        public static int CookieVersion { get; private set; }
        /// <summary>
        /// Cokie Path
        /// </summary>
        public static string CookiePath { get; private set; }
        /// <summary>
        /// Cokie Domain
        /// </summary>
        public static string CookieDomain { get; private set; }
        /// <summary>
        /// Cookie 过期时间（分钟）
        /// </summary>
        public static int CookieExpired { get; private set; }

        /// <summary>
        /// 初始化配置
        /// </summary>
        /// <param name="key">default: AUTH</param>
        /// <param name="version">default: 1</param>
        /// <param name="domain">default: null</param>
        /// <param name="path">default: /</param>
        /// <param name="expired">default: 480（分钟）</param>
        public static void Configure(string key = DEFAULT_KEY, int version = 1, string domain = null, string path = "/", int expired = 480)
        {
            CookieKey = key;
            CookieVersion = version;
            CookieDomain = domain;
            CookiePath = path;
            CookieExpired = expired;
        }


        /// <summary>
        /// 生成用户凭证Id
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        public static string GenerateAuthId(AuthorizedInfo userInfo)
        {
            return FormsAuthentication.Encrypt(new FormsAuthenticationTicket(
                CookieVersion,
                userInfo.Account,
                DateTime.Now,
                DateTime.Now.AddMinutes(CookieExpired),
                false,
                JsonUtility.Serialize(userInfo),
                CookiePath
            ));
        }

        /// <summary>
        /// 记录登录信息（Cookie）
        /// </summary>
        /// <param name="key"></param>
        /// <param name="authId"></param>
        public static void SignIn(string key, string authId)
        {
            // 生成 Cookie
            var authCookie = new HttpCookie(key, authId);
            authCookie.HttpOnly = true;
            authCookie.Path = CookiePath;
            authCookie.Domain = CookieDomain;

            HttpContext.Current.Request.Cookies.Remove(key);
            HttpContext.Current.Response.Cookies.Set(authCookie);
        }

        /// <summary>
        /// 记录登录信息（Cookie，使用默认配置）
        /// </summary>
        /// <param name="userInfo"></param>
        public static void SignIn(AuthorizedInfo userInfo)
        {
            SignIn(FormsAuthentication.FormsCookieName, GenerateAuthId(userInfo));
        }


        /// <summary>
        /// 删除登录信息（Cookie）
        /// </summary>
        /// <param name="key"></param>
        public static void SignOut(string key)
        {
            HttpContext.Current.Request.Cookies.Remove(key);
            var authCookie = new HttpCookie(key);
            authCookie.Expires = DateTime.Today.AddDays(-1);
            HttpContext.Current.Response.Cookies.Set(authCookie);
        }

        /// <summary>
        /// 删除登录信息（Cookie，使用默认配置）
        /// </summary>
        public static void SignOut()
        {
            SignOut(FormsAuthentication.FormsCookieName);
        }


        /// <summary>
        /// 获取授权Cookie
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        protected static HttpCookie AuthCookie(string key)
        {
            try
            {
                // 是否有Cookie
                var cookie = HttpContext.Current.Request.Cookies[key];
                if (cookie == null || !cookie.Value.HasValue())
                    return null;

                // Cookie是否已过期
                // Cookie版本是否正确
                var ticket = FormsAuthentication.Decrypt(cookie.Value);
                if (ticket.Expired)
                    return null;
                if (ticket.Version != CookieVersion)
                    return null;

                return cookie;
            }
            catch { return null; }
        }

        /// <summary>
        /// 获取授权Cookie（使用默认配置）
        /// </summary>
        /// <returns></returns>
        protected static HttpCookie AuthCookie()
        {
            return AuthCookie(CookieKey);
        }

        /// <summary>
        /// 获取已授权用户信息
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="authId"></param>
        /// <returns></returns>
        protected static T GetAuthorizedUserInfo<T>(string authId) where T : AuthorizedInfo
        {
            try
            {
                var ticket = FormsAuthentication.Decrypt(authId);
                return JsonUtility.Deserialize<T>(ticket.UserData);
            }
            catch
            {
                SignOut();
                return null;
            }
        }

        /// <summary>
        /// 获取已授权用户信息（Cookie，使用默认配置）
        /// </summary>
        protected static T GetAuthorizedUserInfo<T>() where T : AuthorizedInfo
        {
            var authCookie = AuthCookie();
            if (authCookie == null || !authCookie.Value.HasValue())
                return null;

            return GetAuthorizedUserInfo<T>(authCookie.Value);
        }

        /// <summary>
        /// 获取已授权用户Id（使用默认配置）
        /// </summary>
        public static int? AuthorizedUserId
        {
            get
            {
                var userInfo = GetAuthorizedUserInfo<AuthorizedInfo>();
                if (userInfo != null)
                    return userInfo.Id;

                return null;
            }
        }


        /// <summary>
        /// 是否已登录授权（Cookie，使用默认配置）
        /// </summary>
        public static bool IsAuthenticated()
        {
            try
            {
                // 是否有Cookie
                var authCookie = AuthCookie();
                if (authCookie == null)
                    return false;

                // 用户信息
                var ticket = FormsAuthentication.Decrypt(authCookie.Value);
                var userInfo = JsonUtility.Deserialize<AuthorizedInfo>(ticket.UserData);
                if (userInfo == null)
                    return false;

                return true;
            }
            catch { return false; }
        }

        /// <summary>
        /// 是否已登录授权
        /// </summary>
        /// <param name="authId"></param>
        /// <returns></returns>
        public static bool IsAuthenticated(string authId)
        {
            try
            {
                // 是否已过期
                // 版本是否正确
                var ticket = FormsAuthentication.Decrypt(authId);
                if (ticket.Expired)
                    return false;
                if (ticket.Version != CookieVersion)
                    return false;

                // 用户信息
                var userInfo = JsonUtility.Deserialize<AuthorizedInfo>(ticket.UserData);
                if (userInfo == null)
                    return false;

                return true;
            }
            catch { return false; }
        }
    }
}
