﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.security;
using Org.BouncyCastle.X509;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;

namespace APP.Core.Utilities
{
    public static class PdfUtility
    {
        public enum FontFamily
        {
            /// <summary>
            /// 宋体
            /// </summary>
            SIMSUN,
            /// <summary>
            /// 黑体
            /// </summary>
            SIMHEI,
            /// <summary>
            /// 雅黑
            /// </summary>
            MSYH
        }

        private class PdfSignatureContainer : IExternalSignatureContainer
        {
            private IDictionary<string, string> SigningSetting { get; set; }
            private Func<Stream, byte[]> HandleSign { get; set; }

            /// <summary>
            /// PDF签章
            /// </summary>
            /// <param name="signingSetting">签署配置</param>
            /// <param name="handleSign">签署信息（返回签署值）</param>
            public PdfSignatureContainer(IDictionary<string, string> signingSetting, Func<Stream, byte[]> handleSign)
            {
                if (handleSign == null)
                    throw new ArgumentNullException("handleSign");

                SigningSetting = signingSetting;
                HandleSign = handleSign;
            }

            public void ModifySigningDictionary(PdfDictionary signDic)
            {
                if (SigningSetting != null && SigningSetting.Any())
                {
                    foreach (var setting in SigningSetting)
                    {
                        var key = CacheUtility.GetOrSetFromCache(CACHEKEY_PDFNAME_PREFIX + setting.Key, () => PDFNAME_TYPE.GetField(setting.Key, BindingFlags.Public | BindingFlags.Static).GetValue(null) as PdfName);
                        var value = CacheUtility.GetOrSetFromCache(CACHEKEY_PDFNAME_PREFIX + setting.Value, () => PDFNAME_TYPE.GetField(setting.Value, BindingFlags.Public | BindingFlags.Static).GetValue(null) as PdfName);
                        signDic.Put(key, value);
                    }
                }
            }

            public byte[] Sign(Stream data)
            {
                return HandleSign(data);
            }
        }

        private class PdfSignature : IExternalSignature
        {
            private Func<string> HandleEncryptionAlgorithm { get; set; }
            private Func<string> HandleHashAlgorithm { get; set; }
            private Func<byte[], byte[]> HandleSign { get; set; }

            /// <summary>
            /// PDF签章
            /// </summary>
            /// <param name="handleEncryptionAlgorithm">加密算法</param>
            /// <param name="handleHashAlgorithm">Hash算法</param>
            /// <param name="handleSign">签署信息（返回签署值）</param>
            public PdfSignature(Func<string> handleEncryptionAlgorithm, Func<string> handleHashAlgorithm, Func<byte[], byte[]> handleSign)
            {
                if (handleEncryptionAlgorithm == null)
                    throw new ArgumentNullException("handleEncryptionAlgorithm");
                if (handleHashAlgorithm == null)
                    throw new ArgumentNullException("handleHashAlgorithm");
                if (handleSign == null)
                    throw new ArgumentNullException("handleSign");

                HandleEncryptionAlgorithm = handleEncryptionAlgorithm;
                HandleHashAlgorithm = handleHashAlgorithm;
                HandleSign = handleSign;
            }

            public string GetEncryptionAlgorithm()
            {
                return HandleEncryptionAlgorithm();
            }

            public string GetHashAlgorithm()
            {
                return HandleHashAlgorithm();
            }

            public byte[] Sign(byte[] message)
            {
                return HandleSign(message);
            }
        }

        public class FieldPositionInfo
        {
            public int Page { get; set; }

            public float X { get; set; }

            public float Y { get; set; }

            public float Width { get; set; }

            public float Height { get; set; }
        }

        public class FormFieldInfo
        {
            /// <summary>
            /// 仅对字体有效
            /// </summary>
            public FontFamily? FontFamily { get; set; }

            /// <summary>
            /// 仅对字体有效
            /// </summary>
            public float? FontSize { get; set; }

            /// <summary>
            /// 仅对字体有效
            /// </summary>
            public System.Drawing.Color? FontColor { get; set; }

            public string Content { get; set; }
        }


        private static readonly Type PDFNAME_TYPE = typeof(PdfName);
        private static readonly string CACHEKEY_PDFNAME_PREFIX = "PDFNAME_";

        private static readonly string FONTPATH_SIMSUN = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "simsun.ttc,0");
        private static readonly string FONTPATH_SIMHEI = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "simhei.ttf");
        private static readonly string FONTPATH_MSYH = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "msyh.ttc,0");
        private static readonly byte[] SIGNATURE_GRAPHIC_DATA = Convert.FromBase64String("iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAKTWlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVN3WJP3Fj7f92UPVkLY8LGXbIEAIiOsCMgQWaIQkgBhhBASQMWFiApWFBURnEhVxILVCkidiOKgKLhnQYqIWotVXDjuH9yntX167+3t+9f7vOec5/zOec8PgBESJpHmomoAOVKFPDrYH49PSMTJvYACFUjgBCAQ5svCZwXFAADwA3l4fnSwP/wBr28AAgBw1S4kEsfh/4O6UCZXACCRAOAiEucLAZBSAMguVMgUAMgYALBTs2QKAJQAAGx5fEIiAKoNAOz0ST4FANipk9wXANiiHKkIAI0BAJkoRyQCQLsAYFWBUiwCwMIAoKxAIi4EwK4BgFm2MkcCgL0FAHaOWJAPQGAAgJlCLMwAIDgCAEMeE80DIEwDoDDSv+CpX3CFuEgBAMDLlc2XS9IzFLiV0Bp38vDg4iHiwmyxQmEXKRBmCeQinJebIxNI5wNMzgwAABr50cH+OD+Q5+bk4eZm52zv9MWi/mvwbyI+IfHf/ryMAgQAEE7P79pf5eXWA3DHAbB1v2upWwDaVgBo3/ldM9sJoFoK0Hr5i3k4/EAenqFQyDwdHAoLC+0lYqG9MOOLPv8z4W/gi372/EAe/tt68ABxmkCZrcCjg/1xYW52rlKO58sEQjFu9+cj/seFf/2OKdHiNLFcLBWK8ViJuFAiTcd5uVKRRCHJleIS6X8y8R+W/QmTdw0ArIZPwE62B7XLbMB+7gECiw5Y0nYAQH7zLYwaC5EAEGc0Mnn3AACTv/mPQCsBAM2XpOMAALzoGFyolBdMxggAAESggSqwQQcMwRSswA6cwR28wBcCYQZEQAwkwDwQQgbkgBwKoRiWQRlUwDrYBLWwAxqgEZrhELTBMTgN5+ASXIHrcBcGYBiewhi8hgkEQcgIE2EhOogRYo7YIs4IF5mOBCJhSDSSgKQg6YgUUSLFyHKkAqlCapFdSCPyLXIUOY1cQPqQ28ggMor8irxHMZSBslED1AJ1QLmoHxqKxqBz0XQ0D12AlqJr0Rq0Hj2AtqKn0UvodXQAfYqOY4DRMQ5mjNlhXIyHRWCJWBomxxZj5Vg1Vo81Yx1YN3YVG8CeYe8IJAKLgBPsCF6EEMJsgpCQR1hMWEOoJewjtBK6CFcJg4Qxwicik6hPtCV6EvnEeGI6sZBYRqwm7iEeIZ4lXicOE1+TSCQOyZLkTgohJZAySQtJa0jbSC2kU6Q+0hBpnEwm65Btyd7kCLKArCCXkbeQD5BPkvvJw+S3FDrFiOJMCaIkUqSUEko1ZT/lBKWfMkKZoKpRzame1AiqiDqfWkltoHZQL1OHqRM0dZolzZsWQ8ukLaPV0JppZ2n3aC/pdLoJ3YMeRZfQl9Jr6Afp5+mD9HcMDYYNg8dIYigZaxl7GacYtxkvmUymBdOXmchUMNcyG5lnmA+Yb1VYKvYqfBWRyhKVOpVWlX6V56pUVXNVP9V5qgtUq1UPq15WfaZGVbNQ46kJ1Bar1akdVbupNq7OUndSj1DPUV+jvl/9gvpjDbKGhUaghkijVGO3xhmNIRbGMmXxWELWclYD6yxrmE1iW7L57Ex2Bfsbdi97TFNDc6pmrGaRZp3mcc0BDsax4PA52ZxKziHODc57LQMtPy2x1mqtZq1+rTfaetq+2mLtcu0W7eva73VwnUCdLJ31Om0693UJuja6UbqFutt1z+o+02PreekJ9cr1Dund0Uf1bfSj9Rfq79bv0R83MDQINpAZbDE4Y/DMkGPoa5hpuNHwhOGoEctoupHEaKPRSaMnuCbuh2fjNXgXPmasbxxirDTeZdxrPGFiaTLbpMSkxeS+Kc2Ua5pmutG003TMzMgs3KzYrMnsjjnVnGueYb7ZvNv8jYWlRZzFSos2i8eW2pZ8ywWWTZb3rJhWPlZ5VvVW16xJ1lzrLOtt1ldsUBtXmwybOpvLtqitm63Edptt3xTiFI8p0in1U27aMez87ArsmuwG7Tn2YfYl9m32zx3MHBId1jt0O3xydHXMdmxwvOuk4TTDqcSpw+lXZxtnoXOd8zUXpkuQyxKXdpcXU22niqdun3rLleUa7rrStdP1o5u7m9yt2W3U3cw9xX2r+00umxvJXcM970H08PdY4nHM452nm6fC85DnL152Xlle+70eT7OcJp7WMG3I28Rb4L3Le2A6Pj1l+s7pAz7GPgKfep+Hvqa+It89viN+1n6Zfgf8nvs7+sv9j/i/4XnyFvFOBWABwQHlAb2BGoGzA2sDHwSZBKUHNQWNBbsGLww+FUIMCQ1ZH3KTb8AX8hv5YzPcZyya0RXKCJ0VWhv6MMwmTB7WEY6GzwjfEH5vpvlM6cy2CIjgR2yIuB9pGZkX+X0UKSoyqi7qUbRTdHF09yzWrORZ+2e9jvGPqYy5O9tqtnJ2Z6xqbFJsY+ybuIC4qriBeIf4RfGXEnQTJAntieTE2MQ9ieNzAudsmjOc5JpUlnRjruXcorkX5unOy553PFk1WZB8OIWYEpeyP+WDIEJQLxhP5aduTR0T8oSbhU9FvqKNolGxt7hKPJLmnVaV9jjdO31D+miGT0Z1xjMJT1IreZEZkrkj801WRNberM/ZcdktOZSclJyjUg1plrQr1zC3KLdPZisrkw3keeZtyhuTh8r35CP5c/PbFWyFTNGjtFKuUA4WTC+oK3hbGFt4uEi9SFrUM99m/ur5IwuCFny9kLBQuLCz2Lh4WfHgIr9FuxYji1MXdy4xXVK6ZHhp8NJ9y2jLspb9UOJYUlXyannc8o5Sg9KlpUMrglc0lamUycturvRauWMVYZVkVe9ql9VbVn8qF5VfrHCsqK74sEa45uJXTl/VfPV5bdra3kq3yu3rSOuk626s91m/r0q9akHV0IbwDa0b8Y3lG19tSt50oXpq9Y7NtM3KzQM1YTXtW8y2rNvyoTaj9nqdf13LVv2tq7e+2Sba1r/dd3vzDoMdFTve75TsvLUreFdrvUV99W7S7oLdjxpiG7q/5n7duEd3T8Wej3ulewf2Re/ranRvbNyvv7+yCW1SNo0eSDpw5ZuAb9qb7Zp3tXBaKg7CQeXBJ9+mfHvjUOihzsPcw83fmX+39QjrSHkr0jq/dawto22gPaG97+iMo50dXh1Hvrf/fu8x42N1xzWPV56gnSg98fnkgpPjp2Snnp1OPz3Umdx590z8mWtdUV29Z0PPnj8XdO5Mt1/3yfPe549d8Lxw9CL3Ytslt0utPa49R35w/eFIr1tv62X3y+1XPK509E3rO9Hv03/6asDVc9f41y5dn3m978bsG7duJt0cuCW69fh29u0XdwruTNxdeo94r/y+2v3qB/oP6n+0/rFlwG3g+GDAYM/DWQ/vDgmHnv6U/9OH4dJHzEfVI0YjjY+dHx8bDRq98mTOk+GnsqcTz8p+Vv9563Or59/94vtLz1j82PAL+YvPv655qfNy76uprzrHI8cfvM55PfGm/K3O233vuO+638e9H5ko/ED+UPPR+mPHp9BP9z7nfP78L/eE8/sl0p8zAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAATSURBVHjaYvj//z8DAAAA//8DAAj8Av7TpXVhAAAAAElFTkSuQmCC");


        private static BaseFont GetFont(FontFamily fontFamily)
        {
            switch (fontFamily)
            {
                case FontFamily.SIMSUN:
                    return BaseFont.CreateFont(FONTPATH_SIMSUN, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, false);
                case FontFamily.SIMHEI:
                    return BaseFont.CreateFont(FONTPATH_SIMHEI, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, false);
                case FontFamily.MSYH:
                    return BaseFont.CreateFont(FONTPATH_MSYH, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, false);
            }

            return BaseFont.CreateFont(FONTPATH_MSYH, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, false);
        }

        private static byte[] ConvertImageData(string imageData)
        {
            return Convert.FromBase64String(imageData.Substring(imageData.IndexOf("base64,", StringComparison.CurrentCultureIgnoreCase) + "base64,".Length));
        }


        /// <summary>
        /// 获取字段位置信息
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static IDictionary<string, FieldPositionInfo> GetFieldPositionInfo(string url)
        {
            var fieldPositions = new Dictionary<string, FieldPositionInfo>();

            using (var reader = new PdfReader(url))
            {
                var fields = reader.AcroFields;
                if (fields != null && fields.Fields != null)
                {
                    foreach (var key in fields.Fields.Keys)
                    {
                        // 记录字段位置
                        var position = fields.GetFieldPositions(key)[0];
                        fieldPositions.Add(key, new FieldPositionInfo
                        {
                            Page = position.page,
                            X = position.position.Left,
                            Y = position.position.Top,
                            Width = position.position.Width,
                            Height = position.position.Height
                        });
                    }
                }

                return fieldPositions;
            }
        }

        /// <summary>
        /// 填充表单
        /// </summary>
        /// <param name="templateUrl">模板地址</param>
        /// <param name="saveUrl">保存地址</param>
        /// <param name="parameters">参数值</param>
        /// <param name="fontFamily">字体（全局默认）</param>
        /// <param name="fontSize">字体大小（全局默认）</param>
        /// <param name="fontColor">字体颜色（全局默认）</param>
        public static void FillForm(string templateUrl, string saveUrl, IDictionary<string, FormFieldInfo> parameters, FontFamily fontFamily = FontFamily.SIMSUN, float? fontSize = null, System.Drawing.Color? fontColor = null)
        {
            using (var reader = new PdfReader(templateUrl))
            using (var stream = new FileStream(saveUrl, FileMode.Create, FileAccess.Write, FileShare.Write))
            using (var stamper = new PdfStamper(reader, stream))
            {
                var fields = stamper.AcroFields;

                // 设置字体（全局默认）
                fields.AddSubstitutionFont(GetFont(fontFamily));

                foreach (var key in fields.Fields.Keys)
                {
                    var position = fields.GetFieldPositions(key)[0];

                    // 填入表单
                    if (parameters.Any(p => p.Key == key && p.Value.Content.HasValue()))
                    {
                        var parameter = parameters.First(p => p.Key == key && p.Value.Content.HasValue());
                        if (parameter.Value.Content.StartsWith("http", StringComparison.CurrentCultureIgnoreCase) ||
                            parameter.Value.Content.StartsWith("data:image", StringComparison.CurrentCultureIgnoreCase))
                        {
                            // 图片
                            var image = parameter.Value.Content.StartsWith("http", StringComparison.CurrentCultureIgnoreCase) ?
                                Image.GetInstance(parameter.Value.Content) :
                                Image.GetInstance(ConvertImageData(parameter.Value.Content));
                            image.SetAbsolutePosition(position.position.Left, position.position.Bottom);
                            image.ScaleToFit(position.position.Width, position.position.Height);
                            stamper.GetOverContent(position.page).AddImage(image);
                        }
                        else
                        {
                            // 文字
                            if (parameter.Value.FontFamily.HasValue)
                                fields.SetFieldProperty(parameter.Key, "textfont", GetFont(parameter.Value.FontFamily.Value), null);
                            if (parameter.Value.FontSize.HasValue)
                                fields.SetFieldProperty(parameter.Key, "textsize", parameter.Value.FontSize.Value, null);
                            else if (fontSize.HasValue)
                                fields.SetFieldProperty(parameter.Key, "textsize", fontSize.Value, null);
                            if (parameter.Value.FontColor.HasValue)
                                fields.SetFieldProperty(parameter.Key, "textcolor", new BaseColor(parameter.Value.FontColor.Value), null);
                            else if (fontColor.HasValue)
                                fields.SetFieldProperty(parameter.Key, "textcolor", new BaseColor(fontColor.Value), null);
                            fields.SetField(parameter.Key, parameter.Value.Content);
                        }
                    }
                }

                // 完成表单
                stamper.FormFlattening = true;
            }
        }

        /// <summary>
        /// 设置签章
        /// </summary>
        /// <param name="templateUrl">模板地址</param>
        /// <param name="saveUrl">保存地址</param>
        /// <param name="position">签署位置</param>
        /// <param name="signingSetting">签署配置</param>
        /// <param name="handleSign">签署信息（返回签署值）</param>
        /// <param name="estimatedSize">签署大小</param>
        /// <param name="signatureImage">签署图片</param>
        /// <param name="reason">签署原因</param>
        /// <param name="location">签署地址</param>
        public static void SetSignature(string templateUrl, string saveUrl, FieldPositionInfo position, IDictionary<string, string> signingSetting, Func<Stream, byte[]> handleSignData, int estimatedSize, string signatureImage = null, string reason = null, string location = null)
        {
            using (var reader = new PdfReader(templateUrl))
            using (var stream = new FileStream(saveUrl, FileMode.Create, FileAccess.Write, FileShare.Write))
            using (var stamper = PdfStamper.CreateSignature(reader, stream, '\0', null, true))
            {
                var appearance = stamper.SignatureAppearance;
                appearance.Reason = reason;
                appearance.Location = location;
                appearance.SignDate = DateTime.Now;
                appearance.SignatureGraphic = Image.GetInstance(signatureImage.HasValue() ? ConvertImageData(signatureImage) : SIGNATURE_GRAPHIC_DATA);
                appearance.CertificationLevel = PdfSignatureAppearance.NOT_CERTIFIED;
                appearance.SignatureRenderingMode = PdfSignatureAppearance.RenderingMode.GRAPHIC;
                appearance.SetVisibleSignature(new Rectangle(position.X, position.Y, position.X + position.Width, position.Y - position.Height), position.Page, Guid.NewGuid().ToString());
                MakeSignature.SignExternalContainer(appearance, new PdfSignatureContainer(signingSetting, handleSignData), estimatedSize);
            }
        }

        /// <summary>
        /// 设置签章
        /// </summary>
        /// <param name="templateUrl">模板地址</param>
        /// <param name="saveUrl">保存地址</param>
        /// <param name="certificate">签署主体证书</param>
        /// <param name="position">签署位置</param>
        /// <param name="handleEncryptionAlgorithm">加密算法</param>
        /// <param name="handleHashAlgorithm">Hash算法</param>
        /// <param name="handleSign">签署信息（返回签署值）</param>
        /// <param name="signatureImage">签署图片</param>
        /// <param name="reason">签署原因</param>
        /// <param name="location">签署地址</param>
        public static void SetSignature(string templateUrl, string saveUrl, string certificate, FieldPositionInfo position, Func<string> handleEncryptionAlgorithm, Func<string> handleHashAlgorithm, Func<byte[], byte[]> handleSign, string signatureImage = null, string reason = null, string location = null)
        {
            using (var reader = new PdfReader(templateUrl))
            using (var stream = new FileStream(saveUrl, FileMode.Create, FileAccess.Write, FileShare.Write))
            using (var stamper = PdfStamper.CreateSignature(reader, stream, '\0', null, true))
            {
                var cert = new X509Certificate2(Convert.FromBase64String(certificate));
                var chain = new Org.BouncyCastle.X509.X509Certificate[]
                {
                    new X509CertificateParser().ReadCertificate(cert.RawData)
                };

                var appearance = stamper.SignatureAppearance;
                appearance.Reason = reason;
                appearance.Location = location;
                appearance.SignDate = DateTime.Now;
                appearance.SignatureGraphic = Image.GetInstance(signatureImage.HasValue() ? ConvertImageData(signatureImage) : SIGNATURE_GRAPHIC_DATA);
                appearance.CertificationLevel = PdfSignatureAppearance.NOT_CERTIFIED;
                appearance.SignatureRenderingMode = PdfSignatureAppearance.RenderingMode.GRAPHIC;
                appearance.SetVisibleSignature(new Rectangle(position.X, position.Y, position.X + position.Width, position.Y - position.Height), position.Page, Guid.NewGuid().ToString());
                MakeSignature.SignDetached(appearance, new PdfSignature(handleEncryptionAlgorithm, handleHashAlgorithm, handleSign), chain, null, null, null, 0, CryptoStandard.CADES);
            }
        }
    }
}
