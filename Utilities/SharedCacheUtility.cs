﻿using APP.Core.Caches;
using System;

namespace APP.Core.Utilities
{
    /// <summary>
    /// 使用 Redis 或 Memory Cache
    /// </summary>
    public static class SharedCacheUtility
    {
        public static CacheEngine CacheEngine { get; private set; }
        public static int MaxErrorWeight { get; private set; }
        public static int RedisErrorWeight { get; private set; }
        public static Func<bool> ErrorHandle { get; private set; }
        public static DateTime LastErrorHandleTime { get; private set; }


        /// <summary>
        /// 初始化配置
        /// </summary>
        /// <param name="cacheEngine">Redis 或 Memory Cache</param>
        /// <param name="maxErrorWeight">不低于1</param>
        /// <param name="handle">错误权限超过maxErrorWeight时处理</param>
        public static void Configure(CacheEngine cacheEngine = CacheEngine.MemoryCache, int maxErrorWeight = 10, Func<bool> errorHandle = null)
        {
            if (cacheEngine != CacheEngine.MemoryCache && cacheEngine != CacheEngine.RedisCache)
                throw new ArgumentException("cacheEngine");
            if (maxErrorWeight < 1)
                throw new ArgumentException("maxErrorWeight");

            CacheEngine = cacheEngine;
            MaxErrorWeight = maxErrorWeight;
            RedisErrorWeight = 0;
            ErrorHandle = errorHandle;
            LastErrorHandleTime = DateTime.MinValue;
        }


        private static void Handle()
        {
            if (ErrorHandle != null && ErrorHandle())
                LastErrorHandleTime = DateTime.Now;
        }

        /// <summary>
        /// 从缓存获取数据，若不存在，则执行逻辑并存入缓存
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="key"></param>
        /// <param name="getFunc"></param>
        /// <param name="getCache"></param>
        /// <param name="cacheMinutes"></param>
        /// <param name="db">仅使用RedisCache</param>
        /// <returns></returns>
        public static TResult GetOrSetFromCache<TResult>(string key, Func<TResult> getFunc, bool getCache = true, double? cacheMinutes = null, int? db = null)
        {
            if (CacheEngine == CacheEngine.RedisCache)
            {
                if (RedisErrorWeight < MaxErrorWeight)
                {
                    try
                    {
                        var result = CacheUtility.GetOrSetFromRedis(key, getFunc, getCache, cacheMinutes, db);
                        RedisErrorWeight = Math.Max(RedisErrorWeight - 1, 0);

                        return result;
                    }
                    catch (Exception ex)
                    {
                        LogUtility.LogError(ex);
                        RedisErrorWeight += 2;
                    }
                }
                else Handle();
            }

            return CacheUtility.GetOrSetFromCache(key, getFunc, getCache, cacheMinutes.HasValue ? CacheExpirationType.Absolute : CacheExpirationType.None, cacheMinutes);
        }

        /// <summary>
        /// 添加1个缓存项，并在指定时间过期（若已存在该 Key，更新数据）
        /// </summary>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <param name="cacheMinutes"></param>
        /// <param name="db">仅使用RedisCache</param>
        /// <returns></returns>
        public static bool Set(string key, object data, double? cacheMinutes = null, int? db = null)
        {
            if (cacheMinutes.HasValue)
            {
                if (CacheEngine == CacheEngine.RedisCache)
                {
                    if (RedisErrorWeight < MaxErrorWeight)
                    {
                        try
                        {
                            var result = CacheManager.RedisCacheProvider.SetValue(key, data, cacheMinutes.Value, db);
                            RedisErrorWeight = Math.Max(RedisErrorWeight - 1, 0);

                            return result;
                        }
                        catch (Exception ex)
                        {
                            LogUtility.LogError(ex);
                            RedisErrorWeight += 2;
                        }
                    }
                    else Handle();
                }

                return CacheManager.MemoryCacheProvider.Set(key, data, CacheExpirationType.Absolute, cacheMinutes.Value);
            }
            else
            {
                if (CacheEngine == CacheEngine.RedisCache)
                {
                    if (RedisErrorWeight < MaxErrorWeight)
                    {
                        try
                        {
                            var result = CacheManager.RedisCacheProvider.SetValue(key, data, db);
                            RedisErrorWeight = Math.Max(RedisErrorWeight - 1, 0);

                            return result;
                        }
                        catch (Exception ex)
                        {
                            LogUtility.LogError(ex);
                            RedisErrorWeight += 2;
                        }
                    }
                    else Handle();
                }

                return CacheManager.MemoryCacheProvider.Set(key, data, CacheExpirationType.None);
            }
        }

        /// <summary>
        /// 获取指定类型 Key 的缓存项
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="db">仅使用RedisCache</param>
        /// <returns>不存在则返回默认值</returns>
        public static T Get<T>(string key, int? db = null)
        {
            if (CacheEngine == CacheEngine.RedisCache)
            {
                if (RedisErrorWeight < MaxErrorWeight)
                {
                    try
                    {
                        var result = CacheManager.RedisCacheProvider.GetValue<T>(key, db);
                        RedisErrorWeight = Math.Max(RedisErrorWeight - 1, 0);

                        return result;
                    }
                    catch (Exception ex)
                    {
                        LogUtility.LogError(ex);
                        RedisErrorWeight += 2;
                    }
                }
                else Handle();
            }

            return CacheManager.MemoryCacheProvider.Get<T>(key);
        }

        /// <summary>
        /// 删除指定 Key 的缓存项
        /// </summary>
        /// <param name="key"></param>
        /// <param name="db">仅使用RedisCache</param>
        /// <returns></returns>
        public static bool Remove(string key, int? db = null)
        {
            if (CacheEngine == CacheEngine.RedisCache)
            {
                if (RedisErrorWeight < MaxErrorWeight)
                {
                    try
                    {
                        var result = CacheManager.RedisCacheProvider.RemoveValue(key, db);
                        RedisErrorWeight = Math.Max(RedisErrorWeight - 1, 0);

                        return result;
                    }
                    catch (Exception ex)
                    {
                        LogUtility.LogError(ex);
                        RedisErrorWeight += 2;
                    }
                }
                else Handle();
            }

            return CacheManager.MemoryCacheProvider.Remove(key);
        }

        /// <summary>
        /// 删除所有缓存项
        /// </summary>
        /// <param name="db">仅使用RedisCache</param>
        /// <returns></returns>
        public static bool RemoveAll(int? db = null)
        {
            if (CacheEngine == CacheEngine.RedisCache)
            {
                if (RedisErrorWeight < MaxErrorWeight)
                {
                    try
                    {
                        var result = CacheManager.RedisCacheProvider.FlushAll(db);
                        RedisErrorWeight = Math.Max(RedisErrorWeight - 1, 0);

                        return result;
                    }
                    catch (Exception ex)
                    {
                        LogUtility.LogError(ex);
                        RedisErrorWeight += 2;
                    }
                }
                else Handle();
            }

            CacheManager.MemoryCacheProvider.RemoveAll();
            return true;
        }
    }
}
