﻿using Ionic.Zip;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace APP.Core.Utilities
{
    public static class ZipUtility
    {
        /// <summary>
        /// 压缩
        /// </summary>
        /// <param name="saveUrl">保存地址</param>
        /// <param name="items">添加项（Key：要添加的文件/文件夹，Value：归档的名称/文件夹，支持层级）</param>
        /// <param name="password">设置密码</param>
        public static MemoryStream Compress(IDictionary<string, string> items, string password = null)
        {
            using (var zip = new ZipFile(Encoding.UTF8))
            {
                // 设置密码
                zip.Password = password;

                if (items != null)
                {
                    // 添加项
                    foreach (var item in items)
                        zip.AddItem(item.Key, item.Value ?? "");
                }

                // 保存压缩
                var stream = new MemoryStream();
                zip.Save(stream);
                return stream;
            }
        }

        /// <summary>
        /// 解压
        /// </summary>
        /// <param name="fileUrl">压缩包路径</param>
        /// <param name="decompressUrl">解压路径（默认同个文件夹）</param>
        /// <param name="password">设置密码</param>
        /// <returns></returns>
        public static bool Decompress(string fileUrl, string decompressUrl = null, string password = null)
        {
            if (!fileUrl.HasValue())
                return false;
            if (!File.Exists(fileUrl))
                return false;

            if (!decompressUrl.HasValue())
                decompressUrl = fileUrl.Substring(0, fileUrl.LastIndexOf('.'));

            using (var zip = new ZipFile(fileUrl, Encoding.UTF8))
            {
                // 设置密码
                zip.Password = password;

                // 解压
                zip.ExtractAll(decompressUrl, ExtractExistingFileAction.OverwriteSilently);
            }
            return true;
        }
    }
}
