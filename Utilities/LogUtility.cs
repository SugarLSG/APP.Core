﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace APP.Core.Utilities
{
    public static class LogUtility
    {
        private enum LogLevel
        {
            Debug,
            Info,
            Warn,
            Error,
            Fatal
        }

        /// <summary>
        /// 默认文件名
        /// </summary>
        private const string DEFAULT_FILE_NAME = "logfile";
        /// <summary>
        /// 缓存日志
        /// </summary>
        private static IDictionary<string, ILog> _loggerCache = new Dictionary<string, ILog>();


        /// <summary>
        /// 初始化配置
        /// </summary>
        /// <param name="filePath">Log 配置文件路径（绝对路径）</param>
        public static void Configure(string filePath)
        {
            XmlConfigurator.Configure(new FileInfo(filePath));
        }

        /// <summary>
        /// 记录日志
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        /// <param name="level">记录级别</param>
        private static void Log(object message, Exception exception, bool logCallerInfo, LogLevel level, string fileName = DEFAULT_FILE_NAME)
        {
            var messageStr = new StringBuilder();

            // 是否记录调用者信息
            if (logCallerInfo)
            {
                // 获取堆栈信息
                StackFrame frame = null;
                if (exception != null)
                {
                    var st = new StackTrace(exception, true);
                    frame = st.GetFrame(0);
                }
                if (frame == null || frame.GetMethod() == null)
                    frame = new StackFrame(2, true);

                messageStr.Append("[");
                if (frame != null)
                {
                    var method = frame.GetMethod();
                    if (method != null)
                    {
                        if (method.DeclaringType != null)
                            messageStr.AppendFormat("class:{0} ", method.DeclaringType.Name);
                        messageStr.AppendFormat("method:{0} ", method.Name);
                    }
                    messageStr.AppendFormat("line:{0}", frame.GetFileLineNumber());
                }
                messageStr.Append("]");
            }

            if (message != null)
                messageStr.AppendFormat("{0}{1}", messageStr.Length > 0 ? "\r\n" : string.Empty, message);
            if (exception != null)
                messageStr.AppendFormat("{0}{1}", messageStr.Length > 0 ? "\r\n" : string.Empty, exception);

            // 记录日志
            ILog logger;
            var cacheName = fileName ?? DEFAULT_FILE_NAME;
            if (_loggerCache.ContainsKey(cacheName))
                logger = _loggerCache[cacheName];
            else
            {
                logger = LogManager.GetLogger(fileName ?? DEFAULT_FILE_NAME);
                _loggerCache.Add(cacheName, logger);
            }
            switch (level)
            {
                case LogLevel.Debug:
                    logger.Debug(messageStr.ToString());
                    break;
                case LogLevel.Info:
                    logger.Info(messageStr.ToString());
                    break;
                case LogLevel.Warn:
                    logger.Warn(messageStr.ToString());
                    break;
                case LogLevel.Error:
                    logger.Error(messageStr.ToString());
                    break;
                case LogLevel.Fatal:
                    logger.Fatal(messageStr.ToString());
                    break;
            }
        }

        /// <summary>
        /// 记录 Debug 日志
        /// </summary>
        /// <param name="message"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogDebug(object message, bool logCallerInfo = true)
        {
            Log(message, null, logCallerInfo, LogLevel.Debug);
        }
        /// <summary>
        /// 记录 Debug 日志
        /// </summary>
        /// <param name="exception"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogDebug(Exception exception, bool logCallerInfo = true)
        {
            Log(null, exception, logCallerInfo, LogLevel.Debug);
        }
        /// <summary>
        /// 记录 Debug 日志
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogDebug(object message, Exception exception, bool logCallerInfo = true)
        {
            Log(message, exception, logCallerInfo, LogLevel.Debug);
        }
        /// <summary>
        /// 记录 Debug 日志
        /// </summary>
        /// <param name="fileName">指定日志文件名</param>
        /// <param name="message"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogDebugToFile(string fileName, object message, bool logCallerInfo = true)
        {
            Log(message, null, logCallerInfo, LogLevel.Debug, fileName);
        }
        /// <summary>
        /// 记录 Debug 日志
        /// </summary>
        /// <param name="fileName">指定日志文件名</param>
        /// <param name="exception"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogDebugToFile(string fileName, Exception exception, bool logCallerInfo = true)
        {
            Log(null, exception, logCallerInfo, LogLevel.Debug, fileName);
        }
        /// <summary>
        /// 记录 Debug 日志
        /// </summary>
        /// <param name="fileName">指定日志文件名</param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogDebugToFile(string fileName, object message, Exception exception, bool logCallerInfo = true)
        {
            Log(message, exception, logCallerInfo, LogLevel.Debug, fileName);
        }

        /// <summary>
        /// 记录 Info 日志
        /// </summary>
        /// <param name="message"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogInfo(object message, bool logCallerInfo = true)
        {
            Log(message, null, logCallerInfo, LogLevel.Info);
        }
        /// <summary>
        /// 记录 Info 日志
        /// </summary>
        /// <param name="exception"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogInfo(Exception exception, bool logCallerInfo = true)
        {
            Log(null, exception, logCallerInfo, LogLevel.Info);
        }
        /// <summary>
        /// 记录 Info 日志
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogInfo(object message, Exception exception, bool logCallerInfo = true)
        {
            Log(message, exception, logCallerInfo, LogLevel.Info);
        }
        /// <summary>
        /// 记录 Info 日志
        /// </summary>
        /// <param name="fileName">指定日志文件名</param>
        /// <param name="message"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogInfoToFile(string fileName, object message, bool logCallerInfo = true)
        {
            Log(message, null, logCallerInfo, LogLevel.Info, fileName);
        }
        /// <summary>
        /// 记录 Info 日志
        /// </summary>
        /// <param name="fileName">指定日志文件名</param>
        /// <param name="exception"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogInfoToFile(string fileName, Exception exception, bool logCallerInfo = true)
        {
            Log(null, exception, logCallerInfo, LogLevel.Info, fileName);
        }
        /// <summary>
        /// 记录 Info 日志
        /// </summary>
        /// <param name="fileName">指定日志文件名</param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogInfoToFile(string fileName, object message, Exception exception, bool logCallerInfo = true)
        {
            Log(message, exception, logCallerInfo, LogLevel.Info, fileName);
        }

        /// <summary>
        /// 记录 Warn 日志
        /// </summary>
        /// <param name="message"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogWarn(object message, bool logCallerInfo = true)
        {
            Log(message, null, logCallerInfo, LogLevel.Warn);
        }
        /// <summary>
        /// 记录 Warn 日志
        /// </summary>
        /// <param name="exception"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogWarn(Exception exception, bool logCallerInfo = true)
        {
            Log(null, exception, logCallerInfo, LogLevel.Warn);
        }
        /// <summary>
        /// 记录 Warn 日志
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogWarn(object message, Exception exception, bool logCallerInfo = true)
        {
            Log(message, exception, logCallerInfo, LogLevel.Warn);
        }
        /// <summary>
        /// 记录 Warn 日志
        /// </summary>
        /// <param name="fileName">指定日志文件名</param>
        /// <param name="message"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogWarnToFile(string fileName, object message, bool logCallerInfo = true)
        {
            Log(message, null, logCallerInfo, LogLevel.Warn, fileName);
        }
        /// <summary>
        /// 记录 Warn 日志
        /// </summary>
        /// <param name="fileName">指定日志文件名</param>
        /// <param name="exception"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogWarnToFile(string fileName, Exception exception, bool logCallerInfo = true)
        {
            Log(null, exception, logCallerInfo, LogLevel.Warn, fileName);
        }
        /// <summary>
        /// 记录 Warn 日志
        /// </summary>
        /// <param name="fileName">指定日志文件名</param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogWarnToFile(string fileName, object message, Exception exception, bool logCallerInfo = true)
        {
            Log(message, exception, logCallerInfo, LogLevel.Warn, fileName);
        }

        /// <summary>
        /// 记录 Error 日志
        /// </summary>
        /// <param name="message"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogError(object message, bool logCallerInfo = true)
        {
            Log(message, null, logCallerInfo, LogLevel.Error);
        }
        /// <summary>
        /// 记录 Error 日志
        /// </summary>
        /// <param name="exception"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogError(Exception exception, bool logCallerInfo = true)
        {
            Log(null, exception, logCallerInfo, LogLevel.Error);
        }
        /// <summary>
        /// 记录 Error 日志
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogError(object message, Exception exception, bool logCallerInfo = true)
        {
            Log(message, exception, logCallerInfo, LogLevel.Error);
        }
        /// <summary>
        /// 记录 Error 日志
        /// </summary>
        /// <param name="fileName">指定日志文件名</param>
        /// <param name="message"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogErrorToFile(string fileName, object message, bool logCallerInfo = true)
        {
            Log(message, null, logCallerInfo, LogLevel.Error, fileName);
        }
        /// <summary>
        /// 记录 Error 日志
        /// </summary>
        /// <param name="fileName">指定日志文件名</param>
        /// <param name="exception"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogErrorToFile(string fileName, Exception exception, bool logCallerInfo = true)
        {
            Log(null, exception, logCallerInfo, LogLevel.Error, fileName);
        }
        /// <summary>
        /// 记录 Error 日志
        /// </summary>
        /// <param name="fileName">指定日志文件名</param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogErrorToFile(string fileName, object message, Exception exception, bool logCallerInfo = true)
        {
            Log(message, exception, logCallerInfo, LogLevel.Error, fileName);
        }

        /// <summary>
        /// 记录 Fatal 日志
        /// </summary>
        /// <param name="message"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogFatal(object message, bool logCallerInfo = true)
        {
            Log(message, null, logCallerInfo, LogLevel.Fatal);
        }
        /// <summary>
        /// 记录 Fatal 日志
        /// </summary>
        /// <param name="exception"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogFatal(Exception exception, bool logCallerInfo = true)
        {
            Log(null, exception, logCallerInfo, LogLevel.Fatal);
        }
        /// <summary>
        /// 记录 Fatal 日志
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogFatal(object message, Exception exception, bool logCallerInfo = true)
        {
            Log(message, exception, logCallerInfo, LogLevel.Fatal);
        }
        /// <summary>
        /// 记录 Fatal 日志
        /// </summary>
        /// <param name="fileName">指定日志文件名</param>
        /// <param name="message"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogFatalToFile(string fileName, object message, bool logCallerInfo = true)
        {
            Log(message, null, logCallerInfo, LogLevel.Fatal, fileName);
        }
        /// <summary>
        /// 记录 Fatal 日志
        /// </summary>
        /// <param name="fileName">指定日志文件名</param>
        /// <param name="exception"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogFatalToFile(string fileName, Exception exception, bool logCallerInfo = true)
        {
            Log(null, exception, logCallerInfo, LogLevel.Fatal, fileName);
        }
        /// <summary>
        /// 记录 Fatal 日志
        /// </summary>
        /// <param name="fileName">指定日志文件名</param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        /// <param name="logCallerInfo">是否记录调用者信息</param>
        public static void LogFatalToFile(string fileName, object message, Exception exception, bool logCallerInfo = true)
        {
            Log(message, exception, logCallerInfo, LogLevel.Fatal, fileName);
        }
    }
}
