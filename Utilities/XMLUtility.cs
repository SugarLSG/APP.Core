﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using static APP.Core.Extensions.Json.ContractResolverExtension;

namespace APP.Core.Utilities
{
    public static class XmlUtility
    {
        #region 序列化数据

        public const string DEFAULT_XML_VERSION = "1.0";
        public const string DEFAULT_XML_ENCODING = "utf-8";
        public const string DEFAULT_ROOT_NAME = "root";

        /// <summary>
        /// 将 Model 序列化为 XML 文档
        /// </summary>
        /// <param name="model">要序列化的 Model</param>
        /// <param name="rootName">根节点名称</param>
        /// <returns></returns>
        public static XmlDocument Serialize(object model, string rootName = DEFAULT_ROOT_NAME)
        {
            var document = new XmlDocument();

            // 声明
            var xmlnode = document.CreateXmlDeclaration(DEFAULT_XML_VERSION, DEFAULT_XML_ENCODING, null);
            document.AppendChild(xmlnode);

            // 根节点
            var root = document.CreateElement(rootName);
            document.AppendChild(root);

            // 转化为节点
            ConvertXmlNode(document, root, true, null, model);

            return document;
        }

        /// <summary>
        /// 将 Model 序列化为 XML Memory Stream
        /// </summary>
        /// <param name="model">要序列化的 Model</param>
        /// <param name="rootName">根节点名称</param>
        /// <returns></returns>
        public static MemoryStream SerializeStream(object model, string rootName = DEFAULT_ROOT_NAME)
        {
            var document = Serialize(model, rootName);

            var stream = new MemoryStream();
            document.Save(stream);
            return stream;
        }

        /// <summary>
        /// 将 Model 序列化为 XML 字符串
        /// </summary>
        /// <param name="model">要序列化的 Model</param>
        /// <param name="rootName">根节点名称</param>
        /// <returns></returns>
        public static string SerializeString(object model, string rootName = DEFAULT_ROOT_NAME)
        {
            var document = Serialize(model, rootName);

            return document.OuterXml;
        }

        /// <summary>
        /// 转化为 XML 节点
        /// </summary>
        /// <param name="document"></param>
        /// <param name="parent"></param>
        /// <param name="newNode"></param>
        /// <param name="name"></param>
        /// <param name="model"></param>
        private static void ConvertXmlNode(XmlDocument document, XmlElement parent, bool newNode, string name, object model)
        {
            if (model == null)
                return;

            var modelType = model.GetType();
            // 值类型
            if (modelType.IsValueType || modelType == CoreConfig.STRING_TYPE)
            {
                if (name.HasValue())
                {
                    if (newNode)
                    {
                        var node = document.CreateElement(name);
                        parent.AppendChild(node);
                        node.AppendChild(document.CreateTextNode(model.ToString()));
                    }
                    else
                        parent.SetAttribute(name, model.ToString());
                }
                else
                    parent.AppendChild(document.CreateTextNode(model.ToString()));
            }
            // 集合类型
            else if (modelType.IsGenericType || modelType.IsArray)
            {
                // 是否为 Dictionary
                if (model is IDictionary)
                {
                    var dict = model as IDictionary;
                    foreach (var key in dict.Keys)
                        ConvertXmlNode(document, parent, true, key.ToString(), dict[key]);
                }
                // 是否为 Enumerable
                else if (model is IEnumerable)
                {
                    foreach (var item in model as IEnumerable)
                        ConvertXmlNode(document, parent, true, name, item);
                }
                // 匿名类型
                else
                {
                    foreach (var property in modelType.GetProperties())
                        ConvertXmlNode(document, parent, true, property.Name, property.GetValue(model));
                }
            }
            // 复杂类型
            else
            {
                XmlElement node;
                if (newNode && name.HasValue())
                {
                    node = document.CreateElement(name);
                    parent.AppendChild(node);
                }
                else
                    node = parent;

                foreach (var property in modelType.GetProperties())
                {
                    var xmlAttributeAttribute = property.GetCustomAttribute<XmlAttributeAttribute>();
                    var xmlElementAttribute = property.GetCustomAttribute<XmlElementAttribute>();

                    var nodeName = xmlAttributeAttribute != null ? xmlAttributeAttribute.AttributeName : (xmlElementAttribute != null ? xmlElementAttribute.ElementName : string.Empty);
                    if (!nodeName.HasValue())
                        nodeName = property.Name;

                    ConvertXmlNode(document, node, xmlAttributeAttribute == null, nodeName, property.GetValue(model));
                }
            }
        }

        #endregion

        #region 反序列化数据

        /// <summary>
        /// 加载 XML 文档
        /// </summary>
        /// <param name="text">XML 内容</param>
        /// <returns></returns>
        public static XDocument Load(string text)
        {
            return XDocument.Parse(text);
        }

        /// <summary>
        /// 加载 XML 文档
        /// </summary>
        /// <param name="stream">XML 流</param>
        /// <returns></returns>
        public static XDocument Load(Stream stream)
        {
            return XDocument.Load(stream);
        }

        /// <summary>
        /// 反序列化为 Model
        /// </summary>
        /// <param name="text">XML 内容</param>
        /// <param name="nodeName">对应节点名称</param>
        /// <returns></returns>
        public static dynamic Deserialize(string text, string nodeName = null)
        {
            return Deserialize<dynamic>(text, nodeName);
        }

        /// <summary>
        /// 反序列化为 Model
        /// </summary>
        /// <param name="stream">XML 流</param>
        /// <param name="nodeName">对应节点名称</param>
        /// <returns></returns>
        public static dynamic Deserialize(Stream stream, string nodeName = null)
        {
            return Deserialize<dynamic>(stream, nodeName);
        }

        /// <summary>
        /// 反序列化为指定类型的 Model
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="text">XML 内容</param>
        /// <param name="nodeName">对应节点名称</param>
        /// <returns></returns>
        public static T Deserialize<T>(string text, string nodeName = null)
        {
            var document = Load(text);
            if (document == null)
                return default(T);

            XElement root = nodeName.HasValue() ?
                document.Descendants(nodeName).FirstOrDefault() :
                document.Root;
            if (root == null)
                return default(T);

            return JsonUtility.Deserialize<T>(JsonUtility.Serialize(ConvertInstanceDictionary(root), propertynametype: PropertyNameType.None));
        }

        /// <summary>
        /// 反序列化为指定类型的 Model
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream">XML 流</param>
        /// <param name="nodeName">对应节点名称</param>
        /// <returns></returns>
        public static T Deserialize<T>(Stream stream, string nodeName = null)
        {
            var document = Load(stream);
            if (document == null)
                return default(T);

            XElement root = nodeName.HasValue() ?
                document.Descendants(nodeName).FirstOrDefault() :
                document.Root;
            if (root == null)
                return default(T);

            return JsonUtility.Deserialize<T>(JsonUtility.Serialize(ConvertInstanceDictionary(root), propertynametype: PropertyNameType.None));
        }

        /// <summary>
        /// 转化为 Dictionary
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private static IDictionary<string, object> ConvertInstanceDictionary(XElement node)
        {
            var value = new Dictionary<string, object>();

            // 所有 Attribute 为值属性
            foreach (var attribute in node.Attributes())
            {
                MergerValue(value, attribute.Name.ToString(), attribute.Value);
            }

            // 所有子节点
            // 有 Attribute 或子 Element 则为类型属性
            // 无则为值属性
            foreach (var element in node.Elements())
            {
                if (element.HasAttributes || element.HasElements)
                    MergerValue(value, element.Name.ToString(), ConvertInstanceDictionary(element));
                else
                    MergerValue(value, element.Name.ToString(), element.Value);
            }

            return value;
        }

        /// <summary>
        /// 合并 Value
        /// </summary>
        /// <param name="dict"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        private static void MergerValue(Dictionary<string, object> dict, string key, object value)
        {
            if (dict.ContainsKey(key))
            {
                var tempValue = dict[key];
                if (tempValue is List<object>)
                {
                    (tempValue as List<object>).Add(value);
                    dict[key] = tempValue;
                }
                else
                {
                    dict[key] = new List<object> { tempValue, value };
                }
            }
            else
                dict.Add(key, value);
        }

        #endregion
    }
}
