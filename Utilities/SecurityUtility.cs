﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace APP.Core.Utilities
{
    public static class SecurityUtility
    {
        /// <summary>
        /// 默认编码
        /// </summary>
        private static readonly Encoding DefaultEncoding = Encoding.UTF8;


        /// <summary>
        /// 16进制字符串转字节数组
        /// </summary>
        /// <param name="hexString">16进制字符串</param>
        /// <returns></returns>
        private static byte[] hexStringToBytes(string hexString)
        {
            if (hexString.Length % 2 != 0)
                throw new ArgumentException("16进制字符串格式不正确");

            var length = hexString.Length / 2;
            var bytes = new byte[length];
            for (var i = 0; i < length; ++i)
            {
                bytes[i] = byte.Parse(hexString.Substring(i * 2, 2), NumberStyles.HexNumber);
            }

            return bytes;
        }


        #region MD5

        /// <summary>
        /// MD5加密（返回16进制字符串）
        /// </summary>
        /// <param name="originalString">原文</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <returns></returns>
        public static string MD5Encrypt(string originalString, Encoding encoding = null)
        {
            using (var md5 = new MD5CryptoServiceProvider())
            {
                var hashCodes = md5.ComputeHash((encoding ?? DefaultEncoding).GetBytes(originalString));
                return BitConverter.ToString(hashCodes).Replace("-", "");
            }
        }

        /// <summary>
        /// MD5 16位加密（返回16进制字符串）
        /// </summary>
        /// <param name="originalString">原文</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <returns></returns>
        public static string MD5Encrypt_16(string originalString, Encoding encoding = null)
        {
            return MD5Encrypt(originalString, encoding).Substring(8, 16);
        }

        #endregion

        #region SHA

        /// <summary>
        /// SHA1加密（返回16进制字符串）
        /// </summary>
        /// <param name="originalString">原文</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <returns></returns>
        public static string SHA1Encrypt(string originalString, Encoding encoding = null)
        {
            using (var sha1 = new SHA1CryptoServiceProvider())
            {
                var hashCodes = sha1.ComputeHash((encoding ?? DefaultEncoding).GetBytes(originalString));
                return BitConverter.ToString(hashCodes).Replace("-", "");
            }
        }

        /// <summary>
        /// SHA256加密（返回16进制字符串）
        /// </summary>
        /// <param name="originalString">原文</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <returns></returns>
        public static string SHA256Encrypt(string originalString, Encoding encoding = null)
        {
            using (var sha256 = new SHA256CryptoServiceProvider())
            {
                var hashCodes = sha256.ComputeHash((encoding ?? DefaultEncoding).GetBytes(originalString));
                return BitConverter.ToString(hashCodes).Replace("-", "");
            }
        }

        /// <summary>
        /// SHA384加密（返回16进制字符串）
        /// </summary>
        /// <param name="originalString">原文</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <returns></returns>
        public static string SHA384Encrypt(string originalString, Encoding encoding = null)
        {
            using (var sha384 = new SHA384CryptoServiceProvider())
            {
                var hashCodes = sha384.ComputeHash((encoding ?? DefaultEncoding).GetBytes(originalString));
                return BitConverter.ToString(hashCodes).Replace("-", "");
            }
        }

        /// <summary>
        /// SHA512加密（返回16进制字符串）
        /// </summary>
        /// <param name="originalString">原文</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <returns></returns>
        public static string SHA512Encrypt(string originalString, Encoding encoding = null)
        {
            using (var sha512 = new SHA512CryptoServiceProvider())
            {
                var hashCodes = sha512.ComputeHash((encoding ?? DefaultEncoding).GetBytes(originalString));
                return BitConverter.ToString(hashCodes).Replace("-", "");
            }
        }

        #endregion

        #region BASE64

        /// <summary>
        /// Base64加密
        /// </summary>
        /// <param name="originalString">原文</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <returns></returns>
        public static string BASE64Encrypt(string originalString, Encoding encoding = null)
        {
            var bytes = (encoding ?? DefaultEncoding).GetBytes(originalString);
            return Convert.ToBase64String(bytes);
        }

        /// <summary>
        /// Base64解密
        /// </summary>
        /// <param name="cryptoString">密文</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <returns></returns>
        public static string BASE64Decrypt(string cryptoString, Encoding encoding = null)
        {
            var bytes = Convert.FromBase64String(cryptoString);
            return (encoding ?? DefaultEncoding).GetString(bytes);
        }

        #endregion

        #region HEX

        /// <summary>
        /// 16进制加密
        /// </summary>
        /// <param name="originalString">原文</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <returns></returns>
        public static string HEXEncrypt(string originalString, Encoding encoding = null)
        {
            var bytes = (encoding ?? DefaultEncoding).GetBytes(originalString);
            return BitConverter.ToString(bytes).Replace("-", "");
        }

        /// <summary>
        /// 16进制解密
        /// </summary>
        /// <param name="cryptoString">密文</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <returns></returns>
        public static string HEXDecrypt(string cryptoString, Encoding encoding = null)
        {
            var bytes = hexStringToBytes(cryptoString);
            return (encoding ?? DefaultEncoding).GetString(bytes);
        }

        #endregion

        #region DES

        /// <summary>
        /// DES默认秘钥
        /// </summary>
        private static readonly byte[] DES_DEFAULT_KEY = Encoding.ASCII.GetBytes("$DESKEY$");
        /// <summary>
        /// DES默认向量
        /// </summary>
        private static readonly byte[] DES_DEFAULT_IV = Encoding.ASCII.GetBytes("$DES_IV$");


        /// <summary>
        /// DES加密（返回字节数组）
        /// </summary>
        /// <param name="originalString">原文</param>
        /// <param name="key">8位秘钥（使用默认秘钥）</param>
        /// <param name="iv">8位向量（CBC模式，使用默认向量）</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <param name="cipher">加密模式</param>
        /// <param name="padding">填充模式</param>
        /// <returns></returns>
        public static byte[] DESEncrypt_Bytes(string originalString, string key = null, string iv = null, Encoding encoding = null, CipherMode cipher = CipherMode.ECB, PaddingMode padding = PaddingMode.PKCS7)
        {
            if (key.HasValue() && key.Length != 8)
                throw new ArgumentException("DES密钥必须为8位.");
            if (iv.HasValue() && iv.Length != 8)
                throw new ArgumentException("DES向量必须为8位.");

            using (var des = new DESCryptoServiceProvider())
            {
                des.Mode = cipher;
                des.Padding = padding;
                des.Key = key.HasValue() ? Encoding.ASCII.GetBytes(key) : DES_DEFAULT_KEY;
                des.IV = iv.HasValue() ? Encoding.ASCII.GetBytes(iv) : DES_DEFAULT_IV;

                var originalBytes = (encoding ?? DefaultEncoding).GetBytes(originalString);
                return des.CreateEncryptor().TransformFinalBlock(originalBytes, 0, originalBytes.Length);
            }
        }

        /// <summary>
        /// DES加密（返回16进制字符串）
        /// </summary>
        /// <param name="originalString">原文</param>
        /// <param name="key">8位秘钥（使用默认秘钥）</param>
        /// <param name="iv">8位向量（CBC模式，使用默认向量）</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <param name="cipher">加密模式</param>
        /// <param name="padding">填充模式</param>
        /// <returns></returns>
        public static string DESEncrypt_Hex(string originalString, string key = null, string iv = null, Encoding encoding = null, CipherMode cipher = CipherMode.ECB, PaddingMode padding = PaddingMode.PKCS7)
        {
            var resultBytes = DESEncrypt_Bytes(originalString, key, iv, encoding, cipher, padding);
            return BitConverter.ToString(resultBytes).Replace("-", "");
        }

        /// <summary>
        /// DES加密（返回Base64字符串）
        /// </summary>
        /// <param name="originalString">原文</param>
        /// <param name="key">8位秘钥（使用默认秘钥）</param>
        /// <param name="iv">8位向量（CBC模式，使用默认向量）</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <param name="cipher">加密模式</param>
        /// <param name="padding">填充模式</param>
        /// <returns></returns>
        public static string DESEncrypt_Base64(string originalString, string key = null, string iv = null, Encoding encoding = null, CipherMode cipher = CipherMode.ECB, PaddingMode padding = PaddingMode.PKCS7)
        {
            var resultBytes = DESEncrypt_Bytes(originalString, key, iv, encoding, cipher, padding);
            return Convert.ToBase64String(resultBytes);
        }


        /// <summary>
        /// DES解密（字节数组解密）
        /// </summary>
        /// <param name="cryptoBytes">密文</param>
        /// <param name="key">8位秘钥（使用默认秘钥）</param>
        /// <param name="iv">8位向量（CBC模式，使用默认向量）</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <param name="cipher">加密模式</param>
        /// <param name="padding">填充模式</param>
        /// <returns></returns>
        public static string DESDecrypt_Bytes(byte[] cryptoBytes, string key = null, string iv = null, Encoding encoding = null, CipherMode cipher = CipherMode.ECB, PaddingMode padding = PaddingMode.PKCS7)
        {
            if (key.HasValue() && key.Length != 8)
                throw new ArgumentException("DES密钥必须为8位.");
            if (iv.HasValue() && iv.Length != 8)
                throw new ArgumentException("DES向量必须为8位.");

            using (var des = new DESCryptoServiceProvider())
            {
                des.Mode = CipherMode.ECB;
                des.Padding = PaddingMode.PKCS7;
                des.Key = key.HasValue() ? Encoding.ASCII.GetBytes(key) : DES_DEFAULT_KEY;
                des.IV = iv.HasValue() ? Encoding.ASCII.GetBytes(iv) : DES_DEFAULT_IV;

                var resultBytes = des.CreateDecryptor().TransformFinalBlock(cryptoBytes, 0, cryptoBytes.Length);
                return (encoding ?? DefaultEncoding).GetString(resultBytes);
            }
        }

        /// <summary>
        /// DES解密（16进制字符串解密）
        /// </summary>
        /// <param name="cryptoString">密文</param>
        /// <param name="key">8位秘钥（使用默认秘钥）</param>
        /// <param name="iv">8位向量（CBC模式，使用默认向量）</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <param name="cipher">加密模式</param>
        /// <param name="padding">填充模式</param>
        /// <returns></returns>
        public static string DESDecrypt_Hex(string cryptoString, string key = null, string iv = null, Encoding encoding = null, CipherMode cipher = CipherMode.ECB, PaddingMode padding = PaddingMode.PKCS7)
        {
            var cryptoBytes = hexStringToBytes(cryptoString);
            return DESDecrypt_Bytes(cryptoBytes, key, iv, encoding, cipher, padding);
        }

        /// <summary>
        /// DES解密（Base64字符串解密）
        /// </summary>
        /// <param name="cryptoString">密文</param>
        /// <param name="key">8位秘钥（使用默认秘钥）</param>
        /// <param name="iv">8位向量（CBC模式，使用默认向量）</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <param name="cipher">加密模式</param>
        /// <param name="padding">填充模式</param>
        /// <returns></returns>
        public static string DESDecrypt_Base64(string cryptoString, string key = null, string iv = null, Encoding encoding = null, CipherMode cipher = CipherMode.ECB, PaddingMode padding = PaddingMode.PKCS7)
        {
            var cryptoBytes = Convert.FromBase64String(cryptoString);
            return DESDecrypt_Bytes(cryptoBytes, key, iv, encoding, cipher, padding);
        }

        #endregion

        #region TripleDES

        /// <summary>
        /// 3DES默认秘钥
        /// </summary>
        private static readonly byte[] TRIPLE_DES_DEFAULT_KEY = Encoding.ASCII.GetBytes("$TRIPLE_DES_DEFAULT_KEY$");
        /// <summary>
        /// 3DES默认向量
        /// </summary>
        private static readonly byte[] TRIPLE_DES_DEFAULT_IV = Encoding.ASCII.GetBytes("$3DESIV$");


        /// <summary>
        /// 3DES加密（返回字节数组）
        /// </summary>
        /// <param name="originalString">原文</param>
        /// <param name="key">24位秘钥（使用默认秘钥）</param>
        /// <param name="iv">8位向量（CBC模式，使用默认向量）</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <param name="cipher">加密模式</param>
        /// <param name="padding">填充模式</param>
        /// <returns></returns>
        public static byte[] TripleDESEncrypt_Bytes(string originalString, string key = null, string iv = null, Encoding encoding = null, CipherMode cipher = CipherMode.ECB, PaddingMode padding = PaddingMode.PKCS7)
        {
            if (key.HasValue() && key.Length != 24)
                throw new ArgumentException("TripleDES密钥必须为24位.");
            if (iv.HasValue() && iv.Length != 8)
                throw new ArgumentException("TripleDES向量必须为8位.");

            using (var des = new TripleDESCryptoServiceProvider())
            {
                des.Mode = cipher;
                des.Padding = padding;
                des.Key = key.HasValue() ? Encoding.ASCII.GetBytes(key) : TRIPLE_DES_DEFAULT_KEY;
                des.IV = iv.HasValue() ? Encoding.ASCII.GetBytes(iv) : TRIPLE_DES_DEFAULT_IV;

                var originalBytes = (encoding ?? DefaultEncoding).GetBytes(originalString);
                return des.CreateEncryptor().TransformFinalBlock(originalBytes, 0, originalBytes.Length);
            }
        }

        /// <summary>
        /// 3DES加密（返回16进制字符串）
        /// </summary>
        /// <param name="originalString">原文</param>
        /// <param name="key">24位秘钥（使用默认秘钥）</param>
        /// <param name="iv">8位向量（CBC模式，使用默认向量）</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <param name="cipher">加密模式</param>
        /// <param name="padding">填充模式</param>
        /// <returns></returns>
        public static string TripleDESEncrypt_Hex(string originalString, string key = null, string iv = null, Encoding encoding = null, CipherMode cipher = CipherMode.ECB, PaddingMode padding = PaddingMode.PKCS7)
        {
            var resultBytes = TripleDESEncrypt_Bytes(originalString, key, iv, encoding, cipher, padding);
            return BitConverter.ToString(resultBytes).Replace("-", "");
        }

        /// <summary>
        /// 3DES加密（返回Base64字符串）
        /// </summary>
        /// <param name="originalString">原文</param>
        /// <param name="key">24位秘钥（使用默认秘钥）</param>
        /// <param name="iv">8位向量（CBC模式，使用默认向量）</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <param name="cipher">加密模式</param>
        /// <param name="padding">填充模式</param>
        /// <returns></returns>
        public static string TripleDESEncrypt_Base64(string originalString, string key = null, string iv = null, Encoding encoding = null, CipherMode cipher = CipherMode.ECB, PaddingMode padding = PaddingMode.PKCS7)
        {
            var resultBytes = TripleDESEncrypt_Bytes(originalString, key, iv, encoding, cipher, padding);
            return Convert.ToBase64String(resultBytes);
        }


        /// <summary>
        /// 3DES解密（字节数组解密）
        /// </summary>
        /// <param name="cryptoBytes">密文</param>
        /// <param name="key">24位秘钥（使用默认秘钥）</param>
        /// <param name="iv">8位向量（CBC模式，使用默认向量）</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <param name="cipher">加密模式</param>
        /// <param name="padding">填充模式</param>
        /// <returns></returns>
        public static string TripleDESDecrypt_Bytes(byte[] cryptoBytes, string key = null, string iv = null, Encoding encoding = null, CipherMode cipher = CipherMode.ECB, PaddingMode padding = PaddingMode.PKCS7)
        {
            if (key.HasValue() && key.Length != 24)
                throw new ArgumentException("TripleDES密钥必须为24位.");
            if (iv.HasValue() && iv.Length != 8)
                throw new ArgumentException("TripleDES向量必须为8位.");

            using (var des = new TripleDESCryptoServiceProvider())
            {
                des.Mode = CipherMode.ECB;
                des.Padding = PaddingMode.PKCS7;
                des.Key = key.HasValue() ? Encoding.ASCII.GetBytes(key) : TRIPLE_DES_DEFAULT_KEY;
                des.IV = iv.HasValue() ? Encoding.ASCII.GetBytes(iv) : TRIPLE_DES_DEFAULT_IV;

                var resultBytes = des.CreateDecryptor().TransformFinalBlock(cryptoBytes, 0, cryptoBytes.Length);
                return (encoding ?? DefaultEncoding).GetString(resultBytes);
            }
        }

        /// <summary>
        /// 3DES解密（16进制字符串解密）
        /// </summary>
        /// <param name="cryptoString">密文</param>
        /// <param name="key">24位秘钥（使用默认秘钥）</param>
        /// <param name="iv">8位向量（CBC模式，使用默认向量）</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <param name="cipher">加密模式</param>
        /// <param name="padding">填充模式</param>
        /// <returns></returns>
        public static string TripleDESDecrypt_Hex(string cryptoString, string key = null, string iv = null, Encoding encoding = null, CipherMode cipher = CipherMode.ECB, PaddingMode padding = PaddingMode.PKCS7)
        {
            var cryptoBytes = hexStringToBytes(cryptoString);
            return TripleDESDecrypt_Bytes(cryptoBytes, key, iv, encoding, cipher, padding);
        }

        /// <summary>
        /// 3DES解密（Base64字符串解密）
        /// </summary>
        /// <param name="cryptoString">密文</param>
        /// <param name="key">24位秘钥（使用默认秘钥）</param>
        /// <param name="iv">8位向量（CBC模式，使用默认向量）</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <param name="cipher">加密模式</param>
        /// <param name="padding">填充模式</param>
        /// <returns></returns>
        public static string TripleDESDecrypt_Base64(string cryptoString, string key = null, string iv = null, Encoding encoding = null, CipherMode cipher = CipherMode.ECB, PaddingMode padding = PaddingMode.PKCS7)
        {
            var cryptoBytes = Convert.FromBase64String(cryptoString);
            return TripleDESDecrypt_Bytes(cryptoBytes, key, iv, encoding, cipher, padding);
        }

        #endregion

        #region AES

        /// <summary>
        /// AES默认秘钥
        /// </summary>
        private static readonly byte[] AES_DEFAULT_KEY = Encoding.ASCII.GetBytes("$AES_DEFAULT_KEY");
        /// <summary>
        /// AES默认向量
        /// </summary>
        private static readonly byte[] AES_DEFAULT_IV = Encoding.ASCII.GetBytes("$AES_DEFAULT_IV$");


        /// <summary>
        /// AES加密（返回字节数组）
        /// </summary>
        /// <param name="originalString">原文</param>
        /// <param name="key">16位秘钥（使用默认秘钥）</param>
        /// <param name="iv">16位向量（CBC模式，使用默认向量）</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <param name="cipher">加密模式</param>
        /// <param name="padding">填充模式</param>
        /// <returns></returns>
        public static byte[] AESEncrypt_Bytes(string originalString, string key = null, string iv = null, Encoding encoding = null, CipherMode cipher = CipherMode.ECB, PaddingMode padding = PaddingMode.PKCS7)
        {
            if (key.HasValue() && key.Length != 16)
                throw new ArgumentException("AES密钥必须为16位.");
            if (iv.HasValue() && iv.Length != 16)
                throw new ArgumentException("AES向量必须为16位.");

            using (var aes = new AesCryptoServiceProvider())
            {
                aes.Mode = cipher;
                aes.Padding = padding;
                aes.Key = key.HasValue() ? Encoding.ASCII.GetBytes(key) : AES_DEFAULT_KEY;
                aes.IV = iv.HasValue() ? Encoding.ASCII.GetBytes(iv) : AES_DEFAULT_IV;

                var originalBytes = (encoding ?? DefaultEncoding).GetBytes(originalString);
                return aes.CreateEncryptor().TransformFinalBlock(originalBytes, 0, originalBytes.Length);
            }
        }

        /// <summary>
        /// AES加密（返回16进制字符串）
        /// </summary>
        /// <param name="originalString">原文</param>
        /// <param name="key">16位秘钥（使用默认秘钥）</param>
        /// <param name="iv">16位向量（CBC模式，使用默认向量）</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <param name="cipher">加密模式</param>
        /// <param name="padding">填充模式</param>
        /// <returns></returns>
        public static string AESEncrypt_Hex(string originalString, string key = null, string iv = null, Encoding encoding = null, CipherMode cipher = CipherMode.ECB, PaddingMode padding = PaddingMode.PKCS7)
        {
            var resultBytes = AESEncrypt_Bytes(originalString, key, iv, encoding, cipher, padding);
            return BitConverter.ToString(resultBytes).Replace("-", "");
        }

        /// <summary>
        /// AES加密（返回Base64字符串）
        /// </summary>
        /// <param name="originalString">原文</param>
        /// <param name="key">16位秘钥（使用默认秘钥）</param>
        /// <param name="iv">16位向量（CBC模式，使用默认向量）</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <param name="cipher">加密模式</param>
        /// <param name="padding">填充模式</param>
        /// <returns></returns>
        public static string AESEncrypt_Base64(string originalString, string key = null, string iv = null, Encoding encoding = null, CipherMode cipher = CipherMode.ECB, PaddingMode padding = PaddingMode.PKCS7)
        {
            var resultBytes = AESEncrypt_Bytes(originalString, key, iv, encoding, cipher, padding);
            return Convert.ToBase64String(resultBytes);
        }


        /// <summary>
        /// AES解密（字节数组解密）
        /// </summary>
        /// <param name="cryptoBytes">密文</param>
        /// <param name="key">16位秘钥（使用默认秘钥）</param>
        /// <param name="iv">16位向量（CBC模式，使用默认向量）</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <param name="cipher">加密模式</param>
        /// <param name="padding">填充模式</param>
        /// <returns></returns>
        public static string AESDecrypt_Bytes(byte[] cryptoBytes, string key = null, string iv = null, Encoding encoding = null, CipherMode cipher = CipherMode.ECB, PaddingMode padding = PaddingMode.PKCS7)
        {
            if (key.HasValue() && key.Length != 16)
                throw new ArgumentException("AES密钥必须为16位.");
            if (iv.HasValue() && iv.Length != 16)
                throw new ArgumentException("AES向量必须为16位.");

            using (var aes = new AesCryptoServiceProvider())
            {
                aes.Mode = CipherMode.ECB;
                aes.Padding = PaddingMode.PKCS7;
                aes.Key = key.HasValue() ? Encoding.ASCII.GetBytes(key) : AES_DEFAULT_KEY;
                aes.IV = iv.HasValue() ? Encoding.ASCII.GetBytes(iv) : AES_DEFAULT_IV;

                var resultBytes = aes.CreateDecryptor().TransformFinalBlock(cryptoBytes, 0, cryptoBytes.Length);
                return (encoding ?? DefaultEncoding).GetString(resultBytes);
            }
        }

        /// <summary>
        /// AES解密（16进制字符串解密）
        /// </summary>
        /// <param name="cryptoString">密文</param>
        /// <param name="key">16位秘钥（使用默认秘钥）</param>
        /// <param name="iv">16位向量（CBC模式，使用默认向量）</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <param name="cipher">加密模式</param>
        /// <param name="padding">填充模式</param>
        /// <returns></returns>
        public static string AESDecrypt_Hex(string cryptoString, string key = null, string iv = null, Encoding encoding = null, CipherMode cipher = CipherMode.ECB, PaddingMode padding = PaddingMode.PKCS7)
        {
            var cryptoBytes = hexStringToBytes(cryptoString);
            return AESDecrypt_Bytes(cryptoBytes, key, iv, encoding, cipher, padding);
        }

        /// <summary>
        /// AES解密（Base64字符串解密）
        /// </summary>
        /// <param name="cryptoString">密文</param>
        /// <param name="key">16位秘钥（使用默认秘钥）</param>
        /// <param name="iv">16位向量（CBC模式，使用默认向量）</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <param name="cipher">加密模式</param>
        /// <param name="padding">填充模式</param>
        /// <returns></returns>
        public static string AESDecrypt_Base64(string cryptoString, string key = null, string iv = null, Encoding encoding = null, CipherMode cipher = CipherMode.ECB, PaddingMode padding = PaddingMode.PKCS7)
        {
            var cryptoBytes = Convert.FromBase64String(cryptoString);
            return AESDecrypt_Bytes(cryptoBytes, key, iv, encoding, cipher, padding);
        }

        #endregion

        #region RSA

        /// <summary>
        /// RSA服务
        /// </summary>
        private static RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();

        /// <summary>
        /// RSA生成秘钥
        /// </summary>
        /// <returns>Item1: Exponent; Item2: Modulus;</returns>
        public static Tuple<string, string> RSAGenerateKey()
        {
            // 使用密钥容器
            var parameters = rsa.ExportParameters(true);

            return new Tuple<string, string>(
                BitConverter.ToString(parameters.Exponent).Replace("-", string.Empty),
                BitConverter.ToString(parameters.Modulus).Replace("-", string.Empty)
            );
        }


        /// <summary>
        /// RSA加密（返回字节数组）
        /// </summary>
        /// <param name="originalString">原文</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <returns></returns>
        public static byte[] RSAEncrypt_Bytes(string originalString, Encoding encoding = null)
        {
            var originalBytes = (encoding ?? DefaultEncoding).GetBytes(originalString);
            return rsa.Encrypt(originalBytes, false);
        }

        /// <summary>
        /// RSA加密（返回16进制字符串）
        /// </summary>
        /// <param name="originalString">原文</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <returns></returns>
        public static string RSAEncrypt_Hex(string originalString, Encoding encoding = null)
        {
            var resultBytes = RSAEncrypt_Bytes(originalString, encoding);
            return BitConverter.ToString(resultBytes).Replace("-", "");
        }

        /// <summary>
        /// RSA加密（返回Base64字符串）
        /// </summary>
        /// <param name="originalString">原文</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <returns></returns>
        public static string RSAEncrypt_Base64(string originalString, Encoding encoding = null)
        {
            var resultBytes = RSAEncrypt_Bytes(originalString, encoding);
            return Convert.ToBase64String(resultBytes);
        }


        /// <summary>
        /// RSA解密（字节数组解密）
        /// </summary>
        /// <param name="cryptoBytes">密文</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <returns></returns>
        public static string RSADecrypt_Bytes(byte[] cryptoBytes, Encoding encoding = null)
        {
            var resultBytes = rsa.Decrypt(cryptoBytes, false);
            return (encoding ?? DefaultEncoding).GetString(resultBytes);
        }

        /// <summary>
        /// RSA解密（16进制字符串解密）
        /// </summary>
        /// <param name="cryptoString">密文</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <returns></returns>
        public static string RSADecrypt_Hex(string cryptoString, Encoding encoding = null)
        {
            var cryptoBytes = hexStringToBytes(cryptoString);
            return RSADecrypt_Bytes(cryptoBytes, encoding);
        }

        /// <summary>
        /// RSA解密（Base64字符串解密）
        /// </summary>
        /// <param name="cryptoString">密文</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <returns></returns>
        public static string RSADecrypt_Base64(string cryptoString, Encoding encoding = null)
        {
            var cryptoBytes = Convert.FromBase64String(cryptoString);
            return RSADecrypt_Bytes(cryptoBytes, encoding);
        }

        #endregion

        #region XOR

        /// <summary>
        /// XOR默认秘钥
        /// </summary>
        private static readonly string XOR_DEFAULT_KEY = MD5Encrypt("$XOR_DEFAULT_KEY$");

        /// <summary>
        /// 异或加密（将秘钥长度重复补长或截短至原文长度）
        /// </summary>
        /// <param name="originalString">原文</param>
        /// <param name="key">秘钥（使用默认秘钥）</param>
        /// <returns></returns>
        public static string XOREncrypt(string originalString, string key = null)
        {
            // 补充秘钥长度
            if (!key.HasValue())
                key = XOR_DEFAULT_KEY;
            var finalKey = new StringBuilder(key);
            var keyLength = originalString.Length;
            while (finalKey.Length < keyLength)
                finalKey.Append(key);

            // 异或处理
            return string.Concat(originalString.Zip(finalKey.ToString().Substring(0, keyLength), (c1, c2) => (char)(c1 ^ c2)));
        }

        /// <summary>
        /// 异或解密（将秘钥长度重复补长或截短至密文长度）
        /// </summary>
        /// <param name="cryptoString">密文</param>
        /// <param name="key">秘钥（使用默认秘钥）</param>
        /// <returns></returns>
        public static string XORDecrypt(string cryptoString, string key = null)
        {
            return XOREncrypt(cryptoString, key);
        }

        #endregion

        #region Password

        /// <summary>
        /// 密码解密
        /// （使用多重加解密方式）
        /// </summary>
        /// <param name="cryptoString">密文</param>
        /// <param name="encoding">编码方式（默认为UTF8）</param>
        /// <returns></returns>
        public static string PasswordDecrypt(string cryptoString, Encoding encoding = null)
        {
            var codes = "ghijklmnopqrstuvwxyz";
            var cryptoChars = cryptoString.ToCharArray();

            var valueBuilder = new StringBuilder();
            var signedBuilder = new StringBuilder();
            var code = '0';
            for (int i = 0, length = cryptoString.Length / 2; i < length; ++i)
            {
                var c1 = cryptoChars[i * 2];
                var c2 = cryptoChars[i * 2 + 1];

                var c = codes.Contains(code) ?
                    codes.IndexOf(code) :
                    int.Parse(code.ToString());
                if (c % 2 == 0)
                {
                    if (!codes.Contains(c1))
                        valueBuilder.Append(c1);
                    if (signedBuilder.Length < 40)
                        signedBuilder.Append(c2);
                    code = c1;
                }
                else
                {
                    if (!codes.Contains(c2))
                        valueBuilder.Append(c2);
                    if (signedBuilder.Length < 40)
                        signedBuilder.Append(c1);
                    code = c2;
                }
            }

            var value = valueBuilder.ToString();
            var t2Builder = new StringBuilder();
            for (int i = 0, length = value.Length / 3; i < length; ++i)
            {
                var c = Convert.ToInt32(value.Substring(i * 3, 1));
                if (c % 2 == 0)
                {
                    t2Builder.Append(value.Substring(i * 3 + 1, 2));
                }
                else
                {
                    t2Builder.Append(value.Substring(i * 3 + 2, 1));
                    t2Builder.Append(value.Substring(i * 3 + 1, 1));
                }
            }

            if (SHA1Encrypt(t2Builder.ToString(), encoding).ToLower() == signedBuilder.ToString())
            {
                if (t2Builder.Length % 3 != 0)
                    t2Builder.Remove(0, 1);
                var t2 = t2Builder.ToString();

                var t1Builder = new StringBuilder();
                for (int i = 0, length = t2.Length / 3; i < length; ++i)
                {
                    t1Builder.Append((char)Convert.ToInt32(t2.Substring(i * 3, 3)));
                }

                return BASE64Decrypt(t1Builder.ToString(), encoding);
            }

            return "";
        }

        #endregion

        #region Parameter

        private static readonly string PARAMETER_ORIGINALS = "+/0123456789=ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        private static readonly string PARAMETER_CRYPTOS = "P782ahA_jByC69c-lQ0DovSUOrg1z!tEdFGZeHxI3JV5KRwYLfTnX4puksMmWbiqN";
        private static readonly IEnumerable<KeyValuePair<char, char>> PARAMETER_CODES = PARAMETER_ORIGINALS.Select((v, i) => new KeyValuePair<char, char>(v, PARAMETER_CRYPTOS[i]));


        /// <summary>
        /// 参数加密
        /// </summary>
        /// <param name="parameters">参数数组</param>
        /// <returns></returns>
        public static string ParameterEncrypt(params object[] parameters)
        {
            if (parameters == null)
                parameters = new object[0];
            var builder = new StringBuilder("!");
            foreach (var c in BASE64Encrypt(JsonUtility.Serialize(parameters)))
                builder.Append(PARAMETER_CODES.First(i => i.Key == c).Value);

            return builder.ToString();
        }

        /// <summary>
        /// 参数解密
        /// </summary>
        /// <param name="cryptoString">密文</param>
        /// <returns></returns>
        public static object[] ParameterDecrypt(string cryptoString)
        {
            if (cryptoString.StartsWith("!"))
            {
                var builder = new StringBuilder();
                foreach (var c in cryptoString.Substring(1))
                    builder.Append(PARAMETER_CODES.First(i => i.Value == c).Key);
                cryptoString = builder.ToString();
            }

            return JsonUtility.Deserialize<object[]>(BASE64Decrypt(cryptoString));
        }

        #endregion
    }
}
