﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;

namespace APP.Core.Utilities
{
    public static class ShortUrlUtility
    {
        private static readonly Regex REGEX_URL = new Regex("url\":\"([^\"]*)\"");
        private static readonly Regex REGEX_SHORTURL = new Regex("shorturl\":\"([^\"]*)\"");
        private static readonly Regex REGEX_URL_SHORT = new Regex("url_short\":\"([^\"]*)\"");

        private readonly static IDictionary<string, string> URL_SETTINGS = new Dictionary<string, string>
        {
            { "SUO_IM", "http://suo.im/api.php?format=json&url=" },
            { "U6_GG", "http://u6.gg/api.php?format=json&url=" },
            { "C7_GG", "http://c7.gg/api.php?format=json&url=" },
            { "KKS_ME", "http://kks.me/api.php?format=json&url=" },
            { "UEE_ME", "http://uee.me/api.php?format=json&url=" },
            { "RRD_ME", "http://rrd.me/api.php?format=json&url=" },
            { "T_CN", "https://goo.gd/action/json.php?source=1681459862&url_long=" },
            { "DWZ_IS", "https://goo.gd/action/json-is.php?url_long=" }
        };


        private static string GetUrl(string result, Regex regex)
        {
            var match = regex.Match(result);
            if (match == null || match.Groups.Count < 2)
                return null;

            return match.Groups[1].Value.Replace("\\", null);
        }


        /// <summary>
        /// 生成 suo.im 域名下短网址
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string SUO_IM(string url)
        {
            var result = HttpRequestUtility.Get(URL_SETTINGS["SUO_IM"] + HttpUtility.UrlEncode(url));
            return GetUrl(result, REGEX_URL);
        }

        /// <summary>
        /// 生成 u6.gg 域名下短网址
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string U6_GG(string url)
        {
            var result = HttpRequestUtility.Get(URL_SETTINGS["U6_GG"] + HttpUtility.UrlEncode(url));
            return GetUrl(result, REGEX_URL);
        }

        /// <summary>
        /// 生成 c7.gg 域名下短网址
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string C7_GG(string url)
        {
            var result = HttpRequestUtility.Get(URL_SETTINGS["C7_GG"] + HttpUtility.UrlEncode(url));
            return GetUrl(result, REGEX_URL);
        }

        /// <summary>
        /// 生成 kks.me 域名下短网址
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string KKS_ME(string url)
        {
            var result = HttpRequestUtility.Get(URL_SETTINGS["KKS_ME"] + HttpUtility.UrlEncode(url));
            return GetUrl(result, REGEX_URL);
        }

        /// <summary>
        /// 生成 uee.me 域名下短网址
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string UEE_ME(string url)
        {
            var result = HttpRequestUtility.Get(URL_SETTINGS["UEE_ME"] + HttpUtility.UrlEncode(url));
            return GetUrl(result, REGEX_URL);
        }

        /// <summary>
        /// 生成 rrd.me 域名下短网址
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string RRD_ME(string url)
        {
            var result = HttpRequestUtility.Get(URL_SETTINGS["RRD_ME"] + HttpUtility.UrlEncode(url));
            return GetUrl(result, REGEX_URL);
        }

        /// <summary>
        /// 生成 t.cn 域名下短网址
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string T_CN(string url)
        {
            var result = HttpRequestUtility.Get(URL_SETTINGS["T_CN"] + HttpUtility.UrlEncode(url));
            return GetUrl(result, REGEX_URL_SHORT);
        }

        /// <summary>
        /// 生成 dwz.is 域名下短网址
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string DWZ_IS(string url)
        {
            var result = HttpRequestUtility.Get(URL_SETTINGS["DWZ_IS"] + HttpUtility.UrlEncode(url));
            return GetUrl(result, REGEX_SHORTURL);
        }
    }
}
