﻿using System.IO;
using System.Text;

namespace APP.Core.Utilities
{
    public static class HashCodeUtility
    {
        /// <summary>
        /// 获取一个文件的 Hash Code
        /// </summary>
        /// <param name="filePath">文件路径</param>
        /// <returns></returns>
        public static string GetFileHashCode(string filePath)
        {
            if (!filePath.HasValue())
                return null;
            if (!File.Exists(filePath))
                return null;

            var text = File.ReadAllText(filePath, Encoding.UTF8);
            return SecurityUtility.MD5Encrypt(text);
        }
    }
}
