﻿using APP.Core.Models;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using NPOI.XWPF.UserModel;
using System;
using System.Data;
using System.IO;
using System.Linq;

namespace APP.Core.Utilities
{
    public static class MicrosoftOfficeUtility
    {
        private const int MAX_COUNT_OF_ROWS_PER_SHEET = 65500;
        private const float HEADER_HEIGHT = 12.75f;
        private const float ROW_HEIGHT = 12.75f;


        /// <summary>
        /// 导入 Excel
        /// </summary>
        /// <param name="filePath">文件路径</param>
        /// <param name="sheetName">Sheet 名称，默认取第一个 Sheet</param>
        /// <param name="exportColumnName">包括顶部标题行</param>
        /// <returns></returns>
        public static DataTable ImportExcel(string filePath, string sheetName = null, bool exportColumnName = false)
        {
            HSSFWorkbook workbook;
            using (var file = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                workbook = new HSSFWorkbook(file);
                file.Close();
            }

            var sheet = sheetName.HasValue() ? workbook.GetSheet(sheetName) : workbook.GetSheetAt(0);
            workbook = null;
            var rows = sheet.GetRowEnumerator();

            var dt = new DataTable();
            var headerRow = sheet.GetRow(0);
            var cellCount = headerRow.LastCellNum;

            // Header
            for (var i = 0; i < cellCount; i++)
            {
                var cell = headerRow.GetCell(i);
                dt.Columns.Add(cell.ToString());
            }

            // Rows
            while (rows.MoveNext())
            {
                var row = (HSSFRow)rows.Current;
                var dr = dt.NewRow();
                for (var i = 0; i < cellCount; i++)
                {
                    var cell = row.GetCell(i);
                    if (cell == null) dr[i] = null;
                    else
                    {
                        var cellType = cell.CellType;
                        DateTime dValue;
                        if (DateTime.TryParse(cell.ToString(), out dValue))
                            dr[i] = cell.ToString();
                        else if (cellType == CellType.Formula || cellType == CellType.Numeric)
                            dr[i] = cell.NumericCellValue;
                        else
                            dr[i] = cell.ToString();
                    }
                }
                dt.Rows.Add(dr);
            }

            if (!exportColumnName)
                dt.Rows.Remove(dt.Rows[0]);

            return dt;
        }

        /// <summary>
        /// 导出 Excel
        /// </summary>
        /// <param name="info">数据</param>
        /// <returns></returns>
        public static MemoryStream ExportExcel(ExportExcelInfo info)
        {
            var workbook = new HSSFWorkbook();

            var sheetIndex = 0;
            var currRowIndex = 0;
            var sheet = workbook.CreateSheet(info.SheetName);

            // Header
            if (info.HeaderCells != null && info.HeaderCells.Any())
            {
                var headerCellStyle = workbook.CreateCellStyle();
                headerCellStyle.FillPattern = FillPattern.SolidForeground;
                headerCellStyle.FillForegroundColor = HSSFColor.Grey25Percent.Index;

                var headerLabelFont = workbook.CreateFont();
                headerLabelFont.Boldweight = (short)FontBoldWeight.Bold;
                headerCellStyle.SetFont(headerLabelFont);

                var headerRow = sheet.CreateRow(currRowIndex);
                headerRow.HeightInPoints = HEADER_HEIGHT;
                for (int i = 0, cellCount = info.HeaderCells.Length; i < cellCount; i++)
                {
                    var cell = headerRow.CreateCell(i);
                    cell.SetCellValue(info.HeaderCells[i]);
                    if (headerCellStyle != null)
                        cell.CellStyle = headerCellStyle;
                }

                ++currRowIndex;
            }

            // Rows
            if (info.Rows != null && info.Rows.Any())
            {
                foreach (var row in info.Rows)
                {
                    if (currRowIndex >= MAX_COUNT_OF_ROWS_PER_SHEET)
                    {
                        sheet = workbook.CreateSheet(string.Format("{0}-{1}", info.SheetName, ++sheetIndex));
                        currRowIndex = 0;
                    }

                    var newRow = sheet.CreateRow(currRowIndex++);
                    newRow.HeightInPoints = ROW_HEIGHT;
                    for (int i = 0, cellCount = row.Length; i < cellCount; i++)
                    {
                        var cell = newRow.CreateCell(i);

                        if (row[i] == null)
                            cell.SetCellValue("");
                        else
                        {
                            var value = row[i].ToString();
                            switch (row[i].GetType().ToString())
                            {
                                case "System.String":
                                    cell.SetCellValue(value);
                                    break;

                                case "System.DateTime":
                                    DateTime dateTimeV;
                                    if (DateTime.TryParse(value, out dateTimeV))
                                        cell.SetCellValue(dateTimeV);
                                    break;

                                case "System.Boolean":
                                    bool boolV;
                                    if (bool.TryParse(value, out boolV))
                                        cell.SetCellValue(boolV);
                                    break;

                                case "System.Int16":
                                case "System.Int32":
                                case "System.Int64":
                                case "System.Byte":
                                case "System.Decimal":
                                case "System.Double":
                                    double doubleV;
                                    if (double.TryParse(value, out doubleV))
                                        cell.SetCellValue(doubleV);
                                    break;

                                default:
                                    cell.SetCellValue("");
                                    break;
                            }
                        }
                    }
                }
            }

            // Auto Size
            for (int i = 0, cellCount = info.HeaderCells.Length; i < cellCount; i++)
                sheet.AutoSizeColumn(i);

            using (var ms = new MemoryStream())
            {
                workbook.Write(ms);
                workbook = null;
                return ms;
            }
        }

        /// <summary>
        /// 创建 Excel 文件
        /// </summary>
        /// <param name="savePath">保存路径</param>
        /// <param name="info">数据</param>
        public static void CreateExcel(string savePath, ExportExcelInfo info)
        {
            using (var fs = new FileStream(savePath, FileMode.Create, FileAccess.Write))
            {
                var ms = ExportExcel(info);
                var data = ms.ToArray();
                fs.Write(data, 0, data.Length);
                fs.Flush();
                fs.Close();
            }
        }


        /// <summary>
        /// 导出 Word
        /// </summary>
        /// <param name="info">数据</param>
        /// <returns></returns>
        public static MemoryStream ExportWord(ExportWordInfo info)
        {
            var document = new XWPFDocument();

            foreach (var row in info.Rows)
            {
                var paragraph = document.CreateParagraph();
                switch (row.TextAlign ?? Enums.Alignment.Left)
                {
                    case Enums.Alignment.Left: paragraph.Alignment = ParagraphAlignment.LEFT; break;
                    case Enums.Alignment.Center: paragraph.Alignment = ParagraphAlignment.CENTER; break;
                    case Enums.Alignment.Right: paragraph.Alignment = ParagraphAlignment.RIGHT; break;
                }
                paragraph.SpacingBefore = row.SpacingBefore ?? 40;
                paragraph.SpacingAfter = row.SpacingAfter ?? 40;

                if (row.Items != null && row.Items.Any())
                {
                    foreach (var item in row.Items)
                    {
                        var run = paragraph.CreateRun();
                        run.FontFamily = item.FontName ?? null;
                        run.FontSize = item.FontSize ?? 12;
                        run.IsBold = item.IsBold ?? false;
                        run.IsItalic = item.IsItalic ?? false;
                        run.SetColor(item.ColorRGBValue ?? "000000");
                        if (item.IsUnderline ?? false)
                            run.SetUnderline(UnderlinePatterns.Single);

                        run.SetText(item.Text);
                    }
                }
            }

            using (var ms = new MemoryStream())
            {
                document.Write(ms);
                document = null;
                return ms;
            }
        }

        /// <summary>
        /// 创建 Word 文件
        /// </summary>
        /// <param name="savePath">保存路径</param>
        /// <param name="info">数据</param>
        public static void CreateWord(string savePath, ExportWordInfo info)
        {
            using (var fs = new FileStream(savePath, FileMode.Create, FileAccess.Write))
            {
                var ms = ExportWord(info);
                var data = ms.ToArray();
                fs.Write(data, 0, data.Length);
                fs.Flush();
                fs.Close();
            }
        }
    }
}
