﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace APP.Core.Utilities
{
    public class MailUtility
    {
        /* 使用示例
        try
        {
            var instance = new MailUtility("smtp.exmail.qq.com", null, "lsg@360juyou.com", "1989sgSG", "sugar.lin");
            instance.SendEmail("wlz@360juyou.com", "test", "test.");
        }
        catch (System.Net.Mail.SmtpException ex)
        {
            LogUtility.LogError(ex);
        }
         */

        /// <summary>
        /// 验证 Email 地址
        /// </summary>
        public static Regex RegexEmail = new Regex(CoreConfig.RegexEmail);

        /// <summary>
        /// SMTP HOST
        /// </summary>
        public string SmtpHost { get; }
        /// <summary>
        /// SMTP PORT
        /// </summary>
        public int? SmtpPort { get; }
        /// <summary>
        /// 发送者邮箱地址
        /// </summary>
        public string FromAddress { get; }
        /// <summary>
        /// 发送者邮箱密码
        /// </summary>
        public string FromPassword { get; }
        /// <summary>
        /// 发送者姓名
        /// </summary>
        public string FromName { get; }
        /// <summary>
        /// 超时时间
        /// </summary>
        public int Timeout { get; }
        /// <summary>
        /// 使用 SSL
        /// </summary>
        public bool EnableSSL { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="smtpHost">SMTP HOST，不允许空</param>
        /// <param name="smtpPort">SMTP PORT，允许空</param>
        /// <param name="fromAddress">发送者邮箱地址，不允许空</param>
        /// <param name="fromPassword">发送者邮箱密码，不允许空</param>
        /// <param name="fromName">发送者姓名，允许空</param>
        public MailUtility(string smtpHost, int? smtpPort, string fromAddress, string fromPassword, string fromName, int timeout = 5000, bool enableSSL = true)
        {
            if (!smtpHost.HasValue())
                throw new ArgumentNullException("smtpHost");
            if (!IsAddressValid(fromAddress))
                throw new ArgumentNullException("fromAddress");
            if (!fromPassword.HasValue())
                throw new ArgumentNullException("fromPassword");
            if (timeout <= 0)
                throw new ArgumentException("timeout");

            SmtpHost = smtpHost;
            SmtpPort = smtpPort;
            FromAddress = fromAddress;
            FromPassword = fromPassword;
            FromName = fromName;
            Timeout = timeout;
            EnableSSL = enableSSL;
        }


        /// <summary>
        /// 验证有效性
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        private bool IsAddressValid(string address)
        {
            return address.HasValue() && RegexEmail.IsMatch(address);
        }

        /// <summary>
        /// 验证有效性
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        private bool IsAddressValid(MailAddress address)
        {
            return address != null && address.Address.HasValue() && RegexEmail.IsMatch(address.Address);
        }

        /// <summary>
        /// 创建 MAIL MESSAGE
        /// </summary>
        /// <param name="TOs"></param>
        /// <param name="CCs"></param>
        /// <param name="BCCs"></param>
        /// <param name="subject"></param>
        /// <param name="content"></param>
        /// <param name="attachments"></param>
        /// <returns>MAIL MESSAGE</returns>
        private MailMessage BuildMailMessage(IEnumerable<MailAddress> TOs, IEnumerable<MailAddress> CCs, IEnumerable<MailAddress> BCCs, string subject, string content, IEnumerable<Attachment> attachments)
        {
            // 验证有效性
            if (TOs == null)
                throw new ArgumentNullException("TOs");
            TOs = TOs.Where(to => IsAddressValid(to));
            if (!TOs.Any())
                throw new ArgumentNullException("TOs");

            // MAIL MESSAGE
            var mailMessage = new MailMessage
            {
                // 发送人
                From = new MailAddress(FromAddress, FromName, Encoding.UTF8),
                // 标题 & 内容
                Subject = subject,
                SubjectEncoding = Encoding.UTF8,
                Body = content,
                IsBodyHtml = true,
                BodyEncoding = Encoding.UTF8
            };

            // 收件人
            foreach (var to in TOs)
                mailMessage.To.Add(to);
            // 抄送
            if (CCs != null)
                foreach (var cc in CCs.Where(cc => IsAddressValid(cc)))
                    mailMessage.CC.Add(cc);
            // 密送
            if (BCCs != null)
                foreach (var bcc in BCCs.Where(bcc => IsAddressValid(bcc)))
                    mailMessage.CC.Add(bcc);

            // 附件
            if (attachments != null)
                foreach (var attachment in attachments)
                    mailMessage.Attachments.Add(attachment);

            return mailMessage;
        }

        /// <summary>
        /// 创建 SMTP CLIENT
        /// </summary>
        /// <returns>SMTP CLIENT</returns>
        private SmtpClient BuildSmtpClient()
        {
            // SMTP CLIENT
            var smtpClient = new SmtpClient
            {
                Host = SmtpHost,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(FromAddress, FromPassword),
                DeliveryMethod = SmtpDeliveryMethod.Network,
                EnableSsl = EnableSSL,
                Timeout = Timeout
            };

            // 端口
            if (SmtpPort.HasValue)
                smtpClient.Port = SmtpPort.Value;

            return smtpClient;
        }


        #region 发送邮件

        #region 同步

        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="to">收件地址</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        public void SendEmail(string to, string subject, string content)
        {
            SendEmail(to, null, null, subject, content, null);
        }
        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="to">收件地址</param>
        /// <param name="cc">抄送地址</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        public void SendEmail(string to, string cc, string subject, string content)
        {
            SendEmail(to, cc, null, subject, content, null);
        }
        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="to">收件地址</param>
        /// <param name="cc">抄送地址</param>
        /// <param name="bcc">密送地址</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        public void SendEmail(string to, string cc, string bcc, string subject, string content)
        {
            SendEmail(to, cc, bcc, subject, content, null);
        }
        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="to">收件地址</param>
        /// <param name="cc">抄送地址</param>
        /// <param name="bcc">密送地址</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        /// <param name="attachmentFileUrl">邮件附件（本地路径）</param>
        public void SendEmail(string to, string cc, string bcc, string subject, string content, string attachmentFileUrl)
        {
            SendEmail(
                to.HasValue() ? new MailAddress[] { new MailAddress(to, null, Encoding.UTF8) } : null,
                cc.HasValue() ? new MailAddress[] { new MailAddress(cc, null, Encoding.UTF8) } : null,
                bcc.HasValue() ? new MailAddress[] { new MailAddress(bcc, null, Encoding.UTF8) } : null,
                subject,
                content,
                attachmentFileUrl.HasValue() ? new string[] { attachmentFileUrl } : null
            );
        }

        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="TOs">收件地址列表</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        public void SendEmail(IEnumerable<string> TOs, string subject, string content)
        {
            SendEmail(TOs, null, null, subject, content, null);
        }
        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="TOs">收件地址列表</param>
        /// <param name="CCs">抄送地址列表</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        public void SendEmail(IEnumerable<string> TOs, IEnumerable<string> CCs, string subject, string content)
        {
            SendEmail(TOs, CCs, null, subject, content, null);
        }
        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="TOs">收件地址列表</param>
        /// <param name="CCs">抄送地址列表</param>
        /// <param name="BCCs">密送地址列表</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        public void SendEmail(IEnumerable<string> TOs, IEnumerable<string> CCs, IEnumerable<string> BCCs, string subject, string content)
        {
            SendEmail(TOs, CCs, BCCs, subject, content, null);
        }
        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="TOs">收件地址列表</param>
        /// <param name="CCs">抄送地址列表</param>
        /// <param name="BCCs">密送地址列表</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        /// <param name="attachmentFileUrls">邮件附件（本地路径）</param>
        public void SendEmail(IEnumerable<string> TOs, IEnumerable<string> CCs, IEnumerable<string> BCCs, string subject, string content, IEnumerable<string> attachmentFileUrls)
        {
            SendEmail(
                TOs == null ? null : TOs.Where(to => to.HasValue()).Select(to => new MailAddress(to, null, Encoding.UTF8)),
                CCs == null ? null : CCs.Where(cc => cc.HasValue()).Select(cc => new MailAddress(cc, null, Encoding.UTF8)),
                BCCs == null ? null : BCCs.Where(bcc => bcc.HasValue()).Select(bcc => new MailAddress(bcc, null, Encoding.UTF8)),
                subject,
                content,
                attachmentFileUrls
            );
        }

        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="to">收件地址</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        public void SendEmail(MailAddress to, string subject, string content)
        {
            SendEmail(to, null, null, subject, content, null);
        }
        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="to">收件地址</param>
        /// <param name="cc">抄送地址</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        public void SendEmail(MailAddress to, MailAddress cc, string subject, string content)
        {
            SendEmail(to, cc, null, subject, content, null);
        }
        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="to">收件地址</param>
        /// <param name="cc">抄送地址</param>
        /// <param name="bcc">密送地址</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        public void SendEmail(MailAddress to, MailAddress cc, MailAddress bcc, string subject, string content)
        {
            SendEmail(to, cc, bcc, subject, content, null);
        }
        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="to">收件地址</param>
        /// <param name="cc">抄送地址</param>
        /// <param name="bcc">密送地址</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        /// <param name="attachmentFileUrl">邮件附件（本地路径）</param>
        public void SendEmail(MailAddress to, MailAddress cc, MailAddress bcc, string subject, string content, string attachmentFileUrl)
        {
            SendEmail(
                new MailAddress[] { to },
                new MailAddress[] { cc },
                new MailAddress[] { bcc },
                subject,
                content,
                attachmentFileUrl.HasValue() ? new string[] { attachmentFileUrl } : null
            );
        }

        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="TOs">收件地址列表</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        public void SendEmail(IEnumerable<MailAddress> TOs, string subject, string content)
        {
            SendEmail(TOs, null, null, subject, content, null);
        }
        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="TOs">收件地址列表</param>
        /// <param name="CCs">抄送地址列表</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        public void SendEmail(IEnumerable<MailAddress> TOs, IEnumerable<MailAddress> CCs, string subject, string content)
        {
            SendEmail(TOs, CCs, null, subject, content, null);
        }
        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="TOs">收件地址列表</param>
        /// <param name="CCs">抄送地址列表</param>
        /// <param name="BCCs">密送地址列表</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        public void SendEmail(IEnumerable<MailAddress> TOs, IEnumerable<MailAddress> CCs, IEnumerable<MailAddress> BCCs, string subject, string content)
        {
            SendEmail(TOs, CCs, BCCs, subject, content, null);
        }
        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="TOs">收件地址列表</param>
        /// <param name="CCs">抄送地址列表</param>
        /// <param name="BCCs">密送地址列表</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        /// <param name="attachmentFileUrls">邮件附件（本地路径）</param>
        public void SendEmail(IEnumerable<MailAddress> TOs, IEnumerable<MailAddress> CCs, IEnumerable<MailAddress> BCCs, string subject, string content, IEnumerable<string> attachmentFileUrls)
        {
            var attachments = attachmentFileUrls == null ? null : attachmentFileUrls.Where(url => url.HasValue()).Select(url => new Attachment(url));
            using (var mailMessage = BuildMailMessage(TOs, CCs, BCCs, subject, content, attachments))
            using (var smtpClient = BuildSmtpClient())
            {
                smtpClient.Send(mailMessage);
            }
        }

        #endregion

        #region 异步

        /// <summary>
        /// 异步发送邮件
        /// </summary>
        /// <param name="to">收件地址</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        public async Task SendEmailAsync(string to, string subject, string content)
        {
            await SendEmailAsync(to, null, null, subject, content, null);
        }
        /// <summary>
        /// 异步发送邮件
        /// </summary>
        /// <param name="to">收件地址</param>
        /// <param name="cc">抄送地址</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        public async Task SendEmailAsync(string to, string cc, string subject, string content)
        {
            await SendEmailAsync(to, cc, null, subject, content, null);
        }
        /// <summary>
        /// 异步发送邮件
        /// </summary>
        /// <param name="to">收件地址</param>
        /// <param name="cc">抄送地址</param>
        /// <param name="bcc">密送地址</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        public async Task SendEmailAsync(string to, string cc, string bcc, string subject, string content)
        {
            await SendEmailAsync(to, cc, bcc, subject, content, null);
        }
        /// <summary>
        /// 异步发送邮件
        /// </summary>
        /// <param name="to">收件地址</param>
        /// <param name="cc">抄送地址</param>
        /// <param name="bcc">密送地址</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        /// <param name="attachmentFileUrl">邮件附件（本地路径）</param>
        public async Task SendEmailAsync(string to, string cc, string bcc, string subject, string content, string attachmentFileUrl)
        {
            await SendEmailAsync(
                to.HasValue() ? new MailAddress[] { new MailAddress(to, null, Encoding.UTF8) } : null,
                cc.HasValue() ? new MailAddress[] { new MailAddress(cc, null, Encoding.UTF8) } : null,
                bcc.HasValue() ? new MailAddress[] { new MailAddress(bcc, null, Encoding.UTF8) } : null,
                subject,
                content,
                attachmentFileUrl.HasValue() ? new string[] { attachmentFileUrl } : null
            );
        }

        /// <summary>
        /// 异步发送邮件
        /// </summary>
        /// <param name="TOs">收件地址列表</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        public async Task SendEmailAsync(IEnumerable<string> TOs, string subject, string content)
        {
            await SendEmailAsync(TOs, null, null, subject, content, null);
        }
        /// <summary>
        /// 异步发送邮件
        /// </summary>
        /// <param name="TOs">收件地址列表</param>
        /// <param name="CCs">抄送地址列表</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        public async Task SendEmailAsync(IEnumerable<string> TOs, IEnumerable<string> CCs, string subject, string content)
        {
            await SendEmailAsync(TOs, CCs, null, subject, content, null);
        }
        /// <summary>
        /// 异步发送邮件
        /// </summary>
        /// <param name="TOs">收件地址列表</param>
        /// <param name="CCs">抄送地址列表</param>
        /// <param name="BCCs">密送地址列表</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        public async Task SendEmailAsync(IEnumerable<string> TOs, IEnumerable<string> CCs, IEnumerable<string> BCCs, string subject, string content)
        {
            await SendEmailAsync(TOs, CCs, BCCs, subject, content, null);
        }
        /// <summary>
        /// 异步发送邮件
        /// </summary>
        /// <param name="TOs">收件地址列表</param>
        /// <param name="CCs">抄送地址列表</param>
        /// <param name="BCCs">密送地址列表</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        /// <param name="attachmentFileUrls">邮件附件（本地路径）</param>
        public async Task SendEmailAsync(IEnumerable<string> TOs, IEnumerable<string> CCs, IEnumerable<string> BCCs, string subject, string content, IEnumerable<string> attachmentFileUrls)
        {
            await SendEmailAsync(
                TOs == null ? null : TOs.Where(to => to.HasValue()).Select(to => new MailAddress(to, null, Encoding.UTF8)),
                CCs == null ? null : CCs.Where(cc => cc.HasValue()).Select(cc => new MailAddress(cc, null, Encoding.UTF8)),
                BCCs == null ? null : BCCs.Where(bcc => bcc.HasValue()).Select(bcc => new MailAddress(bcc, null, Encoding.UTF8)),
                subject,
                content,
                attachmentFileUrls
            );
        }

        /// <summary>
        /// 异步发送邮件
        /// </summary>
        /// <param name="to">收件地址</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        public async Task SendEmailAsync(MailAddress to, string subject, string content)
        {
            await SendEmailAsync(to, null, null, subject, content, null);
        }
        /// <summary>
        /// 异步发送邮件
        /// </summary>
        /// <param name="to">收件地址</param>
        /// <param name="cc">抄送地址</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        public async Task SendEmailAsync(MailAddress to, MailAddress cc, string subject, string content)
        {
            await SendEmailAsync(to, cc, null, subject, content, null);
        }
        /// <summary>
        /// 异步发送邮件
        /// </summary>
        /// <param name="to">收件地址</param>
        /// <param name="cc">抄送地址</param>
        /// <param name="bcc">密送地址</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        public async Task SendEmailAsync(MailAddress to, MailAddress cc, MailAddress bcc, string subject, string content)
        {
            await SendEmailAsync(to, cc, bcc, subject, content, null);
        }
        /// <summary>
        /// 异步发送邮件
        /// </summary>
        /// <param name="to">收件地址</param>
        /// <param name="cc">抄送地址</param>
        /// <param name="bcc">密送地址</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        /// <param name="attachmentFileUrl">邮件附件（本地路径）</param>
        public async Task SendEmailAsync(MailAddress to, MailAddress cc, MailAddress bcc, string subject, string content, string attachmentFileUrl)
        {
            await SendEmailAsync(
                new MailAddress[] { to },
                new MailAddress[] { cc },
                new MailAddress[] { bcc },
                subject,
                content,
                attachmentFileUrl.HasValue() ? new string[] { attachmentFileUrl } : null
            );
        }

        /// <summary>
        /// 异步发送邮件
        /// </summary>
        /// <param name="TOs">收件地址列表</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        public async Task SendEmailAsync(IEnumerable<MailAddress> TOs, string subject, string content)
        {
            await SendEmailAsync(TOs, null, null, subject, content, null);
        }
        /// <summary>
        /// 异步发送邮件
        /// </summary>
        /// <param name="TOs">收件地址列表</param>
        /// <param name="CCs">抄送地址列表</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        public async Task SendEmailAsync(IEnumerable<MailAddress> TOs, IEnumerable<MailAddress> CCs, string subject, string content)
        {
            await SendEmailAsync(TOs, CCs, null, subject, content, null);
        }
        /// <summary>
        /// 异步发送邮件
        /// </summary>
        /// <param name="TOs">收件地址列表</param>
        /// <param name="CCs">抄送地址列表</param>
        /// <param name="BCCs">密送地址列表</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        public async Task SendEmailAsync(IEnumerable<MailAddress> TOs, IEnumerable<MailAddress> CCs, IEnumerable<MailAddress> BCCs, string subject, string content)
        {
            await SendEmailAsync(TOs, CCs, BCCs, subject, content, null);
        }
        /// <summary>
        /// 异步发送邮件
        /// </summary>
        /// <param name="TOs">收件地址列表</param>
        /// <param name="CCs">抄送地址列表</param>
        /// <param name="BCCs">密送地址列表</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容（支持HTML）</param>
        /// <param name="attachmentFileUrls">邮件附件（本地路径）</param>
        public async Task SendEmailAsync(IEnumerable<MailAddress> TOs, IEnumerable<MailAddress> CCs, IEnumerable<MailAddress> BCCs, string subject, string content, IEnumerable<string> attachmentFileUrls)
        {
            var attachments = attachmentFileUrls == null ? null : attachmentFileUrls.Where(url => url.HasValue()).Select(url => new Attachment(url));
            using (var mailMessage = BuildMailMessage(TOs, CCs, BCCs, subject, content, attachments))
            using (var smtpClient = BuildSmtpClient())
            {
                await smtpClient.SendMailAsync(mailMessage);
            }
        }

        #endregion

        #endregion
    }
}
