﻿using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace APP.Core.Utilities
{
    public static class QRCodeUtility
    {
        /// <summary>
        /// 容错率级别
        /// </summary>
        public enum ErrorCorrectionLevel
        {
            /// <summary>
            /// 低
            /// </summary>
            LOW = 0,
            /// <summary>
            /// 中
            /// </summary>
            MIDDLE = 1,
            /// <summary>
            /// 高
            /// </summary>
            HIGH = 3
        }

        /// <summary>
        /// 图片格式
        /// </summary>
        public enum ImgFormat
        {
            Gif,
            Jpeg,
            Png
        }


        /// <summary>
        /// 生成矩阵
        /// </summary>
        /// <param name="content"></param>
        /// <param name="correctionLevel"></param>
        /// <returns></returns>
        public static BitMatrix Generate(string content, ErrorCorrectionLevel correctionLevel = ErrorCorrectionLevel.MIDDLE)
        {
            var level = ((int)correctionLevel).ToEnum<Gma.QrCodeNet.Encoding.ErrorCorrectionLevel>();
            var encoder = new QrEncoder(level);

            var qrCode = encoder.Encode(content);
            return qrCode.Matrix;
        }

        /// <summary>
        /// 生成图片内存流（50倍基础size）
        /// </summary>
        /// <param name="content"></param>
        /// <param name="correctionLevel"></param>
        /// <param name="darkColor">Default: Color.Black</param>
        /// <param name="lightColor">Default: Color.White</param>
        /// <param name="imgFormat"></param>
        /// <returns></returns>
        public static MemoryStream GenerateImage_Stream(string content, ErrorCorrectionLevel correctionLevel = ErrorCorrectionLevel.MIDDLE, Color? darkColor = null, Color? lightColor = null, ImgFormat imgFormat = ImgFormat.Png)
        {
            var render = new GraphicsRenderer(new FixedModuleSize(50, QuietZoneModules.Zero), new SolidBrush(darkColor ?? Color.Black), new SolidBrush(lightColor ?? Color.White));

            var ms = new MemoryStream();
            var imageFormat = imgFormat == ImgFormat.Gif ? ImageFormat.Gif : (imgFormat == ImgFormat.Jpeg ? ImageFormat.Jpeg : ImageFormat.Png);
            render.WriteToStream(Generate(content, correctionLevel), imageFormat, ms);
            ms.Position = 0;

            return ms;
        }

        /// <summary>
        /// 生成图片（50倍基础size）
        /// </summary>
        /// <param name="content"></param>
        /// <param name="correctionLevel"></param>
        /// <param name="darkColor">Default: Color.Black</param>
        /// <param name="lightColor">Default: Color.White</param>
        /// <param name="imgFormat"></param>
        /// <returns></returns>
        public static Bitmap GenerateImage(string content, ErrorCorrectionLevel correctionLevel = ErrorCorrectionLevel.MIDDLE, Color? darkColor = null, Color? lightColor = null, ImgFormat imgFormat = ImgFormat.Png)
        {
            using (var ms = GenerateImage_Stream(content, correctionLevel, darkColor, lightColor, imgFormat))
            {
                return new Bitmap(ms);
            }
        }

        /// <summary>
        /// 生成图片Base64字符串（50倍基础size）
        /// </summary>
        /// <param name="content"></param>
        /// <param name="correctionLevel"></param>
        /// <param name="darkColor">Default: Color.Black</param>
        /// <param name="lightColor">Default: Color.White</param>
        /// <param name="imgFormat"></param>
        /// <returns></returns>
        public static string GenerateImage_Base64(string content, ErrorCorrectionLevel correctionLevel = ErrorCorrectionLevel.MIDDLE, Color? darkColor = null, Color? lightColor = null, ImgFormat imgFormat = ImgFormat.Png)
        {
            using (var ms = GenerateImage_Stream(content, correctionLevel, darkColor, lightColor, imgFormat))
            {
                return string.Format("data:image/{0};base64,{1}", imgFormat.ToString().ToLower(), Convert.ToBase64String(ms.ToArray()));
            }
        }
    }
}
