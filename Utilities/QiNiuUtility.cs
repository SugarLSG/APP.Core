﻿using Qiniu.Http;
using Qiniu.Storage;
using Qiniu.Util;
using System;
using System.IO;

namespace APP.Core.Utilities
{
    public static class QiNiuUtility
    {
        public static string AccessKey { get; set; }
        public static string SecretKey { get; set; }
        public static Zone Zone { get; set; }
        public static string Bucket { get; set; }


        /// <summary>
        /// 初始化配置
        /// </summary>
        /// <param name="accessKey"></param>
        /// <param name="secretKey"></param>
        /// <param name="zone">ZONE_CN_East(华东，默认)、ZONE_CN_North(华北)、ZONE_CN_South(华南)、ZONE_US_North(北美)</param>
        /// <param name="bucket"></param>
        public static void Configure(string accessKey, string secretKey, string zone, string bucket)
        {
            AccessKey = accessKey;
            SecretKey = secretKey;
            switch (zone)
            {
                case "ZONE_CN_North": Zone = Zone.ZONE_CN_North; break;
                case "ZONE_CN_South": Zone = Zone.ZONE_CN_South; break;
                case "ZONE_US_North": Zone = Zone.ZONE_US_North; break;
                default: Zone = Zone.ZONE_CN_East; break;
            }
            Bucket = bucket;
        }


        /// <summary>
        /// 同步文件
        /// </summary>
        /// <param name="key"></param>
        /// <param name="sourceUrl"></param>
        /// <returns></returns>
        public static bool FetchFile(string key, string sourceUrl)
        {
            // 同步
            var result = new BucketManager(
                new Mac(AccessKey, SecretKey),
                new Config { Zone = Zone }
            ).Fetch(sourceUrl, Bucket, key);

            // 处理结果
            if (result.Code == (int)HttpCode.OK)
                return true;
            else
            {
                LogUtility.LogError(string.Format("fetch file to qiniu faild ({0}).", JsonUtility.Serialize(result)));
                return false;
            }
        }


        /// <summary>
        /// 上传文件（自动判断是否分片上传）
        /// </summary>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool UploadFile(string key, string data)
        {
            var base64Data = data.IndexOf("base64,", StringComparison.CurrentCultureIgnoreCase) >= 0 ?
                data.Substring(data.IndexOf("base64,", StringComparison.CurrentCultureIgnoreCase) + "base64,".Length) :
                data;
            return UploadFile(key, Convert.FromBase64String(base64Data));
        }

        /// <summary>
        /// 上传文件（自动判断是否分片上传）
        /// </summary>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool UploadFile(string key, byte[] data)
        {
            using (var ms = new MemoryStream(data))
            {
                return UploadFile(key, ms);
            }
        }

        /// <summary>
        /// 上传文件（自动判断是否分片上传）
        /// </summary>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool UploadFile(string key, Stream data)
        {
            // 上传
            var result = new UploadManager(new Config
            {
                Zone = Zone,
                UseHttps = true,
                UseCdnDomains = true,
                ChunkSize = ChunkUnit.U4096K
            }).UploadStream(data, key, Auth.CreateUploadToken(
                new Mac(AccessKey, SecretKey),
                new PutPolicy
                {
                    Scope = string.Format("{0}:{1}", Bucket, key)
                }.ToJsonString()
            ), null);

            // 处理结果
            if (result.Code == (int)HttpCode.OK)
                return true;
            else
            {
                LogUtility.LogError(string.Format("upload file to qiniu faild ({0}).", JsonUtility.Serialize(result)));
                return false;
            }
        }


        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="key"></param>
        /// <returns>状态码（200：成功，参考 Qiniu.Http.HttpCode）</returns>
        public static int DeleteFile(string key)
        {
            // 删除
            var result = new BucketManager(
                new Mac(AccessKey, SecretKey),
                new Config { Zone = Zone }
            ).Delete(Bucket, key);

            // 处理结果
            if (result.Code != (int)HttpCode.OK)
                LogUtility.LogError(string.Format("delete file from qiniu faild ({0}).", JsonUtility.Serialize(result)));
            return result.Code;
        }
    }
}
