﻿using APP.Core.Extensions.Json;
using Newtonsoft.Json;
using System;
using static APP.Core.Extensions.Json.ContractResolverExtension;

namespace APP.Core.Utilities
{
    public static class JsonUtility
    {
        #region 序列化数据

        /// <summary>
        /// 将 Model 序列化为 Json 字符串
        /// </summary>
        /// <param name="model">要序列化的 Model</param>
        /// <param name="dateTimeFormat">日期格式</param>
        /// <param name="ignoreNull">是否忽略 Null 值的字段</param>
        /// <param name="indented">是否换行缩进</param>
        /// <param name="isCamelCase">属性名序列化类型</param>
        /// <returns></returns>
        public static string Serialize(object model, string dateTimeFormat = DateTimeExtension.DATETIME_DEFAULTFORMAT, bool ignoreNull = true, bool indented = false, PropertyNameType propertynametype = PropertyNameType.CamelCase)
        {
            return JsonConvert.SerializeObject(
                model,
                new JsonSerializerSettings
                {
                    NullValueHandling = ignoreNull ? NullValueHandling.Ignore : NullValueHandling.Include,
                    DateFormatString = dateTimeFormat,
                    ContractResolver = new ContractResolverExtension(propertynametype),
                    Formatting = indented ? Formatting.Indented : Formatting.None
                }
            );
        }

        /// <summary>
        /// 序列化 Ajax 请求成功的 Json 字符串
        /// </summary>
        /// <param name="message">成功描述信息</param>
        /// <param name="data">数据</param>
        /// <param name="dateTimeFormat">日期格式</param>
        /// <param name="ignoreNull">是否忽略 Null 值的字段</param>
        /// <param name="indented">是否换行缩进</param>
        /// <param name="propertynametype">属性名序列化类型</param>
        /// <returns></returns>
        public static string SerializeAjaxSuccessJSON(string message = CoreConfig.MESSAGE_OPERATE_SUCCESS, string code = null, object data = null, string dateTimeFormat = DateTimeExtension.DATETIME_DEFAULTFORMAT, bool ignoreNull = true, bool indented = false, PropertyNameType propertynametype = PropertyNameType.CamelCase)
        {
            return Serialize(new
            {
                result = true,
                code = code,
                message = message,
                data = data,
                timestamp = DateTime.Now.ToTimestamp()
            }, dateTimeFormat, ignoreNull, indented, propertynametype);
        }

        /// <summary>
        /// 序列化 Ajax 请求失败的 Json 字符串
        /// </summary>
        /// <param name="message">失败描述信息</param>
        /// <param name="data">数据</param>
        /// <param name="dateTimeFormat">日期格式</param>
        /// <param name="ignoreNull">是否忽略 Null 值的字段</param>
        /// <param name="indented">是否换行缩进</param>
        /// <param name="propertynametype">属性名序列化类型</param>
        /// <returns></returns>
        public static string SerializeAjaxFailureJSON(string message = CoreConfig.MESSAGE_OPERATE_FAILURE, string code = null, object data = null, string dateTimeFormat = DateTimeExtension.DATETIME_DEFAULTFORMAT, bool ignoreNull = true, bool indented = false, PropertyNameType propertynametype = PropertyNameType.CamelCase)
        {
            return Serialize(new
            {
                result = false,
                code = code,
                message = message,
                data = data,
                timestamp = DateTime.Now.ToTimestamp()
            }, dateTimeFormat, ignoreNull, indented, propertynametype);
        }

        #endregion

        #region 反序列化数据

        /// <summary>
        /// 将 Json 字符串反序列化为 Model
        /// </summary>
        /// <param name="json">Json字符串</param>
        /// <returns></returns>
        public static dynamic Deserialize(string json)
        {
            return Deserialize<dynamic>(json);
        }

        /// <summary>
        /// 将 Json 字符串反序列化为指定类型的 Model
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json">Json字符串</param>
        /// <returns></returns>
        public static T Deserialize<T>(string json)
        {
            if (!json.HasValue())
                return default(T);

            return JsonConvert.DeserializeObject<T>(json);
        }

        /// <summary>
        /// 将 Json 字符串反序列化为指定类型的 Model
        /// </summary>
        /// <param name="json">Json字符串</param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static object Deserialize(string json, Type type)
        {
            if (!json.HasValue())
                return null;

            return JsonConvert.DeserializeObject(json, type);
        }

        #endregion
    }
}
