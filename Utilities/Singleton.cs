﻿using System;
using System.Collections.Generic;

namespace APP.Core.Utilities
{
    /// <summary>
    /// 单例模式
    /// </summary>
    /// <typeparam name="T">单例类型，必须有公共无参构造函数</typeparam>
    public class Singleton<T> where T : new()
    {
        private Singleton() { }

        /// <summary>
        /// 获取单例
        /// </summary>
        public static T Instance
        {
            get { return SingletonCreator.Instance; }
        }

        private class SingletonCreator
        {
            internal static readonly T Instance = new T();
        }
    }

    /// <summary>
    /// 单例模式
    /// </summary>
    public class Singleton
    {
        private Singleton() { }

        /// <summary>
        /// 获取单例
        /// </summary>
        /// <param name="type">单例类型，必须有公共无参构造函数</param>
        /// <returns></returns>
        public static object Instance(Type type)
        {
            return Singleton<SingletonCreator>.Instance[type];
        }

        private class SingletonCreator : Dictionary<Type, object>
        {
            internal new object this[Type key]
            {
                get
                {
                    if (!ContainsKey(key))
                    {
                        lock (this)
                        {
                            if (!ContainsKey(key))
                                Add(key, Activator.CreateInstance(key));
                        }
                    }

                    return base[key];
                }

                set
                {
                    Add(key, value);
                }
            }
        }
    }
}
