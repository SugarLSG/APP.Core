﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace APP.Core.Utilities
{
    public static class ClientRequestUtility
    {
        private static string[] MOBILE_AGENT_KEYWORDS =
        {
            "android",
            "iphone",
            "ipod",
            "ipad",
            "windows phone",
            "mqqbrowser",
            "mozilla",
            "micromessenger",
            "ucweb"
        };

        /// <summary>
        /// 当前请求信息
        /// </summary>
        private static HttpRequest CurrentRequest
        {
            get
            {
                return HttpContext.Current == null ? null : HttpContext.Current.Request;
            }
        }


        /// <summary>
        /// 服务器域名
        /// </summary>
        public static string ServerHost
        {
            get
            {
                var request = CurrentRequest;
                return request == null ? null : request.Url.Host.ToLower();
            }
        }

        /// <summary>
        /// 服务器全域名（包括 http|https 前缀，若非80端口，则包括端口）
        /// </summary>
        public static string ServerFullHost
        {
            get
            {
                var request = CurrentRequest;
                return request == null ? null : (request.Url.IsDefaultPort ?
                    string.Format("{0}://{1}", request.IsSecureConnection ? "https" : "http", ServerHost) :
                    string.Format("{0}://{1}:{2}", request.IsSecureConnection ? "https" : "http", ServerHost, request.Url.Port));
            }
        }

        /// <summary>
        /// 客户端IP
        /// </summary>
        public static string ClientIP
        {
            get
            {
                var request = CurrentRequest;
                if (request == null)
                    return null;

                // 使用代理
                var xForwardedFor = request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (xForwardedFor.HasValue())
                    return xForwardedFor.Split(',')[0].Trim();

                // 未使用代理
                var remoteAddr = request.ServerVariables["REMOTE_ADDR"];
                if (remoteAddr.HasValue())
                    return remoteAddr;

                // 默认
                return request.UserHostAddress;
            }
        }

        /// <summary>
        /// 客户端IP
        /// </summary>
        public static string ClientHost
        {
            get
            {
                var request = CurrentRequest;
                if (request == null)
                    return null;

                return request.ServerVariables["SERVER_NAME"];
            }
        }

        /// <summary>
        /// 客户端用户代理
        /// </summary>
        public static string ClientUserAgent
        {
            get
            {
                var request = CurrentRequest;
                if (request == null)
                    return null;

                var userAgent = request.UserAgent;
                if (!userAgent.HasValue())
                    return null;

                return userAgent.ToLower();
            }
        }


        /// <summary>
        /// 是否GET请求
        /// </summary>
        public static bool IsGetRequest
        {
            get
            {
                var request = CurrentRequest;
                return request == null ? false : request.HttpMethod.ToUpper() == "GET";
            }
        }

        /// <summary>
        /// 是否POST请求
        /// </summary>
        public static bool IsPostRequest
        {
            get
            {
                var request = CurrentRequest;
                return request == null ? false : request.HttpMethod.ToUpper() == "POST";
            }
        }

        /// <summary>
        /// 是否Ajax请求
        /// </summary>
        public static bool IsAjaxRequest
        {
            get
            {
                var request = CurrentRequest;
                return request == null ? false : (new HttpRequestWrapper(request)).IsAjaxRequest();
            }
        }

        /// <summary>
        /// 是否手机端请求
        /// </summary>
        public static bool IsMobileRequest
        {
            get
            {
                var userAgent = ClientUserAgent;
                return userAgent.HasValue() &&
                    // 非 Windows 桌面系统
                    (!userAgent.Contains("windows nt") || (userAgent.Contains("windows nt") && userAgent.Contains("compatible; msie 9.0;"))) &&
                    // 非苹果桌面系统
                    !userAgent.Contains("macintosh") &&
                    // 包含手机端关键字
                    MOBILE_AGENT_KEYWORDS.Any(item => userAgent.Contains(item));
            }
        }

        /// <summary>
        /// 是否微信客户端请求
        /// </summary>
        public static bool IsWechatRequest
        {
            get
            {
                var userAgent = ClientUserAgent;
                return userAgent.HasValue() ? userAgent.Contains("micromessenger") : false;
            }
        }


        /// <summary>
        /// 请求虚拟路径
        /// </summary>
        /// <returns></returns>
        public static string RequestVirtualPath
        {
            get
            {
                var request = CurrentRequest;
                return request == null ? null : request.Path.TrimEnd('/').ToLower();
            }
        }

        /// <summary>
        /// 请求真实路径
        /// </summary>
        /// <returns></returns>
        public static string RequestRealPath
        {
            get
            {
                var request = CurrentRequest;
                if (request == null)
                    return null;

                var requestContext = request.RequestContext;
                return string.Format(
                    "/{0}/{1}/{2}",
                    requestContext.RouteData.DataTokens.ContainsKey("area") ? requestContext.RouteData.DataTokens["area"] : null,
                    requestContext.RouteData.Values["controller"],
                    requestContext.RouteData.Values["action"]
                ).Replace("//", "/").ToLower();
            }
        }


        /// <summary>
        /// Url中的所有参数（一般为GET请求）
        /// </summary>
        public static IDictionary<string, string> QueryStringParameters
        {
            get
            {
                var request = CurrentRequest;
                if (request == null)
                    return null;

                var parameters = new Dictionary<string, string>();
                foreach (var key in request.QueryString.AllKeys)
                {
                    if (!parameters.ContainsKey(key))
                        parameters.Add(key, request.QueryString[key]);
                }
                return parameters;
            }
        }

        /// <summary>
        /// Form中的所有参数（一般为POST请求）
        /// </summary>
        public static IDictionary<string, string> FormParameters
        {
            get
            {
                var request = CurrentRequest;
                if (request == null)
                    return null;

                var parameters = new Dictionary<string, string>();
                foreach (var key in request.Form.AllKeys)
                {
                    if (!parameters.ContainsKey(key))
                        parameters.Add(key, request.QueryString[key]);
                }
                return parameters;
            }
        }
    }
}
