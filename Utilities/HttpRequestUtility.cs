﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Routing;

namespace APP.Core.Utilities
{
    public static class HttpRequestUtility
    {
        /// <summary>
        /// 默认编码
        /// </summary>
        private static readonly Encoding DefaultEncoding = Encoding.UTF8;
        /// <summary>
        /// 默认超时时间（毫秒）
        /// </summary>
        private const int DefaultTimeOut = 10000;


        /// <summary>
        /// 转为 Request
        /// </summary>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="cookies"></param>
        /// <param name="timeout"></param>
        /// <param name="userAgent"></param>
        /// <param name="contentType"></param>
        /// <param name="accept"></param>
        /// <param name="proxy"></param>
        /// <returns></returns>
        private static HttpWebRequest ConvertRequest(string url, IDictionary<string, string> headers, CookieContainer cookies, int timeout, string userAgent, string contentType, string accept, WebProxy proxy)
        {
            var request = WebRequest.Create(url) as HttpWebRequest;

            request.ProtocolVersion = HttpVersion.Version10;
            if (headers != null)
            {
                foreach (var item in headers)
                    request.Headers.Set(item.Key, item.Value);
            }
            if (cookies != null)
                request.CookieContainer = cookies;
            request.Timeout = timeout;
            request.UserAgent = userAgent;
            request.ContentType = contentType + "; charset=utf-8";
            if (accept.HasValue())
                request.Accept = accept;
            if (proxy != null)
                request.Proxy = proxy;

            return request;
        }

        /// <summary>
        /// 获取 Response 内容
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        private static string GetResponseContent(HttpWebResponse response)
        {
            using (var stream = response.GetResponseStream())
            using (var reader = new StreamReader(stream, DefaultEncoding))
            {
                var content = reader.ReadToEnd();
                response.Close();

                return content;
            }
        }


        #region Get

        /// <summary>
        /// 获取 Get 请求 Response
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static HttpWebResponse GetResponse(string url)
        {
            return GetResponse(url, null, null);
        }
        /// <summary>
        /// 获取 Get 请求 Response
        /// </summary>
        /// <param name="url"></param>
        /// <param name="parameters">自动拼接在 Url 后面</param>
        /// <returns></returns>
        public static HttpWebResponse GetResponse(string url, IDictionary<string, object> parameters)
        {
            return GetResponse(url, parameters, null);
        }
        /// <summary>
        /// 获取 Get 请求 Response
        /// </summary>
        /// <param name="url"></param>
        /// <param name="cookies"></param>
        /// <returns></returns>
        public static HttpWebResponse GetResponse(string url, CookieContainer cookies)
        {
            return GetResponse(url, null, cookies);
        }
        /// <summary>
        /// 获取 Get 请求 Response
        /// </summary>
        /// <param name="url"></param>
        /// <param name="parameters">自动拼接在 Url 后面</param>
        /// <param name="cookies"></param>
        /// <param name="headers"></param>
        /// <param name="timeout">超时时间（毫秒）</param>
        /// <param name="userAgent"></param>
        /// <param name="contentType"></param>
        /// <param name="accept"></param>
        /// <param name="proxy"></param>
        /// <returns></returns>
        public static HttpWebResponse GetResponse(string url, IDictionary<string, object> parameters, CookieContainer cookies, IDictionary<string, string> headers = null, int timeout = DefaultTimeOut, string userAgent = CoreConfig.USERAGENT_DEFAULT, string contentType = CoreConfig.CONTENTTYPE_TEXT_HTML, string accept = null, WebProxy proxy = null)
        {
            if (!url.HasValue())
                return null;

            if (parameters != null)
            {
                var result = new RouteValueDictionary(parameters);
                url = string.Format(
                    "{0}{1}{2}",
                    url,
                    url.Contains("?") ? "&" : "?",
                    string.Join("&", result.Select(i => string.Format("{0}={1}", i.Key, i.Value)))
                );
            }

            var request = ConvertRequest(url, headers, cookies, timeout, userAgent, contentType, accept, proxy);
            request.Method = HttpMethod.Get.Method;

            return request.GetResponse() as HttpWebResponse;
        }

        /// <summary>
        /// 获取 Get 请求 Response
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string Get(string url)
        {
            return Get(url, null, null);
        }
        /// <summary>
        /// 获取 Get 请求 Response
        /// </summary>
        /// <param name="url"></param>
        /// <param name="parameters">自动拼接在 Url 后面</param>
        /// <returns></returns>
        public static string Get(string url, IDictionary<string, object> parameters)
        {
            return Get(url, parameters, null);
        }
        /// <summary>
        /// 获取 Get 请求 Response
        /// </summary>
        /// <param name="url"></param>
        /// <param name="cookies"></param>
        /// <returns></returns>
        public static string Get(string url, CookieContainer cookies)
        {
            return Get(url, null, cookies);
        }
        /// <summary>
        /// 获取 Get 请求 Response
        /// </summary>
        /// <param name="url"></param>
        /// <param name="parameters">自动拼接在 Url 后面</param>
        /// <param name="cookies"></param>
        /// <param name="headers"></param>
        /// <param name="timeout">超时时间（毫秒）</param>
        /// <param name="userAgent"></param>
        /// <param name="contentType"></param>
        /// <param name="accept"></param>
        /// <param name="proxy"></param>
        /// <returns></returns>
        public static string Get(string url, IDictionary<string, object> parameters, CookieContainer cookies, IDictionary<string, string> headers = null, int timeout = DefaultTimeOut, string userAgent = CoreConfig.USERAGENT_DEFAULT, string contentType = CoreConfig.CONTENTTYPE_TEXT_HTML, string accept = null, WebProxy proxy = null)
        {
            using (var response = GetResponse(url, parameters, cookies, headers, timeout, userAgent, contentType, accept, proxy))
            {
                return GetResponseContent(response);
            }
        }

        /// <summary>
        /// 获取 Get 请求 Response
        /// </summary>
        /// <typeparam name="T">自定义类型</typeparam>
        /// <param name="url"></param>
        /// <returns></returns>
        public static T Get<T>(string url) where T : class
        {
            return Get<T>(url, null, null);
        }
        /// <summary>
        /// 获取 Get 请求 Response
        /// </summary>
        /// <typeparam name="T">自定义类型</typeparam>
        /// <param name="url"></param>
        /// <param name="parameters">自动拼接在 Url 后面</param>
        /// <returns></returns>
        public static T Get<T>(string url, IDictionary<string, object> parameters) where T : class
        {
            return Get<T>(url, parameters, null);
        }
        /// <summary>
        /// 获取 Get 请求 Response
        /// </summary>
        /// <typeparam name="T">自定义类型</typeparam>
        /// <param name="url"></param>
        /// <param name="cookies"></param>
        /// <returns></returns>
        public static T Get<T>(string url, CookieContainer cookies) where T : class
        {
            return Get<T>(url, null, cookies);
        }
        /// <summary>
        /// 获取 Get 请求 Response
        /// </summary>
        /// <typeparam name="T">自定义类型</typeparam>
        /// <param name="url"></param>
        /// <param name="parameters">自动拼接在 Url 后面</param>
        /// <param name="cookies"></param>
        /// <param name="headers"></param>
        /// <param name="timeout">超时时间（毫秒）</param>
        /// <param name="userAgent"></param>
        /// <param name="contentType"></param>
        /// <param name="accept"></param>
        /// <param name="proxy"></param>
        /// <returns></returns>
        public static T Get<T>(string url, IDictionary<string, object> parameters, CookieContainer cookies, IDictionary<string, string> headers = null, int timeout = DefaultTimeOut, string userAgent = CoreConfig.USERAGENT_DEFAULT, string contentType = CoreConfig.CONTENTTYPE_TEXT_HTML, string accept = null, WebProxy proxy = null) where T : class
        {
            if (typeof(T) == typeof(string))
                throw new ArgumentException("Response Type Error");

            var response = Get(url, parameters, cookies, headers, timeout, userAgent, contentType, accept, proxy);
            return JsonUtility.Deserialize<T>(response);
        }

        #endregion

        #region Post JSON

        /// <summary>
        /// 获取 Post 请求 Response（FORM + JSON 格式请求）
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static HttpWebResponse PostResponse(string url)
        {
            return PostResponse(url, null, null);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + JSON 格式请求）
        /// </summary>
        /// <param name="url"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static HttpWebResponse PostResponse(string url, object parameters)
        {
            return PostResponse(url, parameters, null);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + JSON 格式请求）
        /// </summary>
        /// <param name="url"></param>
        /// <param name="cookies"></param>
        /// <returns></returns>
        public static HttpWebResponse PostResponse(string url, CookieContainer cookies)
        {
            return PostResponse(url, null, cookies);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + JSON 格式请求）
        /// </summary>
        /// <param name="url"></param>
        /// <param name="parameters"></param>
        /// <param name="cookies"></param>
        /// <param name="headers"></param>
        /// <param name="timeout">超时时间（毫秒）</param>
        /// <param name="userAgent"></param>
        /// <param name="contentType"></param>
        /// <param name="accept"></param>
        /// <param name="proxy"></param>
        /// <returns></returns>
        public static HttpWebResponse PostResponse(string url, object parameters, CookieContainer cookies, IDictionary<string, string> headers = null, int timeout = DefaultTimeOut, string userAgent = CoreConfig.USERAGENT_DEFAULT, string contentType = CoreConfig.CONTENTTYPE_APPLICATION_JSON, string accept = null, WebProxy proxy = null)
        {
            if (!url.HasValue())
                return null;

            var request = ConvertRequest(url, headers, cookies, timeout, userAgent, contentType, accept, proxy);
            request.Method = HttpMethod.Post.Method;

            if (parameters != null)
            {
                var data = JsonUtility.Serialize(parameters);
                var buffer = DefaultEncoding.GetBytes(data);

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(buffer, 0, buffer.Length);
                }
            }
            else request.ContentLength = 0;

            return request.GetResponse() as HttpWebResponse;
        }

        /// <summary>
        /// 获取 Post 请求 Response（FORM + JSON 格式请求）
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string Post(string url)
        {
            return Post(url, null, null);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + JSON 格式请求）
        /// </summary>
        /// <param name="url"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static string Post(string url, object parameters)
        {
            return Post(url, parameters, null);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + JSON 格式请求）
        /// </summary>
        /// <param name="url"></param>
        /// <param name="cookies"></param>
        /// <returns></returns>
        public static string Post(string url, CookieContainer cookies)
        {
            return Post(url, null, cookies);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + JSON 格式请求）
        /// </summary>
        /// <param name="url"></param>
        /// <param name="parameters"></param>
        /// <param name="cookies"></param>
        /// <param name="headers"></param>
        /// <param name="timeout">超时时间（毫秒）</param>
        /// <param name="userAgent"></param>
        /// <param name="contentType"></param>
        /// <param name="accept"></param>
        /// <param name="proxy"></param>
        /// <returns></returns>
        public static string Post(string url, object parameters, CookieContainer cookies, IDictionary<string, string> headers = null, int timeout = DefaultTimeOut, string userAgent = CoreConfig.USERAGENT_DEFAULT, string contentType = CoreConfig.CONTENTTYPE_APPLICATION_JSON, string accept = null, WebProxy proxy = null)
        {
            using (var response = PostResponse(url, parameters, cookies, headers, timeout, userAgent, contentType, accept, proxy))
            {
                return GetResponseContent(response);
            }
        }

        /// <summary>
        /// 获取 Post 请求 Response（FORM + JSON 格式请求，JSON 格式响应）
        /// </summary>
        /// <typeparam name="T">自定义类型</typeparam>
        /// <param name="url"></param>
        /// <returns></returns>
        public static T Post<T>(string url) where T : class
        {
            return Post<T>(url, null, null);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + JSON 格式请求，JSON 格式响应）
        /// </summary>
        /// <typeparam name="T">自定义类型</typeparam>
        /// <param name="url"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static T Post<T>(string url, object parameters) where T : class
        {
            return Post<T>(url, parameters, null);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + JSON 格式请求，JSON 格式响应）
        /// </summary>
        /// <typeparam name="T">自定义类型</typeparam>
        /// <param name="url"></param>
        /// <param name="cookies"></param>
        /// <returns></returns>
        public static T Post<T>(string url, CookieContainer cookies) where T : class
        {
            return Post<T>(url, null, cookies);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + JSON 格式请求，JSON 格式响应）
        /// </summary>
        /// <typeparam name="T">自定义类型</typeparam>
        /// <param name="url"></param>
        /// <param name="parameters"></param>
        /// <param name="cookies"></param>
        /// <param name="headers"></param>
        /// <param name="timeout">超时时间（毫秒）</param>
        /// <param name="userAgent"></param>
        /// <param name="contentType"></param>
        /// <param name="accept"></param>
        /// <param name="proxy"></param>
        /// <returns></returns>
        public static T Post<T>(string url, object parameters, CookieContainer cookies, IDictionary<string, string> headers = null, int timeout = DefaultTimeOut, string userAgent = CoreConfig.USERAGENT_DEFAULT, string contentType = CoreConfig.CONTENTTYPE_APPLICATION_JSON, string accept = null, WebProxy proxy = null) where T : class
        {
            if (typeof(T) == typeof(string))
                throw new ArgumentException("Response Type Error");

            var response = Post(url, parameters, cookies, headers, timeout, userAgent, contentType, accept, proxy);
            return JsonUtility.Deserialize<T>(response);
        }

        #endregion

        #region Post URL

        /// <summary>
        /// 获取 Post 请求 Response（FORM + URL ENCODED 格式请求）
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static HttpWebResponse PostURLResponse(string url)
        {
            return PostURLResponse(url, null, null);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + URL ENCODED 格式请求）
        /// </summary>
        /// <param name="url"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static HttpWebResponse PostURLResponse(string url, IDictionary<string, object> parameters)
        {
            return PostURLResponse(url, parameters, null);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + URL ENCODED 格式请求）
        /// </summary>
        /// <param name="url"></param>
        /// <param name="cookies"></param>
        /// <returns></returns>
        public static HttpWebResponse PostURLResponse(string url, CookieContainer cookies)
        {
            return PostResponse(url, null, cookies);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + URL ENCODED 格式请求）
        /// </summary>
        /// <param name="url"></param>
        /// <param name="parameters"></param>
        /// <param name="cookies"></param>
        /// <param name="headers"></param>
        /// <param name="timeout">超时时间（毫秒）</param>
        /// <param name="userAgent"></param>
        /// <param name="contentType"></param>
        /// <param name="accept"></param>
        /// <param name="proxy"></param>
        /// <returns></returns>
        public static HttpWebResponse PostURLResponse(string url, IDictionary<string, object> parameters, CookieContainer cookies, IDictionary<string, string> headers = null, int timeout = DefaultTimeOut, string userAgent = CoreConfig.USERAGENT_DEFAULT, string contentType = CoreConfig.CONTENTTYPE_APPLICATION_FORMURLENCODED, string accept = null, WebProxy proxy = null)
        {
            if (!url.HasValue())
                return null;

            var request = ConvertRequest(url, headers, cookies, timeout, userAgent, contentType, accept, proxy);
            request.Method = HttpMethod.Post.Method;

            if (parameters != null)
            {
                var buffer = string.Join("&", parameters.Select(i => string.Format("{0}={1}", i.Key, i.Value)));
                var data = DefaultEncoding.GetBytes(buffer);
                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
            }
            else request.ContentLength = 0;

            return request.GetResponse() as HttpWebResponse;
        }

        /// <summary>
        /// 获取 Post 请求 Response（FORM + URL ENCODED 格式请求）
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string PostURL(string url)
        {
            return PostURL(url, null, null);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + URL ENCODED 格式请求）
        /// </summary>
        /// <param name="url"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static string PostURL(string url, IDictionary<string, object> parameters)
        {
            return PostURL(url, parameters, null);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + URL ENCODED 格式请求）
        /// </summary>
        /// <param name="url"></param>
        /// <param name="cookies"></param>
        /// <returns></returns>
        public static string PostURL(string url, CookieContainer cookies)
        {
            return PostURL(url, null, cookies);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + URL ENCODED 格式请求）
        /// </summary>
        /// <param name="url"></param>
        /// <param name="parameters"></param>
        /// <param name="cookies"></param>
        /// <param name="headers"></param>
        /// <param name="timeout">超时时间（毫秒）</param>
        /// <param name="userAgent"></param>
        /// <param name="contentType"></param>
        /// <param name="accept"></param>
        /// <param name="proxy"></param>
        /// <returns></returns>
        public static string PostURL(string url, IDictionary<string, object> parameters, CookieContainer cookies, IDictionary<string, string> headers = null, int timeout = DefaultTimeOut, string userAgent = CoreConfig.USERAGENT_DEFAULT, string contentType = CoreConfig.CONTENTTYPE_APPLICATION_FORMURLENCODED, string accept = null, WebProxy proxy = null)
        {
            using (var response = PostURLResponse(url, parameters, cookies, headers, timeout, userAgent, contentType, accept, proxy))
            {
                return GetResponseContent(response);
            }
        }

        /// <summary>
        /// 获取 Post 请求 Response（FORM + URL ENCODED 格式请求，JSON 格式响应）
        /// </summary>
        /// <typeparam name="T">自定义类型</typeparam>
        /// <param name="url"></param>
        /// <returns></returns>
        public static T PostURL<T>(string url) where T : class
        {
            return PostURL<T>(url, null, null);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + URL ENCODED 格式请求，JSON 格式响应）
        /// </summary>
        /// <typeparam name="T">自定义类型</typeparam>
        /// <param name="url"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static T PostURL<T>(string url, IDictionary<string, object> parameters) where T : class
        {
            return PostURL<T>(url, parameters, null);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + URL ENCODED 格式请求，JSON 格式响应）
        /// </summary>
        /// <typeparam name="T">自定义类型</typeparam>
        /// <param name="url"></param>
        /// <param name="cookies"></param>
        /// <returns></returns>
        public static T PostURL<T>(string url, CookieContainer cookies) where T : class
        {
            return PostURL<T>(url, null, cookies);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + URL ENCODED 格式请求，JSON 格式响应）
        /// </summary>
        /// <typeparam name="T">自定义类型</typeparam>
        /// <param name="url"></param>
        /// <param name="parameters"></param>
        /// <param name="cookies"></param>
        /// <param name="headers"></param>
        /// <param name="timeout">超时时间（毫秒）</param>
        /// <param name="userAgent"></param>
        /// <param name="contentType"></param>
        /// <param name="accept"></param>
        /// <param name="proxy"></param>
        /// <returns></returns>
        public static T PostURL<T>(string url, IDictionary<string, object> parameters, CookieContainer cookies, IDictionary<string, string> headers = null, int timeout = DefaultTimeOut, string userAgent = CoreConfig.USERAGENT_DEFAULT, string contentType = CoreConfig.CONTENTTYPE_APPLICATION_FORMURLENCODED, string accept = null, WebProxy proxy = null) where T : class
        {
            if (typeof(T) == typeof(string))
                throw new ArgumentException("Response Type Error");

            var response = PostURL(url, parameters, cookies, headers, timeout, userAgent, contentType, accept, proxy);
            return JsonUtility.Deserialize<T>(response);
        }

        #endregion

        #region Post XML

        /// <summary>
        /// 获取 Post 请求 Response（FORM + XML 格式请求）
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static HttpWebResponse PostXMLResponse(string url)
        {
            return PostXMLResponse(url, null, null);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + XML 格式请求）
        /// </summary>
        /// <param name="url"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static HttpWebResponse PostXMLResponse(string url, object parameters)
        {
            return PostXMLResponse(url, parameters, null);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + XML 格式请求）
        /// </summary>
        /// <param name="url"></param>
        /// <param name="cookies"></param>
        /// <returns></returns>
        public static HttpWebResponse PostXMLResponse(string url, CookieContainer cookies)
        {
            return PostXMLResponse(url, null, cookies);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + XML 格式请求）
        /// </summary>
        /// <param name="url"></param>
        /// <param name="parameters"></param>
        /// <param name="cookies"></param>
        /// <param name="headers"></param>
        /// <param name="timeout">超时时间（毫秒）</param>
        /// <param name="userAgent"></param>
        /// <param name="contentType"></param>
        /// <param name="accept"></param>
        /// <param name="proxy"></param>
        /// <returns></returns>
        public static HttpWebResponse PostXMLResponse(string url, object parameters, CookieContainer cookies, IDictionary<string, string> headers = null, int timeout = DefaultTimeOut, string userAgent = CoreConfig.USERAGENT_DEFAULT, string contentType = CoreConfig.CONTENTTYPE_APPLICATION_XML, string accept = null, WebProxy proxy = null)
        {
            if (!url.HasValue())
                return null;

            var request = ConvertRequest(url, headers, cookies, timeout, userAgent, contentType, accept, proxy);
            request.Method = HttpMethod.Post.Method;

            if (parameters != null)
            {
                using (var stream = request.GetRequestStream())
                {
                    var xml = XmlUtility.Serialize(parameters);
                    xml.Save(stream);
                }
            }
            else request.ContentLength = 0;

            return request.GetResponse() as HttpWebResponse;
        }

        /// <summary>
        /// 获取 Post 请求 Response（FORM + XML 格式请求）
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string PostXML(string url)
        {
            return PostXML(url, null, null);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + XML 格式请求）
        /// </summary>
        /// <param name="url"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static string PostXML(string url, object parameters)
        {
            return PostXML(url, parameters, null);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + XML 格式请求）
        /// </summary>
        /// <param name="url"></param>
        /// <param name="cookies"></param>
        /// <returns></returns>
        public static string PostXML(string url, CookieContainer cookies)
        {
            return PostXML(url, null, cookies);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + XML 格式请求）
        /// </summary>
        /// <param name="url"></param>
        /// <param name="parameters"></param>
        /// <param name="cookies"></param>
        /// <param name="headers"></param>
        /// <param name="timeout">超时时间（毫秒）</param>
        /// <param name="userAgent"></param>
        /// <param name="contentType"></param>
        /// <param name="accept"></param>
        /// <param name="proxy"></param>
        /// <returns></returns>
        public static string PostXML(string url, object parameters, CookieContainer cookies, IDictionary<string, string> headers = null, int timeout = DefaultTimeOut, string userAgent = CoreConfig.USERAGENT_DEFAULT, string contentType = CoreConfig.CONTENTTYPE_APPLICATION_XML, string accept = null, WebProxy proxy = null)
        {
            using (var response = PostXMLResponse(url, parameters, cookies, headers, timeout, userAgent, contentType, accept, proxy))
            {
                return GetResponseContent(response);
            }
        }

        /// <summary>
        /// 获取 Post 请求 Response（FORM + XML 格式请求，XML 格式响应）
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <returns></returns>
        public static T PostXML<T>(string url)
        {
            return PostXML<T>(url, null, null);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + XML 格式请求，XML 格式响应）
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static T PostXML<T>(string url, object parameters)
        {
            return PostXML<T>(url, parameters, null);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + XML 格式请求，XML 格式响应）
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <param name="cookies"></param>
        /// <returns></returns>
        public static T PostXML<T>(string url, CookieContainer cookies)
        {
            return PostXML<T>(url, null, cookies);
        }
        /// <summary>
        /// 获取 Post 请求 Response（FORM + XML 格式请求，XML 格式响应）
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <param name="parameters"></param>
        /// <param name="cookies"></param>
        /// <param name="headers"></param>
        /// <param name="timeout">超时时间（毫秒）</param>
        /// <param name="userAgent"></param>
        /// <param name="contentType"></param>
        /// <param name="accept"></param>
        /// <param name="proxy"></param>
        /// <returns></returns>
        public static T PostXML<T>(string url, object parameters, CookieContainer cookies, IDictionary<string, string> headers = null, int timeout = DefaultTimeOut, string userAgent = CoreConfig.USERAGENT_DEFAULT, string contentType = CoreConfig.CONTENTTYPE_APPLICATION_XML, string accept = null, WebProxy proxy = null)
        {
            if (typeof(T) == typeof(string))
                throw new ArgumentException("Response Type Error");

            var response = PostXMLResponse(url, parameters, cookies, headers, timeout, userAgent, contentType, accept, proxy);

            return XmlUtility.Deserialize<T>(GetResponseContent(response));
        }

        #endregion
    }
}
