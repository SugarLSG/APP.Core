﻿using APP.Core.Caches;
using System;
using System.Collections.Generic;
using System.Linq;

namespace APP.Core.Utilities
{
    public static class CacheUtility
    {
        private static ICacheProvider GetCacheManager(CacheEngine cacheEngine)
        {
            switch (cacheEngine)
            {
                case CacheEngine.HTTPCache:
                    return CacheManager.HTTPCacheProvider;
                case CacheEngine.MemoryCache:
                    return CacheManager.MemoryCacheProvider; ;
            }

            return null;
        }


        /// <summary>
        /// 从缓存获取数据，若不存在，则执行逻辑并存入缓存（MemoryCache）
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="key"></param>
        /// <param name="getFunc"></param>
        /// <param name="getCache"></param>
        /// <param name="expirationType"></param>
        /// <param name="cacheMinutes">expirationType != None，必须设置 cacheMinutes</param>
        /// <returns></returns>
        public static TResult GetOrSetFromCache<TResult>(string key, Func<TResult> getFunc, bool getCache = true, CacheExpirationType expirationType = CacheExpirationType.None, double? cacheMinutes = null)
        {
            var cacheManager = GetCacheManager(CacheEngine.MemoryCache);

            TResult result = default(TResult);
            if (getCache && cacheManager.Contains(key))
                result = cacheManager.Get<TResult>(key);
            else
            {
                result = getFunc();
                if (result != null)
                    cacheManager.Set(key, result, expirationType, cacheMinutes);
            }

            return result;
        }

        /// <summary>
        /// 从 Redis 获取数据（Key & Value），若不存在，则执行逻辑并存入 Redis
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="key"></param>
        /// <param name="getFunc"></param>
        /// <param name="getCache"></param>
        /// <param name="cacheMinutes"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static TResult GetOrSetFromRedis<TResult>(string key, Func<TResult> getFunc, bool getCache = true, double? cacheMinutes = null, int? db = null)
        {
            var cacheManager = CacheManager.RedisCacheProvider;

            var result = cacheManager.GetValue<TResult>(key, db);
            if (!getCache || result == null)
            {
                result = getFunc();
                if (result != null)
                {
                    if (cacheMinutes.HasValue)
                        cacheManager.SetValue(key, result, cacheMinutes.Value, db);
                    else cacheManager.SetValue(key, result, db);
                }
            }

            return result;
        }


        /// <summary>
        /// 删除指定 Key 的缓存
        /// </summary>
        /// <param name="key"></param>
        /// <param name="cacheEngine"></param>
        /// <param name="redisCacheMethod">仅 cacheEngine = RedisCache</param>
        /// <param name="db">仅 cacheEngine = RedisCache</param>
        public static void DeleteCacheByKey(string key, CacheEngine cacheEngine = CacheEngine.MemoryCache, RedisCacheMethod? redisCacheMethod = null, int? db = null)
        {
            DeleteCacheByKeys(new string[] { key }, cacheEngine, redisCacheMethod);
        }

        /// <summary>
        /// 删除指定 Key 的缓存
        /// </summary>
        /// <param name="keys"></param>
        /// <param name="cacheEngine"></param>
        /// <param name="redisCacheMethod">仅 cacheEngine = RedisCache</param>
        /// <param name="db">仅 cacheEngine = RedisCache</param>
        public static void DeleteCacheByKeys(IEnumerable<string> keys, CacheEngine cacheEngine = CacheEngine.MemoryCache, RedisCacheMethod? redisCacheMethod = null, int? db = null)
        {
            if (keys != null && keys.Any())
            {
                if (cacheEngine == CacheEngine.RedisCache)
                {
                    if (redisCacheMethod.HasValue)
                    {
                        var cacheManager = CacheManager.RedisCacheProvider;
                        foreach (var cacheKey in keys)
                        {
                            if ((redisCacheMethod.Value & RedisCacheMethod.Value) != 0)
                                cacheManager.RemoveValue(cacheKey, db);
                            if ((redisCacheMethod.Value & RedisCacheMethod.List) != 0)
                                cacheManager.RemoveList(cacheKey, db);
                            if ((redisCacheMethod.Value & RedisCacheMethod.Set) != 0)
                                cacheManager.RemoveSet(cacheKey, db);
                            if ((redisCacheMethod.Value & RedisCacheMethod.SortedSet) != 0)
                                cacheManager.RemoveSortedSet(cacheKey, db);
                        }
                    }
                }
                else
                {
                    var cacheManager = GetCacheManager(cacheEngine);
                    foreach (var cacheKey in keys)
                        cacheManager.Remove(cacheKey);
                }
            }
        }

        /// <summary>
        /// 删除指定关键词的缓存
        /// </summary>
        /// <param name="keys"></param>
        /// <param name="cacheEngine">不支持 RedisCache</param>
        public static void DeleteCacheByKeyword(string keyword, CacheEngine cacheEngine = CacheEngine.MemoryCache)
        {
            if (keyword.HasValue())
            {
                if (cacheEngine == CacheEngine.RedisCache)
                    return;

                var cacheManager = GetCacheManager(cacheEngine);
                var keys = cacheManager.GetAllKeys().Where(key => key.Contains(keyword));
                DeleteCacheByKeys(keys, cacheEngine);
            }
        }

        /// <summary>
        /// 删除指定名称的方法缓存（MemoryCache）
        /// </summary>
        /// <param name="methodName"></param>
        /// <param name="parameters"></param>
        public static void DeleteFunctionCache(string methodName, IDictionary<string, object> parameters = null)
        {
            if (!string.IsNullOrEmpty(methodName))
            {
                var keys = CacheManager.MemoryCacheProvider
                    .GetAllKeys()
                    .Where(key =>
                    {
                        var waitingForDelete = key.StartsWith(CoreConfig.CACHEKEY_METHODRETURN_PREFIX) && key.Contains(string.Format("{0}@", methodName));
                        if (parameters != null && parameters.Any())
                            foreach (var parameter in parameters)
                            {
                                if (waitingForDelete)
                                    waitingForDelete = key.Contains(string.Format("#{0}:{1}#", parameter.Key, parameter.Value));
                            }

                        return waitingForDelete;
                    })
                    .ToArray();
                DeleteCacheByKeys(keys, CacheEngine.MemoryCache);
            }
        }

        /// <summary>
        /// 删除指定主键的数据库表实例缓存（HTTPCache）
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <param name="cacheEngine">不支持 RedisCache</param>
        public static void DeleteDBEntityCache<T>(object id, CacheEngine cacheEngine = CacheEngine.MemoryCache) where T : class
        {
            if (cacheEngine == CacheEngine.RedisCache)
                return;

            var cacheManager = GetCacheManager(cacheEngine);
            var typeName = typeof(T).FullName;
            var cacheKeyword = string.Format(":{0}#{1}@", typeName, id);
            DeleteCacheByKeyword(cacheKeyword, cacheEngine);
        }
    }
}
