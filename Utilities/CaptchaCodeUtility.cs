﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;

namespace APP.Core.Utilities
{
    public static class CaptchaCodeUtility
    {
        private static readonly Random _random = new Random();
        private static readonly char[] MixedCodes = "2345678abcdefhijkmnpqrstuvwxyz".ToCharArray();
        private static readonly string[] _fonts = new string[] { "Arial", "Candara", "Consolas", "Corbel", "Monaco" };


        /// <summary>
        /// 生成图片（Base64）
        /// </summary>
        /// <param name="text"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="isWave"></param>
        /// <returns></returns>
        private static string GenerateImageData(string text, int width, int height, bool isWave)
        {
            using (var bitmap = new Bitmap(width, height))
            using (var graphics = Graphics.FromImage(bitmap))
            using (var brush = new SolidBrush(Color.Black))
            using (var stream = new MemoryStream())
            {
                // 画横线
                for (int i = 0, c = _random.Next(2, 3); i < c; ++i)
                {
                    var sx = _random.Next(width / 4, width / 3);
                    var sy = _random.Next(height / 3, height * 2 / 3);

                    var ex = width - _random.Next(width / 4, width / 3);
                    var ey = sy;

                    using (var pen = new Pen(Color.FromArgb(0, 0, 0), _random.Next(2, 3)))
                        graphics.DrawLine(pen, sx, sy, ex, ey);
                }

                // 写Code
                for (int i = 0, l = text.Length, w = width / l; i < l; ++i)
                {
                    float size = Math.Min(w, _random.Next(height * 2 / 3, height)),
                        left = w * i * 0.65f + w / 2,
                        top = (float)_random.NextDouble() * (height - size) / 3;
                    using (var font = new Font(_fonts[_random.Next(0, _fonts.Length)], size))
                        graphics.DrawString(text[i].ToString(), font, brush, left, top);
                }

                if (isWave)
                {
                    // 正弦扭曲
                    using (var wave = new Bitmap(width, height))
                    {
                        var doublePI = Math.PI * (1 + _random.NextDouble() * 2);
                        var phase = _random.Next(0, 7);
                        var mult = _random.Next(3, 6);
                        for (int x = 0; x < width; ++x)
                        {
                            for (int y = 0; y < height; ++y)
                            {
                                var ox = x + (int)(Math.Sin(doublePI * y / width + phase) * mult);
                                var oy = y + (int)(Math.Sin(doublePI * x / width + phase) * mult);

                                if (ox >= 0 && ox < width && oy >= 0 && oy < height)
                                    wave.SetPixel(ox, oy, bitmap.GetPixel(x, y));
                            }
                        }

                        // 生成图片结果
                        wave.Save(stream, ImageFormat.Png);
                        return "data:image/png;base64," + Convert.ToBase64String(stream.ToArray());
                    }
                }
                else
                {
                    // 生成图片结果
                    bitmap.Save(stream, ImageFormat.Png);
                    return "data:image/png;base64," + Convert.ToBase64String(stream.ToArray());
                }
            }
        }


        /// <summary>
        /// 生成数字类验证码
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="count"></param>
        /// <param name="isWave"></param>
        /// <returns></returns>
        public static KeyValuePair<string, string> GenerateDigitalCode(int width, int height, int count, bool isWave = true)
        {
            var sb = new StringBuilder();
            for (var i = 0; i < count; ++i)
                sb.Append(_random.Next(0, 10));
            var text = sb.ToString();

            // 生成图片
            return new KeyValuePair<string, string>(text, GenerateImageData(text, width, height, isWave));
        }

        /// <summary>
        /// 生成字母类验证码
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="count"></param>
        /// <param name="isWave"></param>
        /// <returns></returns>
        public static KeyValuePair<string, string> GenerateLetterCode(int width, int height, int count, bool isWave = true)
        {
            var sb = new StringBuilder();
            for (var i = 0; i < count; ++i)
                sb.Append((char)_random.Next(97, 123));
            var text = sb.ToString();

            // 生成图片
            return new KeyValuePair<string, string>(text, GenerateImageData(text, width, height, isWave));
        }

        /// <summary>
        /// 生成混合类验证码（数字 OR 字母）
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="count"></param>
        /// <param name="isWave"></param>
        /// <returns></returns>
        public static KeyValuePair<string, string> GenerateMixedCode(int width, int height, int count, bool isWave = true)
        {
            var sb = new StringBuilder();
            for (var i = 0; i < count; ++i)
                sb.Append(MixedCodes[_random.Next(0, MixedCodes.Length)]);
            var text = sb.ToString();

            // 生成图片
            return new KeyValuePair<string, string>(text, GenerateImageData(text, width, height, isWave));
        }
    }
}
